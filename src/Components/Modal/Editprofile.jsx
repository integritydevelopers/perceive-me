import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { editEducation,educationType} from '../../providers/auth';
import { toast } from 'react-toastify';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import { connect } from 'react-redux';

class Editprofile extends React.Component{

    constructor(props){
        super(props);
        this.state={
          ModalShow : false,
          eduType:'',
          school:'',
          degree:'',
          fieldstudy :'',
          grade: '',
          startdate: '',
          enddate : '',
          activity :'',
          desc: '',
          educationTypes:[],
  
        };
      
       this.handleDegreeChange =this.handleDegreeChange.bind(this);
       this.handlesubmit = this.handlesubmit.bind(this);
       educationType().then(res=>{
        this.setState({educationTypes:res.data.data});
       
    })
  
      }
    
    componentWillReceiveProps(nextProps){
     
      if(nextProps.show)
      {
        this.setState({ ModalShow:true});
      }
      else
      {
        this.setState({ ModalShow:false});
      }
      this.setState({
        eduId:nextProps.eduobject.eduId,
        eduType:nextProps.eduobject.eduTypeId,
        school:nextProps.eduobject.schoolName,
        degree:nextProps.eduobject.degree,
        fieldstudy :nextProps.eduobject.feildStudy,
        grade: nextProps.eduobject.grade,
        startdate: nextProps.eduobject.eduStartDate,
        enddate : nextProps.eduobject.eduEndDate,
        activity :nextProps.eduobject.activities,
        desc: nextProps.eduobject.eduDescription,

      });
    }
    handleSchoolChange = (e) =>{
    this.setState({school:e.target.value});
       }

    handleDegreeChange(e){
      //this.setState({degree:});
     
      this.setState({degree:e.target.value});
     
       }
       
    handlefieldstudyChange = (e)=>{
      this.setState({fieldstudy:e.target.value});
     }
     
    handlegradeChange = (e)=>{
      this.setState({grade:e.target.value});
     }
     
    handlestartdateChange = (e)=>{
      this.setState({startdate:e.target.value});
     }
     
    handleenddateChange = (e)=>{
      this.setState({enddate:e.target.value});
     }
     
    handleactivityChange = (e)=>{
      this.setState({activity:e.target.value});
     }
     
    handledescChange = (e)=>{
      this.setState({desc:e.target.value});
     }
    
     handleeduTypeChange = (e)=>{
      this.setState({eduType:e.target.value})
     }
      
    handlesubmit(){
      if(this.state.school !== '' && this.state.degree !== '' && this.state.fieldstudy !== ''  &&  this.state.startdate!==null &&  this.state.startdate!==''&& this.state.enddate!==null && this.state.enddate!=='' &&  this.state.eduType!==''&&  this.state.eduType!=='edutype'&&  this.state.eduType!==null){

       
        this.props.startLoader();
        editEducation(this.state.eduId,this.state.school,this.state.degree,this.state.fieldstudy,this.state.eduType,this.state.grade,this.state.startdate,this.state.enddate,this.state.activity,this.state.desc).then((res)=>{
            this.props.hideLoader();
            if(res.data.status){
              this.setState({ ModalShow:false});
              toast.success(res.data.msg,{autoClose:2000});
           }
            else{
              this.setState({ ModalShow:false});
                toast.error(res.data.msg,{autoClose:2000});
            }
            window.location.reload();
        })
    }else{
        toast.error('Please fill all the feilds.',{})
    
    }
   
     }
    
    
    render(props){
     
        return(
         
            <Modal
            {...this.props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            show={this.state.ModalShow}
            onHide={this.props.onHide}
          >
              
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Edit Education
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
           
            <div className="form-group">
                 <input type="text" className="form-control" name="schoolName"  defaultValue={this.state.school} onChange={this.handleSchoolChange} placeholder="School/University" />
                 </div>
                 {/* <div className="form-group">
                 <select className="form-control"  name="eduType" >
                 <option>Education -Type</option>
                 {this.state.educationTypes.map((eduType,key)=>(
                     <option value={eduType.pf_id} key={key}>{eduType.pf_name}</option>
                 ))}
				
				
				</select>
                </div> */}
                <div className="form-group">
                 <select className="form-control"  name="eduType" defaultValue={this.state.eduType}  onChange={this.handleeduTypeChange}>
                 <option value="edutype">Education -Type</option>
                 {this.state.educationTypes.map((eduType,key)=>(
                     <option value={eduType.pf_id} key={key}>{eduType.pf_name}</option>
                 ))}
				       </select>
                 </div> 
              <div className="form-group">
                 <input type="text" className="form-control" name="degree" placeholder="Degree" defaultValue={this.state.degree} onChange={this.handleDegreeChange}/>
                 </div>
                 <div className="form-group">
                 <input type="text" className="form-control" name="feildStudy" placeholder="Field of Study" defaultValue={this.state.fieldstudy} onChange={this.handlefieldstudyChange}/>
                 </div> 
                 <div className="form-group">
                 <input type ="text" className="form-control" name="grade" placeholder="Grade" defaultValue={this.state.grade} onChange={this.handlegradeChange}/>
                 </div>
                 <div className="row">
                 <div className="form-group col-md-6">
                 <label>Start Date</label>
                 <input type="date" className="form-control" name="eduStartDate" defaultValue={this.state.startdate} onChange={this.handlestartdateChange} />
                 </div>
                 <div className="form-group col-md-6">
                 <label>End Date</label>
                 <input type="date" className="form-control" name="eduEndDate" defaultValue={this.state.enddate} onChange={this.handleenddateChange}/>
                 </div>   
                 </div>
                 <div className="form-group">
                 <textarea type ="textarea" className="form-control" name="activities"  rows="3" placeholder="Activities and societies" defaultValue={this.state.activity} onChange={this.handleactivityChange}/>
                 </div>

                 <div className="form-group">
                 <textarea type ="textarea" className="form-control" name="eduDescription"  rows="3" placeholder="decscription" defaultValue={this.state.desc} onChange={this.handledescChange} />
                 </div>   
    
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.props.onHide} >Close</Button>
              <Button onClick={this.handlesubmit} >Save</Button>
            </Modal.Footer>
          </Modal>
        );
                 
    }
   
}
const mapStateToProps = state=>{
  return{
  loader : state.loaderReducer.loader

  }
}


const mapDisptchToProps = dispatch=>{
  return{
    startLoader : () =>dispatch(showLoader()),
    hideLoader : () =>dispatch(hideLoader())
  }
  
}
export default connect(mapStateToProps,mapDisptchToProps) (Editprofile);