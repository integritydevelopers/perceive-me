import actionTypes from './actionTypes';

export function showLoader(){
    return{
        type : actionTypes.showLoader
    }
}
export function hideLoader(){
    return{
        type : actionTypes.hideLoader
    }
}