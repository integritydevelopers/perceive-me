import React from 'react';

class Newslatter extends React.Component{
    render(){
        return(
            <section id="sec5">
                <div className="custom-container">
                <div className="sec5-inner">
                <h1>Subscribe to Our Newsletter</h1>
                <p>Join millions of professionals who use Elementor to build WordPress websites faster and better than ever before</p>
                <div className="subscribe">
                <form>
                <div className="form-group">
                <input type="text" className="form-control" placeholder="Enter your email"/>
                <button className="subscribe-btn">Subscribe</button>
                </div>
                </form>
                </div>
                </div>
                </div>
                </section>
        )
    }
}

export default Newslatter;