import actionTypes from './actionTypes';
import { getProfilePic } from '../../providers/auth';


export function manageAuth(){
    return{
        type : actionTypes.manageAuth,
    }
}
export function removeAuth(){
    return{
        type : actionTypes.removeAuth
    }
}
export function updateAuth(authObject){
    return{
        type : actionTypes.updateAuth,
        value : authObject
    }
}