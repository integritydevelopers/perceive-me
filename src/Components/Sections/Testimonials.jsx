import React from 'react';
import testimonial from '../../assets/images/testimonial.jpg'
import quote from '../../assets/images/quote.png'
//import AwesomeSlider from 'react-awesome-slider';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
//import '../../assets/css/awseme.css';
class Testimonials extends React.Component{
    render(){
        const responsive = {

            desktop: {
              breakpoint: { max: 3000, min: 1024 },
              items: 3
            },
        };
        return(
            <section id="sec3">
            <div className="">
            <div className="custom-container">
            <h6>Testimonials</h6>
            <h1>What our clients say about us.</h1>
           
            <div className="owl-theme">
            <Carousel responsive={responsive}>
                <div className="item">
                <div className="sec3-item-top">
                <img src={quote}/>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                </div>
                <div className="sec3-item-bottom">
                <img src={testimonial}/>
                <h5>Asmin jha</h5>
                <span>software developer</span>
                </div>
                
                </div>
                <div className="item">
                <div className="sec3-item-top">
                <img src={quote}/>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                </div>
                <div className="sec3-item-bottom">
                <img src={testimonial}/>
                <h5>Asmin jha</h5>
                <span>software developer</span>
                </div>
                
                </div>
                <div className="item">
                <div className="sec3-item-top">
                <img src={quote}/>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                </div>
                <div className="sec3-item-bottom">
                <img src={testimonial}/>
                <h5>Asmin jha</h5>
                <span>software developer</span>
                </div>
                
                </div>
                <div className="item">
                <div className="sec3-item-top">
                <img src={quote}/>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                </div>
                <div className="sec3-item-bottom">
                <img src={testimonial}/>
                <h5>Asmin jha</h5>
                <span>software developer</span>
                </div>
                
                </div>
                <div className="item">
                <div className="sec3-item-top">
                <img src={quote}/>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                </div>
                <div className="sec3-item-bottom">
                <img src={testimonial}/>
                <h5>Asmin jha</h5>
                <span>software developer</span>
                </div>
                
                </div>
                <div className="item">
                <div className="sec3-item-top">
                <img src={quote}/>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                </div>
                <div className="sec3-item-bottom">
                <img src={testimonial}/>
                <h5>Asmin jha</h5>
                <span>software developer</span>
                </div>
                
                </div>
               
                          </Carousel>
                {/* <div className="item">
                <div className="sec3-item-top">
                <img src={quote}/>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                </div>
                <div className="sec3-item-bottom">
                <img src={testimonial}/>
                <h5>Asmin jha</h5>
                <span>software developer</span>
                </div>
                </div>
               
                 <div className="item">
                <div className="sec3-item-top">
                <img src={quote}/>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                </div>
                <div className="sec3-item-bottom">
                <img src={testimonial}/>
                <h5>Asmin jha</h5>
                <span>software developer</span>
                </div>
                </div>
                <div className="item">
                <div className="sec3-item-top">
                <img src={quote}/>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                </div>
                <div className="sec3-item-bottom">
                <img src={testimonial}/>
                <h5>Asmin jha</h5>
                <span>software developer</span>
                </div>
                </div> 
        */}
            </div>
           
            </div>
            </div>
            </section>
        )
    }
}
export default Testimonials;