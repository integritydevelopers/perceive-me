import React from 'react';
import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';
import cross from '../../assets/images/cros.png';
import userin from '../../assets/images/userin.png';
import loader from '../../assets/images/loding.png';
import experience from '../../assets/images/exprience.jpg';
import { addExpQuestion, UserTag,expertlist,expertView,addfeedback,constpubpvt } from '../../providers/auth';
import { AddUserConsultation } from '../../providers/auth';
import { toast } from 'react-toastify';
import { consultationList } from '../../providers/auth';
import { UserlistTag } from '../../providers/auth';
import { connect } from 'react-redux';
import ReactTags from 'react-tag-autocomplete';
import moment from 'moment';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import { DomainTag,Domain,consultationrRequest,feedbackvaluelist } from '../../providers/auth';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { confirmAlert } from 'react-confirm-alert';
//import ConsultImage from '../../Components/Modal/ConsultImage';
import ReactMultiSelectCheckboxes from 'react-multiselect-checkboxes';
import testimonial from '../../assets/images/testimonial.jpg';
import quote from '../../assets/images/quote.png';
import { manageAuth,updateAuth } from '../../redux/Actions/authAction';
import { Button} from 'react-bootstrap';
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import ShowMoreText from 'react-show-more-text';
import Speech from 'react-speech';
import ReviewRequest from '../../Components/Modal/ReviewRequest';
import DocumentViewer from '../../Components/Modal/documentViewer';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import ModalVideo from 'react-modal-video';
import MicIcon from '@material-ui/icons/Mic';
import  Speechtotext  from '../../Components/Media/speechTotext';
import Tooltip from '@material-ui/core/Tooltip';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

const isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
const isFirefox = typeof InstallTrigger !== 'undefined';
var isOpera = (!!window.opera && !!window.opera.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && window['safari'].pushNotification));
class Consulatation extends React.Component{
  dataObject = {}
    constructor(props) {
        super(props);
        console.log(this.props);
        console.log(this.props.location.data);
       //console.log(this.props.location.data.user_exp_id);
       this.consultationstep=localStorage.getItem('step');
    this.state= {
      checked: false,
      values: [],
      reviewShow : false,
        showexpForm: false,
        searchbox: false,
        showlistview: false,
        exptitle: '',
        imgfile:[],
        imgdata: '',
        expdetails : '',
        domain: '',
        Colleagues :'',
        question: [],
        input: {},
        getpostValue:[],
        showinput:false,
        gettaguser:[],
        gettagdomain:[],
        evalparam: [],
      
        tags: [],
        suggestions: [],
        
        tagsDomain: [],
        suggestionsDomain: [],
        showAppointment: false,
        expertlist:'',
        subdomain:[],
        options:[],
        newdomain:[],
        audioShow : false,
        isOpen: false,
        situisOpen: false,
        taskisOpen: false,
        actisOpen: false,
        resisOpen: false,
        index:0,
        step:'',
        expertviewList:[],
        masId:'',
        openMic : false,
        starvalue:[],
       // checked:false
        
       expertfeedback:[],
       feedbacktxt:'',
       feedbackpoint:[],
       point:[],
       record:[],
       textboxId:''
        
      };
      this.toggleExpForm = this.toggleExpForm.bind(this);
      this.toggleSearch = this.toggleSearch.bind(this);
      this.togglelistview = this.togglelistview.bind(this);
      this.handletitlechange = this.handletitlechange.bind(this); 
      this.toggleAppointForm = this.toggleAppointForm.bind(this);
     this.handleCheckClick=this.handleCheckClick.bind(this);
      
      this.reactTags = React.createRef()
      //this.toggleClass= this.toggleClass.bind(this);
      
     
      addExpQuestion().then(res=>{
         
          this.setState({
            question:res['data']['data']
          
          });
      
  
            });
            expertlist().then(res=>{
         
              console.log(res.data.data);
              this.setState({
                expertlist:res.data.data
              });
               console.log(this.state.expertlist);
      
                });
  
            this.props.startLoader();
            consultationList().then(res=>{
              console.log(res.data);
              this.setState({
                getpostValue:res.data
                },console.log(this.state.getpostValue)
                );
                this.props.hideLoader();
                
              });
  
              UserlistTag().then(res=>{
              this.setState({
                  gettaguser:res['data']['data']
                
                });
         
               console.log(this.state.gettaguser);
                this.state.gettaguser.forEach(el=>{
                  this.state.suggestions.push({id:el.user_id,name:el.user_fname+' '+el.user_lname});
                })
  
        
                  });

                  DomainTag().then(res=>{

                    this.setState({
                      gettagdomain:res['data']['data']
                    
                    });
  
                    console.log(this.state.gettagdomain);
                this.state.gettagdomain.forEach(el=>{
                  this.state.suggestionsDomain.push({id:el.user_exp_dom_id,name:el.user_exp_domain});
                })
                        });


                        if( this.props && this.props.location.data != null){

                          Domain(this.state.user_exp_id).then(res=>{
                            console.log(res.data);
  
                            this.setState({
                              subdomain:res['data']['data']
                            
                            });
                            console.log(this.state.subdomain);
                            this.state.subdomain.forEach(el=>{
                              this.state.options.push({label:el.user_exp_sub_domain,value:el.user_exp_sub_dom_id});
                            })
                            console.log(this.state.options);
                            
                          })

                        }

                        feedbackvaluelist().then(res=>{
                          console.log(res.data.data);
                          this.setState({
                            expertfeedback:res.data.data
                          
                          });
                          console.log(this.state.expertfeedback);

                         });


                         expertView().then(res=>{
                          console.log(res.data);
                          this.setState({
                            expertviewList:res.data
                          
                          });
                          console.log(this.state.expertviewList);

                         });

                       
         }
         componentWillReceiveProps(nextProps)
         {
           console.log('hi');
           console.log(nextProps)
           console.log(this.state.openMic);
           if(this.state.openMic)
           {
             console.log(nextProps.text.textboxId);
           this.state.record[nextProps.text.textboxId]=nextProps.text.text;
           }
         }
         openMic = () =>{
          this.setState({openMic : !this.state.openMic});
          console.log(this.state.openMic);
         }
  
    toggleExpForm() {
      
      const currentState=this.state.showexpForm;
      this.setState({ showexpForm: !currentState});
     
   }
   toggleAppointForm() {
    console.log(this.state.showAppointment);
   const currentState=this.state.showAppointment;
   this.setState({showAppointment : !currentState});
  
 }
   reviewSituationdata = e => event =>{
    this.setState({reviewShow : true})
    console.log(this.state.reviewShow);
    this.dataObject = {'exp_Id':e.user_exp_id,'user_situation':e.experience.user_situation}
  }
  reviewTaskdata = e => event =>{
    this.setState({reviewShow : true})
    console.log(this.state.reviewShow);
    this.dataObject = {'exp_Id':e.user_exp_id,'user_task':e.experience.user_task}
  }
  reviewActiondata = e => event =>{
    this.setState({reviewShow : true})
    console.log(this.state.reviewShow);
    this.dataObject = {'exp_Id':e.user_exp_id,'user_action':e.experience.user_action}
  }
  reviewResultdata = e => event =>{
    this.setState({reviewShow : true})
    console.log(this.state.reviewShow);
    this.dataObject = {'exp_Id':e.user_exp_id,'user_result':e.experience.user_result}
  }
  imgSlider = e => event => {
    this.setState({ reviewShow: true })
    this.dataimageObject = e;
    console.log(this.dataimageObject);
  }
   toggleSearch() {
     
    const currentState=this.state.searchbox;
    this.setState({ searchbox: !currentState});
    
  }
  
  togglelistview() {
    const currentState=this.state.showlistview;
    this.setState({ showlistview: !currentState});
    
  }
  
  onChangeUserTag(e){
  
  }
  
  audioPlayer = e => event => {
    this.setState({ audioShow: true });
    this.dataaudioObject=e;
  }

  docViewer= (doc)=>event =>{
    console.log(doc);
      let path='https://cors-anywhere.herokuapp.com/'
      doc.forEach(el=>{
        this.state.docFile=path+el;
      })
      console.log(this.state.docFile);
     this.datadocObject = {"doc":'https://cors-anywhere.herokuapp.com/http://www.muralidhargirlscollege.org/notice/FINAL_GUIDELINES_SEMII_SEMIV_PARTI_II_EXAM2020.pdf'};
   //this.datadocObject = {"doc":this.state.docFile};
      this.setState({docView: !this.state.docView});
      console.log(this.datadocObject);
 }

   
   handletitlechange(e){
    let input = this.state.input;
      input[e.target.name] = e.target.value;
    
      this.setState({
        input:input
      });
    console.log(this.state.input.expquestion);
   }
  
   handleexpTitleChange = (e) =>{
   
  this.setState({exptitle: e.target.value});
  console.log(this.state.exptitle);
  
   }
   handleexpDetailsChange = (e) =>{
   
  this.setState({expdetails: e.target.value});
  
  
  }
  handleexpdominChange = (e) =>{
  
  this.setState({domain: e.target.value});
  
  }
  handletagcolleageChange = (e) =>{
   
  this.setState({Colleagues: e.target.value});
  
  }

  onFileChange = (e)=>{
    e.preventDefault();
  let file = e.target.files[0];
  let reader = new FileReader();
  reader.readAsDataURL(file);
  this.setState({imgfile: reader});;
    console.log(reader);
   
  }

  modalVideo= e => event =>{
    this.setState({index:e});
    this.setState({isOpen:true});
  }
  situmodalVideo= e => event =>{
    this.setState({index:e});
    this.setState({situisOpen:true});
  }
  taskmodalVideo= e => event =>{
    this.setState({index:e});
    this.setState({taskisOpen:true});
  }
  actmodalVideo= e => event =>{
    this.setState({index:e});
    this.setState({actisOpen:true});
  }
  resmodalVideo= e => event =>{
    console.log(e);
    this.setState({index:e});
    this.setState({resisOpen:true});
  }
  
  handlesubmit = (e)=>{
    console.log(this.state.input.expquestion);
    
    this.state.imgdata = this.state.imgfile.result;
    this.props.startLoader();
   
    AddUserConsultation(this.state.exptitle,this.state.expdetails,this.state.tags,this.state.imgdata).then(res=>{
      console.log(res);
      toast.success(res.data.msg,{});
      //window.location.reload();
    this.props.hideLoader();
    });
  
   }
  
   onDelete (i) {
    const tags = this.state.tags.slice(0)
    tags.splice(i, 1)
    this.setState({ tags })
    console.log(this.state.tags);
  }
  
  onAddition (tag) {
    const tags = [].concat(this.state.tags, tag)
    this.setState({ tags })
    setTimeout(() => {
      console.log(this.state.tags);
    }, 2000);
   
  }
  
  onDeletedomain (i) {
    const  tagsDomain = this.state. tagsDomain.slice(0)
    tagsDomain.splice(i, 1)
    this.setState({  tagsDomain })
  }
  
  onAdditiondomain (tagd) {
    const  tagsDomain = [].concat(this.state. tagsDomain, tagd)
    this.setState({  tagsDomain })
  }
  // handleCheckClick = () => {
  //   this.setState({ checked: !this.state.checked });
  // }

  handleCheckClick(value, event) {
  
  console.log(value, event);
  const evalparam = value;
  // this.state.evalparam = value;
  this.setState({evalparam});
  setTimeout(() => {
   // console.log(this.state.evalparam);
  }, 100);
}


  handleexpertconsultation = e => event =>{
    confirmAlert({
      title: 'Confirm to Request',
      message: 'Are you sure to do this.',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
              this.props.startLoader();
             consultationrRequest(this.props.location.data.user_exp_id,this.state.evalparam,e).then(res=>{
             console.log(res);
             toast.success(res.data.msg,{});
             window.location.reload();
            this.props.hideLoader();
   
            });
          }
        },
        {
          label: 'No',
          onClick: () => console.log("not requested")
        }
      ]
    });
     // console.log(this.props.location.data.user_exp_id);
    //  console.log(e);
     // console.log(this.state.evalparam);
   
  
  }
  

  Next= (key,fid,record)=>{
    if(this.state.record[key]!='' && this.state.record[key]!=undefined && this.state.starvalue[key]!='' && this.state.starvalue[key]!=undefined){
      this.state.point[key]={id:fid,point:this.state.starvalue[key],feedback_text:record,feedback_video:'',feedback_audio:''}
      this.setState({index:key+1,step:''});
    }else
    {
      toast.error('Please fill mandetory fields.',{});
    }

    }
    
  

    handlerecord(event,key){
      this.state.record[key]=event.target.value;
      console.log(this.state.record[key]);
    }
    Prev= (key)=>{
      this.setState({index:key-1,step:''});
     }
     toggleClass(index,key,fid,record){
      //this.setState(this.state.feedbackpoint[key]=[index]);
      console.log(key);
     
       //this.state.starvalue[key]=index;
     
       this.setState(this.state.starvalue[key]=[index]);
      console.log(this.state.starvalue[key]);
      this.state.feedbackpoint[key]=index;


      }

    submit = (id,key,fid,record)=>{
      if(this.state.record[key]!='' && this.state.record[key]!=undefined && this.state.starvalue[key]!='' && this.state.starvalue[key]!=undefined){
        this.state.point[key]={id:fid,point:this.state.starvalue[key],feedback_text:record,feedback_video:'',feedback_audio:''}
      console.log(this.state.point);
      console.log(id);
       this.props.startLoader();
      addfeedback(id,this.state.point).then(res => {
         console.log(res);
         toast.success(res.data.msg, {});
         console.log(res.data);
         this.props.hideLoader();
         this.setState({
          expertviewList:res.data.data
        
        });
        console.log(this.state.expertviewList);
      });

      }
      else{
        toast.error('Please fill mandetory fields.',{});
      }
      
     
     
    }

    changestate= (id)=>{
      console.log(id);
     
      constpubpvt(id).then(res => {
       
        console.log(res.data);
        toast.success(res.data.msg,{});
        window.location.reload();
      });
  
    }


      render(){
        let requestmicModalClose =() =>{this.setState({openMic:false})}
        console.log(this.state.point);
        if(this.state.feedbackpoint[this.state.index]!=0 && this.state.feedbackpoint[this.state.index]!='')
        {
          this.state.starvalue[this.state.index]=this.state.feedbackpoint[this.state.index];
        }
          console.log(this.state.feedbackpoint);
          console.log(this.state.starvalue);
          console.log('hi');
        //let requestModalClose = () =>{this.setState({reviewShow:false})}
       let requestModalClose = () => { this.setState({ audioShow: false }) }
       let  docViewClose=() =>{this.setState({docView:false})}
      
        const responsive = {

          desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 3
          },
      };
     
          return(
              <div className="col-sm-6">
                
              <div className="profile-sec2-middle">
                  <div className="profile-sec2-middle-inne">
                  <h2 className="innerhed"> My Consultaion View <a href="#" className="closebc float-right">Close</a> </h2>
                  <Tabs defaultActiveKey="get" transition={false} id="noanim-tab-example" className="viewconsult" onSelect={(k) => this.setKey(k)}>
                    <Tab eventKey="get" title="Offline Consultaion "></Tab>
                    <Tab eventKey="offer" title="Public Profile Resume"></Tab>
                    <Tab eventKey="offer" title="Live Career Advice"></Tab>
                    
                    </Tabs>
             { 
       this.state.showlistview && this.state.expertviewList.map((e, key)=>{
         
      this.state.masId=(e.user_exp_id)
          return  <Accordion className="mainacr"><Card>
          	<div key={key}  className="">
            <Card.Header>
                      <div className="d-flex justify-content-between">
                          <div className="extitle">
                          <Speech 
                    textAsButton={true}    
                      displayText={<i className="fa fa-volume-up"></i>}
                       text={e.experience.user_exp_title} voice="Google UK English Female"></Speech>
                          
                          <Accordion.Toggle as={Button} variant="link" eventKey="0" className="pl-0 pb-0">
                          
					               	<h2>{e.experience.user_exp_title}</h2>
                        </Accordion.Toggle>
                         
                         
                          <a href="#">{moment(e.exp_date_added).format('LL')}</a>
                          <p className="mb-0 mt-2">{e.experience.user_exp_summary}</p>
						</div>
           
          
                          <div className="exoption d-flex">

                         
                          <a href="#"><i className="fa fa-ellipsis-v"></i></a>
  
                          </div>
                      </div>

                      </Card.Header>
                      <Accordion.Collapse eventKey="0">
                      <Card.Body>
                      <div className="taglist mb-3">
      <div className="tagdomain">
           {e.domain_tagged!=''&&  <span className="domain">Skill :</span>}
            {e.domain_tagged && e.domain_tagged.map((jc,jcb)=>{
					return	<span key={jcb}> <i  className="fa fa-tags"></i>    {jc.name}  |</span> 
       })} 
       </div>

       <div className="tagdomain">
           {e.user_association!=''&&  <span className="domain">Organization :</span>}
            {e.user_association && e.user_association.map((jc,jcb)=>{
					return	<span key={jcb}> <i ></i>    {jc.organization_name}  |</span> 
       })} 
       </div>

       <div className="tagcolg">
{e.user_tagged!=''&&  <span className="colgi">Colleagues : </span>}
{e.user_tagged && e.user_tagged.map((jc,jcb)=>{
					return	<span key={jcb}><i  className="fa fa-user"></i>    {jc.f_name + " "+jc.l_name}  |</span> 
       })} 
       </div>
       <div className="ragenfile">
      {e.user_image_files>0 &&
            <a onClick={this.imgSlider(e.user_image_files)} >
          <span className="badge">{e.user_image_files.length}</span>
            <i className="fa fa-file-image-o"></i>

            </a>
     }
      {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.isOpen} url={e.videos[0]} onClose={() => this.setState({isOpen: false})} />
     }
      {e.videos.length>0 &&
            <a onClick={this.modalVideo(key)}>
            <span className="badge">{e.videos.length}</span>
            <i className="fa fa-file-video-o"></i>
           
            </a>
     }
      {e.audio.length>0 &&
      <a  onClick={this.audioPlayer(e.audio)}  >
       
            <span className="badge ">{e.audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
          
            </a>
     }
      {e.doc.length>0 &&
            <a onClick={this.docViewer(e.doc)}>
             
            <span className="badge ">{e.doc.length}</span>
            <i className="fa fa-files-o"></i>
          
            </a>
     }
            </div>

       </div>
       
          <div className="multiimg pb-2">
          <div className="row">
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
        <h3>My Situation</h3>
         
          <div className="d-flex justify-content-left rafile">
          {e.experience.user_situation_files.length>0 &&
            <a  onClick={this.reviewSituationdata(e)}>
        <span className="badge rabadge">{e.experience.user_situation_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
       }
      {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.isOpen} url={e.situation_video[0]} onClose={() => this.setState({isOpen: false})} />
     }
        {e.situation_video.length>0 &&
            <a onClick={this.modalVideo(key)}>
            <span className="badge rabadge">{e.situation_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
       {e.situation_audio.length>0 &&
            <a onClick={this.audioPlayer(e.situation_audio)} >
            <span className="badge rabadge">{e.situation_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
       {e.situation_doc_file.length>0 &&
            <a onClick={this.docViewer(e.situation_doc_file)}>
            <span className="badge rabadge">{e.situation_doc_file.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
            </Accordion.Toggle>
      
      </Card.Header>
      <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
      <p>{e.experience.user_situation}</p>
           </ShowMoreText>
     
      </Card.Body>
      </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
          <h3>My Task</h3>
           <div className="d-flex justify-content-left rafile">
           {e.experience.user_task_files.length>0 &&
            <a onClick={this.imgSlider(e.experience.user_task_files)}>
        <span className="badge rabadge">{e.experience.user_task_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
       }
       {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.taskisOpen} url={e.experience.user_task_files[0]} onClose={() => this.setState({taskisOpen: false})} />
     }
        {e.experience.user_task_files.length>0 &&
            <a onClick={this.taskmodalVideo(key)}>
            <span className="badge rabadge">{e.experience.user_task_files.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
       }
        {e.experience.user_task_files.length>0 &&
            <a onClick={this.audioPlayer(e.experience.user_task_files)}>
            <span className="badge rabadge">{e.experience.user_task_files.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
       }
        {e.experience.user_task_files.length>0 &&
            <a onClick={this.docViewer(e.experience.user_task_files)}>
            <span className="badge rabadge">{e.experience.user_task_files.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
       }
           </div>
           </Accordion.Toggle>
      
      </Card.Header>
      <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
                <p>{e.experience.user_task}</p>
           </ShowMoreText>
      
      </Card.Body>
      </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
     
          </div>

          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
          <h3>My Action</h3>
          <div className="d-flex justify-content-left rafile">
          {e.experience.user_action_files.length>0 &&
            <a onClick={this.reviewActiondata(e)} >
        <span className="badge rabadge">{e.experience.user_action_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
       }
            </div>
            </Accordion.Toggle>
      
      </Card.Header>
      <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
    
                <p>{e.experience.user_action}</p>
           </ShowMoreText>
      
      </Card.Body>
      </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
          <h3>My Result</h3>
      
           <div className="d-flex justify-content-left rafile">
           {e.experience.user_result_files.length>0 &&
            <a  onClick={this.reviewResultdata(e)}>
        <span className="badge rabadge">{e.experience.user_result_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
       }
       {this.state.index==key &&
            <ModalVideo channel='custom' isOpen={this.state.actisOpen} url={e.experience.user_result_files[0]} onClose={() => this.setState({actisOpen: false})} />
            }
        {e.experience.user_result_files.length>0 &&
            <a onClick={this.actmodalVideo(key)}>
            <span className="badge rabadge">{e.experience.user_result_files.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
       }
        {e.experience.user_result_files.length>0 &&
            <a onClick={this.audioPlayer(e.experience.user_result_files)}>
            <span className="badge rabadge">{e.experience.user_result_files.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
       }
        {e.experience.user_result_files.length>0 &&
            <a onClick={this.docViewer(e.experience.user_result_files)}>
            <span className="badge rabadge">{e.experience.user_result_files.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
       }
            </div>
            </Accordion.Toggle>
      
      </Card.Header>
      <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
                <p>{e.experience.user_result}</p>
           </ShowMoreText>
      
        </Card.Body>
        </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
          </div>
      
       </div>   
        
 <div className="rafrom">
        <h2>Request From</h2>
        <ul>
          <li>Tagged by : <span>{e.tagged_by}</span></li>
          <li>Date : <span>{moment(e.exp_date_added).format('LL')}</span></li>
          <li>Parameters :
          {e.parameters.map((jc,jcb)=>{
            
         return  <span key={jcb}> {jc.name} |</span>
          
        })} 
        </li>
        </ul>
      
         </div>

         {this.state.expertfeedback.map((e,key)=>{
             if(this.state.index==key && !this.state.openMic)
             {
         return <div key={key}  className="exprtfeed">
         <h2>Expert Feedback</h2>
         
         <div className="rafeed">
       
          <h3>{e.user_feedback_id}. {e.feedback_param}<span>{e.user_feedback_id}/{this.state.expertfeedback.length}</span></h3>
          <ul>
          <li ><a className={(this.state.starvalue[key]==1? 'active': '')} onClick={()=>this.toggleClass(1,key,e.user_feedback_id,this.state.record[key])}>1</a></li>
          <li><a className={(this.state.starvalue[key]==2? 'active': '')} onClick={()=>this.toggleClass(2,key,e.user_feedback_id,this.state.record[key])} >2</a></li>
          <li><a className={(this.state.starvalue[key]==3? 'active': '')} onClick={()=>this.toggleClass(3,key,e.user_feedback_id,this.state.record[key])} >3</a></li>
          <li><a className={(this.state.starvalue[key]==4? 'active': '')} onClick={()=>this.toggleClass(4,key,e.user_feedback_id,this.state.record[key])}>4</a></li>
          <li><a className={(this.state.starvalue[key]==5? 'active': '')} onClick={()=>this.toggleClass(5,key,e.user_feedback_id,this.state.record[key])}>5</a></li>
          <li><a className={(this.state.starvalue[key]==6? 'active': '')} onClick={()=>this.toggleClass(6,key,e.user_feedback_id,this.state.record[key])}>6</a></li>
          <li><a className={(this.state.starvalue[key]==7? 'active': '')} onClick={()=>this.toggleClass(7,key,e.user_feedback_id,this.state.record[key])}>7</a></li>
          <li><a className={(this.state.starvalue[key]==8? 'active': '')} onClick={()=>this.toggleClass(8,key,e.user_feedback_id,this.state.record[key])}>8</a></li>
          <li><a className={(this.state.starvalue[key]==9? 'active': '')} onClick={()=>this.toggleClass(9,key,e.user_feedback_id,this.state.record[key])}>9</a></li>
          <li><a className={(this.state.starvalue[key]==10? 'active': '')} onClick={()=>this.toggleClass(10,key,e.user_feedback_id,this.state.record[key])}>10</a></li>
          </ul>
      
          {this.state.index!=(this.state.expertfeedback.length-1) &&
          <a  onClick={()=>this.Next(key,e.user_feedback_id,this.state.record[key])} className="ranext">Next</a>
            }
             {this.state.index!=0 &&
          <a  onClick={()=>this.Prev(key)} className="raprev">Previous</a>
             }
          
          <div class="form-group">
          <textarea name="record" onChange={(event)=>this.handlerecord(event,key)} defaultValue={this.state.record[key]} className="form-control" id="exampleFormControlTextarea1" placeholder="Enter or record your feedback" rows="3"></textarea>
          {isChrome && !isFirefox && !isOpera && !isSafari &&
          <a  onClick={()=>this.openMic()}><MicIcon className="audiospk"/></a>
          }
          </div>
          <div class="clearfix"></div>
          {this.state.index==(this.state.expertfeedback.length-1) &&
          <button type="submit" class="btn btn-primary rasubmit" onClick={() =>{this.submit(this.state.masId,key,e.user_feedback_id,this.state.record[key])}}>Submit your feedback</button>
            }
     
         </div>
      
         </div>
            }
            else if(this.state.index==key && this.state.openMic)
            {
                return <Speechtotext show={this.state.openMic} textboxId={key} onHide={requestmicModalClose}/>
            }
         })}
               
      
         
</Card.Body>
    </Accordion.Collapse>
      
                      <div className="clearfix"></div>
          
                  </div>

                  </Card>
                </Accordion>    
       
      })
  }
                
  
            {/* { this.state.showlistview &&
                  <div className="exbg">
                  <div className="form-group rslt">
         <div className="rafrom">
        <h2>Request From</h2>
        <ul>
          <li>Tagged by : <span>jone doue</span></li>
          <li>Date : <span>December 16,2020</span></li>
          <li>Parameters : <span>Example </span></li>
        </ul>
         </div>
        {this.state.expertfeedback.map((e,key)=>{
            if(this.state.index==key )
            {
         return <div key={key}  className="exprtfeed">
         <h2>Expart Feedback</h2>
         
         <div className="rafeed">
       
          <h3>{e.user_feedback_id}. {e.feedback_param}<span>{e.user_feedback_id}/{this.state.expertfeedback.length}</span></h3>
          <ul>
          <li ><a className={(this.state.feedbackpoint[key]==1? 'active': '')} onClick={()=>this.toggleClass(1,key,e.user_feedback_id,this.state.record[key])}>1</a></li>
          <li><a className={(this.state.feedbackpoint[key]==2? 'active': '')} onClick={()=>this.toggleClass(2,key,e.user_feedback_id,this.state.record[key])} >2</a></li>
          <li><a className={(this.state.feedbackpoint[key]==3? 'active': '')} onClick={()=>this.toggleClass(3,key,e.user_feedback_id,this.state.record[key])} >3</a></li>
          <li><a className={(this.state.feedbackpoint[key]==4? 'active': '')} onClick={()=>this.toggleClass(4,key,e.user_feedback_id,this.state.record[key])}>4</a></li>
          <li><a className={(this.state.feedbackpoint[key]==5? 'active': '')} onClick={()=>this.toggleClass(5,key,e.user_feedback_id,this.state.record[key])}>5</a></li>
          <li><a className={(this.state.feedbackpoint[key]==6? 'active': '')} onClick={()=>this.toggleClass(6,key,e.user_feedback_id,this.state.record[key])}>6</a></li>
          <li><a className={(this.state.feedbackpoint[key]==7? 'active': '')} onClick={()=>this.toggleClass(7,key,e.user_feedback_id,this.state.record[key])}>7</a></li>
          <li><a className={(this.state.feedbackpoint[key]==8? 'active': '')} onClick={()=>this.toggleClass(8,key,e.user_feedback_id,this.state.record[key])}>8</a></li>
          <li><a className={(this.state.feedbackpoint[key]==9? 'active': '')} onClick={()=>this.toggleClass(9,key,e.user_feedback_id,this.state.record[key])}>9</a></li>
          <li><a className={(this.state.feedbackpoint[key]==10? 'active': '')} onClick={()=>this.toggleClass(10,key,e.user_feedback_id,this.state.record[key])}>10</a></li>
          </ul>
      
          {this.state.index!=(this.state.expertfeedback.length-1) &&
          <a  onClick={()=>this.Next(key)} className="ranext">Next</a>
            }
             {this.state.index!=0 &&
          <a  onClick={()=>this.Prev(key)}  >Previous</a>
             }
          <form>
          <div class="form-group">
          <textarea name="record" onChange={(event)=>this.handlerecord(event,key)} defaultValue={this.state.record[key]} className="form-control" id="exampleFormControlTextarea1" placeholder="Enter or record your feedback" rows="3"></textarea>
          <a onClick={()=>this.openMic('exsummery')}><MicIcon className="audiospk"/></a>
          </div>
         
          <button type="submit" class="btn btn-primary rasubmit">Submit your feedback</button>
          </form>

         </div>
      
         </div>
            }
         })}
                  </div>				
          <div className="clearfix">
        </div>
      </div>
      } */}
  
  
            { this.state.showexpForm &&
                      <div className="exbg">
        <h3>Add Review Request</h3>
          
               <div className="form-group">
   <div className="btn-group">
      
      
                   </div>
    </div>   
  
       <div  className="form-group">
             <input type="text" className="form-control" placeholder="Enter Review Request title" value={this.state.exptitle} onChange={this.handleexpTitleChange}/>
              </div>
         
               <div className="form-group">
                   <textarea type="text" className="form-control" rows="4" placeholder="Enter Review Request details" value={this.state.expdetails} onChange={this.handleexpDetailsChange}></textarea>
                  
              </div>
           
              <div className="form-group ">
              <ReactTags
          ref={this.reactTags}
          tags={this.state.tagsDomain}
          suggestions={this.state.suggestionsDomain}
          onDelete={this.onDeletedomain.bind(this)}
          onAddition={this.onAdditiondomain.bind(this)}
  
          placeholderText ={'Tag Domain'} />
               
                    <ReactTags
          ref={this.reactTags}
          tags={this.state.tags}
          suggestions={this.state.suggestions}
          onDelete={this.onDelete.bind(this)}
          onAddition={this.onAddition.bind(this)}
  
          placeholderText ={'Tag Expert'} />
              </div>
     
              
             <div className="form-group">
     
              <div className="preview-zone hidden">
                <div className="box box-solid">
                  <div className="box-header with-border">
                    <div><b></b></div>
                    <div className="box-tools pull-right">
                      
                    </div>
                  </div>
                  <div className="box-body"></div>
                </div>
              </div>
              <div className="dropzone-wrapper">
                <div className="dropzone-desc">
              
                  <p>Drop your files here</p>
                    <p className="orcolor">OR</p>
                    
                     <span className="input-group-btn">
                      <span className="btn btn-primary btn-file"> Browse Files <input type="file" multiple /></span>
                  </span>
                </div>
                <input type="file" name="img_logo" className="dropzone" onChange={this.onFileChange} />
              </div>
            </div>
            
              <div className="form-group" style={{textAlign: 'right'}}>
              <button className="cancel">Cancel</button>
                  <button className="cancel submit" onClick={this.handlesubmit}>Submit</button>
              </div>
            
     
          <div className="clearfix"></div>
          </div>
      }
      {this.state.showAppointment &&
                <div className="notification mb-4">
                <div className="exbg">
                    {/* {location.pathname == '/profile'} */}
                    <h3>Consultation Notifications</h3>
                    <a href="#" className="pl-1"><i className="fa fa-filter"></i></a>
                    <a href="#"><i className="fa fa-sort-amount-down-alt"></i></a>
    
                    <div className="apoinbox">
                    <div className="d-flex justify-content-between notibox accept">
                    <div className="accept_left">
                    <h4>Experience</h4>
                    <p>Agenda Domain</p>
                    </div>
                        <div className="accept_right">
                        <a href="#"><i className="fa fa-ellipsis-v"></i></a>
                        <span>2d</span>
                        </div>
                    </div>
                    <div className="d-flex justify-content-between notibox pending">
                    <div className="accept_left">
                    <h4>Experience</h4>
                    <p>Agenda Domain</p>
                    </div>
                        <div className="accept_right">
                        <a href="#"><i className="fa fa-ellipsis-v"></i></a>
                        <span>2d</span>
                        </div>
                    </div>
                    <div className="d-flex justify-content-between notibox rejected">
                    <div className="accept_left">
                    <h4>Experience</h4>
                    <p>Agenda Domain</p>
                    </div>
                        <div className="accept_right">
                        <a href="#"><i className="fa fa-ellipsis-v"></i></a>
                        <span>2d</span>
                        </div>
                    </div>
                    <div className="d-flex justify-content-between notibox resudule">
                    <div className="accept_left">
                    <h4>Experience</h4>
                    <p>Agenda Domain</p>
                    </div>
                        <div className="accept_right">
                        <a href="#"><i className="fa fa-ellipsis-v"></i></a>
                        <span>2d</span>
                        </div>
                    </div>
                        <div className="d-flex justify-content-between notibox resudule">
                    <div className="accept_left">
                    <h4>Experience</h4>
                    <p>Agenda Domain</p>
                    </div>
                        <div className="accept_right">
                        <a href="#"><i className="fa fa-ellipsis-v"></i></a>
                        <span>2d</span>
                        </div>
                    </div>
                    </div>
                    <div className="legend">
                    <ul>
                        <li><i className="fa fa-circle color1"></i> Accepted</li>
                        <li><i className="fa fa-circle color2"></i> Pending</li>
                        <li><i className="fa fa-circle color3"></i> Rejected</li>
                        <li><i className="fa fa-circle color4"></i> Reschedule</li>
                    </ul>
                    </div>
                    <div className="clearfix"></div>
                </div>
                </div>
                }
                 { 
       this.props && this.props.location.data != null &&
       <Accordion className="exbg exprience raconsltmd"><Card>
            <Card.Header>
                      <div className="d-flex justify-content-between">
                          <div className="extitle">
                          <Speech 
                    textAsButton={true}    
                      displayText={<i className="fa fa-volume-up"></i>}
                       text={this.props.location.data.user_exp_title} voice="Google UK English Female"></Speech>
                          <Accordion.Toggle as={Button} variant="link" eventKey="0" className="pl-0 pb-0">
                          <h2>{this.props.location.data.user_exp_title}</h2>
                        </Accordion.Toggle>
                          <a href="#">{moment(this.props.location.data.exp_date_added).format('LL')}</a>

                         
                          <p className="mb-0 mt-2">{this.props.location.data.summary}</p>
                          </div>
                          <div className="exoption d-flex">
                         

                          <a href="#"><i className="fa fa-ellipsis-v"></i></a>
                          
  
                          </div>
                      </div>
                      </Card.Header>
                      <Accordion.Collapse eventKey="0">
                      <Card.Body>
                      <p> </p>
                     
          <div className="multiimg pb-2 mb-3">
          <div className="row">
          <div className="col-sm-12">
          <div className="yract racons">
          <Accordion defaultActiveKey="0">
  <Card>
 <div className="tagdomain">
  {this.props.location.data.domain_tagged!=''&&  <span className="domain">Skill :</span>}
            {this.props.location.data.domain_tagged && this.props.location.data.domain_tagged.map((jc,jcb)=>{
					return	<span key={jcb}> <i  className="fa fa-tags"></i>    {jc.name}  |</span> 
       })} 
       </div>
    <Card.Header>
    <div className="tagdomain">
           {this.props.location.data.user_association!=''&&  <span className="domain">Organization :</span>}
            {this.props.location.data.user_association && this.props.location.data.user_association.map((jc,jcb)=>{
					return	<span key={jcb}> <i ></i>    {jc.organization_name}  |</span> 
       })} 
       </div>
       <div className="tagcolg">
{this.props.location.data.user_tagged!=''&&  <span className="colgi">Colleagues : </span>}
{this.props.location.data.user_tagged && this.props.location.data.user_tagged.map((jc,jcb)=>{
					return	<span key={jcb}><i  className="fa fa-user"></i>    {jc.f_name + " "+jc.l_name}  |</span> 
       })} 
       </div>
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
        <h3>My Situation</h3>
       
          <div className="d-flex justify-content-left rafile">
          {this.props.location.data.situation_image_count>0 &&
            <a  onClick={this.reviewSituationdata(this.props.location.data)}>
        <span className="badge rabadge">{this.props.location.data.situation_image_count}</span>
            <i className="fa fa-file-image-o"></i>
            </a>
        }
        
            </div>
            </Accordion.Toggle>
      
      </Card.Header>
      <Accordion.Collapse eventKey="0">
      <Card.Body>
      <p>{this.props.location.data.situation_text}</p>
        </Card.Body>
        </Accordion.Collapse>
        </Card>
        </Accordion>
          </div>
          </div>
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
          <h3>My Task</h3>
         
           <div className="d-flex justify-content-left rafile">
           {this.props.location.data.task_image_count>0 &&
            <a >
        <span className="badge rabadge">{this.props.location.data.task_image_count}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
      }
      {/* {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.taskisOpen} url={e.this.props.location.data.user_task_files[0]} onClose={() => this.setState({taskisOpen: false})} />
     } */}
     {/* onClick={this.taskmodalVideo(key)} */}
       {this.props.location.data.user_task_files.length>0 &&
            <a >
            <span className="badge rabadge">{this.props.location.data.user_task_files.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
      }
            {this.props.location.data.user_task_files.length>0 &&
            <a onClick={this.audioPlayer(this.props.location.data.user_task_files)}>
            <span className="badge rabadge">{this.props.location.data.user_task_files.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
      }
            {this.props.location.data.user_task_files.length>0 &&
            <a onClick={this.docViewer(this.props.location.data.user_task_files)}>
            <span className="badge rabadge">{this.props.location.data.user_task_files.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
      }
           </div>
           </Accordion.Toggle>
           </Card.Header>
           <Accordion.Collapse eventKey="0">
      <Card.Body>
      <p>{this.props.location.data.task_text}</p>
        </Card.Body>
        </Accordion.Collapse>
        </Card>
        </Accordion>
          </div>
          </div>
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
          <h3>My Action</h3>
        
          <div className="d-flex justify-content-left rafile">
          {this.props.location.data.action_image_count>0 &&
            <a onClick={this.reviewActiondata(this.props.location.data)} >
        <span className="badge rabadge">{this.props.location.data.action_image_count}</span>
            <i className="fa fa-file-image-o"></i>
            </a>
      }
            </div>
            </Accordion.Toggle>
           </Card.Header>
           <Accordion.Collapse eventKey="0">
      <Card.Body>
      <p>{this.props.location.data.action_text}</p>
      </Card.Body>
      </Accordion.Collapse>
      </Card>
      </Accordion>
          </div>
          </div>
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
          <h3>My Result</h3>
         
           <div className="d-flex justify-content-left rafile">
           {this.props.location.data.result_image_count>0 &&
            <a  onClick={this.reviewResultdata(this.props.location.data)}>
        <span className="badge rabadge">{this.props.location.data.result_image_count}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
      }
      {/* {this.state.index==key &&
            <ModalVideo channel='custom' isOpen={this.state.actisOpen} url={e.action_video[0]} onClose={() => this.setState({actisOpen: false})} />
            } */}
            {/* onClick={this.actmodalVideo(key)} */}
      {this.props.location.data.user_result_files.length>0 &&
            <a >
            <span className="badge rabadge">{this.props.location.data.user_result_files.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
      }
      {this.props.location.data.user_result_files.length>0 &&
            <a onClick={this.audioPlayer(this.props.location.data.user_result_files)}>
            <span className="badge rabadge">{this.props.location.data.user_result_files.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
      }
      {this.props.location.data.user_result_files.length>0 &&
            <a onClick={this.docViewer(this.props.location.data.user_result_files)}>
            <span className="badge rabadge">{this.props.location.data.user_result_files.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
      }
            </div>
            </Accordion.Toggle>
           </Card.Header>
           <Accordion.Collapse eventKey="0">
      <Card.Body>
      <p>{this.props.location.data.result_text}</p>
        </Card.Body>
        </Accordion.Collapse>
        </Card>
        </Accordion>
          </div>
          </div>
       
          </div>
      
       </div>   
           
           </Card.Body>
           </Accordion.Collapse>
           { this.props.location.data &&
            
            <div className="multiselect">
               <label>Review Parameter</label>
            <ReactMultiSelectCheckboxes options={this.state.options} value={this.state.evalparam} onChange={this.handleCheckClick}/>
            </div>
         }

           { this.props.location.data &&
             <div className="owl-theme consult">
               
                <label>Available Experts</label>
            <Carousel responsive={responsive}>
            {this.state.expertlist && this.state.expertlist.map((jc,key)=>
                <div key={key} className="item">
                <div className="sec3-item-bottom">
                  
                <img src={jc.user_image} onError={(e)=>{e.target.onerror = null; e.target.src=testimonial}}/>
                <h4>{jc.user_fname+" "+jc.user_lname}</h4>
                <h5>{jc.user_email}</h5>
                <p>{jc.company}</p>
                <span>{jc.designation}</span>
                <button className="cancel submit" onClick={this.handleexpertconsultation(jc.user_id)}>Select</button>

                </div>
                
                </div>
                 )} 
              </Carousel>
              </div>
       }
                      <div className="clearfix"></div>
                  </Card>
                  </Accordion>
        
  }

       { 
       this.state.showlistview==false && this.state.getpostValue.map((e, key)=>{
      
          return  <Accordion className="mainacr"><Card>
          	<div key={key}  className="">
        
            <Card.Header>
                      <div className="d-flex justify-content-between">
                          <div className="extitle">
                          <Speech 
                    textAsButton={true}    
                      displayText={<i className="fa fa-volume-up"></i>}
                       text={e.experience.user_exp_title} voice="Google UK English Female"></Speech>
                          
                          <Accordion.Toggle as={Button} variant="link" eventKey="0" className="pl-0 pb-0">
                          
					               	<h2>{e.experience.user_exp_title}</h2>
                        </Accordion.Toggle>
                         
                         
                          <a href="#">{moment(e.exp_date_added).format('LL')}</a>
                          <p className="mb-0 mt-2">{e.experience.user_exp_summary}</p>
						</div>
           
                          <div className="exoption d-flex">
                           
                         
                          <a href="#"><i className="fa fa-ellipsis-v"></i></a>
  
                          </div>
                      </div>

                      </Card.Header>
                      <Accordion.Collapse eventKey="0">
                      <Card.Body>
                      <div className="taglist mb-3">
      <div className="tagdomain">
           {e.domain_tagged!=''&&  <span className="domain">Skill :</span>}
            {e.domain_tagged && e.domain_tagged.map((jc,jcb)=>{
					return	<span key={jcb}> <i  className="fa fa-tags"></i>    {jc.name}  |</span> 
       })} 
       </div>

       <div className="tagdomain">
           {e.user_association!=''&&  <span className="domain">Organization :</span>}
            {e.user_association && e.user_association.map((jc,jcb)=>{
					return	<span key={jcb}> <i ></i>    {jc.organization_name}  |</span> 
       })} 
       </div>

       <div className="tagcolg">
{e.user_tagged!=''&&  <span className="colgi">Colleagues : </span>}
{e.user_tagged && e.user_tagged.map((jc,jcb)=>{
					return	<span key={jcb}><i  className="fa fa-user"></i>    {jc.f_name + " "+jc.l_name}  |</span> 
       })} 
       </div>
       <div className="ragenfile">
      {e.user_image_files>0 &&
            <a onClick={this.imgSlider(e.user_image_files)} >
          <span className="badge">{e.user_image_files.length}</span>
            <i className="fa fa-file-image-o"></i>

            </a>
     }
      {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.isOpen} url={e.videos[0]} onClose={() => this.setState({isOpen: false})} />
     }
      {e.videos.length>0 &&
            <a onClick={this.modalVideo(key)}>
            <span className="badge">{e.videos.length}</span>
            <i className="fa fa-file-video-o"></i>
           
            </a>
     }
      {e.audio.length>0 &&
      <a  onClick={this.audioPlayer(e.audio)}  >
       
            <span className="badge ">{e.audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
          
            </a>
     }
      {e.doc.length>0 &&
            <a onClick={this.docViewer(e.doc)}>
             
            <span className="badge ">{e.doc.length}</span>
            <i className="fa fa-files-o"></i>
          
            </a>
     }
            </div>

       </div>
       
          <div className="multiimg pb-2">
          <div className="row">
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
        <h3>My Situation</h3>
         
          <div className="d-flex justify-content-left rafile">
          {e.experience.user_situation_files.length>0 &&
            <a  onClick={this.reviewSituationdata(e)}>
        <span className="badge rabadge">{e.experience.user_situation_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
       }
        {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.isOpen} url={e.situation_video[0]} onClose={() => this.setState({isOpen: false})} />
     }
        {e.situation_video.length>0 &&
            <a onClick={this.modalVideo(key)}>
            <span className="badge rabadge">{e.situation_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
       {e.situation_audio.length>0 &&
            <a onClick={this.audioPlayer(e.situation_audio)} >
            <span className="badge rabadge">{e.situation_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
       {e.situation_doc_file.length>0 &&
            <a onClick={this.docViewer(e.situation_doc_file)}>
            <span className="badge rabadge">{e.situation_doc_file.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
            </Accordion.Toggle>
      
      </Card.Header>
      <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
      <p>{e.experience.user_situation}</p>
           </ShowMoreText>
     
      </Card.Body>
      </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
          <h3>My Task</h3>
           <div className="d-flex justify-content-left rafile">
           {e.experience.user_task_files.length>0 &&
            <a onClick={this.imgSlider(e.experience.user_task_files)}>
        <span className="badge rabadge">{e.experience.user_task_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
       }
       {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.taskisOpen} url={e.experience.user_task_files[0]} onClose={() => this.setState({taskisOpen: false})} />
     }
        {e.experience.user_task_files.length>0 &&
            <a onClick={this.taskmodalVideo(key)}>
            <span className="badge rabadge">{e.experience.user_task_files.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
       }
        {e.experience.user_task_files.length>0 &&
            <a onClick={this.audioPlayer(e.experience.user_task_files)}>
            <span className="badge rabadge">{e.experience.user_task_files.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
       }
        {e.experience.user_task_files.length>0 &&
            <a  onClick={this.docViewer(e.experience.user_task_files)}>
            <span className="badge rabadge">{e.experience.user_task_files.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
       }
           </div>
           </Accordion.Toggle>
      
      </Card.Header>
      <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
                <p>{e.experience.user_task}</p>
           </ShowMoreText>
      
      </Card.Body>
      </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
     
          </div>

          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
          <h3>My Action</h3>
          <div className="d-flex justify-content-left rafile">
          {e.experience.user_action_files.length>0 &&
            <a onClick={this.reviewActiondata(e)} >
        <span className="badge rabadge">{e.experience.user_action_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
       }
            </div>
            </Accordion.Toggle>
      
      </Card.Header>
      <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
    
                <p>{e.experience.user_action}</p>
           </ShowMoreText>
      
      </Card.Body>
      </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
          <h3>My Result</h3>
      
           <div className="d-flex justify-content-left rafile">
           {e.experience.user_result_files.length>0 &&
            <a  onClick={this.reviewResultdata(e)}>
        <span className="badge rabadge">{e.experience.user_result_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
       }
       {this.state.index==key &&
            <ModalVideo channel='custom' isOpen={this.state.actisOpen} url={e.experience.user_result_files[0]} onClose={() => this.setState({actisOpen: false})} />
            }
        {e.experience.user_result_files.length>0 &&
            <a onClick={this.actmodalVideo(key)}>
            <span className="badge rabadge">{e.experience.user_result_files.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
       }
        {e.experience.user_result_files.length>0 &&
            <a onClick={this.audioPlayer(e.experience.user_result_files)}>
            <span className="badge rabadge">{e.experience.user_result_files.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
       }
        {e.experience.user_result_files.length>0 &&
            <a onClick={this.docViewer(e.experience.user_result_files)}>
            <span className="badge rabadge">{e.experience.user_result_files.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
       }
            </div>
            </Accordion.Toggle>
      
      </Card.Header>
      <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
                <p>{e.experience.user_result}</p>
           </ShowMoreText>
      
        </Card.Body>
        </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
          </div>
      
       </div>   
      
      <div className="rafrom requstpublic">

                    {e.feedback_rate!='' &&                    
              <div className="pprivate">
              Public
              <label className="switch" >
              {e.consultation_type=="Public" ?
              <input checked type="checkbox" onClick={() =>{this.changestate(e.user_exp_id)}}  />
              :<input  type="checkbox" onClick={() =>{this.changestate(e.user_exp_id)}} />}
              <span className="slider round"></span>
              </label>
              </div>
              }
     
        <h2>Request to</h2>
        { 
         e.expert_tagged.map((jc,jcb)=>{
        
        return<ul key={jcb}>
          <li>Expert : <span>{jc.f_name +" " + jc.l_name}</span></li>
          <li>Date : <span>{moment(e.exp_date_added).format('LL')}</span></li>
          <li>Parameters : 
          {e.parameters.map((jc,jcb)=>{
            
            return  <span key={jcb}> {jc.name} |</span>
             
           })} 
            </li>
        </ul>
         })}
         </div>
         {
           e.feedback_rate!='' &&
         


         <div className="exprtfeed">
         <h2>Expert Feedback</h2>
         <div className="rariview">
          
      
                 <ul>
                { e.feedback_rate &&
              e.feedback_rate.map((ac,cc)=>{
                return <li  key ={cc}>{ac.feedback_param} <span>{ac.feedback_point}/10</span>
                <Tooltip  title={ac.feedback_text}>
                <i className="fa fa-envelope"></i>
                </Tooltip></li>
              })}
           
           </ul>
           
            <span className="ratotal">Total : <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
           {/* <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
             ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo 
             viverra maecenas accumsan lacus vel facilisis. </p> */}
         </div>
         
    <div class="clearfix"></div>
         </div>
       }
</Card.Body>
    </Accordion.Collapse>
      
                      <div className="clearfix"></div>
          
                  </div>

                  </Card>
                </Accordion>    
       
      })
  }
                  </div>
                  </div>
                  {this.state.reviewShow && (
          <Lightbox
            mainSrc={this.dataimageObject[this.state.photoIndex]}
            nextSrc={this.dataimageObject[(this.state.photoIndex + 1) % this.dataimageObject.length]}
            prevSrc={this.dataimageObject[(this.state.photoIndex + this.dataimageObject.length - 1) % this.dataimageObject.length]}
            onCloseRequest={() => this.setState({ reviewShow: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (this.state.photoIndex + this.dataimageObject.length - 1) % this.dataimageObject.length,
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (this.state.photoIndex + 1) % this.dataimageObject.length,
              })
            }
          />
        )}
                  
                  <DocumentViewer
        show={this.state.docView}
        onHide={docViewClose}
        dataobject={this.datadocObject}
      />
                {/* <Imageslider/> */}
                 <ReviewRequest
        show={this.state.audioShow}
        onHide={requestModalClose}
       dataobject={this.dataaudioObject}
      />  
            </div>
            
    
          );
      }
    }
    const mapStateToProps = state=>{
     
      return{
          loader : state.loaderReducer.loader,
          expList: JSON.parse(state.authReducer.expList),
          text : state.mediaReducer.text
        }
    }
    
    const mapDispatchFromProps = dispatch=>{
      return{
        fetchAuth : () =>dispatch(manageAuth()),
          startLoader : () =>dispatch(showLoader()),
          hideLoader : () =>dispatch(hideLoader())
      }
    }
    
    export default connect(mapStateToProps,mapDispatchFromProps)(Consulatation);

   