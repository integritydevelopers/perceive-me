import React from 'react';
import social1 from '../../assets/images/social-icon1.png';
import social2 from '../../assets/images/social-icon2.png';
import social3 from '../../assets/images/social-icon3.png';
import social4 from '../../assets/images/social-icon4.png';
import logo2 from '../../assets/images/logo.png'

class Footer extends React.Component{
    render(){
        return(
            <section id="main-footer">
<div className="custom-container">
<img src={logo2}/>

<div className="social-icon">
<ul>
<li><a href="#"><img src={social1}/></a></li>
<li><a href="#"><img src={social2}/></a></li>
<li><a href="#"><img src={social3}/></a></li>
<li><a href="#"><img src={social4}/></a></li>
</ul>
</div>

<div className="footer-menu">
<ul>
<li><a href="#">Home</a></li>
<li><a href="#">how we help</a></li>
<li><a href="#">experts</a></li>
<li><a href="#">Testimonials</a></li>
<li><a href="#" className="linehide">Articles</a></li>
</ul>
</div>

<p>© 2020 Perceiveme.ai . All Rights Reserved | Design by - inwebinfo</p>

</div>
</section>
        )
    }
}

export default Footer;