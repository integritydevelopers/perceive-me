import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { toast } from 'react-toastify';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import { connect } from 'react-redux';
import { editWork } from '../../providers/auth';

class Editemployment extends React.Component{

    constructor(props){
        super(props);
       this.state = {
        ModalShow : false,
        CompanyName : '',
        companyId: '',
        Location : '',
        role : '',
        Url : '',
        empType : '',
        empStartDate : '',
        empEndDate : '',
        empDescription : '',
       }
    }
    handleCompanyInputChange = (e) =>{
      this.setState({[e.target.name]:e.target.value})
  }
  componentWillReceiveProps(nextProps){
    
    if(nextProps.show)
    {
      this.setState({ ModalShow:true});
    }
    else
    {
      this.setState({ ModalShow:false});
    }
    this.setState({
      CompanyName : nextProps.empobject.CompanyName,
      companyId : nextProps.empobject.companyId,
      Location : nextProps.empobject.Location,
      role : nextProps.empobject.role,
      Url : nextProps.empobject.Url,
      empStartDate : nextProps.empobject.empStartDate,
      empEndDate : nextProps.empobject.empEndDate,
      empDescription : nextProps.empobject.empDescription,
      empType:nextProps.empobject.type
    })
  }

  editCompany = () =>{
    if(this.state.CompanyName !=='' && this.state.empStartDate !=='' && this.state.role!=='' && this.state.empType!=='emptype' && this.state.empType!==''&& this.state.empType!==null){
        this.props.startLoader();
        editWork(this.state.companyId,this.state.CompanyName,this.state.Location,this.state.role,this.state.Url,this.state.empType,this.state.empStartDate,this.state.empEndDate,this.state.empDescription).then((res)=>{
            this.props.hideLoader();
            if(res.data.status){
              this.setState({ ModalShow:false});
            toast.success(res.data.msg,{autoClose:2000});
             
           
            }
            else{
              this.setState({ ModalShow:false});
                toast.error(res.data.msg,{autoClose:2000});
            }
            window.location.reload();
        })
    }else{
        toast.error('Please fill all the feilds.',{})
    }
   
}

    render(props){
        return(
            <Modal
            {...this.props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            show={this.state.ModalShow}
            onHide={this.props.onHide}
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Edit Employment History
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <div className="form-group">
                 <input type="text" className="form-control" name="CompanyName" placeholder="Company Name" defaultValue={this.state.CompanyName} onChange={this.handleCompanyInputChange}/>
                 </div>
                
              <div className="form-group">
                 <input type="text" className="form-control" name="Location" placeholder="Company location" defaultValue={this.state.Location} onChange={this.handleCompanyInputChange}/>
                 </div>
                 <div className="form-group">
                 <input type="text" className="form-control" name="role" placeholder="Job Role" defaultValue={this.state.role} onChange={this.handleCompanyInputChange}/>
                 </div> 
                 <div className="form-group">
                 <input type ="text" className="form-control" name="Url" placeholder="Company Website Url" defaultValue={this.state.Url} onChange={this.handleCompanyInputChange}/>
                 </div>
                 <div className="form-group">
                 <select className="form-control"  name="empType" defaultValue={this.state.empType} onChange={this.handleCompanyInputChange}>
                 <option value="emptype">Employee -Type</option>
				<option value="full">Full-Time</option>
				<option value="part_time">Part-Time</option>
				<option value="self_employed">Self-Employed</option>
				<option value="freelance">Freelance</option>
                <option value="internship">Internship</option>
                <option value="trainee">Trainee</option>
				</select>
                </div>
                 <div className="row">
                 <div className="form-group col-md-6">
                 <label>Start Date</label>
                 <input type="date" className="form-control" name="empStartDate" defaultValue={this.state.empStartDate} onChange={this.handleCompanyInputChange}/>
                 </div>
                 <div className="form-group col-md-6">
                 <label>End Date</label>
                 <input type="date" className="form-control" name="empEndDate" defaultValue={this.state.empEndDate} onChange={this.handleCompanyInputChange}/>
                 </div>   
                 </div>
               

                 <div className="form-group">
                 <textarea type ="textarea" className="form-control" name="empDescription"  rows="3" placeholder="decscription" defaultValue={this.state.empDescription} onChange={this.handleCompanyInputChange}/>
                 </div>   
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.props.onHide}>Close</Button>
              <Button  onClick={this.editCompany}>Save</Button>
            </Modal.Footer>
          </Modal>
        );
    }

}
const mapStateToProps = state=>{
  return{
  loader : state.loaderReducer.loader

  }
}


const mapDisptchToProps = dispatch=>{
  return{
    startLoader : () =>dispatch(showLoader()),
    hideLoader : () =>dispatch(hideLoader())
  }
  
}

export default connect(mapStateToProps,mapDisptchToProps)(Editemployment);