import { apiUrl } from '../const';
import axios from 'axios';
import React from 'react';
import {Redirect} from 'react-router-dom';

const publicexpUrl = 'public/exp/nonauth';
export const PublicExp = (id)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicexpUrl,{user_id:id}).then(res=>{
            resolve(res);

        },err=>{reject(err)})
    })
}

const publicworkUrl = 'public/work/non/auth';
export const PublicWork = (id)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicworkUrl,{user_id:id}).then(res=>{
            resolve(res);

        },err=>{reject(err)})
    })
}

const publiceducationUrl = 'public/education/nonauth';
export const PublicEducation = (id)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publiceducationUrl,{user_id:id}).then(res=>{
            resolve(res);

        },err=>{reject(err)})
    })
}

const publicinfoUrl = 'public/info/nonauth';
export const PublicInfo = (id)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicinfoUrl,{user_id:id}).then(res=>{
            resolve(res);

        },err=>{reject(err)})
    })
}

const publicskillUrl = 'public/skills/nonauth';
export const PublicSkill = (id)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicskillUrl,{user_id:id}).then(res=>{
            resolve(res);

        },err=>{reject(err)})
    })
}

const publicjobskillUrl = 'public/jobskills/nonauth';
export const PublicjobSkill = (id)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicjobskillUrl,{user_id:id}).then(res=>{
            resolve(res);

        },err=>{reject(err)})
    })
}

const publicdomainUrl = 'public/domain/nonauth';
export const PublicDomain = (skillid,id)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicdomainUrl,{skill_id:skillid,user_id:id}).then(res=>{
            resolve(res);

        },err=>{reject(err)})
    })
}

const publicfeedbackUrl = 'public/feedback/nonauth';
export const PublicFeedback = (id)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicfeedbackUrl,{user_id:id}).then(res=>{
            resolve(res);

        },err=>{reject(err)})
    })
}

const publicconsultationUrl = 'public/consult/nonauth';
export const PublicConsultation = (id)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicconsultationUrl,{user_id:id}).then(res=>{
            resolve(res);

        },err=>{reject(err)})
    })
}

const publicexpjobUrl = 'public/exp/jobs/nonauth';
export const Publicexpjob = (skillid,id)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicexpjobUrl,{user_id:skillid,user_id:id}).then(res=>{
            resolve(res);

        },err=>{reject(err)})
    })
}

const publicConsultjobUrl = 'public/consult/jobs/nonauth';
export const PublicConsultjob = (skillid,id)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicConsultjobUrl,{user_id:skillid,user_id:id}).then(res=>{
            resolve(res);

        },err=>{reject(err)})
    })
}

const publicConsultdomainUrl = 'public/consult/domain/non/auth';
export const PublicConsultDomain = (skillid,id)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicConsultdomainUrl,{user_id:skillid,user_id:id}).then(res=>{
            resolve(res);

        },err=>{reject(err)})
    })
}