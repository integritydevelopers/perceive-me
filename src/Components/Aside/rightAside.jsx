import React from 'react';
import innerUser2 from '../../assets/images/profile-sec2-right-inner2-user.png';
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import { PieChart } from 'react-minimal-pie-chart';
import { withRouter } from 'react-router-dom';
import mix from '../../assets/images/mix.jpg';
import mix2 from '../../assets/images/mix2.jpg';
import { profilecmplt} from '../../providers/auth';

class RightAside extends React.Component{
    constructor(props) {
        super(props);
        this.state= {
            percentage:'',
        };
        profilecmplt().then((res=>{
            this.setState({ percentage:res.data.data});
            
        }))
        
    }

   

    render(){
       
        return(
            <div className="col-sm-3">
            <div className="profile-sec2-right">
                {this.props.location.pathname == '/profile' &&
                <div className="exbg">
                   
                <CircularProgressbar value={this.state.percentage} text={`${this.state.percentage}%`} />;
                <h2>Complete Your profile</h2>
                <p>It looks like you've missed a step. A complete profile is stronger and will make you stand out. 
                     Finish the steps by clicking the button below.</p>
                </div>
    }
        {this.props.location.pathname == '/experience' &&
           <div>     
       <div className="exbg" >

       {/* <PieChart
  data={[
    { title: 'One', value: 10, color: '#E38627' },
    { title: 'Two', value: 15, color: '#C13C37' },
    { title: 'Three', value: 20, color: '#6A2135' },
    { title: 'four', value: 20, color: '#683627' },
    { title: 'five', value: 20, color: '#542768' },
  ]}
/>; */}
<h3>Actual Experience Mix</h3>
        <img src={mix} className="img-fluid"></img>

      </div>
     <div className="exbg">
     <h3>Recommended Experience Mix</h3>
          <img src={mix2} className="img-fluid"></img>
      </div>
      </div>
    }
     

        
                {/* <div className="profile-sec2-right-inner">
                <h3>Summery by category</h3>
                    <div className="sec2-right-item">
                    <p>Have you ever made a <br/>
mistake? <span>10</span></p>
                    </div>
                    
                    <div className="sec2-right-item">
                    <p className="backcolorchangeitem">Have you ever made a <br/>
mistake? <span>5</span></p>
                    </div>
                    
                    <div className="sec2-right-item">
                    <p className="backcolorchangeitem">Have you ever made a <br/>
mistake? <span>5</span></p>
                    </div>
                    
                    <div className="sec2-right-item">
                    <p className="backcolorchangeitem">Have you ever made a <br/>
mistake? <span>5</span></p>
                    </div>
                    
                    <div className="sec2-right-item">
                    <p className="backcolorchangeitem">Have you ever made a <br/>
mistake? <span>5</span></p>
                    </div>
                    
                    <div className="sec2-right-item backcolorchangeitem">
                    <p className="backcolorchangeitem">Have you ever made a <br/>
mistake? <span>5</span></p>
                    </div>
                    
                    
                </div>
                <div className="profile-sec2-right-inner2">
                <h3>Skill Test By Expert</h3>
                    <div className="profile-sec2-right-inner2-inner">
                   <div className="profimgarea">
                        <img src={innerUser2}/>
                       <span>4.2</span>
                        </div>
                        <div className="proftextarea">
                        <p>Dipak Das</p>
                            <span>Php Devoloper<br/>
6-7 years Experience </span>
                        </div>
                        <div className="profrsarea">
                        <p>$80 <span>/hr</span></p>
                        </div>
                    </div>
                    <div className="profile-sec2-right-inner2-inner bggry">
                   <div className="profimgarea">
                        <img src={innerUser2}/>
                       <span>4.2</span>
                        </div>
                        <div className="proftextarea">
                        <p>Palash Saha</p>
                            <span>App Devoloper<br/>
3-4 years Experience </span>
                        </div>
                        <div className="profrsarea">
                        <p>$150 <span>/hr</span></p>
                        </div>
                    </div>
                    
                     <div className="profile-sec2-right-inner2-inner bggry">
                   <div className="profimgarea">
                        <img src={innerUser2}/>
                       <span>4.2</span>
                        </div>
                        <div className="proftextarea">
                        <p>Amit Yadav</p>
                            <span>java Devoloper<br/>
3-4 years Experience</span>
                        </div>
                        <div className="profrsarea">
                        <p>$50 <span>/hr</span></p>
                        </div>
                    </div>
                    
                       <div className="profile-sec2-right-inner2-inner bggry">
                   <div className="profimgarea">
                        <img src={innerUser2}/>
                       <span>4.2</span>
                        </div>
                        <div className="proftextarea">
                        <p>Raj Kumar Paul</p>
                            <span>Html & CSS & Photoshop<br/>
6-7 years Experience</span>
                        </div>
                        <div className="profrsarea">
                        <p>$100 <span>/hr</span></p>
                        </div>
                    </div>
                    
                    
                    
                    
                    
                    
                       <div className="profile-sec2-right-inner2-inner bggry">
                   <div className="profimgarea">
                        <img src={innerUser2}/>
                       <span>4.2</span>
                        </div>
                        <div className="proftextarea">
                        <p>Rjiv Singhoroy</p>
                            <span>Python<br/>
4-5 years Experience</span>
                        </div>
                        <div className="profrsarea">
                        <p>$100 <span>/hr</span></p>
                        </div>
                    </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                </div>
                 */}
                
              
                
                
                </div>
            </div>
        );
    }
}

export default withRouter(RightAside);