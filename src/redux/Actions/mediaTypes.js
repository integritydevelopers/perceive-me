export default{
    fetchText : 'FETCH_TEXT',
    saveText : 'SAVE_TEXT',
    fetchVideoData : 'FETCH_VIDEO_DATA',
    saveVideoData : 'SAVE_VIDEO_DATA',
    emptyVideoData:'EMPTY_VIDEO_DATA'
}