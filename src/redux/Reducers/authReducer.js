import actionTypes from '../Actions/actionTypes';

const initialState = {authData:JSON.parse(localStorage.getItem('authData')),authToken:localStorage.getItem('authToken'),loginStatus:false,profileImage : localStorage.getItem('user_cover'),expList : localStorage.getItem('explist')};

export function authReducer(state=initialState,action){
    // console.log(action);
    switch(action.type){       
        case actionTypes.manageAuth : return{
            ...state,
            authData : JSON.parse(localStorage.getItem('authData')),
            authToken : localStorage.getItem('authToken'),
            loginStatus : true,
            profileImage : localStorage.getItem('profilePic'),
            expList : localStorage.getItem('explist')
        }
        case actionTypes.removeAuth : return{
            ...state,
            authData : {},
            authToken : {},
            loginStatus : false
        }
        case actionTypes.updateAuth : return{
            ...state,
            profileImage : action.value
        }
        default : return{
            ...state
        }
    }

}