import React from 'react';

import cross from '../../assets/images/cros.png';
import userin from '../../assets/images/userns.png';
import loader from '../../assets/images/loding.png';
import experience from '../../assets/images/exprience.jpg';
import AddMyGoals from '../../Components/Modal/addMyGoals';
import EditGoals from '../../Components/Modal/EditGoals';
import { GoalList,deleteGoal } from '../../providers/auth';
import moment, { calendarFormat } from 'moment';
import { Dropdown } from 'react-bootstrap';
import Speech from 'react-speech';
import { connect } from 'react-redux';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import { confirmAlert } from 'react-confirm-alert';
import { toast } from 'react-toastify';
import { Button } from 'react-bootstrap';

class Mygoals extends React.Component{
  constructor(props) {
    super(props);

    this.state={
      addgoalpage: false,
      editgoal :false,
      comname:[],
      goallist:[]
    }
  }

  componentDidMount(){

    this.props.startLoader();
    
    GoalList().then(res=>{
      console.log(res.data);
      this.setState({
        goallist:res['data']
      
      });
      console.log(this.state.goallist);

      this.props.hideLoader();
    })
  

  }
  addGoal = e => event =>{
    this.setState({addgoalpage : true})
  }

  editgoalData = e => event =>{
    this.setState({editgoal : true})
    console.log(e);
    e.company.forEach(el=>{
     this.state.comname.push(el.goal_company);
    })
     this.dataObject = {"goal_id":e.user_goal_id,"end_date":e.start_date,"title":e.goal_title,"goal_desc":e.goals_des,"skill":e.skills,"cmpname":this.state.comname}
  
    }

    deletegoal = (id)=>{
      confirmAlert({
          title: 'Confirm to Delete',
          message: 'Are you sure to do this.',
          buttons: [
            {
              label: 'Yes',
              onClick: () => {
                  this.props.startLoader();
                  deleteGoal(id).then(res=>{
                      this.props.hideLoader();
                      if(res.data.status){
                          toast.success(res.data.msg,{autoClose:5000});
                          window.location.reload();
                      }else{
                          toast.error(res.data.msg,{autoClose:5000});
                          window.location.reload();
                      }
                      GoalList().then(res=>{
                          this.setState({worklist:res.data.data});
                     })
                  })
              }
            },
            {
              label: 'No',
              onClick: () => console.log("not deleted")
            }
          ]
        });
  }

  skillcheck = e => event => {
    console.log(e);

    this.props.history.push({
      pathname: '/skills',
      data: e
    })
}

    render() {
      let addgoalclose = () =>{this.setState({addgoalpage:false})}
      let editModalClose = () => { this.setState({ editgoal: false }) }
       return (
        <div className="col-sm-6">
        <div className="d-flex justify-content-between">
            <div className="listview">
                <label className="switch">
                <input type="checkbox"/>
                <span className="slider round"></span>
                </label>
                List View
                </div>
            <a href="#"><i className="fa fa-search"></i></a>
        
        </div>
        <div className="profile-sec2-middle">
        <div className="profile-sec2-middle-inne">
            <div className="exbg d-flex justify-content-between">
            <div className="exptext">
            <img src={this.props.profilePic} className="img-fluid" onError={(e)=>{e.target.onerror = null; e.target.src=userin}}/>
            <span>Add My Goals</span>
            </div>
            <div className="adexp">
                <a href="#"  onClick={this.addGoal()}><i className="fa fa-plus-circle"></i></a>
            </div>
            
            </div>
          
{
  this.state.goallist.map((e, key)=>{

    return  <div key={key}  className="exbg exprience raskillp">
            <div className="d-flex justify-content-between mygoal">
                <div className="extitle">
                  <Speech   textAsButton={true}    
                   displayText={<h2><i className="fa fa-volume-up"></i>{e.goal_title}</h2>}
                   text={e.goal_title} voice="Google UK English Female"></Speech>
               {/* <h2><i className="fa fa-volume-up"></i>{e.goal_title}</h2> */}
               <a href="#">Target Date : <span>{moment(e.start_date).format('LL')} </span></a>
                <span className="domain">Skill :</span>
                {e.skills && e.skills.map((jc,jcb)=>{
					return	<span key={jcb} className="raskil">  {jc.name }  |</span> 
       })} 
                 <p>{e.goals_des} </p>
                </div>
                <div className="exoption d-flex">
                <Dropdown>
  <Dropdown.Toggle variant="Warning"  id="dropdown-basic">
  <a ><i className="fa fa-ellipsis-v"></i></a>
  </Dropdown.Toggle>

  <Dropdown.Menu>
    <Dropdown.Item onClick={this.editgoalData(e)} >Edit</Dropdown.Item>
    <Dropdown.Item onClick={() =>{this.deletegoal(e.user_goal_id)}}>Delete</Dropdown.Item>
  </Dropdown.Menu>
</Dropdown>
               

                </div>
            </div>
           
            <div className="clearfix"></div>
            <Button type="submit" onClick={this.skillcheck(e)} className="rqstr rskillb"><span className="text">Check skills</span><i className="fa fa-graduation-cap"></i></Button>
        </div>
        })
      }
    
        </div>
        </div>


        <AddMyGoals
        show={this.state.addgoalpage}
        onHide={addgoalclose}
        
      />
      <EditGoals
        show={this.state.editgoal}
        onHide={editModalClose}
        dataobject={this.dataObject}
        
      />
    </div>

       );
    }
 }

 const mapStateToProps = state=>{
  return{
  loader : state.loaderReducer.loader,
  profilePic : state.authReducer.profileImage

  }
}
const mapDisptchToProps = dispatch=>{
  return{
    startLoader : () =>dispatch(showLoader()),
    hideLoader : () =>dispatch(hideLoader())
  }
  
}
export default connect(mapStateToProps,mapDisptchToProps)(Mygoals);


 