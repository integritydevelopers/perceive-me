import React from 'react';
import bannerImage from '../../assets/images/banner-img.png';
import bannerbuttomImage from '../../assets/images/banner-img-bottom.png';

class Banner extends React.Component{
    render(){
        return(
            <section id="banner-area">
                <div className="custom-container">
                <h1>Hi there! </h1>
                <h6>Did you know that 60% of human perception is based on your body language, 30% depends on your vocal tone and only 10% comes from the actual words spoken? Ever wondered how others perceive you at work, at home or during an interview? Sign up to be a part of a growing list of professionals that are taking advantage of a community to learn and improve their interpersonal skills"</h6>
                <img src={bannerImage} className="banner-img"/>
                <img src={bannerbuttomImage} className="bottom-banner-img"/>
                </div>
            </section>
        )
    }
}

export default Banner;