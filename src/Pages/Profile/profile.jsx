import React, {useState} from 'react';
import 'react-accessible-accordion/dist/fancy-example.css';
import cover from '../../assets/images/profile_cover.jpg';
import user from '../../assets/images/userns.png';
import { manageAuth,updateAuth } from '../../redux/Actions/authAction';
import { connect } from 'react-redux';
import { updateProfile,getProfilePic,getEducationList,workList,coverPicSave,getCoverPic,profilePicSave,saveWork,saveEducation,educationType,editEducation,deleteWork,deleteEdu } from '../../providers/auth';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import { toast } from 'react-toastify';
import Editprofile from '../../Components/Modal/Editprofile';
import moment from 'moment';
import { Dropdown } from 'react-bootstrap';
import { ImageUrl } from '../../const';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import Editemployment from '../../Components/Modal/editemployment';
import fileUpload from '../../Components/Aws/aws';

class Profile extends React.Component{
    eduObject = {}
    empObject = {}
    constructor(props) {
        super(props);
        this.profilestep=localStorage.getItem('step');
        this.state= {
            showForm: false,
            showForm2:false,
            showprofile :false,
            fname : this.props.auth.user_fname,
            lname : this.props.auth.user_lname,
            phone : this.props.auth.user_phno,
            address : this.props.auth.user_address,
            gender: this.props.auth.user_gender,
            dob:this.props.auth.user_dob,
            fb: this.props.auth.user_facebook,
            twiter: this.props.auth.user_twitter,
            linkedin: this.props.auth.user_linkedin,
            userImage:'',
            coverImage:'',
            worklist:[],
            educationlist:[],
            base64:'',
            errors : {"fname":'',"lname" : ''},
            companyName : '',
            companyId: '',
            companyLocation : '',
            jobRole : '',
            companyWebsite : '',
            empType : 'emptype',
            startDate : '',
            endDate : '',
            description : '',
            schoolName : '',
            degree : '',
            feildStudy : '',
            grade : '',
            eduStartDate:'',
            eduEndDate:'',
            activities:'',
            eduDescription : '',
            educationTypes:[],
            eduType:'edutype',
            editStatus : false,
            eduEditStatus : false,
            eduId : '',
            workModalShow : false,
            EmpworkModalShow : false,
        
            //company : {companyName:'',companyLocation:'',jobRole:'',companyWebsite:'',empType:'',startDate:'',endDate:'',description:''}
            
        };
       
        
        this.props.startLoader();
        getProfilePic().then(res=>{
            if(res.data.data.user_image !=null){
                localStorage.setItem('profilePic',res.data.data.user_image);
             this.setState({userImage:res.data.data.user_image});
            }
            
        })
        getCoverPic().then(res=>{
            console.log(res.data.data);
            if(res.data.data.user_cover !=null){
                console.log("here")
                localStorage.setItem('coverPic',res.data.data.user_cover );
             this.setState({coverImage: res.data.data.user_cover});
             this.props.hideLoader();
            }
           
            
            
        })
        
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleForm2 = this.toggleForm2.bind(this);
        this.toggleprofile = this.toggleprofile.bind(this);
        
    }

    componentDidMount(){
        this.props.startLoader();
        workList().then(res=>{
            this.setState({worklist:res.data.data});
       })
       getEducationList().then(res=>{
            this.setState({educationlist:res.data.data});
            console.log(this.state.educationlist);
           
       })
       educationType().then(res=>{
          
           this.setState({educationTypes:res.data.data});
       })
       this.props.hideLoader();
    }


    toggleForm() {
    
        const currentState=this.state.showForm;
        this.setState({ showForm: !currentState});
     }

     toggleForm2() {

        const currentState=this.state.showForm2;
        this.setState({ showForm2: !currentState});
     }


     toggleprofile() {

        const currentState=this.state.showprofile;
        this.setState({ showprofile: !currentState});
     }

     handlefnameChange = (e) =>{
        if(e.target.value.length<1 || e.target.value ==''){
            this.state.errors.fname = "First Name Should not be Blank";
         }else{
            this.state.errors.fname = "";
         }
        this.setState({fname: e.target.value});
     }
     handlelnameChange = (e) =>{
        
        if(e.target.value.length<1 || e.target.value ==''){
            this.state.errors.lname = "Last Name Should not be Blank";
         }else{
            this.state.errors.lname = "";
         }
        this.setState({lname: e.target.value});
    }
    handlephoneChange = (e) =>{
        
        this.setState({"phone":e.target.value});
    }
    handleaddressChange = (e) =>{
        
        this.setState({"address":e.target.value});
    }
    handlegenderChange = (e) =>{
        
        this.setState({"gender":e.target.value});
    }
    handledobChange = (e) =>{
        
        this.setState({"dob":e.target.value});
       
    }
    handlefbChange = (e) =>{
        
        this.setState({"fb":e.target.value});
    }
    handlelinkedinChange = (e) =>{
        
        this.setState({"linkedin":e.target.value});
    }
    handletwitterChange = (e) =>{
        this.setState({"twitter":e.target.value});
    }
    handleCompanyInputChange = (e) =>{
        this.setState({[e.target.name]:e.target.value})
    }
    handleCompanyInputTypeChange=(e)=>{
        this.setState({"empType":e.target.value})
    }
    handleEducationInputChange = (e) =>{
        this.setState({[e.target.name]:e.target.value})
    }
    saveCompany = ()=>{
        if(this.state.companyName !=='' && this.state.jobRole !=='' &&  this.state.startDate !=='' && this.state.empType !=='emptype'){
            this.props.startLoader();
            saveWork(this.state.companyName,this.state.companyLocation,this.state.jobRole,this.state.companyWebsite,this.state.empType,this.state.startDate,this.state.endDate,this.state.description).then((res)=>{
                
                this.props.hideLoader();
                if(res.data.status){
                    this.toggleForm();
                    this.setState(prevState => ({
                        worklist: [...prevState.worklist, res.data.data]
                      }))
                toast.success(res.data.msg)
                }
                else{
                    toast.error(res.data.msg);
                }
            })
        }else{
            toast.error('Please fill all the feilds.',{})
        }
    }
    saveEducation = ()=>{
      
        if(this.state.schoolName !== '' && this.state.degree !==''  && this.state.feildStudy !==''  && this.state.eduType !=='edutype' && this.state.eduStartDate !=='' && this.state.eduEndDate ){
            this.props.startLoader();
            saveEducation(this.state.schoolName,this.state.degree,this.state.feildStudy,this.state.grade,this.state.eduStartDate,this.state.eduEndDate,this.state.activities,this.state.eduDescription,this.state.eduType).then((res)=>{
                this.props.hideLoader();
                if(res.data.status){
                    this.toggleForm2();
                    this.setState(prevState => ({
                        educationlist: [...prevState.educationlist, res.data.data]
                      }))
                toast.success(res.data.msg)
                }
                else{
                    toast.error(res.data.msg);
                }
            })
        }else{
            toast.error('Please fill all the feilds.',{})
        
        }
    }
    EditEducation = ()=>{
        if(this.state.schoolName !== '' && this.state.eduStartDate !=='' && this.state.eduEndDate){
            this.props.startLoader();
            editEducation(this.state.eduId,this.state.schoolName,this.state.degree,this.state.feildStudy,this.state.grade,this.eduStartDate,this.state.eduEndDate,this.state.activities,this.state.eduDescription,this.state.eduType).then((res)=>{
                this.props.hideLoader();
                if(res.data.status){
                    toast.success(res.data.msg)
                    //window.location.reload();
                }
              
                else{
                    toast.error(res.data.msg);
                }
            })
        }else{
            toast.error('Please fill all the feilds.',{})
        
        }
    }
    editEduData = edu => event =>{
        this.setState({workModalShow : true})
        this.eduObject = {"eduId":edu.user_eprf_id,"schoolName":edu.user_eprf_value,"degree":edu.user_eprf_degree,"feildStudy":edu.user_eprf_fieldofstudy,"grade":edu.user_eprf_grade,"eduStartDate":edu.start_date,"eduEndDate":edu.end_date,"eduDescription":edu.user_eprf_description,"activities":edu.user_eprf_activities,"eduType":edu.pf_name,"eduTypeId":edu.user_epf}

    }
    editData = work => event =>{
        this.setState({EmpworkModalShow : true})
        this.empObject ={'companyId':work.user_prf_id,'CompanyName':work.user_prf_value,"Location":work.user_prf_location,"role":work.user_prf_role,
        "Url":work.user_prf_website,"empStartDate":work.start_date,"empEndDate":work.end_date,"empDescription":work.user_prf_description,"type":work.user_prf_type}
        this.setState({editStatus:true});
        
    }
    
   

    updateData = () =>{
        if(this.state.phone !='' && this.state.address != ''){
        this.props.startLoader();
        updateProfile(this.state.fname,this.state.lname,this.state.phone,this.state.address,
            this.state.gender,this.state.dob,this.state.fb,this.state.linkedin,this.state.twitter).then((res)=>{
               this.props.hideLoader();
               this.toggleprofile();
            if(res.data.status){
                
                localStorage.setItem('authData',JSON.stringify(res.data.data));
               
                toast.success(res.data.msg,{autoClose:2000});
            }else{
                toast.error(res.data.msg,{autoClose:2000});
            }
            window.location.reload();
                
            });   
            
        }else{
            this.props.hideLoader();
             toast.error('Please fill mindtory fields')
           }
        }

        onFileChange = (e)=>{
            this.props.startLoader();
            e.preventDefault();
            let file = e.target.files[0];
            fileUpload(file,'cover').then((awsLink)=>{
            coverPicSave(awsLink).then((res=>{
                    this.props.hideLoader();
                    this.setState({coverImage:awsLink});
                    localStorage.setItem('coverPic',awsLink);
                    
                        toast.success('Cover image updated successfully');
                        // window.location.reload();
                    
                }))
            })
           
        }
        profilePicChange = (e)=>{
            this.props.startLoader();
            e.preventDefault();
            let file = e.target.files[0];
            fileUpload(file,'profile').then((awsLink)=>{
            // let reader = new FileReader();
            // reader.readAsDataURL(file)
            // reader.onloadend = () => {
                profilePicSave(awsLink).then((res=>{
                        this.props.hideLoader();
                        localStorage.setItem('profilePic',awsLink );
                        this.setState({userImage:awsLink});
                        toast.success('Profile image updated successfully');
                         window.location.reload();
                }))
            })
            // };
        }
       

        deleteWorkRow = (id)=>{
            confirmAlert({
                title: 'Confirm to Delete',
                message: 'Are you sure to do this.',
                buttons: [
                  {
                    label: 'Yes',
                    onClick: () => {
                        this.props.startLoader();
                        deleteWork(id).then(res=>{
                            this.props.hideLoader();
                            if(res.data.status){
                                toast.success(res.data.msg,{autoClose:5000});
                               
                            }else{
                                toast.error(res.data.msg,{autoClose:5000});
                            }
                            workList().then(res=>{
                                this.setState({worklist:res.data.data});
                           })
                        })
                    }
                  },
                  {
                    label: 'No',
                    onClick: () => console.log("not deleted")
                  }
                ]
              });
        }
        deleteEdu = (id) =>{
            
            confirmAlert({
                title: 'Confirm to Delete',
                message: 'Are you sure to do this.',
                buttons: [
                  {
                    label: 'Yes',
                    onClick: () => {
                        this.props.startLoader();
                        deleteEdu(id).then(res=>{
                            this.props.hideLoader();
                            if(res.data.status){
                                toast.success(res.data.msg,{autoClose:5000});
                               
                            }else{
                                toast.error(res.data.msg,{autoClose:5000});
                            }
                            getEducationList().then(res=>{
                                this.setState({educationlist:res.data.data});
                               
                           })
                        })
                    }
                  },
                  {
                    label: 'No',
                    
                  }
                ]
              });
        }
        
    render(){
        
        let workModalClose = () =>{this.setState({workModalShow:false})}
        let EmpworkModalClose = () =>{this.setState({EmpworkModalShow:false})}

        return(
            <div className="col-sm-6">
                <div className="profile-sec2-middle">
                    {/* <h3>My Profile</h3> */}
                    <div className="exbg">
                    {/* Personal Information  */}
                   
                    <div className="profilepic">

                        <div className="coverpic">
                            <img className="img-fluid" src={this.state.coverImage} onError={(e)=>{e.target.onerror = null; e.target.src=cover}} ></img>
                            <a onClick={(e) => this.upload.click() } onChange={this.onFileChange} className="camera"><i className="fa fa-camera">
                           
                            <input id="myInput" type="file" ref={(ref) => this.upload = ref} style={{ display: 'none' }} />
                                </i></a>
                        </div>
                        <div className="userpic mx-auto d-block">
                            <img className="img-fluid"  src={this.state.userImage} onError={(e)=>{e.target.onerror = null; e.target.src=user}}></img>
                            <a onClick={(e) => this.uploadpic.click() } onChange={this.profilePicChange} className="camera"><i className="fa fa-camera">
                            <input id="myprofileInput" type="file" ref={(ref) => this.uploadpic = ref} style={{ display: 'none' }} />
                            </i></a>

                        </div>
                    </div>
                    { 
     this.state.showprofile !==false &&
                <div>
                 <div className="row rapfl">
                 <div className="form-group col-md-6">
                 <input type="text" className="form-control" placeholder="First name" defaultValue={this.props.auth.user_fname} onChange={this.handlefnameChange}/>
                 <p style={{color:"red",fontSize:"14px"}}>{this.state.errors.fname}</p>
                 </div>
                
                 <div className="form-group col-md-6">
                 <input type="text" className="form-control" placeholder="Last name" defaultValue={this.props.auth.user_lname} onChange={this.handlelnameChange}/>
                 <p style={{color:"red",fontSize:"14px"}}>{this.state.errors.lname}</p>
                 </div>   
                 
                 </div>
                
                 <div className="form-group">
                 <input type="text" className="form-control" placeholder="Phone no" defaultValue={this.props.auth.user_phno} onChange={this.handlephoneChange}/>
                 </div>
                 <div className="form-group">
                 <input type="text" className="form-control" placeholder="Address" defaultValue={this.props.auth.user_address} onChange={this.handleaddressChange} />
                 </div>
                 <div className="row">
                 <div className="form-group col-md-6">
                     <p>Gender</p>
                 <select className="form-control"  name="sellist1" defaultValue={this.props.auth.user_gender} onChange={this.handlegenderChange}>
				<option value=''>Gender</option>
				<option value='Male'>Male</option>
				<option value='Female'>Female</option>
				<option value='others'>Others</option>
				</select>
                 </div>
                 <div className="form-group col-md-6">
                     <p>Date of Birth</p>
                 <input type="date" className="form-control" name = "dob"  placeholder="Date of Birth" defaultValue= {moment(this.props.auth.user_dob).format('YYYY-MM-DD')} onChange={this.handledobChange}/>
                 </div>   
                 </div>
                 {/* <div className="form-group">
                 <input type="text" className="form-control" placeholder="Password"/>
                 </div> */}
                 <div className="form-group">
                 <input type="text" className="form-control" placeholder="Facebook url" defaultValue={this.props.auth.user_facebook} onChange={this.handlefbChange}/>
                 </div>
                 <div className="form-group">
                 <input type="text" className="form-control" placeholder="Twitter url" defaultValue={this.props.auth.user_twitter} onChange={this.handletwitterChange}/>
                 </div>

                 <div className="form-group">
                 <input type="text" className="form-control" placeholder="Linkedin url" defaultValue={this.props.auth.user_linkedin} onChange={this.handlelinkedinChange}/>
                 </div>
             
                 <div className="form-group text-right">
                 <button className="cancel" onClick={this.toggleprofile}  >cancel</button>
                     <button className="cancel submit"  onClick={this.updateData} >Update</button>
                 </div>
                    
         
         <div className="clearfix"></div>

         </div>
          }

{ this.state.showprofile === false &&
             <div>
             <div className="wrklist" >
 <div className="col-sm-12 userpfl">   
 <a className="edit" >
             <i className="fa fa-pencil" onClick={this.toggleprofile} ></i>
                    </a>         
 <h2>{this.props.auth.user_fname} {this.props.auth.user_lname}</h2>
<div className="row">
<div className="col-sm-8 userpf">
    {this.props.auth.user_phno != "" &&  this.props.auth.user_phno != null &&
                <p><i className="fa fa-phone"></i> {this.props.auth.user_phno}</p>}
                  {this.props.auth.user_address != "" &&  this.props.auth.user_address != null &&
                <p><i className="fa fa-map-marker"></i> {this.props.auth.user_address}</p>}
                  {this.props.auth.user_gender!= "" &&  this.props.auth.user_gender != null &&
                <p><i className="fa fa-transgender"></i> {this.props.auth.user_gender}</p>}
                  {this.props.auth.user_dob != "" &&  this.props.auth.user_dob != null &&
                <p><i className="fa fa-calendar"></i> {moment(this.props.auth.user_dob).format('LL')}</p>}
               
               
</div>
<div className="col-sm-4 userpf text-sm-right">
                <a href="#"><i className="fa fa-facebook"></i></a>
                <a href="#"><i className="fa fa-twitter"></i></a>
                <a href="#"><i className="fa fa-linkedin"></i></a>
</div>
    </div>
    </div>
                {/* <h2>{this.props.auth.user_fname}{this.props.auth.user_lname}</h2>
                <p>{this.props.auth.user_phno}</p>
                <p>{this.props.auth.user_address}</p>
                <p>{this.props.auth.user_gender}</p>
                <p>{this.props.auth.user_dob}</p>
                <p>{this.props.auth.user_facebook}</p>
                <p>{this.props.auth.user_twitter}</p>
                <p>{this.props.auth.user_linkedin}</p> */}
             </div>
             
                        
             </div>
    }
         
 </div>
   
    
 
         {/* Employment History */}
 
           
                <div className="exbg awke mt-4 mb-4">
                   
                    <div className="d-flex justify-content-between">
                        
                         <h3 className="mb-0">Employment History</h3>
                        
                            <a href="javascript:void(0)" onClick={this.toggleForm}>
                                <i className="fa fa-plus"></i>
                            </a>
                        
                    </div>

                      { this.state.showForm &&
                 <div className="exbg non-exbg">
                <div className="form-group">
                 <input type="text" className="form-control" placeholder="Company Name" name="companyName"  onChange={this.handleCompanyInputChange}/>
                 </div>
                 <div className="form-group">
                 <input type="text" className="form-control" placeholder="Company Location" name="companyLocation"  onChange={this.handleCompanyInputChange}/>
                 </div>
                 <div className="form-group">
                 <input type="text" className="form-control" name="jobRole" placeholder="Job Role" onChange={this.handleCompanyInputChange}/>
                 </div>
                 <div className="form-group">
                 <input type ="text" className="form-control" name="companyWebsite" placeholder="Company Website Url"  onChange={this.handleCompanyInputChange}/>
                 </div>
                 <div className="form-group">
                 <select className="form-control"  name="empType"  onChange={this.handleCompanyInputTypeChange}>
                 <option value="emptype">Employee -Type</option>
				<option value="full">Full-Time</option>
				<option value="part_time">Part-Time</option>
				<option value="self_employed">Self-Employed</option>
				<option value="freelance">Freelance</option>
                <option value="internship">Internship</option>
                <option value="trainee">Trainee</option>
				</select>
                </div>
                 <div className="row">
                 <div className="form-group col-md-6">
                 <label>Start Date</label>
                 <input type="date" className="form-control" name="startDate" placeholder="start"  onChange={this.handleCompanyInputChange}/>
                 </div>
                 <div className="form-group col-md-6">
                 <label>End Date</label>
                 <input type="date" className="form-control" name="endDate" placeholder="end"  onChange={this.handleCompanyInputChange}/>
                 </div>   
                 </div>

            
                 <div className="form-group">
                 <textarea type ="textarea" className="form-control" name="description"  rows="3" placeholder="decscription" onChange={this.handleCompanyInputChange}/>
                 </div>

                 <div className="form-group text-right">
                  <button className="cancel submit" onClick={this.saveCompany}>Save</button>
                     
                 </div>
                 <div className="clearfix"></div>         
                 </div>
                    }
                {this.state.worklist.map((work,i) => (
         <div className="exbg d-flex emp non-exbg" key={i}>
        <a className="edit" onClick={this.editData(work)}>
             <i className="fa fa-pencil"></i>
                    </a>
         <a className= "del" onClick={() =>{this.deleteWorkRow(work.user_prf_id)}}>
         <i className="fa fa-trash"  ></i>
                    </a>
             <div className="wrkl">
             <i className="fa fa-briefcase"></i>
             </div>
             <div className="wrklist" >
                <h2>{work.user_prf_role}</h2>
                <p>{work.user_prf_value}</p>
                <span>{moment(work.start_date).format('LL')} to {moment(work.end_date).format('LL')}</span>
             </div>
             
                        
             </div>
               ))}  
                </div>

               
         <div className="clearfix"></div>
         {/* Education */}
        
                <div className="exbg awke mt-4 mb-4">
                    <div className="d-flex justify-content-between">
                        
                         <h3 className="mb-0">Education History</h3>
                        
                            <a href="javascript:void(0)" onClick={this.toggleForm2}>
                                <i className="fa fa-plus"></i>
                            </a>
                        
                    </div>
                    { this.state.showForm2 && 
             <div className="exbg mb-3 non-exbg">
                <div className="form-group">
                 <input type="text" className="form-control" name="schoolName"  placeholder="School/University" onChange={this.handleEducationInputChange}/>
                 </div>
                 <div className="form-group">
                 <select className="form-control"  name="eduType"  onChange={this.handleEducationInputChange}>
                 <option value="edutype">Education -Type</option>
                 {this.state.educationTypes.map((eduType,key)=>(
                     <option value={eduType.pf_id} key={key}>{eduType.pf_name}</option>
                 ))}
				
				
				</select>
                </div>
                 <div className="form-group">
                 <input type="text" className="form-control" name="degree" placeholder="Degree"  onChange={this.handleEducationInputChange}/>
                 </div>
                 <div className="form-group">
                 <input type="text" className="form-control" name="feildStudy"  placeholder="Field of Study" onChange={this.handleEducationInputChange}/>
                 </div> 
                 <div className="form-group">
                 <input type ="text" className="form-control" name="grade"  placeholder="Grade" onChange={this.handleEducationInputChange}/>
                 </div>
                 <div className="row">
                 <div className="form-group col-md-6">
                 <label>Start Date</label>
                 <input type="date" className="form-control" name="eduStartDate"  placeholder="start" onChange={this.handleEducationInputChange}/>
                 </div>
                 <div className="form-group col-md-6">
                 <label>End Date</label>
                 <input type="date" className="form-control" name="eduEndDate" placeholder="end" onChange={this.handleEducationInputChange}/>
                 </div>   
                 </div>
                 <div className="form-group">
                 <textarea type ="textarea" className="form-control" name="activities"  rows="3" placeholder="Activities and societies" onChange={this.handleEducationInputChange}/>
                 </div>

                 <div className="form-group">
                 <textarea type ="textarea" className="form-control" name="eduDescription"  rows="3" placeholder="decscription" onChange={this.handleEducationInputChange}/>
                 </div>

                 <div className="form-group text-right">
                 <button className="cancel submit" onClick={this.saveEducation}>Save</button>
                 </div> 
                 <div className="clearfix"></div>        
                 </div>
    }
                {this.state.educationlist.map((edu,i) => (
         <div className="exbg d-flex emp non-exbg" key={i}>
             
         <a className="edit" onClick={this.editEduData(edu)}>
             <i className="fa fa-pencil"></i>
                    </a>
         <a className= "del" onClick={() =>{this.deleteEdu(edu.user_eprf_id)}}>
         <i className="fa fa-trash"  ></i>
                    </a>
             <div className="wrkl">
             <i className="fa fa-graduation-cap" aria-hidden="true"></i>
             </div>
             <div className="wrklist">
                <h2>{edu.user_eprf_value}</h2>
                <p>{edu.user_eprf_fieldofstudy}</p>
                <span>{moment(edu.start_date).format('LL')} to {moment(edu.end_date).format('LL')}</span>
             </div>
            
                        
             </div>
                ))}
                </div>



         <div className="clearfix"></div>
                   
              
                  
                </div>
                <Editprofile
        show={this.state.workModalShow}
        onHide={workModalClose}
        eduobject={this.eduObject}
      />

        <Editemployment
        show={this.state.EmpworkModalShow}
        onHide={EmpworkModalClose}
        empobject={this.empObject}
      />
            </div>    
            
    
        );
        
    }

    

}

const mapStateToProps = state=>{
    
    return{
    loader : state.loaderReducer.loader,
      auth : state.authReducer.authData
  
    }
  }
  

const mapDisptchToProps = dispatch=>{
    return{
      fetchAuth : () =>dispatch(manageAuth()),
      startLoader : () =>dispatch(showLoader()),
      hideLoader : () =>dispatch(hideLoader()),
      updateProfilePic : (data) =>dispatch(updateAuth(data))
    }
    
  }

export default connect(mapStateToProps,mapDisptchToProps)(Profile);