import React from 'react';
import {Link} from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Carousel from "react-multi-carousel";
import { addExpQuestion, UserTag,expertlist,expertView,addfeedback,constpubpvt } from '../../providers/auth';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import { Range } from 'react-range';
import ReactStars from "react-rating-stars-component";

class Hubinner extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            expertlist : [],
            publicdata:this.props.location,
            values: [50]
        }
 

        expertlist().then(res=>{
         
            console.log(res.data.data);
            this.setState({
              expertlist:res.data.data
            });
             console.log(this.state.expertlist);
    
              });
              
    }

    

    render(){
        const responsive = {
            desktop: {
              breakpoint: { max: 3000, min: 1024 },
              items: 3
            },
        };
       
        console.log(this.state.publicdata.data);

           
        return(
            <>
            <div className="col-sm-6">
            <Tabs defaultActiveKey="get" transition={false} id="noanim-tab-example" className="raconstab" onSelect={(k) => this.setKey(k)}>
                    <Tab eventKey="get" title="Get Consultaion">
                    </Tab>
                    <Tab eventKey="offer" title="Offer Consultaion">
                    </Tab>
                    
                    </Tabs>
            <div className="exbg mt-3">
            <div className="rainnerhub">
                {this.state.publicdata.data=='profile' &&
                
                    <h2>Public Profile Resume</h2>
                }
                {this.state.publicdata.data=='offlineconsult' &&
                 <h2>Get Offline Consultation</h2>

                }
           
            {this.state.publicdata.data=='profile' &&

             <div className="form-group">
             <span class="has-float-label">
            <input class="form-control" id="first" type="text" placeholder="Please enter your public profile URL"/>
            <label for="first">Enter Your Public Profile Link</label>
            </span>
             </div>

            }
             <div className="form-group">
             <span class="has-float-label">
            <input class="form-control" id="first" type="text" placeholder="Select Your Expriences"/>
            <label for="first">Enter Your Expriences</label>
            </span>
             </div>
             <div className="form-group">
             <span class="has-float-label">
            <input class="form-control" id="first" type="text" placeholder="What you wants the advisor to know"/>
            <label for="first">What you wants </label>
            </span>
             </div>
           
             <div className="form-group">
             <span class="has-float-label">
            <input class="form-control" id="first" type="text" placeholder="Select Parameter"/>
            <label for="first">Enter Parameter</label>
            </span>
             </div>
             <div className="owl-theme consult">
               
               <label>Available Experts</label>
           <Carousel responsive={responsive}>
           {this.state.expertlist && this.state.expertlist.map((jc,key)=>
               <div key={key} className="item">
               <div className="sec3-item-bottom">
                 
               <img src={jc.user_image} />
               <div className="sladvisor">
               <a href="#"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></a>
               <span>60 Reviews</span>
               <label>50 coins</label>
               <h4>{jc.user_fname+" "+jc.user_lname}</h4>
               <p>Expertise, Expertise2</p>

               </div>
               

               {/* <h4>{jc.user_fname+" "+jc.user_lname}</h4>
               <h5>{jc.user_email}</h5>
               <p>{jc.company}</p> */}
               {/* <span>{jc.designation}</span> */}
               <button className="cancel submit" >Select</button>

               </div>
               
               </div>
                )} 
             </Carousel>
             </div>
             <div className="float-right mt-4">
             <Link to='/consulatationhub'><Button className="ranewpre" >Back</Button></Link>
             <Button className="ranewnext">Submit</Button>
             </div>
             <div className="clearfix"></div>
            </div>
            </div>
            </div>
            <div className="col-sm-3">
            <div className="exbg rafilteradv">
            <h2>Filters For Advisor</h2>
            <form>
            <div className="form-group">
            <label for="exampleInputEmail1">Search Advisor area based</label>
            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Search"/>
            </div>

            <div className="form-group">
            <label for="exampleInputEmail1">Available Current week</label>
            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Date"/>
            </div>
            <div className="form-group coinbtn">
            <label for="exampleInputEmail1">Charges</label>
            
            {/* <button type="button" class="btn btn-light">10 Coins</button>
            <button type="button" class="btn btn-light">20 Coins</button>
            <button type="button" class="btn btn-light">30 Coins</button>
            <button type="button" class="btn btn-light">40 Coins</button>
            <button type="button" class="btn btn-light">50 Coins</button> */}

<Range
        step={0.1}
        min={0}
        max={100}
        values={this.state.values}
        onChange={(values) => this.setState({ values })}
        renderTrack={({ props, children }) => (
          <div
            {...props}
            style={{
              ...props.style,
              height: '6px',
              width: '100%',
              backgroundColor: '#c64466',
              marginTop: '30px',
              marginBottom: '20px',
            }}
          >
            {children}
            <span className="mt-2 float-left">0</span>
            <span className="mt-2 float-right">100</span>
          </div>
        )}
        renderThumb={({ props }) => (
          <div
            {...props}
            style={{
              ...props.style,
              height: '32px',
              width: '32px',
              backgroundColor: '#ec5b38',
              borderRadius: '50%',
            }}
          />
        )}
      />

      
            </div>
            <div className="form-group star">
            <label for="exampleInputEmail1">Minimum Ratings</label>
            <ReactStars
    count={5}
    size={24}
    isHalf={true}
    emptyIcon={<i className="far fa-star"></i>}
    halfIcon={<i className="fa fa-star-half-alt"></i>}
    fullIcon={<i className="fa fa-star"></i>}
    activeColor="#ffd700"
  />
            {/* <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></a> */}
            </div>
            <div className="form-group">
            <label for="exampleInputEmail1">Language spoken</label>
            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"/>
            </div>
            <div className="form-group">
            <label for="exampleInputEmail1">Search as Company Name</label>
            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"/>
            
            </div>

            </form>
            <div className="clearfix"></div>
            </div>
            </div>
            
            </>
        )
    }
}

export default Hubinner;