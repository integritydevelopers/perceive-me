import AWS from 'aws-sdk';
import { accessKeyId,secretAccessKey,accesUrl} from '../../const';

const fileUpload = (file,uploadType)=>{
    console.log(file);
    const fileName=(new Date()) - Math.floor(Math.random()*10000000000);
    console.log(fileName);
    AWS.config.update({
        accessKeyId: accessKeyId,
        secretAccessKey: secretAccessKey
      })
    const myBucket = new AWS.S3({
        params: { Bucket: 'perceivemebucket'},
        region: 'ap-south-1',
        
      })
      let params={}
      if(uploadType == 'cover'){
         params = {
            ACL: 'public-read',
            Key: 'coverImage/' + file.name,
            ContentType: file.type,
            Body: file,
          }
      }
      else if(uploadType == 'profile'){
         params = {
            ACL: 'public-read',
            Key: 'profileImage/' + file.name,
            ContentType: file.type,
            Body: file,
          }
      }
      else if(uploadType == 'experience'){
        let type=file.type.split('/');
         params = {
            ACL: 'public-read',
            Key: 'experience/' + fileName + '.'+type[1],
            ContentType: file.type,
            Body: file,
          }
      }
      else if(uploadType == 'story'){
        let type=file.type.split('/');
         params = {
            ACL: 'public-read',
            Key: 'experience/' + fileName + '.'+type[1],
            ContentType: file.type,
            Body: file,
          }
      }
      else if(uploadType == 'expWebcam'){
         params = {
            ACL: 'public-read',
            Key: 'experience/' + fileName + '.webm',
            ContentType: 'video/webm',
            Body: file,
          }
      }
      else if(uploadType == 'storyWebcam'){
         params = {
            ACL: 'public-read',
            Key: 'story/' + fileName + '.webm',
            ContentType: 'video/webm',
            Body: file,
          }
      }
      
//       myBucket.managedUpload(params)
// .on('httpUploadProgress', (evt) => {
//     console.log(evt)
//   // that's how you can keep track of your upload progress
// //   this.setState({
// //     progress: Math.round((evt.loaded / evt.total) * 100),
// //   })
// })
// .send((err) => {
//    if (err) {
//        console.log(err);
//      // handle the error here
//    }
// })
     

      return new Promise((resolve,reject)=>{
        const awsReturn = myBucket.putObject(params);
        awsReturn.promise().then((res)=>{
            resolve (accesUrl+params.Key);
        },err=>{reject(err)})
    })
    
    // })
}

export default fileUpload;