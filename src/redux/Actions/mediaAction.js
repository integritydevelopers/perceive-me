import mediaTypes from './mediaTypes';


export function fetchText(){
    return{
        type : mediaTypes.fetchText
    }
}
export function saveText(data){
    return{
        type : mediaTypes.saveText,
        text:data
    }
}
export function fetchVideoData(){
    return{
        type : mediaTypes.fetchVideoData,
        
    }
}
export function saveVideoData(data,blobdata){
    return{
        type : mediaTypes.fetchVideoData,
        videoData:data,
        blobVideoData:blobdata
        
    }
}
export function emptyVideoData(){
    return{
        type : mediaTypes.emptyVideoData,
        
    }
}