import React from 'react';
import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';
import VideocamIcon from '@material-ui/icons/Videocam';
import ImageIcon from '@material-ui/icons/Image';
import MicIcon from '@material-ui/icons/Mic';
import DescriptionIcon from '@material-ui/icons/Description';
import { addExpQuestion, DomainTag, UserTag, associationcmplt, getFilter,Publicpvt,ExpSearch } from '../../providers/auth';
import { AddUserExp, getsorting } from '../../providers/auth';
import { manageAuth, updateAuth } from '../../redux/Actions/authAction';
import { toast } from 'react-toastify';
import { UserPost } from '../../providers/auth';
import { UserlistTag } from '../../providers/auth';
import { connect } from 'react-redux';
import ReactTags from 'react-tag-autocomplete';
import moment, { calendarFormat } from 'moment';
import user from '../../assets/images/userns.png';
import { showLoader, hideLoader } from '../../redux/Actions/loaderAction';
// import Imageslider from '../../Components/Modal/Imageslider';
import Editexperience from '../../Components/Modal/editexperience';
import Dropdown from 'react-bootstrap/Dropdown'
import ShowMoreText from 'react-show-more-text';
import { Button } from 'react-bootstrap';
import ReviewRequest from '../../Components/Modal/ReviewRequest';
//import SpeechRecognition, { useSpeechRecognition } from 'react-speech-recognition';
import 'react-accessible-accordion/dist/fancy-example.css';
import ReactMultiSelectCheckboxes from 'react-multiselect-checkboxes';
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import Addexperiance from '../../Components/Modal/addExperiance';
import DocumentViewer from '../../Components/Modal/documentViewer';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import Speech from 'react-speech';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import ModalVideo from 'react-modal-video';
import 'react-modal-video/css/modal-video.min.css';
import TextFieldsIcon from '@material-ui/icons/TextFields';
import fileUpload from '../../Components/Aws/aws';
import TempWebCam from '../../Components/Media/webcamera';
import {Recorder} from 'react-voice-recorder';
import 'react-voice-recorder/dist/index.css'
import CloseIcon from '@material-ui/icons/Close';

const SpeechRecognition =
  window.SpeechRecognition || window.webkitSpeechRecognition;
  if ('SpeechRecognition' in window) {
const mic = new SpeechRecognition()

mic.continuous = true
mic.interimResults = true
mic.lang = 'en-US'
  }

class Experience extends React.Component {

  dataObject = {}
  dataimageObject = {}
  datadocObject={}
  dataaudioObject={}
  constructor(props) {
    super(props);
    this.steps=localStorage.getItem('step');
    this.experiencestep=localStorage.getItem('step');
    this.state= {
     
      filterlistview:false,
      shortlistview:false,
      showexpForm: false,
      showAppointment: false,
      searchbox: false,
      showlistview: false,
      photoIndex: 0,
      imgdata: [],
      exptitle: '',
      expdetails: '',
      domain: '',
      Colleagues: '',
      question: [],
      input: {},
      imgfile: [],
      getpostValue: [],
      showinput: false,
      gettaguser: [],
      gettagdomain: [],
      getassociation: [],
      file: '',
      listdomain: [],
      displaydomain: [],

      subjectTitle: '',
      summary: '',
      problemRecord: '',
      task: '',
      assoc: [],
      action: '',
      solution: '',
      problempic: [],
      imagetask: [],
      imageaction: [],
      imagesolution: [],
      association: [],


      tags: [],
      suggestions: [],

      tagsDomain: [],
      suggestionsDomain: [],
      reviewShow : false,
      audioShow : false,
      docFile:'',
      docView:false,
      workModalShow : false,
      addexperiance :false,
      filterstartdate :'',
      filterenddate: '',
      otherdate: '',
      dayvalue: '',
      sortcond: '',
      suggestionstext:[],
      expsearchArray:[],
      searchView:false,
      isOpen: false,
      situisOpen: false,
      taskisOpen: false,
      actisOpen: false,
      resisOpen: false,
      index:0,
      // posttags: [],
      // postsuggestions: [],
      openCam:false,
      audioDetails: {
        url: null,
        blob: null,
        chunks: null,
        duration: {
          h: 0,
          m: 0,
          s: 0
        },
        showaudio:false,
        showtext:false
      }


    };
    associationcmplt().then(res => {
      this.setState({
        getassociation: res.data.association
      });
    });
    this.toggleExpForm = this.toggleExpForm.bind(this);
    this.toggleAppointForm = this.toggleAppointForm.bind(this);
    this.toggleSearch = this.toggleSearch.bind(this);
    this.togglelistview = this.togglelistview.bind(this);
    this.toggleshortlistview = this.toggleshortlistview.bind(this);
    this.handlefilter = this.handlefilter.bind(this);
    this.handlesorting = this.handlesorting.bind(this);
    this.toggleaudio = this.toggleaudio.bind(this);
    this.toggletext = this.toggletext.bind(this);
    this.reactTags = React.createRef()


          this.props.startLoader();
          UserPost().then(res=>{
            this.setState({
              getpostValue:res['data']
              });
              this.props.hideLoader();
              console.log(res.data);
             
        
            });



    UserlistTag().then(res => {
      this.setState({
        gettaguser: res['data']['data']

      });



      // console.log(this.state.gettaguser);
      this.state.gettaguser.forEach(el => {
        this.state.suggestions.push({ id: el.user_id, name: el.user_fname + ' ' + el.user_lname });
      })


    });

    DomainTag().then(res => {

      this.setState({
        gettagdomain: res['data']['data']

      });



      // console.log(this.state.gettagdomain);
      this.state.gettagdomain.forEach(el => {
        this.state.suggestionsDomain.push({ id: el.user_exp_dom_id, name: el.user_exp_domain });
      })

    });
  }



  toggleExpForm() {

    const currentState = this.state.showexpForm;
    this.setState({ showexpForm: !currentState });

  }
  toggleAppointForm() {
    const currentState = this.state.showAppointment;
    this.setState({ showAppointment: !currentState });

  }
  toggleshortlistview() {
    const currentState = this.state.shortlistview;
    this.setState({ shortlistview: !currentState });
    this.setState({ filterlistview: false });
  }


  toggleSearch() {

    const currentState = this.state.searchbox;
    this.setState({ searchbox: !currentState });

  }

  togglelistview() {
    const currentState = this.state.filterlistview;
    this.setState({ filterlistview: !currentState });
    this.setState({ shortlistview: false });
  }

  // filter

  handleDate = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }
  handledaychnage = (e) => {
    // console.log(e.target.id);
    this.setState({ [e.target.name]: e.target.id })
  }

  handlesortChange = (e) => {

    this.setState({ "sortcond": e.target.value });
  }


  handlesubjecttitle = (e) => {
    this.setState({ subjectTitle: e.target.value });
  }
  handlesummary = (e) => {
    this.setState({ summary: e.target.value });
  }
  handleproblemRecord = (e) => {
    this.setState({ problemRecord: e.target.value });

  }
  handleTask = (e) => {
    this.setState({ task: e.target.value });

  }
  handleAction = (e) => {
    this.setState({ action: e.target.value });


  }
  handleSolution = (e) => {
    this.setState({ solution: e.target.value });


  }



  handleexpdominChange = (e) => {

    this.setState({ domain: e.target.value });

  }
  handletagcolleageChange = (e) => {

    this.setState({ Colleagues: e.target.value });

  }
  recordprobChange = (e) => {
    e.preventDefault();
    let imageTemoArray = [];
    for (let i = 0; i < e.target.files.length; i++) {
      let file = e.target.files[i];
      let reader = new FileReader();
      reader.readAsDataURL(file);

      reader.onload = function (event) {

        imageTemoArray.push(event.currentTarget.result);

      }

    }

    this.setState({ problempic: imageTemoArray });
  }

  taskChange = (e) => {
    e.preventDefault();
    let imageTemoArray = [];
    for (let i = 0; i < e.target.files.length; i++) {
      let file = e.target.files[i];
      let reader = new FileReader();
      reader.readAsDataURL(file);

      reader.onload = function (event) {

        imageTemoArray.push(event.currentTarget.result);

      }

    }

    this.setState({ imagetask: imageTemoArray });

  }

  actionChange = (e) => {
    e.preventDefault();
    let imageTemoArray = [];
    for (let i = 0; i < e.target.files.length; i++) {
      let file = e.target.files[i];
      let reader = new FileReader();
      reader.readAsDataURL(file);

      reader.onload = function (event) {

        imageTemoArray.push(event.currentTarget.result);

      }

    }

    this.setState({ imageaction: imageTemoArray });

  }

  solutionChange = (e) => {
    e.preventDefault();
    let imageTemoArray = [];
    for (let i = 0; i < e.target.files.length; i++) {
      let file = e.target.files[i];
      let reader = new FileReader();
      reader.readAsDataURL(file);

      reader.onload = function (event) {

        imageTemoArray.push(event.currentTarget.result);

      }

    }

    this.setState({ imagesolution: imageTemoArray });

  }
  handleAssociationChange = (e) => {
    let assocArray = [];
    assocArray.push(e.target.value);
    this.setState({ assoc: assocArray });
  }

  handlesubmit = (e) => {
    this.props.startLoader();
    if (this.state.subjectTitle != '' && this.state.summary != '') {
      AddUserExp(this.state.subjectTitle, this.state.problemRecord, this.state.task, this.state.action, this.state.solution, this.state.summary, this.state.tags, this.state.tagsDomain,
        this.state.assoc, this.state.problempic, this.state.imagetask, this.state.imageaction, this.state.imagesolution).then(res => {
          toast.success(res.data.msg, {});
          window.location.reload();
          this.props.hideLoader();

        });
    } else {
      this.props.hideLoader();
      toast.error('Please enter experience title')
      toast.error('Please enter Summary')
    }


  }




  onDelete(i) {
    const tags = this.state.tags.slice(0)
    tags.splice(i, 1)
    this.setState({ tags })
    // console.log(this.state.tags);
  }

  onAddition(tag) {
    const tags = [].concat(this.state.tags, tag)
    this.setState({ tags })
    setTimeout(() => {
      // console.log(this.state.tags);
    }, 2000);

  }

  onDeletedomain(i) {
    const tagsDomain = this.state.tagsDomain.slice(0)
    tagsDomain.splice(i, 1)
    this.setState({ tagsDomain })
    // console.log(this.state.tags);
  }

  onAdditiondomain(tagd) {
    const tagsDomain = [].concat(this.state.tagsDomain, tagd)
    this.setState({ tagsDomain })
    setTimeout(() => {
      //  console.log(this.state.tagsDomain);
    }, 2000);
  }


  // onDelete (i) {
  //   const tags = this.state.tags.slice(0)
  //   tags.splice(i, 1)
  //   this.setState({ tags })
  //   console.log(this.state.tags);
  // }

  // onAddition (tag) {
  //   const tags = [].concat(this.state.tags, tag)
  //   this.setState({ tags })
  //   setTimeout(() => {
  //     console.log(this.state.tags);
  //   }, 2000);

  // }

  handlesorting() {
    this.props.startLoader();
    getsorting(this.state.sortcond).then(res => {
      this.setState({
        getpostValue: res['data']
      });
      this.props.hideLoader();
      console.log(res.data);


    });
  }


  handlefilter() {
    // console.log(this.state.tagsDomain);
    //console.log(this.state.otherdate);
    if (this.state.otherdate == "Today") {
      this.state.dayvalue = '0'

    } else if (this.state.otherdate == 'Yesterday') {
      this.state.dayvalue = '1'
    }
    else if (this.state.otherdate == 'This Month') {
      this.state.dayvalue = '30'
    } else if (this.state.otherdate == 'This Year') {
      this.state.dayvalue = '365'
    } else if (this.state.otherdate == 'Last 3 Years') {
      this.state.dayvalue = '1095'
    }

    //console.log(this.state.dayvalue);
    this.props.startLoader();
    getFilter(this.state.tagsDomain, this.state.filterstartdate, this.state.filterenddate, this.state.dayvalue).then(res => {
      this.setState({
        getpostValue: res['data']
      });
      this.props.hideLoader();
      console.log(res.data);


    });
  }

  reviedata = e => event => {
    console.log(e);

    this.props.history.push({
      pathname: '/consulatation',
      data: e
    })
}
editEduData = e => event =>{
  console.log(e)
  this.setState({workModalShow : true})
  e.user_association.forEach(el=>{
    this.state.association.push(el.organization_name);
  })
  this.dataObject = {"expId":e.user_exp_id,"expTitle":e.user_exp_title,"actionText":e.action_text,"expDate":e.exp_date_added,"resultText":e.result_text,"situationText":e.situation_text,"taskText":e.task_text,"Summary":e.summary,"organization":this.state.association,"situation_image_files":e.user_situation_files,"task_image_files":e.user_task_files,"action_image_files":e.user_action_files,"result_image_files":e.user_result_files, "user_img":e.user_image_files,"situation_audio":e.situation_audio,"action_audio":e.action_audio,"task_audio":e.task_audio,"result_audio":e.result_audio,"user_audio":e.audio,"situation_video":e.situation_video,"task_video":e.task_video,"action_video":e.action_video,"result_video":e.result_video,"user_video":e.videos,"situation_doc":e.situation_doc_file,"task_doc":e.task_doc_file,"action_doc":e.action_doc,"result_doc":e.result_doc,"user_doc":e.doc,"user_tagged":e.user_tagged,"location":e.location,"Tagged_Domain":e.domain_tagged}

  }
  


  addExp = e => event => {
    this.setState({ addexperiance: true })
    // console.log("abc");
  }
  modalVideo= e => event =>{
    this.setState({index:e});
    this.setState({isOpen:true});
  }
  situmodalVideo= e => event =>{
    this.setState({index:e});
    this.setState({situisOpen:true});
  }
  taskmodalVideo= e => event =>{
    this.setState({index:e});
    this.setState({taskisOpen:true});
  }
  actmodalVideo= e => event =>{
    this.setState({index:e});
    this.setState({actisOpen:true});
  }
  resmodalVideo= e => event =>{
    console.log(e);
    this.setState({index:e});
    this.setState({resisOpen:true});
  }
  imgSlider = e => event => {
    this.setState({ reviewShow: true })
    this.dataimageObject = e;
    console.log(this.dataimageObject);
  }
  audioPlayer = e => event => {
    this.setState({ audioShow: true });
    this.dataaudioObject=e;
  }

  onTextChnaged = (e)=>{
    let value =e.target.value;
    //this.props.startLoader();
    ExpSearch(value).then(res=>{
    //this.props.hideLoader();
    this.setState({
      suggestionstext:res['data']
      });
       console.log(this.state.suggestionstext);
       if(this.state.suggestionstext.length>0)
       {
         this.setState({searchView:true});
       }
       else
       {
         this.setState({searchView:false});
       }
      });
  }


  docViewer= (doc)=>event =>{
     console.log(doc);
       doc.forEach(el=>{
         this.state.docFile=el;
       })
       console.log(this.state.docFile);
      this.datadocObject = {"doc":this.state.docFile};
    //this.datadocObject = {"doc":this.state.docFile};
       this.setState({docView: !this.state.docView});
       console.log(this.datadocObject);
  }


  changestate= (id)=>{
    console.log(id);
   
    Publicpvt(id).then(res => {
     
      console.log(res.data);
      toast.success(res.data.msg,{});
      window.location.reload();
    });

  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps.videodata);
    console.log(nextProps.blobvideodata);
    this.setState({webcamBase64:nextProps.videodata});
    fileUpload(nextProps.blobvideodata,'expWebcam').then((awsLink)=>{
      this.setState({ webcamAwsLink:awsLink},console.log(this.state.webcamAwsLink));
   })
    if(nextProps.show)
    {
      this.setState({ ModalShow:true});
    }
    else
    {
      this.setState({ ModalShow:false});
    }
    
  }

  openCam = () =>{
   
    this.setState({openCam : !this.state.openCam});
    // this.setState({genvideochng:false});
    //   this.setState({videoShow: true});
      // this.state.videovalue.pop();
      // this.state.genvideo.pop();
   
    } 

    handleAudioStop(data){
      console.log(data)
      this.setState({ audioDetails: data });
  }
  handleAudioUpload(file) {
      console.log(file);
//       var reader = new FileReader();
//      reader.readAsDataURL(file); 
//      reader.onloadend = function() {
//      var base64data = reader.result;                
//      console.log(base64data);
//  }
  }
  handleRest() {
    const reset = {
      url: null,
      blob: null,
      chunks: null,
      duration: {
        h: 0,
        m: 0,
        s: 0
      }
    };
    this.setState({ audioDetails: reset });
  }

  toggleaudio() {
    const currentState=this.state.showaudio;
    this.setState({ showaudio: !currentState});
   
  }
  toggletext() {
    const currentState=this.state. showtext;
    this.setState({  showtext: !currentState});
    
  }


  render() {
    console.log(this.state.searchView);
    let workModalClose = () => { this.setState({ workModalShow: false }) }
    let addexperianceclose = () => { this.setState({ addexperiance: false }) }
    let requestModalClose = () => { this.setState({ audioShow: false }) }
       let  docViewClose=() =>{this.setState({docView:false})}
      
      //  const style = {
      //   play: {
      //     button: {
      //       width: '28',
      //       height: '28',
      //       cursor: 'pointer',
      //       pointerEvents: 'none',
      //       outline: 'none',
      //       backgroundColor: 'yellow',
      //       border: 'solid 1px rgba(255,255,255,1)',
      //       borderRadius: 6
      //     },
      //   }
      // };
       const type = 'pdf'
      const options  = [
        
        { label: 'Association 1', value: 1},
      ];
     
      let requestModalCamClose =() =>{this.setState({openCam:false})}
      return(

            <div className="col-sm-6">
              <div className="d-flex justify-content-between mb-2 px-3 py-3 px-md-0 py-md-0">
				      <div className="statusn">
						{/* <label className="switch">
						<input type="checkbox" onClick={this.togglelistview} />
						<span className="slider round" ></span>
						</label>
						List View  */}
            <a onClick={this.togglelistview} className="pl-1"><i className="fa fa-filter"></i></a>
            <a onClick={this.toggleshortlistview} className="pl-1"><i className="fa fa-sort"></i></a>
          </div>
          <div className="statusn">
            <a href="#" onClick={this.toggleAppointForm}><span className="svg-icon svg-icon-md svg-icon-primary"><NotificationsActiveIcon /></span></a>
          </div>
        </div>
        {this.state.reviewShow && (
          <Lightbox
            mainSrc={this.dataimageObject[this.state.photoIndex]}
            nextSrc={this.dataimageObject[(this.state.photoIndex + 1) % this.dataimageObject.length]}
            prevSrc={this.dataimageObject[(this.state.photoIndex + this.dataimageObject.length - 1) % this.dataimageObject.length]}
            onCloseRequest={() => this.setState({ reviewShow: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (this.state.photoIndex + this.dataimageObject.length - 1) % this.dataimageObject.length,
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (this.state.photoIndex + 1) % this.dataimageObject.length,
              })
            }
          />
        )}
        {this.state.showAppointment &&
          <div className="notification mb-4">
            <div className="exbg">
              {/* {location.pathname == '/profile'} */}
              <h3>Experiance Notifications</h3>
              <a href="#" className="pl-1"><i className="fa fa-filter"></i></a>
              <a href="#"><i className="fa fa-sort-amount-down-alt"></i></a>

              <div className="apoinbox">
                <div className="d-flex justify-content-between notibox accept">
                  <div className="accept_left">
                    <h4>Experience</h4>
                    <p>Agenda Domain</p>
                  </div>
                  <div className="accept_right">
                    <a href="#"><i className="fa fa-ellipsis-v"></i></a>
                    <span>2d</span>
                  </div>
                </div>
                <div className="d-flex justify-content-between notibox pending">
                  <div className="accept_left">
                    <h4>Experience</h4>
                    <p>Agenda Domain</p>
                  </div>
                  <div className="accept_right">
                    <a href="#"><i className="fa fa-ellipsis-v"></i></a>
                    <span>2d</span>
                  </div>
                </div>
                <div className="d-flex justify-content-between notibox rejected">
                  <div className="accept_left">
                    <h4>Experience</h4>
                    <p>Agenda Domain</p>
                  </div>
                  <div className="accept_right">
                    <a href="#"><i className="fa fa-ellipsis-v"></i></a>
                    <span>2d</span>
                  </div>
                </div>
                <div className="d-flex justify-content-between notibox resudule">
                  <div className="accept_left">
                    <h4>Experience</h4>
                    <p>Agenda Domain</p>
                  </div>
                  <div className="accept_right">
                    <a href="#"><i className="fa fa-ellipsis-v"></i></a>
                    <span>2d</span>
                  </div>
                </div>
                <div className="d-flex justify-content-between notibox resudule">
                  <div className="accept_left">
                    <h4>Experience</h4>
                    <p>Agenda Domain</p>
                  </div>
                  <div className="accept_right">
                    <a href="#"><i className="fa fa-ellipsis-v"></i></a>
                    <span>2d</span>
                  </div>
                </div>
              </div>
              <div className="legend">
                <ul>
                  <li><i className="fa fa-circle color1"></i> Accepted</li>
                  <li><i className="fa fa-circle color2"></i> Pending</li>
                  <li><i className="fa fa-circle color3"></i> Rejected</li>
                  <li><i className="fa fa-circle color4"></i> Reschedule</li>
                </ul>
              </div>
              <div className="clearfix"></div>
            </div>
          </div>
        }


        <div className="profile-sec2-middle">
          <div className="profile-sec2-middle-inne">
            {this.state.filterlistview &&
              <div className="exbg">
                <h4>Filter</h4>
                <div className="row">
                  <div className= "form-group col-md-12">
                  <input type="text" onChange={this.onTextChnaged} className="form-control" name="word" placeholder="Search"/>
                  </div>
                  <div className="form-group col-md-6">
                   
                    <ReactTags
                      ref={this.reactTags}
                      tags={this.state.tagsDomain}
                      suggestions={this.state.suggestionsDomain}
                      onDelete={this.onDeletedomain.bind(this)}
                      onAddition={this.onAdditiondomain.bind(this)}

                      placeholderText={'Tag Domain'} />
                  </div>
                  {/* <div className="col-md-6">
                  <input type="text" className="form-control" name="word" placeholder="Search"/> 
                  </div> */}
                  <div className="col-md-6">
                    <Dropdown>
                      <Dropdown.Toggle id="dropdown-basic" className="rafilter">

                        <CalendarTodayIcon /> Any date

  </Dropdown.Toggle>
                      {this.state.filterstartdate != '' && this.state.otherdate == '' &&
                        <p>{this.state.filterstartdate} to {this.state.filterenddate} </p>}


                      <p>{this.state.otherdate}</p>

                      <Dropdown.Menu>

                        <Dropdown.Item href="#/action-1">
                          <div className="form-group">
                            <label>From</label>
                            <input type="date" className="form-control" name="filterstartdate" onChange={this.handleDate} />
                          </div>
                          <div className="form-group">
                            <label>To</label>
                            <input type="date" className="form-control" name="filterenddate" placeholder="Search" onChange={this.handleDate} />
                          </div>
                        </Dropdown.Item>
                        <Dropdown.Item name="otherdate" id="Today" onClick={this.handledaychnage} >Today</Dropdown.Item>
                        <Dropdown.Item name="otherdate" id="Yesterday" onClick={this.handledaychnage}>Yesterday</Dropdown.Item>
                        <Dropdown.Item name="otherdate" id="This Month" onClick={this.handledaychnage}>This Month</Dropdown.Item>
                        <Dropdown.Item name="otherdate" id="This Year" onClick={this.handledaychnage}>This Year</Dropdown.Item>
                        <Dropdown.Item name="otherdate" id="Last 3 Years" onClick={this.handledaychnage}>Last 3 Years</Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>

                  {/* <input type="text" className="form-control" placeholder="Search"/>
        <select className="form-control"  name="assoc" placeholder="value1" >
                 <option>Select Your Organization</option>
        </select>
        <select className="form-control"  name="assoc" placeholder="value1" >
                 <option>Select Your Organization</option>
        </select>
        <select className="form-control"  name="assoc" placeholder="value1" >
                 <option>Select Your Organization</option>
        </select> */}

                </div>
                <div className="mt-3">
                  <Button className="w-100 rabtns" onClick={this.handlefilter}>Apply</Button>
                </div>

                <div className="clearfix">
                </div>
              </div>
            }
            {this.state.shortlistview &&
              <div className="exbg">
                <h4>Sort By</h4>
                <div className="row">
                  {/* <div className="form-group col-md-6">
                 <select className="form-control"  name="assoc" placeholder="value1" >
                 <option>Select</option>
                 </select>
                 </div> */}
                  <div className="form-group col-md-12">
                    <select className="form-control" name="assoc" onChange={this.handlesortChange} >
                      <option>Select</option>
                      <option value='newest to oldest'>newest to oldest</option>
                      <option value='oldest to newest'>oldest to newest</option>

                    </select>
                  </div>
                  {/* <input type="text" className="form-control" placeholder="Search"/>
        <select className="form-control"  name="assoc" placeholder="value1" >
                 <option>Select Your Organization</option>
        </select>
        <select className="form-control"  name="assoc" placeholder="value1" >
                 <option>Select Your Organization</option>
        </select>
        <select className="form-control"  name="assoc" placeholder="value1" >
                 <option>Select Your Organization</option>
        </select> */}

                </div>
                <div className="mt-0">
                  <Button className="w-100 rabtns"onClick={this.handlesorting}>Apply</Button>
                </div>

                <div className="clearfix">
                </div>
              </div>
            }
            <div className="exbg">
              <div className="justify-content-between">
                <div className="exptext text-center">
                  <span>Add Your Experience</span>
                </div>
                <div className="d-flex justify-content-center ramidea">
                
                {/* <a onClick={this.addExp()}><ImageIcon className="photo_icon" /> Photo</a> */}
                <a  onClick={this.openCam}><VideocamIcon className="video_icon" /> Video</a>
                <a onClick={this.toggleaudio} ><MicIcon className="audio_icon" />Audio</a>
                <a onClick={this.toggletext}><TextFieldsIcon className="document_icon" />Text</a>
              </div>
              {this.state.showaudio &&
                  <Recorder
                record={true}
                title={"Recording"}
                audioURL={this.state.audioDetails.url}
                showUIAudio
                handleAudioStop={data => this.handleAudioStop(data)}
                handleAudioUpload={data => this.handleAudioUpload(data)}
                handleRest={() => this.handleRest()} 
                />
              }

             {this.state.showtext  &&
               <div className="form-group mt-3">
              <textarea type="text" name="summery" onChange={this.handlesummary} className="form-control" id="exampleFormControlTextarea1" 
              rows="7" defaultValue={this.state.summery} placeholder={"what about your Experience " }></textarea>

              <a onClick={this.toggletext}><CloseIcon className="raclose"/></a>
              <Button  className="exphome mt-3 float-right">Submit Your Experience</Button>
              </div>
              }
                
              </div>
              <div className="clearfix"></div>
              <div className="adexp text-center">
                  {/* <a href="#"><i className="fa fa-microphone"></i></a> */}
                  {/* onClick={this.toggleExpForm} */}

                  <a href="#" onClick={this.addExp()}><i className="fa fa-plus-circle"  ></i></a>
                </div>
            </div>

            {this.state.showlistview &&
              <div className="exbg">
                <div className="form-group rslt">
                  <input type="text" className="form-control" placeholder="Search" />
                </div>
                <div className="clearfix">
                </div>
              </div>
            }


       
{ 
     this.state.searchView && this.state.suggestionstext.map((e, key)=>{
        return	<Accordion key={key} className="mainacr"><Card>
          <div   className="">


<Card.Header>
     
					<div className="d-flex justify-content-between">
          
						<div className="extitle"> 
            {/* <Speech 
  textAsButton={true}    
   displayText={<i className="fa fa-volume-up"></i>}
  text={e.user_exp_title} voice="Google UK English Female"></Speech> */}

            <Accordion.Toggle as={Button} variant="link" eventKey="0" className="pl-0 pb-0 raextw">
            
						<h2>{e.user_exp_title}</h2>
            </Accordion.Toggle>
						<a href="#">{moment(e.exp_date_added).format('LL')}</a>
            
     <p className="mb-0 mt-2">{e.summary}</p>
     
						</div>
           
						<div className="exoption d-flex">
						<div className="pprivate">
							Public
							<label className="switch" >
                {e.exp_type=="Public" ?
            <input checked type="checkbox" onClick={() =>{this.changestate(e.user_exp_id)}} />
          :<input  type="checkbox" onClick={() =>{this.changestate(e.user_exp_id)}} />}
						<span className="slider round"></span>
						</label>
						</div>
           
						
						<a onClick={this.editEduData(e)}><i className="fa fa-pencil" style={{color:'#a2a2a2 '}}></i></a>

						</div>
					</div>
          
    </Card.Header>
    <Accordion.Collapse eventKey="0">
    <Card.Body>
      <div className="taglist">
      <div className="tagdomain">
           {e.domain_tagged!=''&&  <span className="domain">Skill :</span>}
            {e.domain_tagged && e.domain_tagged.map((jc,jcb)=>{
					return	<span key={jcb}> <i  className="fa fa-tags"></i>    {jc.name}  |</span> 
       })} 
       </div>

       <div className="tagdomain">
           {e.user_association!=''&&  <span className="domain">Organization :</span>}
            {e.user_association && e.user_association.map((jc,jcb)=>{
					return	<span key={jcb}> <i ></i>    {jc.organization_name}  |</span> 
       })} 
       </div>

<div className="tagcolg">
{e.user_tagged!=''&&  <span className="colgi">Colleagues : </span>}
{e.user_tagged && e.user_tagged.map((jc,jcb)=>{
					return	<span key={jcb}><i  className="fa fa-user"></i>    {jc.f_name + " "+jc.l_name}  |</span> 
       })} 
       </div>
       <div className="ragenfile">
      {e.user_image_files.length>0 &&
            <a onClick={this.imgSlider(e.user_image_files)}>
          <span className="badge">{e.user_image_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
     }
     {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.isOpen} url={e.videos[0]} onClose={() => this.setState({isOpen: false})} />
     }
     {e.videos.length>0 &&
            <a onClick={this.modalVideo(key)}>
            <span className="badge">{e.videos.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
      {e.audio.length>0 &&
      <a onClick={this.audioPlayer(e.audio)}>
            <span className="badge ">{e.audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
      {e.doc.length>0 &&
            <a onClick={this.docViewer(e.doc)}>
            <span className="badge ">{e.doc.length}</span>
            <i className="fa fa-files-o"></i>
            
            {/* <p>Files</p> */}
            </a>
     }
            </div>


      </div>
					<p>{e.user_exp_answer} </p>
          <div className="multiimg">
          <ul>
          {e.exp_image && e.exp_image.map((val,key)=>
            <li>
					<img src={val.user_exp_img} alt="" className="img-fluid resize" />
            </li>
              )}
              
          </ul>
          <div className="row">
          {e.situation_text !='' &&
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
      Situation
      <div className="d-flex justify-content-left rafile">
      {e.user_situation_files.length>0 &&
            <a onClick={this.imgSlider(e.user_situation_files)}>
          <span className="badge rabadge">{e.user_situation_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
          }
          {this.state.index==key &&
           <ModalVideo channel='custom' isOpen={this.state.situisOpen} url={e.situation_video[0]} onClose={() => this.setState({situisOpen: false})} />
          }
           {e.situation_video.length>0 &&
            <a onClick={this.situmodalVideo(key)}>
            <span className="badge rabadge">{e.situation_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
       {e.situation_audio.length>0 &&
            <a onClick={this.audioPlayer(e.situation_audio)}>
            <span className="badge rabadge">{e.situation_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
       {e.situation_doc_file.length>0 &&
            <a onClick={this.docViewer(e.situation_doc_file)}>
            <span className="badge rabadge">{e.situation_doc_file.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
      </Accordion.Toggle>
      
    </Card.Header>
    
    <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
              
      <p>{e.situation_text}</p>
      </ShowMoreText>
          
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
     }
     {e.task_text !='' &&
          <div className="col-sm-12">
          <div className="yract">
          <Accordion  defaultActiveKey="0">
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
      Task
      <div className="d-flex justify-content-left rafile">
      {e.user_task_files.length>0 &&
            <a onClick={this.imgSlider(e.user_task_files)}>
          <span className="badge rabadge">{e.user_task_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
     }
     {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.taskisOpen} url={e.task_video[0]} onClose={() => this.setState({taskisOpen: false})} />
     }
      {e.task_video.length>0 &&
            <a onClick={this.taskmodalVideo(key)}>
            <span className="badge rabadge">{e.task_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
      {e.task_audio.length>0 &&
            <a onClick={this.audioPlayer(e.task_audio)}>
            <span className="badge rabadge">{e.task_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
      {e.task_doc_file.length>0 &&
            <a onClick={this.docViewer(e.task_doc_file)}>
            <span className="badge rabadge">{e.task_doc_file.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
      <p>{e.task_text}</p>
           </ShowMoreText>
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
     }
     {e.action_text !='' &&
          <div className="col-sm-12">
          <div className="yract">
          <Accordion  defaultActiveKey="0">
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
      Action
      <div className="d-flex justify-content-left rafile">
      {e.user_action_files.length>0 &&
            <a onClick={this.imgSlider(e.user_action_files)}>
            <span className="badge rabadge">{e.user_action_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
            }
            {this.state.index==key &&
            <ModalVideo channel='custom' isOpen={this.state.actisOpen} url={e.action_video[0]} onClose={() => this.setState({actisOpen: false})} />
            }
             {e.action_video.length>0 &&
            <a onClick={this.actmodalVideo(key)}>
            <span className="badge rabadge">{e.action_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
       {e.action_audio.length>0 &&
            <a onClick={this.audioPlayer(e.action_audio)}>
            <span className="badge rabadge">{e.action_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
      {e.action_doc.length>0 &&
            <a onClick={this.docViewer(e.action_doc)}>
            <span className="badge rabadge">{e.action_doc.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
      <p>{e.action_text}</p>
      </ShowMoreText>
          
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
     }
     {e.result_text !='' &&
          <div className="col-sm-12">
          <div className="yract">
          <Accordion  defaultActiveKey="0">
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
      Result
      <div className="d-flex justify-content-left rafile">
      {e.user_result_files.length>0 &&
            <a onClick={this.imgSlider(e.user_result_files)}>
          <span className="badge rabadge">{e.user_result_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
     }
     {this.state.index==key &&
       <ModalVideo channel='custom' isOpen={this.state.resisOpen} url={e.result_video[0]} onClose={() => this.setState({resisOpen: false})} />
     }
     {e.result_video.length>0 && 
            <a onClick={this.resmodalVideo(key)}>
            <span className="badge rabadge">{e.result_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
       {e.result_audio.length>0 && 
            <a onClick={this.audioPlayer(e.result_audio)}>
            <span className="badge rabadge">{e.result_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
       {e.result_doc.length>0 && 
            <a onClick={this.docViewer(e.result_doc)}>
            <span className="badge rabadge">{e.result_doc.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="0">
      <Card.Body>  
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
              <p>{e.result_text}</p>
               
        
           </ShowMoreText>
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
     }
          </div>
          <Button type="submit" onClick={this.reviedata(e)} className="rqstr"><span className="text">Request for review</span><i className="fa fa-street-view"></i></Button>
          <div className="clearfix"></div>
          </div>
          </Card.Body>
    </Accordion.Collapse>
          <div className="clearfix"></div>
         
   
				</div>
        </Card>
</Accordion>
        
     
	})
}           
{ 
     this.state.showlistview==false && this.state.searchView==false && this.state.getpostValue.map((e, key)=>{
        return	<Accordion key={key} className="mainacr"><Card>
          <div   className="">


<Card.Header>
     
					<div className="d-flex justify-content-between">
          
						<div className="extitle"> 
            {/* <Speech 
  textAsButton={true}    
   displayText={<i className="fa fa-volume-up"></i>}
  text={e.user_exp_title} voice="Google UK English Female"></Speech> */}

            <Accordion.Toggle as={Button} variant="link" eventKey="0" className="pl-0 pb-0 raextw">
            
						<h2>{e.user_exp_title}</h2>
            </Accordion.Toggle>
						<a href="#">{moment(e.exp_date_added).format('LL')}</a>
            
     <p className="mb-0 mt-2">{e.summary}</p>
     
						</div>
           
						<div className="exoption d-flex">
						<div className="pprivate">
							Public
							<label className="switch" >
                {e.exp_type=="Public" ?
            <input checked type="checkbox" onClick={() =>{this.changestate(e.user_exp_id)}} />
          :<input  type="checkbox" onClick={() =>{this.changestate(e.user_exp_id)}} />}
						<span className="slider round"></span>
						</label>
						</div>
           
						
						<a onClick={this.editEduData(e)}><i className="fa fa-pencil" style={{color:'#a2a2a2 '}}></i></a>

						</div>
					</div>
          
    </Card.Header>
    <Accordion.Collapse eventKey="0">
    <Card.Body>
      <div className="taglist">
      <div className="tagdomain">
           {e.domain_tagged!=''&&  <span className="domain">Skill :</span>}
            {e.domain_tagged && e.domain_tagged.map((jc,jcb)=>{
					return	<span key={jcb}> <i  className="fa fa-tags"></i>    {jc.name}  |</span> 
       })} 
       </div>

       <div className="tagdomain">
           {e.user_association!=''&&  <span className="domain">Organization :</span>}
            {e.user_association && e.user_association.map((jc,jcb)=>{
					return	<span key={jcb}> <i ></i>    {jc.organization_name}  |</span> 
       })} 
       </div>

<div className="tagcolg">
{e.user_tagged!=''&&  <span className="colgi">Colleagues : </span>}
{e.user_tagged && e.user_tagged.map((jc,jcb)=>{
					return	<span key={jcb}><i  className="fa fa-user"></i>    {jc.f_name + " "+jc.l_name}  |</span> 
       })} 
       </div>
 {e.location!='' &&  
       <div>
       <span className="colgi">Location : </span>
       <span className="colgi">{e.location}</span>
       </div>
 }
       <div className="ragenfile">
      {e.user_image_files.length>0 &&
            <a onClick={this.imgSlider(e.user_image_files)}>
          <span className="badge">{e.user_image_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
     }
     {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.isOpen} url={e.videos[0]} onClose={() =>this.setState({isOpen: false})} />
     }
      {e.videos.length>0 &&
            <a onClick={this.modalVideo(key)}>
            <span className="badge">{e.videos.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
      {e.audio.length>0 &&
      <a onClick={this.audioPlayer(e.audio)}>
            <span className="badge ">{e.audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
      {e.doc.length>0 &&
            <a onClick={this.docViewer(e.doc)}>
            <span className="badge ">{e.doc.length}</span>
            <i className="fa fa-files-o"></i>
            
            {/* <p>Files</p> */}
            </a>
     }
            </div>


      </div>
					<p>{e.user_exp_answer} </p>
          <div className="multiimg">
          <ul>
          {e.exp_image && e.exp_image.map((val,key)=>
            <li>
					<img src={val.user_exp_img} alt="" className="img-fluid resize" />
            </li>
              )}
              
          </ul>
          <div className="row">
          {e.situation_text !='' &&
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
      Situation
      <div className="d-flex justify-content-left rafile">
      {e.user_situation_files.length>0 &&
            <a onClick={this.imgSlider(e.user_situation_files)}>
          <span className="badge rabadge">{e.user_situation_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
          }
           {this.state.index==key &&
          <ModalVideo channel='custom' isOpen={this.state.situisOpen} url={e.situation_video[0]} onClose={() => this.setState({situisOpen: false})} />
           }
          {e.situation_video.length>0 &&
            <a onClick={this.situmodalVideo(key)}>
            <span className="badge rabadge">{e.situation_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
       {e.situation_audio.length>0 &&
            <a onClick={this.audioPlayer(e.situation_audio)}>
            <span className="badge rabadge">{e.situation_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
       {e.situation_doc_file.length>0 &&
            <a onClick={this.docViewer(e.situation_doc_file)}>
            <span className="badge rabadge">{e.situation_doc_file.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
      </Accordion.Toggle>
      
    </Card.Header>
    
    <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
              
      <p>{e.situation_text}</p>
      </ShowMoreText>
          
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
     }
     {e.task_text !='' &&
          <div className="col-sm-12">
          <div className="yract">
          <Accordion  defaultActiveKey="0">
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
      Task
      <div className="d-flex justify-content-left rafile">
      {e.user_task_files.length>0 &&
            <a onClick={this.imgSlider(e.user_task_files)}>
          <span className="badge rabadge">{e.user_task_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
     }
     {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.taskisOpen} url={e.task_video[0]} onClose={() => this.setState({taskisOpen: false})} />
     }
      {e.task_video.length>0 &&
            <a onClick={this.taskmodalVideo(key)}>
            <span className="badge rabadge">{e.task_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
      {e.task_audio.length>0 &&
            <a onClick={this.audioPlayer(e.task_audio)}>
            <span className="badge rabadge">{e.task_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
      {e.task_doc_file.length>0 &&
            <a onClick={this.docViewer(e.task_doc_file)}>
            <span className="badge rabadge">{e.task_doc_file.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
      <p>{e.task_text}</p>
           </ShowMoreText>
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
     }
     {e.action_text !='' &&
          <div className="col-sm-12">
          <div className="yract">
          <Accordion  defaultActiveKey="0">
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
      Action
      <div className="d-flex justify-content-left rafile">
      {e.user_action_files.length>0 &&
            <a onClick={this.imgSlider(e.user_action_files)}>
            <span className="badge rabadge">{e.user_action_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
            }
            {this.state.index==key &&
            <ModalVideo channel='custom' isOpen={this.state.actisOpen} url={e.action_video[0]} onClose={() => this.setState({actisOpen: false})} />
            }
             {e.action_video.length>0 &&
            <a onClick={this.actmodalVideo(key)}>
            <span className="badge rabadge">{e.action_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
       {e.action_audio.length>0 &&
            <a onClick={this.audioPlayer(e.action_audio)}>
            <span className="badge rabadge">{e.action_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
      {e.action_doc.length>0 &&
            <a onClick={this.docViewer(e.action_doc)}>
            <span className="badge rabadge">{e.action_doc.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
      <p>{e.action_text}</p>
      </ShowMoreText>
          
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
     }
     {e.result_text !='' &&
          <div className="col-sm-12">
          <div className="yract">
          <Accordion  defaultActiveKey="0">
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
      Result
      <div className="d-flex justify-content-left rafile">
      {e.user_result_files.length>0 &&
            <a onClick={this.imgSlider(e.user_result_files)}>
          <span className="badge rabadge">{e.user_result_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
     }
     {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.resisOpen} url={e.result_video[0]} onClose={() => this.setState({resisOpen: false})} />
     }
     {e.result_video.length>0 && 
            <a onClick={this.resmodalVideo(key)}>
            <span className="badge rabadge">{e.result_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
       {e.result_audio.length>0 && 
            <a onClick={this.audioPlayer(e.result_audio)}>
            <span className="badge rabadge">{e.result_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
       {e.result_doc.length>0 && 
            <a onClick={this.docViewer(e.result_doc)}>
            <span className="badge rabadge">{e.result_doc.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="0">
      <Card.Body>  
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
              <p>{e.result_text}</p>
               
        
           </ShowMoreText>
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
     }
     
          </div>
          {e.review_request!="Yes" &&
          <Button type="submit" onClick={this.reviedata(e)} className="rqstr"><span className="text">Request for review</span><i className="fa fa-street-view"></i></Button>
              }
          <div className="clearfix"></div>
          </div>
          </Card.Body>
    </Accordion.Collapse>
          <div className="clearfix"></div>
         
   
				</div>
        </Card>
</Accordion>
        
     
	})
}


{ this.state.showlistview && this.state.getpostValue.map((e, key)=>{
   return <div key={key} className="exbg exprience">
					<div className="d-flex justify-content-between">
						<div className="extitle">
						<h2>Subject</h2>
						<a href="#">{moment(e.exp_date_added).format('LL')}</a>
						<span><i className="fa fa-tags"></i>Challenging</span>
						
						</div>
						<div className="exoption d-flex">
						<div className="pprivate">
							Public
							<label className="switch">
						<input type="checkbox" />
						<span className="slider round"></span>
						</label>
						</div>
						
						<a href="#"><i className="fa fa-ellipsis-v"></i></a>

						</div>
					</div>
				
         
				
				</div>
        
        })
    }

                </div>
                </div>
                <Editexperience
        show={this.state.workModalShow}
        onHide={workModalClose}
        dataobject={this.dataObject}
      />

<Addexperiance
        show={this.state.addexperiance}
        onHide={addexperianceclose}
        
      />
      <DocumentViewer
        show={this.state.docView}
        onHide={docViewClose}
        dataobject={this.datadocObject}
      />
                {/* <Imageslider/> */}
                 <ReviewRequest
        show={this.state.audioShow}
        onHide={requestModalClose}
       dataobject={this.dataaudioObject}
      />  
    
    <TempWebCam show={this.state.openCam} onHide={requestModalCamClose} />
          </div>
  
        );
    }

}

const mapStateToProps = state=>{
  return{
      loader : state.loaderReducer.loader,
      auth : state.authReducer.authData,
      profilePic : state.authReducer.profileImage
    }
}

const mapDispatchFromProps = dispatch=>{
  return{
      startLoader : () =>dispatch(showLoader()),
      hideLoader : () =>dispatch(hideLoader()),
      updateProfilePic : (data) =>dispatch(updateAuth(data))
  }
}

export default connect(mapStateToProps,mapDispatchFromProps)(Experience);

