import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { editEducation,educationType} from '../../providers/auth';
import { toast } from 'react-toastify';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import { connect } from 'react-redux';
import VideocamIcon from '@material-ui/icons/Videocam';
import ImageIcon from '@material-ui/icons/Image';
import MicIcon from '@material-ui/icons/Mic';
import DescriptionIcon from '@material-ui/icons/Description';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import GraphicEqIcon from '@material-ui/icons/GraphicEq';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import VideoCallIcon from '@material-ui/icons/VideoCall';
import  Speechtotext  from '../Media/speechTotext';
import WebCamera from '../Media/webcamera';
import ReactTags from 'react-tag-autocomplete';
import CloseIcon from '@material-ui/icons/Close';
import TextFieldsIcon from '@material-ui/icons/TextFields'
//import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import { manageAuth,updateAuth } from '../../redux/Actions/authAction';
import {EditUserExp,associationcmplt,DomainTag,UserlistTag,deleteExpList} from '../../providers/auth';
import audioImage from '../../assets/images/audiofile.png';
import docImage from '../../assets/images/documentfile.png';
import SimpleImageSlider from "react-simple-image-slider";
import ReactPlayer from 'react-player';
import fileUpload from '../../Components/Aws/aws';
import { MDBCloseIcon } from 'mdbreact';
import {emptyVideoData} from '../../redux/Actions/mediaAction';
import { confirmAlert } from 'react-confirm-alert';


const isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
const isFirefox = typeof InstallTrigger !== 'undefined';
var isOpera = (!!window.opera && !!window.opera.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && window['safari'].pushNotification));
class Editexperience extends React.Component{

    constructor(props){
        super(props);

        this.steps=localStorage.getItem('step');

        if(this.steps==0 || this.steps==1 || this.steps==2 || this.steps==3 || this.steps==4 )
        {
            this.state = {
                step : this.steps,
                userImage:'',       
                };
       }
       else
       {
        this.state = {
            step : 1,
            userImage:'',       
            };
    
       }
    

        this.reactTags = React.createRef();
        this.state={
          ModalShow : false,
          showexpForm: false,
          showAppointment: false,
          searchbox: false,
          reviewShow : false,
          showlistview: false,
          imgdata:[],
          exptitle: '',
          expdetails : '',
          domain: '',
          Colleagues :'',
          question: [],
          input: {},
          imgfile:[],
          getpostValue:[],
          showinput:false,
          gettaguser:[],
          tagdomain:false,
          getassociation:[],
          file:'',
          listdomain:[],
          displaydomain:[],
         mas_id:'',
          subjectTitle:'',
         
          problemRecord:'',
          task:'',
          assoc: [],
          action:'',
          solution:'',
          
        
    
          tags: [],
          suggestions: [],
          
          tagsDomain: [],
          suggestionsDomain: [],
          reviewShow : false,

            // new code
            addTitleInput : false,
            situationInput : true,
            situaudioShow : false,
            taskaudioShow : false,
            actaudioShow : false,
            resaudioShow : false,
            genaudioShow : false,
            situdocShow : false,
            taskdocShow : false,
            actdocShow : false,
            resdocShow : false,
            gendocShow : false,
            taskInput : false,
            actionInput : false,
            resultInput : false,
            openMic : false,
            openCam:false,
            textboxId : '',
            imagevalue:[],
            summery:'',
            situation:'',
            task:'',
            action:'',
            result:'',
            title:'',
            
            situtatonmpic: [],
            imagetask:[],
            imageaction:[],
            imagesolution:[],
            userImg:[],
            situation_video: [],
            task_video:[],
            action_video:[],
            result_video:[],
            user_video:[],
            situation_audio: [],
            action_audio:[],
            task_audio:[],
            result_audio:[],
            user_audio:[],
            situation_doc:[],
            task_doc:[],
            action_doc:[],
            result_doc:[],
            user_doc:[],
         
            //new code
            videoShow:false,
      situvideoShow:false,
      taskvideoShow:false,
      actvideoShow:false,
      resvideoShow:false,
      videovalue:[],
      situvideovalue:[],
      taskvideovalue:[],
      actvideovalue:[],
      resvideovalue:[],
      imagevalueLink:[],
      situimagevalueLink:[],
      taskimagevalueLink:[],
      actimagevalueLink:[],
      resimagevalueLink:[],
      imagevalue:[],
      situimagevalue:[],
      taskimagevalue:[],
      actimagevalue:[],
      resimagevalue:[],
      imgiconvalue:[],
      situaudioFile : [],
      taskaudioFile : [],
      actaudioFile : [],
      resaudioFile : [],
      genaudioFile : [],
      situpdfFile:[],
      taskpdfFile:[],
      actpdfFile:[],
      respdfFile:[],
      genpdfFile:[],
      genvideochng:false,
      situvideochng:false,
      taskvideochng:false,
      actvideochng:false,
      resvideochng:false,
      situimgShow : false,
      taskimgShow : false,
      actimgShow : false,
      resimgShow : false,
      genimgShow : false,
      situimgpropsShow : false,
      taskimgpropsShow : false,
      actimgpropsShow : false,
      resimgpropsShow : false,
      genimgpropsShow : false,
      situvideopropsShow : false,
      taskvideopropsShow : false,
      actvideopropsShow : false,
      resvideopropsShow : false,
      genvideopropsShow : false,
      situaudiopropsShow : false,
      taskaudiopropsShow : false,
      actaudiopropsShow : false,
      resaudiopropsShow : false,
      genaudiopropsShow : false,
      situdocpropsShow : false,
      taskdocpropsShow : false,
      actdocpropsShow : false,
      resdocpropsShow : false,
      gendocpropsShow : false,
      genvideo:[],
      situvideo:[],
      taskvideo:[],
      actvideo:[],
      resvideo:[],
      situpdfFileLink:[],
      taskpdfFileLink:[],
      actpdfFileLink:[],
      respdfFileLink:[],
      genpdfFileLink:[],
      genvideoLink:[],
      situvideoLink:[],
      taskvideoLink:[],
      actvideoLink:[],
      resvideoLink:[],
      situaudioFileLink:[],
      taskaudioFileLink:[],
      actaudioFileLink:[],
      resaudioFileLink:[],
      genaudioFileLink:[],
      webcamBase64:[],
      webcamAwsLink:'',
      audioName:'',
      docName:'',
      Link:[],
      images_length:'',
      addLocation:false,
      videoClose:false,
      expId:'',

      toggleindex:1,
      showstarframework:false,
      indexarray : ['','situation', 'task','action','result'],
      inputtoggletext:[false,false,false, false,false]


    };
        associationcmplt().then(res=>{
         
          this.setState({
            getassociation:res.data.association
          });
         
        });
        
        // UserlistTag().then(res=>{
        //   this.setState({
        //     gettaguser:res['data']['data']
        //   });
        //   this.state.gettaguser.forEach(el=>{
        //     this.state.suggestions.push({id:el.user_id,name:el.user_fname+' '+el.user_lname});
        //   })
    
        //   });


          DomainTag().then(res=>{
            console.log(res['data']['data']);
            this.setState({
              gettaguser:res['data']['data']
            });
            
            this.state.gettaguser.forEach(el=>{
              this.state.suggestions.push({id:el.user_exp_dom_id,name:el.user_exp_domain });
            })
            
            console.log(this.state.suggestions);
    
            
            });

      
     
       this.handlesubmit = this.handlesubmit.bind(this);
       this.togglestarframework = this.togglestarframework.bind(this);
       this.toggletext= this.toggletext.bind(this);

       educationType().then(res=>{
        this.setState({educationTypes:res.data.data});
       
    })
   
      }

      isOpenTag = (e)=>{
        const currentstate=this.state.tagdomain;
        this.setState({tagdomain:!currentstate});
      }

      togglestarframework() {
        const currentState=this.state.showstarframework;
        this.setState({ showstarframework: !currentState});
        
      }

      changeInput = (index,id)=>{
        const currentState = index;
    this.setState({ step: currentState,toggleindex:index });
    localStorage.setItem('step',currentState);

        switch(id){
          case 'title' :
          this.setState({addTitleInput:!this.state.addTitleInput});
          break;
          case 'situation' :
          this.setState({situationInput:!this.state.situationInput});
          this.setState({taskInput:false});
          this.setState({actionInput:false});
          this.setState({resultInput:false});
          break;
          case 'task' :
          this.setState({taskInput:!this.state.taskInput});
          this.setState({situationInput:false});
          this.setState({actionInput:false});
          this.setState({resultInput:false});
          break;
          case 'action' :
          this.setState({actionInput:!this.state.actionInput});
          this.setState({situationInput:false});
          this.setState({taskInput:false});
          this.setState({resultInput:false});
          break;
          case 'result' :
          this.setState({resultInput:!this.state.resultInput});
          this.setState({situationInput:false});
          this.setState({actionInput:false});
          this.setState({taskInput:false});
          break;
          case 'description':
            this.setState({situationInput:false});
            this.setState({actionInput:false});
            this.setState({taskInput:false});
            this.setState({resultInput:false});
          break;
        }
        
      }

      changeaddInput = ()=>{
        this.props.clearVideo();
        this.setState({addTitleInput:!this.state.addTitleInput});
      }
   
    componentWillReceiveProps(nextProps){
      console.log(nextProps);
      this.setState({webcamBase64:nextProps.videodata});
      fileUpload(nextProps.blobvideodata,'expWebcam').then((awsLink)=>{
        this.setState({ webcamAwsLink:awsLink});
     })
      if(nextProps.show)
      {
        this.setState({expId:nextProps.dataobject.expId});
        this.setState({ ModalShow:true});
        // let taguesr=[];
        // nextProps.dataobject.user_tagged.map(el=>{
        // taguesr= [].concat(taguesr,{id:el.id,name:el.f_name+" "+el.l_name});

        let taguesr=[];
        nextProps.dataobject.Tagged_Domain.map(el=>{
        taguesr= [].concat(taguesr,{id:el.id,name:el.name});
        
       
      }
        )
        this.setState({
          mas_id:nextProps.dataobject.expId,
          assoc:nextProps.dataobject.organization,
          situtatonmpic:nextProps.dataobject.situation_image_files,
          imagetask:nextProps.dataobject.task_image_files,
          imagesolution:nextProps.dataobject.result_image_files,
          imageaction:nextProps.dataobject.action_image_files,
          tags:taguesr, 
          userImg:nextProps.dataobject.user_img,
          situation_video:nextProps.dataobject.situation_video,
          task_video:nextProps.dataobject.task_video,
          action_video:nextProps.dataobject.action_video,
          result_video:nextProps.dataobject.result_video,
          user_video:nextProps.dataobject.user_video,
          situation_audio: nextProps.dataobject.situation_audio,
          action_audio:nextProps.dataobject.action_audio,
          task_audio:nextProps.dataobject.task_audio,
          result_audio:nextProps.dataobject.result_audio,
          user_audio:nextProps.dataobject.user_audio,
          situation_doc:nextProps.dataobject.situation_doc,
          task_doc:nextProps.dataobject.task_doc,
          action_doc:nextProps.dataobject.action_doc,
          result_doc:nextProps.dataobject.result_doc,
          user_doc:nextProps.dataobject.user_doc,
          location:nextProps.dataobject.location
        })
        console.log(nextProps.dataobject);
      if(!this.state.openMic)
      {
       this.setState({
        title:nextProps.dataobject.expTitle,
        summery:nextProps.dataobject.Summary,
        situation:nextProps.dataobject.situationText,
        task:nextProps.dataobject.taskText,
        action:nextProps.dataobject.actionText,
        result:nextProps.dataobject.resultText
       }) 
      }
      if(nextProps.dataobject.situation_image_files.length>0)
      {
        this.setState({ situimagevalueLink:nextProps.dataobject.situation_image_files});
        this.setState({situimgpropsShow:true});
      }
      if(nextProps.dataobject.task_image_files.length>0)
      {
        this.setState({ taskimagevalueLink:nextProps.dataobject.task_image_files});
        this.setState({taskimgpropsShow:true});
      }
      if(nextProps.dataobject.result_image_files.length>0)
      {
        this.setState({ resimagevalueLink:nextProps.dataobject.result_image_files});
        this.setState({resimgpropsShow:true});
      }
      if(nextProps.dataobject.action_image_files.length>0)
      {
        this.setState({ actimagevalueLink:nextProps.dataobject.action_image_files});
        this.setState({actimgpropsShow:true});
      }
      if(nextProps.dataobject.user_img.length>0)
      {
        this.setState({ imagevalueLink:nextProps.dataobject.user_img});
        this.setState({genimgpropsShow:true});
      }
      if(!this.state.situvideochng && !this.state.situvideoShow && nextProps.dataobject.situation_video.length>0)
      {
        this.setState({ situvideoLink:nextProps.dataobject.situation_video});
        this.setState({situvideopropsShow:true});
      }
      if(!this.state.taskvideochng && !this.state.taskvideoShow && nextProps.dataobject.task_video.length>0)
      {
        this.setState({ taskvideoLink:nextProps.dataobject.task_video});
        this.setState({taskvideopropsShow:true});
      }
      if(!this.state.actvideochng && !this.state.actvideoShow && nextProps.dataobject.action_video.length>0)
      {
        this.setState({ actvideoLink:nextProps.dataobject.action_video});
        this.setState({actvideopropsShow:true});
      }
      if(!this.state.resvideochng && !this.state.resvideoShow && nextProps.dataobject.result_video.length>0)
      {
        this.setState({ resvideoLink:nextProps.dataobject.result_video});
        this.setState({resvideopropsShow:true});
      }
      if(!this.state.genvideochng && !this.state.videoShow && nextProps.dataobject.user_video.length>0)
      {
            this.setState({genvideoLink:nextProps.dataobject.user_video});
            this.setState({genvideopropsShow:true});
      }
      if(nextProps.dataobject.situation_audio.length>0)
      {
        this.setState({ situaudioFileLink:nextProps.dataobject.situation_audio});
        let audioName=nextProps.dataobject.situation_audio[0].split('/');
        this.setState({audioName:audioName[4]});
        this.setState({situaudiopropsShow:true});
      }
      if(nextProps.dataobject.task_audio.length>0)
      {
        this.setState({ taskaudioFileLink:nextProps.dataobject.task_audio});
        let audioName=nextProps.dataobject.task_audio[0].split('/');
        this.setState({audioName:audioName[4]});
        this.setState({taskaudiopropsShow:true});
      }
      if(nextProps.dataobject.action_audio.length>0)
      {
        this.setState({ actaudioFileLink:nextProps.dataobject.action_audio});
        let audioName=nextProps.dataobject.action_audio[0].split('/');
        this.setState({audioName:audioName[4]});
        this.setState({actaudiopropsShow:true});
      }
      if(nextProps.dataobject.result_audio.length>0)
      {
        this.setState({ resaudioFileLink:nextProps.dataobject.result_audio});
        let audioName=nextProps.dataobject.result_audio[0].split('/');
        this.setState({audioName:audioName[4]});
        this.setState({resaudiopropsShow:true});
      }
      if(nextProps.dataobject.user_audio.length>0)
      {
        this.setState({ genaudioFileLink:nextProps.dataobject.user_audio});
        let audioName=nextProps.dataobject.user_audio[0].split('/');
        this.setState({audioName:audioName[4]});
        this.setState({genaudiopropsShow:true});
      }
      if(nextProps.dataobject.situation_doc.length>0)
      {
        this.setState({situpdfFileLink:nextProps.dataobject.situation_doc});
        let docName=nextProps.dataobject.situation_doc[0].split('/');
        this.setState({docName:docName[4]});
        this.setState({situdocpropsShow:true});
      }
      if(nextProps.dataobject.task_doc.length>0)
      {
        this.setState({taskpdfFileLink:nextProps.dataobject.task_doc});
        let docName=nextProps.dataobject.task_doc[0].split('/');
        this.setState({docName:docName[4]});
        this.setState({taskdocpropsShow:true});
      }
      if(nextProps.dataobject.action_doc.length>0)
      {
        this.setState({actpdfFileLink:nextProps.dataobject.action_doc});
        let docName=nextProps.dataobject.action_doc[0].split('/');
        this.setState({docName:docName[4]});
        this.setState({actdocpropsShow:true});
      }
      if(nextProps.dataobject.result_doc.length>0)
      {
        this.setState({respdfFileLink:nextProps.dataobject.result_doc});
        let docName=nextProps.dataobject.result_doc[0].split('/');
        this.setState({docName:docName[4]});
        this.setState({resdocpropsShow:true});
      }
      if(nextProps.dataobject.user_doc.length>0)
      {
        this.setState({genpdfFileLink:nextProps.dataobject.user_doc});
        let docName=nextProps.dataobject.user_doc[0].split('/');
        this.setState({docName:docName[4]});
        this.setState({gendocpropsShow:true});
      }
    }
      else
      {
        this.setState({ ModalShow:false});
        this.setState({situimgpropsShow:false,situimgShow:false,situimagevalueLink:[],taskimgpropsShow:false,taskimgShow:false,taskimagevalueLink:[],actimgpropsShow:false,actimgShow:false,actimagevalueLink:[],resimgpropsShow:false,resimgShow:false,resimagevalueLink:[],genimgpropsShow:false,genimgShow:false,imagevalueLink:[]});
        this.setState({videoShow:false,genvideochng:false,genvideoLink:[],resvideoShow:false,resvideochng:false,resvideoLink:[],situvideoShow:false,situvideochng:false,situvideoLink:[],actvideoShow:false,actvideochng:false,actvideoLink:[],taskvideoShow:false,taskvideochng:false,taskvideoLink:[]});
        this.setState({situaudiopropsShow:false,situaudioShow:false,situaudioFileLink:[],taskaudiopropsShow:false,taskaudioShow:false,taskaudioFileLink:[],actaudiopropsShow:false,actaudioFileLink:[],actaudioShow:false,resaudioFileLink:[],resaudioShow:false,resaudiopropsShow:false,genaudioFileLink:[],genaudioShow:false,genaudiopropsShow:false});
        this.setState({situdocpropsShow:false,situdocShow:false,situpdfFileLink:[],taskdocpropsShow:false,taskdocShow:false,taskpdfFileLink:[],actdocpropsShow:false,actdocShow:false,actpdfFileLink:[],resdocpropsShow:false,resdocShow:false,respdfFileLink:[],gendocpropsShow:false,gendocShow:false,genpdfFileLink:[]});
      }
if(this.state.openMic)
{
      if(nextProps.text.textboxId == 'editsummery')
      {
        this.setState({summery:nextProps.text.text});
       // console.log(this.state.summery);
      }
      else if(nextProps.text.textboxId == 'editsituation')
            {
              this.setState({situation :nextProps.text.text});
              console.log(this.state.situation);
            } 
            else if(nextProps.text.textboxId == 'edittask')
                  {
                    this.setState({task :nextProps.text.text});
                  }
                  else if(nextProps.text.textboxId == 'editaction')
                  {
                    this.setState({action :nextProps.text.text});
                  }
                  else if(nextProps.text.textboxId == 'editresult')
                  {
                    this.setState({result :nextProps.text.text});
                  }
                  else if(nextProps.text.textboxId == 'edittitle')
                  {
                    this.setState({title :nextProps.text.text});
                  }

}
      
    }
    
    handlesummary = (e) =>{
      this.setState({summery:e.target.value});
    }
    handletitle = (e) =>{
      this.setState({title:e.target.value});
    }
    handlesituation = (e) =>{
      this.setState({situation:e.target.value});
    }
    handletask = (e) =>{
      this.setState({task:e.target.value});
    }
    handleaction = (e) =>{
      this.setState({action:e.target.value});
    }
    handleresult = (e) =>{
      this.setState({result:e.target.value});
    }
    handlelocation = (e) =>{
      this.setState({location:e.target.value});
    }
    openMic = (id) =>{
      this.setState({openMic : !this.state.openMic})
      this.setState({textboxId : id})
      //console.log(statename);
      // this.setState({[statename] : ''},console.log(this.state))
      
    }
    openCam = () =>{
      this.props.clearVideo();
      this.setState({openCam : !this.state.openCam});
      if(!this.state.situationInput &&!this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
        this.setState({ genvideoLink:[] });
        this.setState({genvideochng:false});
        this.setState({videoShow: true});
        this.setState({situvideoShow: false});
        this.setState({taskvideoShow: false});
        this.setState({actvideoShow: false});
        this.setState({resvideoShow: false});
        this.state.videovalue.pop();
        this.state.genvideo.pop();
      }
      else if(this.state.situationInput &&!this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
        this.setState({ situvideoLink:[] });
        this.setState({situvideochng:false});
        this.setState({situvideoShow: true});
        this.setState({videoShow: false});
        this.setState({taskvideoShow: false});
        this.setState({actvideoShow: false});
        this.setState({resvideoShow: false});
        this.state.situvideovalue.pop();
        this.state.situvideo.pop();
      }
      else if(!this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
        this.setState({ taskvideoLink:[] });
        this.setState({taskvideochng:false});
        this.setState({taskvideoShow: true});
        this.setState({situvideoShow: false});
        this.setState({videoShow: false});
        this.setState({actvideoShow: false});
        this.setState({resvideoShow: false});
        this.state.taskvideovalue.pop();
        this.state.taskvideo.pop();
      }
      else if(!this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput)
      {
        this.setState({ actvideoLink:[] });
        this.setState({actvideochng:false});
        this.setState({actvideoShow: true});
        this.setState({situvideoShow: false});
        this.setState({taskvideoShow: false});
        this.setState({videoShow: false});
        this.setState({resvideoShow: false});
        this.state.actvideovalue.pop();
        this.state.actvideo.pop();
      }
      else if(!this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput)
      {
        this.setState({ resvideoLink:[] });
        this.setState({resvideochng:false});
        this.setState({resvideoShow: true});
        this.setState({videoShow: false});
        this.setState({taskvideoShow: false});
        this.setState({actvideoShow: false});
        this.setState({situvideoShow: false});
        this.state.resvideovalue.pop();
        this.state.resvideo.pop();
      }
  } 
  
    
    
            // onDeletedomain (i) {
            //   const  tagsDomain = this.state. tagsDomain.slice(0)
            //   tagsDomain.splice(i, 1)
            //   this.setState({  tagsDomain })
            //  // console.log(this.state.tags);
            // }
            // onAdditiondomain (tagd) {
            //   const  tagsDomain = [].concat(this.state. tagsDomain, tagd)
            //   this.setState({  tagsDomain })
            //   setTimeout(() => {
            //   //  console.log(this.state.tagsDomain);
            //   }, 2000);
            // }
            onDelete(i) {
              const tags = this.state.tags.slice(0)
              tags.splice(i, 1)
              this.setState({ tags })
              // console.log(this.state.tags);
            }
          
            onAddition(tag) {
              console.log(this.state.tags);
              const tags = [].concat(this.state.tags, tag)
              this.setState({ tags })
              setTimeout(() => {
                // console.log(this.state.tags);
              }, 2000);
          
            }
            audioChange = (e) =>{
              let link=[];
              let audioTemoArray=[];
              e.preventDefault();
              for (let i = 0; i < e.target.files.length; i++) {
                let file = e.target.files[i];
                fileUpload(file,'experience').then((awsLink)=>{
                  let audioName=awsLink.split('/');
                  this.setState({audioName:audioName[4]});
                  link.push(awsLink);
                  console.log(link)
                let reader = new FileReader();
                reader.readAsDataURL(file);
                const scope = this
                reader.onload = function(event) {
                  //console.log(event.target.result);
                  //imageTemoArray.push(reader.result);
                  audioTemoArray.push(event.target.result);
                  if(scope.state.situationInput &&!scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
                  {
                    scope.setState({ situaudioFile:audioTemoArray})
                    scope.state.situaudioFileLink[i]=link[i];
                    console.log(scope.state.situaudioFile.length);
                    if(scope.state.situaudioFile.length>0)
                    {
                    scope.setState({situaudioShow: true});
                    }
                  //scope.dataimageObject=scope.state.audioFile;
                  //onsole.log(this.state.audioFile);
                    }
                    else  if(!scope.state.situationInput && scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
                    {
                      scope.setState({ taskaudioFile:audioTemoArray})
                      scope.state.taskaudioFileLink[i]=link[i];
                      console.log(scope.state.taskaudioFile.length);
                      if(scope.state.taskaudioFile.length>0)
                      {
                      scope.setState({taskaudioShow: true});
                      }
                    //scope.dataimageObject=scope.state.audioFile;
                    //onsole.log(this.state.audioFile);
                    }
                    else  if(!scope.state.situationInput && !scope.state.taskInput && scope.state.actionInput && !scope.state.resultInput)
                    {
                      scope.setState({ actaudioFile:audioTemoArray})
                      scope.state.actaudioFileLink[i]=link[i];
                      //console.log(scope.state.taskaudioFile.length);
                      if(scope.state.actaudioFile.length>0)
                      {
                      scope.setState({actaudioShow: true});
                      }
                    //scope.dataimageObject=scope.state.audioFile;
                    //onsole.log(this.state.audioFile);
                    }
                    else  if(!scope.state.situationInput && !scope.state.taskInput && !scope.state.actionInput && scope.state.resultInput)
                    {
                      scope.setState({ resaudioFile:audioTemoArray})
                      scope.state.resaudioFileLink[i]=link[i];
                      //console.log(scope.state.taskaudioFile.length);
                      if(scope.state.resaudioFile.length>0)
                      {
                      scope.setState({resaudioShow: true});
                      }
                    //scope.dataimageObject=scope.state.audioFile;
                    //onsole.log(this.state.audioFile);
                    }
                    else if(!scope.state.situationInput && !scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
                    {
                      scope.setState({ genaudioFile:audioTemoArray})
                      scope.state.genaudioFileLink[i]=link[i];
                      if(scope.state.genaudioFile.length>0)
                      {
                      scope.setState({genaudioShow: true});
                      }
                    //scope.dataimageObject=scope.state.audioFile;
                    //onsole.log(this.state.audioFile);
                    }
                }
              })
            }
            }

            imageChange = (e)=>{
              let imageTemoArray=[];
              let link=[];
              e.preventDefault();
              for (let i = 0; i < e.target.files.length; i++) {
                let file = e.target.files[i];
                let reader = new FileReader();
                reader.readAsDataURL(file);
               
                const scope = this
                reader.onload = function(event) {
                imageTemoArray.push(event.target.result);
                fileUpload(file,'experience').then((awsLink)=>{
                  console.log(awsLink);
                  link[i]=awsLink;
              if(scope.state.situationInput &&!scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
               {
                    scope.setState({ situimagevalue:imageTemoArray},console.log(scope.state.situimagevalue))
                    if(scope.state.situimagevalueLink.length>0)
                    {
                      let index=scope.state.situimagevalueLink.length;
                      scope.state.situimagevalueLink[index]=link[i];
                      index=index+1;
                    }
                    else
                    {
                      scope.state.situimagevalueLink[i]=link[i];
                    }
                    if(scope.state.situimagevalue.length>0)
                    {
                    scope.setState({situimgShow: true});
                    }
                
              }else if(scope.state.taskInput && !scope.state.situationInput && !scope.state.actionInput && !scope.state.resultInput)
                {
                      scope.setState({ taskimagevalue:imageTemoArray},console.log(scope.state.taskimagevalue))
                      if(scope.state.taskimagevalueLink.length>0)
                        {
                          let index=scope.state.taskimagevalueLink.length;
                          scope.state.taskimagevalueLink[index]=link[i];
                          index=index+1;
                        }
                        else
                        {
                          scope.state.taskimagevalueLink[i]=link[i];
                        }
                      if(scope.state.taskimagevalue.length>0)
                      {
                        scope.setState({taskimgShow: true});
                      }

                 }else if(scope.state.actionInput &&!scope.state.taskInput && !scope.state.situationInput && !scope.state.resultInput)
                 {
                        scope.setState({ actimagevalue:imageTemoArray},console.log(scope.state.actimagevalue))
                        if(scope.state.actimagevalueLink.length>0)
                        {
                          let index=scope.state.actimagevalueLink.length;
                          scope.state.actimagevalueLink[index]=link[i];
                          index=index+1;
                        }
                        else
                        {
                          scope.state.actimagevalueLink[i]=link[i];
                        }
                        if(scope.state.actimagevalue.length>0)
                        {
                        scope.setState({actimgShow: true});
                        }
                  }else if(scope.state.resultInput &&!scope.state.situationInput && !scope.state.actionInput && !scope.state.taskInput)
                  {
                        scope.setState({ resimagevalue:imageTemoArray},console.log(scope.state.resimagevalue))
                        if(scope.state.resimagevalueLink.length>0)
                          {
                            let index=scope.state.resimagevalueLink.length;
                            scope.state.resimagevalueLink[index]=link[i];
                            index=index+1;
                          }
                          else
                          {
                            scope.state.resimagevalueLink[i]=link[i];
                          }
                        if(scope.state.resimagevalue.length>0)
                        {
                          scope.setState({resimgShow: true});
                        }
                   }else if(!scope.state.resultInput &&!scope.state.situationInput && !scope.state.actionInput && !scope.state.taskInput) 
                   {
                          scope.setState({ imagevalue:imageTemoArray})
                          console.log(scope.state.imagevalueLink)
                          if(scope.state.imagevalueLink.length>0)
                          {
                            let index=scope.state.imagevalueLink.length;
                            scope.state.imagevalueLink[index]=link[i];
                            index=index+1;
                          }
                          else
                          {
                            scope.state.imagevalueLink[i]=link[i];
                            console.log(scope.state.imagevalueLink);
                          }
                          if(scope.state.imagevalue.length>0)
                        {
                          scope.setState({genimgShow: true});
                        }
                   }
                  })
                }
              }
              
              
            }
            videoChange = (e)=>{
              let videoTemoArray=[];
              let link=[];
               e.preventDefault();
               for (let i = 0; i < e.target.files.length; i++) {
                 let file = e.target.files[i];
                 fileUpload(file,'experience').then((awsLink)=>{
                   link.push(awsLink);
               let reader = new FileReader();
               reader.readAsDataURL(file);
              
               const scope = this
               reader.onload = function(event) {
                 scope.props.startLoader();
                 videoTemoArray.pop();
                 videoTemoArray.push(event.target.result);
                 if(!scope.state.situationInput &&!scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
                  {
                   scope.props.clearVideo();
                   scope.state.genvideo.pop();
                   scope.state.videovalue.pop();
                   scope.setState({videoShow: false});
                   scope.setState({genvideochng:true});
                   scope.setState({genvideopropsShow:false});
                   scope.setState({ videovalue:videoTemoArray},console.log(scope.state.videovalue))
                   scope.state.genvideoLink.pop();
                   console.log(link);
                   scope.state.genvideoLink[i]=link[i];
                   console.log(scope.state.genvideoLink);
                   scope.props.hideLoader();
                   console.log(scope.state.videovalue);
                   //scope.dataimageObject=scope.state.videovalue;
                  }
                  else  if(scope.state.situationInput && !scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
                  {
                   scope.props.clearVideo();
                   scope.state.situvideo.pop();
                   scope.state.situvideovalue.pop();
                   scope.setState({situvideoShow: false});
                   scope.setState({situvideochng:true});
                   scope.setState({situvideopropsShow:false});
                   scope.setState({ situvideovalue:videoTemoArray},console.log(scope.state.situvideovalue))
                   scope.state.situvideoLink.pop();
                   scope.state.situvideoLink[i]=link[i];
                   scope.props.hideLoader();
                   console.log(scope.state.situvideovalue);
                   //scope.dataimageObject=scope.state.videovalue;
                  }
                  else  if(!scope.state.situationInput && scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
                  {
                   scope.props.clearVideo();
                   scope.state.taskvideo.pop();
                   scope.state.taskvideovalue.pop();
                   scope.setState({taskvideoShow: false});
                   scope.setState({taskvideochng:true});
                   scope.setState({taskvideopropsShow:false});
                   scope.setState({ taskvideovalue:videoTemoArray},console.log(scope.state.taskvideovalue))
                   scope.state.taskvideoLink.pop();
                   scope.state.taskvideoLink[i]=link[i];
                   scope.props.hideLoader();
                   console.log(scope.state.taskvideovalue);
                   //scope.dataimageObject=scope.state.videovalue;
                  }
                  else  if(!scope.state.situationInput && !scope.state.taskInput && scope.state.actionInput && !scope.state.resultInput)
                  {
                   scope.props.clearVideo();
                   scope.state.actvideo.pop();
                   scope.state.actvideovalue.pop();
                   scope.setState({actvideoShow: false});
                   scope.setState({actvideochng:true});
                   scope.setState({actvideopropsShow:false});
                   scope.setState({ actvideovalue:videoTemoArray});
                   scope.state.actvideoLink.pop();
                   scope.state.actvideoLink[i]=link[i];
                   scope.props.hideLoader();
                   console.log(scope.state.actvideovalue);
                   //scope.dataimageObject=scope.state.videovalue;
                  }
                  else  if(!scope.state.situationInput && !scope.state.taskInput && !scope.state.actionInput && scope.state.resultInput)
                  {
                   scope.props.clearVideo();
                   scope.state.resvideo.pop();
                   scope.state.resvideovalue.pop();
                   scope.setState({resvideoShow: false});
                   scope.setState({resvideochng:true});
                   scope.setState({resvideopropsShow:false});
                   scope.setState({ resvideovalue:videoTemoArray},console.log(scope.state.resvideovalue))
                   scope.state.resvideoLink.pop();
                   scope.state.resvideoLink[i]=link[i];
                   scope.props.hideLoader();
                   console.log(scope.state.resvideovalue);
                   //scope.dataimageObject=scope.state.videovalue;
                  }
               }
              })
             }
             
            
             }
            locationChange=(e)=>
             {
      this.setState({addLocation:!this.state.addLocation});
               }

        toggletext(index)
              {
                let temp = this.state.inputtoggletext;
                temp[index] = !this.state.inputtoggletext[index];
                
                this.setState({inputtoggletext:temp});
                console.log(this.state.inputtoggletext);
              }  
 
            
               pdfChange = (e) =>{
                let pdfTemoArray=[];
                let link=[];
                for (let i = 0; i < e.target.files.length; i++) {
                let file = e.target.files[i];
                this.setState({docName:file.name});
                fileUpload(file,'experience').then((awsLink)=>{
                  let docName=awsLink.split('/');
                  console.log(docName);
                  this.setState({docName:docName[4]});
                  link.push(awsLink);
                let reader = new FileReader();
                reader.readAsDataURL(file);
                const scope = this
                reader.onload = function(event) {
                  console.log(event.target.result);
                  pdfTemoArray.push(event.target.result);
                  if(scope.state.situationInput &&!scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
                  {
                  scope.setState({situpdfFile: pdfTemoArray});
                  scope.state.situpdfFileLink[i]=link[i];
                  //console.log(scope.state.pdfFile);
                  if(scope.state.situpdfFile.length>0)
                  {
                    scope.setState({situdocShow: true});
                  }
                }
                else if(!scope.state.situationInput && scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
                {
                scope.setState({taskpdfFile: pdfTemoArray});
                scope.state.taskpdfFileLink[i]=link[i];
                //console.log(scope.state.pdfFile);
                if(scope.state.taskpdfFile.length>0)
                {
                  scope.setState({taskdocShow: true});
                }
              }
              else if(!scope.state.situationInput && !scope.state.taskInput && scope.state.actionInput && !scope.state.resultInput)
              {
              scope.setState({actpdfFile: pdfTemoArray});
              scope.state.actpdfFileLink[i]=link[i];
              //console.log(scope.state.pdfFile);
              if(scope.state.actpdfFile.length>0)
              {
                scope.setState({actdocShow: true});
              }
            }
            else if(!scope.state.situationInput && !scope.state.taskInput && !scope.state.actionInput && scope.state.resultInput)
            {
            scope.setState({respdfFile: pdfTemoArray});
            scope.state.respdfFileLink[i]=link[i];
            //console.log(scope.state.pdfFile);
            if(scope.state.respdfFile.length>0)
            {
              scope.setState({resdocShow: true});
            }
            }
            else if(!scope.state.situationInput && !scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
            {
            scope.setState({genpdfFile: pdfTemoArray});
            scope.state.genpdfFileLink[i]=link[i];
            //console.log(scope.state.pdfFile);
            if(scope.state.genpdfFile.length>0)
            {
              scope.setState({gendocShow: true});
            }
            }
            
                }
              })
              }
              }
             
            
        deleteExpList = (id)=>{
          this.setState({ ModalShow:false});
          confirmAlert({
              title: 'Confirm to Delete',
              message: 'Are you sure to do this.',
              buttons: [
                {
                  label: 'Yes',
                  onClick: () => {
                    this.props.onHide();
                      this.props.startLoader();
                      deleteExpList(id).then(res=>{
                          if(res.data.status){
                              toast.success(res.data.msg,{autoClose:5000});
                              window.location.reload();
                             
                          }else{
                              toast.error(res.data.msg,{autoClose:5000});
                          }
                        //   workList().then(res=>{
                        //       this.setState({worklist:res.data.data});
                        //  })
                      })
                  }
                },
                {
                  label: 'No',
                  onClick: () => console.log("not deleted")
                }
              ]
            });
      }
      
    
    handlesubmit(){
      this.props.onHide();
      if(this.state.title !='' && this.state.summary != ''){
        this.props.startLoader();
        if(this.state.title !='' ){
            EditUserExp(this.state.mas_id,this.state.title,this.state.situation,this.state.task,this.state.action,this.state.result,this.state.summery,this.state.tags,this.state.situimagevalueLink,this.state.taskimagevalueLink,this.state.actimagevalueLink,this.state.resimagevalueLink,this.state.imagevalueLink,this.state.genvideoLink,this.state.situvideoLink,this.state.taskvideoLink,this.state.actvideoLink,this.state.resvideoLink,this.state.genaudioFileLink,this.state.situaudioFileLink,this.state.taskaudioFileLink,this.state.actaudioFileLink,this.state.resaudioFileLink,this.state.genpdfFileLink,this.state.situpdfFileLink,this.state.taskpdfFileLink,this.state.actpdfFileLink,this.state.respdfFileLink,this.state.location).then(res=>{
            toast.success(res.data.msg);
            window.location.reload();
            this.props.hideLoader();
              
            });
           }else{
            this.props.hideLoader();
             toast.error('Please enter experience title')
             toast.error('Please enter Summary')
           }
      }

     }
    

    render(props){
      const situimages = [];
      const taskimages = [];
      const actionimages = [];
      const resultimages = [];
      const images = [];
  
      if(this.state.situimagevalueLink.length>0)
      {
            this.state.situimagevalueLink.forEach(el=>{
            situimages.push({url:el});
            })
      }
      if(this.state.taskimagevalueLink.length>0)
      {
            this.state.taskimagevalueLink.forEach(el=>{
            taskimages.push({url:el});
            })
      }
      if(this.state.actimagevalueLink.length>0)
      {
            this.state.actimagevalueLink.forEach(el=>{
            actionimages.push({url:el});
            })
      }
      if(this.state.resimagevalueLink.length>0)
      {
            this.state.resimagevalueLink.forEach(el=>{
            resultimages.push({url:el});
            })
      }
      if(this.state.imagevalueLink.length>0)
      {
            this.state.imagevalueLink.forEach(el=>{
            images.push({url:el});
            })
      }
 
      if(this.state.videoShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
         if(this.state.webcamBase64!='')
         {
          this.state.genvideo[0]=this.state.webcamBase64;
          this.state.genvideoLink.pop();
          this.state.genvideoLink[0]=this.state.webcamAwsLink;
         }
      }
      else if(this.state.genvideochng && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
        if(this.state.videovalue.length>0)
        {
        this.state.videovalue.forEach(el=>{
          this.state.genvideo[0]=el;
        })
        }
        
      }
      else if(!this.state.videoShow && !this.state.genvideochng)
      {
        if(this.state.user_video.length>0)
        {
          this.state.user_video.forEach(el=>{
            this.state.genvideo[0]=el;
          })
        }
      }
      if(this.state.situvideoShow && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
        if(this.state.webcamBase64!='')
        {
         this.state.situvideo[0]=this.state.webcamBase64;
         this.state.situvideoLink.pop();
         this.state.situvideoLink[0]=this.state.webcamAwsLink;
        }
      }
      else if(this.state.situvideochng && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
        console.log(this.state.situvideovalue);
        if(this.state.situvideovalue.length>0)
        {
        this.state.situvideovalue.forEach(el=>{
          this.state.situvideo[0]=el;
        })
        }
      }
      else if(!this.state.situvideoShow && !this.state.situvideochng)
      {
        if(this.state.situation_video.length>0)
        {
          this.state.situation_video.forEach(el=>{
            this.state.situvideo[0]=el;
          })
        }
      }
      if(this.state.taskvideoShow && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
        if(this.state.webcamBase64!='')
        {
         this.state.taskvideo[0]=this.state.webcamBase64;
         this.state.taskvideoLink.pop();
         this.state.taskvideoLink[0]=this.state.webcamAwsLink;
        }
      }
      else if(this.state.taskvideochng && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
        console.log(this.state.taskvideovalue);
        if(this.state.taskvideovalue.length>0)
        {
        this.state.taskvideovalue.forEach(el=>{
          this.state.taskvideo[0]=el;
        })
        }
      }
      else if(!this.state.taskvideochng && this.state.taskvideoShow)
      {
        if(this.state.task_video.length>0)
        {
          this.state.task_video.forEach(el=>{
            this.state.taskvideo[0]=el;
          })
        }
      }
      if(this.state.actvideoShow && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput)
      {
        if(this.state.webcamBase64!='')
        {
         this.state.actvideo[0]=this.state.webcamBase64;
         this.state.actvideoLink.pop();
         this.state.actvideoLink[0]=this.state.webcamAwsLink;
        }
      }
      else if(this.state.actvideochng && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput)
      {
        console.log(this.state.actvideovalue);
        if(this.state.actvideovalue.length>0)
        {
        this.state.actvideovalue.forEach(el=>{
          this.state.actvideo[0]=el;
        })
        }
      }
      else if(!this.state.actvideoShow && !this.state.actvideochng)
        {
          if(this.state.action_video.length>0)
          {
            this.state.action_video.forEach(el=>{
              this.state.actvideo[0]=el;
            })
          }
        }
      if( this.state.resvideoShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput)
      {
        if(this.state.webcamBase64!='')
        {
         this.state.resvideo[0]=this.state.webcamBase64;
         this.state.resvideoLink.pop();
         this.state.resvideoLink[0]=this.state.webcamAwsLink;
        }
      }
      else if(this.state.resvideochng && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput)
      {
        if(this.state.resvideovalue.length>0)
        {
        this.state.resvideovalue.forEach(el=>{
          this.state.resvideo[0]=el;
        })
        }
      }
      else if(!this.state.resvideoShow && !this.state.resvideochng)
      {
        if(this.state.result_video.length>0)
        {
          this.state.result_video.forEach(el=>{
            this.state.resvideo[0]=el;
          })
        }
      }
     

      let requestModalClose =() =>{this.setState({openMic:false})}
      let requestModalCamClose =() =>{this.setState({openCam:false})}
      
   
        return(
         
         
            <Modal
            {...this.props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            show={this.state.ModalShow}
            onHide={this.props.onHide}
            className="raexpmodal"
          >
              
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
              Edit Experience
              </Modal.Title>
              {this.state.showstarframework && 
              <a href="#" className="quckadd" onClick={this.togglestarframework} > Advance Edit </a> 
              }
              {  this.state.showstarframework == false &&
                <a href="#" className="quckadd" onClick={this.togglestarframework}> Quick Edit </a>
               } 
            </Modal.Header>
            <Modal.Body className="palash">
           
            <div>
              {this.state.showstarframework==false &&
                    <>
             
              
               <div className="raiconnext">
               {/* <a  id= "general" className={(this.state.toggleindex=='0'? 'ractive' : '')}  onClick={()=>this.changeInput(0,'description')}><i className="fa fa-child"></i><span>General</span></a> */}
               <a id= "situation" className={(this.state.toggleindex=='1'? 'ractive' : '')}  onClick={()=>this.changeInput(1,'situation')}><i className="fa fa-hourglass-start"></i><span>Situation</span></a>
               <a  id="task" className={(this.state.toggleindex=='2'? 'ractive' : '')}><i onClick={()=>this.changeInput(2,'task')}className="fa fa-tasks"></i><span>Task</span></a>
               <a id="action" className= {(this.state.toggleindex=='3'? 'ractive' : '')}><i onClick={()=>this.changeInput(3,'action')} className="fa fa-handshake-o"></i><span>Action</span></a>
               <a  id="result" className={(this.state.toggleindex=='4'? 'ractive' : '')} ><i onClick={()=>this.changeInput(4,'result')} className="fa fa-trophy"></i><span>Result</span></a>
               </div>

               <div className="raadexp">
               <a onClick={this.openCam} ><VideoCallIcon className="video_icon" /> Video </a>
              <a onClick={()=>this.openMic('exsummery')} ><MicIcon className="audio_icon" /> Audio</a>
              <a onClick={()=>this.toggletext(this.state.toggleindex)} ><TextFieldsIcon className="document_icon"/> Text</a>

              </div>
                </>
               } 
           

{/* 
              <div className="d-flex mb-4">
              <div className="addpic">
              <img src={this.props.profilepic} className="img-fluid" />	
              </div>
              <div className="poption">
        <p>{this.props.auth.user_fname} {this.props.auth.user_lname}</p>
                <div className="form-group">
                <select className="form-control" id="sel1" name="sellist1">
                <option>Private</option>
                <option>Public</option>
                </select>
                </div>
              </div>

              </div> */}
              <div className="ramodalh">
              <form>
              {this.state.showstarframework &&
              <>

               <div className="raadexp">
              <a onClick={this.openCam} className="exvideo"><VideoCallIcon/></a>
              <a onClick={()=>this.openMic('exsummery')} className="exmicro"><MicIcon/></a>
              <a onClick={()=>this.toggletext(0)} className="extext"><TextFieldsIcon/></a>

              </div>


              {this.state.inputtoggletext[0] === true &&
              <div className="form-group raquickadd">
              <textarea  type="text" className="form-control" name="summery" onChange={this.handlesummary} id="exampleFormControlTextarea1" rows="5" defaultValue={this.state.summery} placeholder={"what about your Experience " + this.props.auth.user_fname + "?"}></textarea>
              {/* {isChrome && !isFirefox && !isOpera && !isSafari &&
              <a onClick={()=>this.openMic('editsummery')}><MicIcon className="audiospk"/></a>
              } */}
              {/* <a onClick={this.openCam}><VideoCallIcon className="webcam"/></a> */}
              <a  onClick={()=>this.toggletext(0)}><CloseIcon className="raclose"/></a>
              </div>
              }
              </>
              }

                  { this.state.showstarframework==false  && this.state.inputtoggletext[1] === true && this.state.situationInput && !this.state.openMic &&
                  <div className="form-group raadvaddd">
                  <textarea className="form-control" id="exampleFormControlTextarea1"  name="situation" onChange={this.handlesituation} rows="5" defaultValue={this.state.situation} placeholder={"Describe your Experience Situation " + this.props.auth.user_fname }></textarea>
                  {/* {isChrome && !isFirefox && !isOpera && !isSafari &&
                  <a onClick={()=>this.openMic('editsituation')}><MicIcon className="audiospk"/></a> 
                  } */}

                {/* <a onClick={this.openCam}><VideoCallIcon className="webcam"/></a> */}
                <a onClick={()=>this.toggletext(this.state.toggleindex)}><CloseIcon className="raclose"/></a>
                </div>
              }
              { this.state.showstarframework==false && this.state.inputtoggletext[2] === true && this.state.taskInput && !this.state.openMic &&
              <div className="form-group raadvaddd">
              <textarea className="form-control" id="exampleFormControlTextarea1"  name="task" onChange={this.handletask} rows="5" defaultValue={this.state.task} placeholder={"Describe your Experience Task " + this.props.auth.user_fname }></textarea>
              {/* {isChrome && !isFirefox && !isOpera && !isSafari &&
              <a onClick={()=>this.openMic('edittask')}><MicIcon className="audiospk"/></a>
                } */}
              {/* <a onClick={this.openCam}><VideoCallIcon className="webcam"/></a> */}
              <a onClick={()=>this.toggletext(this.state.toggleindex)}><CloseIcon className="raclose"/></a>
              </div>
            }
            { this.state.showstarframework==false && this.state.inputtoggletext[3] ===true && this.state.actionInput && !this.state.openMic &&
            <div className="form-group raadvaddd">
            <textarea className="form-control" id="exampleFormControlTextarea1"  name="action" onChange={this.handleaction} defaultValue={this.state.action} rows="5" placeholder={"Describe your Experience Action " + this.props.auth.user_fname }></textarea>
            {/* {isChrome && !isFirefox && !isOpera && !isSafari &&
            <a onClick={()=>this.openMic('editaction')}><MicIcon className="audiospk"/></a>
              } */}
            {/* <a onClick={this.openCam}><VideoCallIcon className="webcam"/></a> */}
            <a onClick={()=>this.toggletext(this.state.toggleindex)}><CloseIcon className="raclose"/></a>
            </div>
          }
            {this.state.showstarframework==false && this.state.inputtoggletext[4] === true && this.state.resultInput && !this.state.openMic &&
            <div className="form-group raadvaddd">
            <textarea className="form-control" id="exampleFormControlTextarea1"   name="result" onChange={this.handleresult}rows="5" defaultValue={this.state.result} placeholder={"Describe your Experience Result " + this.props.auth.user_fname }></textarea>
            {/* {isChrome && !isFirefox && !isOpera && !isSafari &&
            <a onClick={()=>this.openMic('editresult')}><MicIcon className="audiospk"/></a>
              } */}
            {/* <a onClick={this.openCam}><VideoCallIcon className="webcam"/></a> */}
            <a onClick={()=>this.toggletext(this.state.toggleindex)}><CloseIcon className="raclose"/></a>
            </div>
          }
              { this.state.showstarframework==false && this.state.addTitleInput && !this.state.openMic &&
              <div className="form-group">
              <textarea className="form-control" id="exampleFormControlInput1"  name="title" onChange={this.handletitle} defaultValue={this.state.title} placeholder="Add your experience title here"/>
              {isChrome && !isFirefox && !isOpera && !isSafari &&
              <a onClick={()=>this.openMic('edittitle')}><MicIcon className="audiospk"/></a>
                }
              {/* <a onClick={this.openCam}><VideoCallIcon className="webcam"/></a> */}
              </div>
            }
</form>
</div>
<div className="radocfile">
{(this.state.situaudiopropsShow && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)||(this.state.situaudioShow && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)?
                    <div className="pa-text">
                    <a><img src={audioImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ situaudioShow: false,situaudiopropsShow:false,situaudioFileLink:[] }) }} /></a> 
                    <div><p>{this.state.audioName}</p></div>
                    </div>
:''}
                    {(this.state.taskaudiopropsShow && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput)||(this.state.taskaudioShow && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput)?
                      <div className="pa-text">
                      <a><img src={audioImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ taskaudioShow: false,taskaudiopropsShow:false,taskaudioFileLink:[] }) }} /></a> 
                      <div><p>{this.state.audioName}</p></div>
                      </div>
                   :''}
                    {(this.state.actaudiopropsShow && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput)||(this.state.actaudioShow && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput)?
                     <div className="pa-text"> 
                      <a><img src={audioImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ actaudioShow: false,actaudiopropsShow:false,actaudioFileLink:[] }) }} /></a>
                      <div><p>{this.state.audioName}</p></div>
                     </div>
                   :''}
                    {(this.state.resaudiopropsShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput)||(this.state.resaudioShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput)?
                    <div className="pa-text">
                      <a><img src={audioImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ resaudioShow: false,resaudiopropsShow:false,resaudioFileLink:[] }) }} /></a>
                      <div><p>{this.state.audioName}</p></div>
                    </div>
                   :''}
                    {(this.state.genaudiopropsShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)||(this.state.genaudioShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)?
                    <div className="pa-text">
                    <a><img src={audioImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ genaudioShow: false,genaudiopropsShow :false,genaudioFileLink:[] }) }} /></a>
                    <div><p>{this.state.audioName}</p></div>
                    </div>
                   :''}
                  {(this.state.situdocpropsShow && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)||(this.state.situdocShow && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)?
                    <div className="pa-text">
                    <a><img src={docImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ situdocShow: false,situdocpropsShow:false,situpdfFileLink:[] }) }} /></a>
                    <div><p>{this.state.docName}</p></div>
                    </div>
      :''} 
      {(this.state.taskdocpropsShow && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput)||(this.state.taskdocShow && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput)?
      <div className="pa-text">
      <a><img src={docImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ taskdocShow: false,taskdocpropsShow:false,taskpdfFileLink:[] }) }} /></a>
      <div><p>{this.state.docName}</p></div>
      </div>
      :''} 
      {(this.state.actdocpropsShow && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput)||(this.state.actdocShow && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput)?
      <div className="pa-text">
      <a><img src={docImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ actdocShow: false,actdocpropsShow:false,actpdfFileLink:[] }) }} /></a>
      <div><p>{this.state.docName}</p></div>
      </div>
      :''} 
      {(this.state.resdocpropsShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput)||(this.state.resdocShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput)?
      <div className="pa-text">
      <a><img src={docImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ resdocShow: false,resdocpropsShow:false,respdfFileLink:[] }) }} /></a>
      <div><p>{this.state.docName}</p></div>
      </div>
      :''} 
      {(this.state.gendocpropsShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)||(this.state.gendocShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)?
      <div className="pa-text">
      <a><img src={docImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ gendocShow: false,gendocpropsShow:false,genpdfFileLink:[] }) }} /></a>
      <div><p>{this.state.docName}</p></div>
      </div>
      :''} 
       </div>
        
  
  <div className="clearfix"></div>
            <div className="adtitle">
            {/* <button id="title" onClick={this.changeaddInput}>Change Title</button> */}
            <input type="text" className="form-control rainput"  placeholder="My Exprience" />
            {/* {(this.state.resultInput || this.state.situationInput || this.state.actionInput || this.state.taskInput )?<button id="description" onClick={this.changeInput}>Add Description</button>:''
            } */}
            </div>
            {this.state.addLocation &&
            <div className="input-group ralocation">
<i class="fa fa-map-marker" aria-hidden="true"></i>
            <textarea name="location" onChange={this.handlelocation} className="form-control mb-2" id="exampleFormControlInput1" defaultValue={this.state.location} placeholder="Add your experience location here"/>
            </div>
    }
            {this.state.tagdomain &&
            <div className="form-group ">
              
            <ReactTags
        ref={this.reactTags}
        tags={this.state.tags}
        suggestions={this.state.suggestions}
        onDelete={this.onDelete.bind(this)}
        onAddition={this.onAddition.bind(this)}

        placeholderText ={'Tag Colleagues'} />
        </div>
              }
           

  <div className="clearfix"></div>
  {(this.state.situimgpropsShow && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)||(this.state.situimgShow && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)?
            <>
              <MDBCloseIcon onClick={() => { this.setState({ situimgShow: false,situimgpropsShow:false,situimagevalueLink:[] })}} />
              <SimpleImageSlider
                  width={520}
                  height={234}
                  images={situimages}
              />
           </>
       :''}
        {(this.state.taskimgpropsShow && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput)||(this.state.taskimgShow && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput)?
          <>
            <MDBCloseIcon onClick={() => { this.setState({ taskimgShow: false,taskimgpropsShow:false,taskimagevalueLink:[] }) }} />  
            <SimpleImageSlider
                width={520}
                height={234}
                images={taskimages}
            />
          </>
       :''}
        {(this.state.actimgpropsShow && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput)||(this.state.actimgShow && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput)?
            <>
             <MDBCloseIcon onClick={() => { this.setState({ actimgShow: false,actimgpropsShow:false,actimagevalueLink:[] }) }} />  
            <SimpleImageSlider
               width={520}
               height={234}
               images={actionimages}
           />
          </>
       :''}
        {(this.state.resimgpropsShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput)||(this.state.resimgShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput)?
          <>
            <MDBCloseIcon onClick={() => { this.setState({ resimgShow: false,resimgpropsShow:false,resimagevalueLink:[]}) }} /> 
           <SimpleImageSlider
               width={520}
               height={234}
               images={resultimages}
       />
       </>
       :''}
        {(this.state.genimgpropsShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)|| (this.state.genimgShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)?
            <>
            <MDBCloseIcon onClick={() => { this.setState({ genimgShow: false,genimgpropsShow:false,imagevalueLink:[]})}} /> 
             <SimpleImageSlider
              width={520}
              height={234}
              images={images}
       />
       </>
      :''}
        {this.state.genvideoLink.length!=0 && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
        <>
            <MDBCloseIcon onClick={() => { this.setState({ genvideoLink:[],videoShow:false }) }} />
            <ReactPlayer width={520} height={234} className="ravidp" controls playing url={this.state.genvideoLink[0]} />
       </>
       
       }
      {this.state.situvideoLink.length>0 && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
          <>
            <MDBCloseIcon onClick={() => { this.setState({ situvideoLink:[],situvideoShow:false}) }} />
            <ReactPlayer width={520} height={234} className="ravidp" controls playing url={this.state.situvideoLink[0]} />
          </>
      }
      {this.state.taskvideoLink.length>0 && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
          <>
            <MDBCloseIcon onClick={() => { this.setState({ taskvideoLink:[],taskvideoShow:false}) }} />
            <ReactPlayer width={520} height={234} className="ravidp" controls playing url={this.state.taskvideoLink[0]} />
          </>
      }
      {this.state.actvideoLink.length>0 && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput &&
           <>
           <MDBCloseIcon onClick={() => { this.setState({ actvideoLink:[],actvideoShow:false}) }} /> 
           <ReactPlayer width={520} height={234} className="ravidp" controls playing url={this.state.actvideoLink[0]} />
           </>
      }
      {this.state.resvideoLink.length>0 && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput &&
           <>
           <MDBCloseIcon onClick={() => { this.setState({ resvideoLink:[],resvideoShow:false }) }} /> 
           <ReactPlayer width={520} height={234} className="ravidp" controls playing url={this.state.resvideoLink[0]} />
           </>
      }
  <div className="d-flex justify-content-between exprimedia">
  {/* <p>Add to Your Experience</p> */}
  {/* {this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
            <p>Edit Your Situation Files</p>
            }
            {!this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
            <p>Edit Your Task Files</p>
            }
            {!this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput &&
            <p>Edit Your Action Files</p>
            }
            {!this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput &&
            <p>Edit Your Result Files</p>
            }
            {!this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
            <p>Edit Your Experience</p>
            } */}
  <div></div>
  <div className="exmedia">
            {/* <a href="#">
            <input type="file" onChange={this.imageChange} accept="image/x-png,image/jpeg" multiple className="dropzone" style={{width:"4%",overflow:'hidden'}}/>
            <ImageIcon className="photo_icon"/>
            </a> */}
            {/* <a href="#">
            <input type="file" onChange={this.videoChange} accept="video/mp4" className="dropzone" style={{width:"4%",overflow:'hidden'}}/>
              <VideocamIcon className="video_icon"/></a> */}
            {/* <a href="#">
            <input type="file" onChange={this.audioChange} accept="audio/mp3" className="dropzone" style={{width:"4%",overflow:'hidden'}}/>
              <GraphicEqIcon className="audio_icon"/></a> */}
            {/* <a href="#">
            <input type="file" onChange={this.pdfChange} accept="application/pdf" className="dropzone" style={{width:"4%",overflow:'hidden'}}/>
              <DescriptionIcon className="document_icon"/></a> */}
            {/* <a onClick={this.locationChange}>
              <LocationOnIcon className="location_icon"/>
              </a> */}
            {/* <a onClick={this.isOpenTag}><LocalOfferIcon className="tag_icon"/></a> */}
            </div>

  </div>
  <div className="clearfix"></div>
  <ReactTags
        ref={this.reactTags}
        tags={this.state.tags}
        suggestions={this.state.suggestions}
        onDelete={this.onDelete.bind(this)}
        onAddition={this.onAddition.bind(this)}

        placeholderText ={'Related Skill'} />
  {/* {this.state.imagevalue && this.state.imagevalue.map((im,img)=>{
 return  <p key={img}>hi</p>

  })} */}
  <div className="clearfix"></div>
  {/* <div className="starf">
  <ul>
    <li><a id="situation" onClick={this.changeInput}>Situation</a></li>
    <li><a id="task" onClick={this.changeInput}>Task</a></li>
    <li><a id="action" onClick={this.changeInput}>Action</a></li>
    <li><a id="result" onClick={this.changeInput}>Result</a></li>
  </ul>
  <div className="clearfix"></div>
  </div> */}
  </div>

   </Modal.Body>
            <Modal.Footer className="addraft">
              <Button onClick={() =>{this.deleteExpList(this.state.expId)}} className="radlt">Delete</Button>
              {this.state.toggleindex!='1'&& this.state.showstarframework==false &&
                <Button onClick={()=>this.changeInput(this.state.toggleindex-1,this.state.indexarray[this.state.toggleindex-1])} className="ranewpre">previous</Button>
              }
              {this.state.toggleindex!='4'&& this.state.showstarframework==false && 
             
             <Button  onClick={()=>this.changeInput(this.state.toggleindex+1,this.state.indexarray[this.state.toggleindex+1])} className="ranewnext">next</Button>
           
           }
            { this.state.toggleindex=='4'&& this.state.showstarframework==false &&
              <Button onClick={this.handlesubmit} className="ranewnext">Save</Button>
             }

             {this.state.showstarframework && 
              <Button onClick={this.handlesubmit} className="ranewnext">Save</Button>
             }
              {/* <Button onClick={this.handlesubmit} >Save</Button> */}
            </Modal.Footer>
            <Speechtotext show={this.state.openMic} onHide={requestModalClose} textboxId={this.state.textboxId}/>
            <WebCamera show={this.state.openCam} onHide={requestModalCamClose}/>
          </Modal>
        );
                 
    }
   
}
const mapStateToProps = state=>{
  console.log(state);
  return{
      loader : state.loaderReducer.loader,
      auth : state.authReducer.authData,
      profilepic : state.authReducer.profileImage,
      text : state.mediaReducer.text,
      videodata : state.mediaReducer.videoData,
      blobvideodata :state.mediaReducer.blobVideoData
    }
}

const mapDispatchFromProps = dispatch=>{
  return{
      startLoader : () =>dispatch(showLoader()),
      hideLoader : () =>dispatch(hideLoader()),
      clearVideo :()=>dispatch(emptyVideoData())
  }
}
export default connect(mapStateToProps,mapDispatchFromProps)(Editexperience);