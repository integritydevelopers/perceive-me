import React from 'react';
import {Route,BrowserRouter,Switch} from 'react-router-dom';
import './assets/css/bootstrap.min.css';
import './assets/css/style.css';
 import './assets/css/owl.carousel.min.css';
import './assets/css/owl.theme.default.min.css';
import './assets/css/responsive.css';
import './assets/css/bootstrap-float-label.css';
import './assets/font/stylesheet.css';
import 'react-toastify/dist/ReactToastify.css';
import Login from '../src/Pages/Auth/Login';
import Signup from '../src/Pages/Auth/Signup';
import { ToastContainer } from 'react-toastify';
import "react-datepicker/dist/react-datepicker.css";
import PageLoader from './Components/Common/loader';
import Layout from './Components/Layouts/layout';
import { connect } from 'react-redux';
import { PrivateRoute } from './providers/authGard';
import Home from './Pages/Home/home';
import ForgotPassword from './Pages/Auth/Forgotpassword';
import Globalprofile from './Pages/Profile/globalprofile';
function App(props) {
  
  return (
    <BrowserRouter>
      <div >
      {props.loader ? <PageLoader/> :''}
      <ToastContainer />
            
                  <Switch>
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/signup" component={Signup} /> 
                    <Route exact path="/forgotpassword" component={ForgotPassword} /> 
                    <Route exact path="/home" component={Home} />  
                    {/* <PrivateRoute path="/globleprofile" component={Globleprofile}/>  */}
                    <Route exact path='/globalprofile/:id?' component={Globalprofile}/>               
                    <PrivateRoute path="/" component={Layout}/>
                   
                  </Switch>
                
      </div>
    
 
     
      
    </BrowserRouter>
   
    
  );
}

const mapStateToProps = state=>{
  return{
    loader : state.loaderReducer.loader

  }
}


export default connect(mapStateToProps,null)(App);
