import React from 'react';
import sec2Item1 from '../../assets/images/sec2-tem1.png';
import sec2Item2 from '../../assets/images/sec2-tem2.png';
import sec2Item3 from '../../assets/images/sec2-tem3.png';
import sec2Item4 from '../../assets/images/sec2-tem4.png';
import sec2Item5 from '../../assets/images/sec2-tem5.png';
import sec2Item6 from '../../assets/images/sec2-tem6.png';
import starActive from '../../assets/images/star-active.png';
import star from '../../assets/images/star.png';
import user from '../../assets/images/userns.png';
import { connect } from 'react-redux';
import { getProfilePic} from '../../providers/auth';
import {Link} from 'react-router-dom';
import { withRouter } from 'react-router-dom';

class LeftAside extends React.Component{
constructor(props){
    super(props);
    this.steps=localStorage.getItem('step');
    
    
    if(this.steps==2 || this.steps==3 || this.steps==4 || this.steps==5 || this.steps==6|| this.steps==7)
    {
        this.state = {
            step : this.steps,
            userImage:'',       
            };
   }
   else
   {
    this.state = {
        step : 1,
        userImage:'',       
        };

   }
 
          getProfilePic().then(res=>{
            if(res.data.data.user_image)
             this.setState({userImage:res.data.data.user_image});
        })
    this.toggleClass= this.toggleClass.bind(this);

    
}

toggleClass(index) {
       
    console.log(this.props);
    const currentState = index;
    this.setState({ step: currentState });
    localStorage.setItem('step',currentState);
 };

    render(){
        // const location = useLocation();
        this.pathname=this.props.location.pathname;
        return(
            <div className="col-sm-3">
                 {this.props.location.pathname != `/publicprofile/${this.props.auth.user_id}` &&
            <div className="profile-sec2-left">
                <div className="left-item1">
                {/* <img className="img-fluid" src={this.state.userImage} onError={(e) => e.target.src = user}/> */}
                <img className="img-fluid"  src={this.props.profilePic} onError={(e)=>{e.target.onerror = null; e.target.src=user}}/>
                {/* <span>1.0</span> */}
                    
                    {/* <ul>
                    <li><img src={starActive}/></li>
                        <li><img src={star}/></li>
                         <li><img src={star}/></li>
                         <li><img src={star}/></li>
                         <li><img src={star}/></li>
                    </ul> */}
        <h3 className="mt-3">{this.props.auth.user_fname}  {this.props.auth.user_lname}</h3>
                    {/* <p>UI / UX Designer</p> */}
                </div>
               
                <div className="left-item2">
                <ul>
                <li className={(this.pathname=='/profile'? 'active' : '')} onClick={()=>this.toggleClass(1)}><a href="/profile" className="btop"><span><img src={sec2Item1}/></span> My Profile</a></li>
                {/* <li className={(this.pathname=='/mygoals'? 'active' : '')} onClick={()=>this.toggleClass(2)}><a href="/mygoals"><span><img src={sec2Item2}/></span> My Goal</a></li> */}
                <li className={(this.pathname=='/experience' ? 'active' : '')} onClick={()=>this.toggleClass(3)} ><a href="/experience"><span><img src={sec2Item3}/> </span> My Experience</a></li>
                {/* <li className={(this.pathname=='/mytest'? 'active' : '')} onClick={()=>this.toggleClass(4)}><a href="/mytest"><span><img src={sec2Item4}/> </span> My Test</a></li> */}
                {/* <li className={(this.pathname=='/consulatation'? 'active' : '')} onClick={()=>this.toggleClass(5)}><a href="/consulatation"><span><img src={sec2Item5}/> </span> My Consultations</a></li> */}
                <li className={(this.pathname=='/consulatationhub'? 'active' : '')} onClick={()=>this.toggleClass(5)}><a href="/consulatationhub"><span><img src={sec2Item5}/> </span> Consultations Hub</a></li>
                {/* <li className={(this.pathname=='/skills'? 'active' : '')} onClick={()=>this.toggleClass(6)}><a href="/skills"><span><img src={sec2Item6}/> </span> My Skill</a></li>
                <li className={(this.pathname=='/subscription'? 'active' : '')} onClick={()=>this.toggleClass(7)}><a href="/subscription"><span><img src={sec2Item1}/> </span> My Subscription</a></li> */}
                <li className={(this.pathname=='/publicprofile'? 'active' : '')} onClick={()=>this.toggleClass(8)}><a href={`/publicprofile/${this.props.auth.user_id}`}><span><img src={sec2Item1}/> </span> Public profile</a></li>
                {/* <li className={(this.pathname=='/publicprofile'? 'active' : '')} onClick={()=>this.toggleClass(8)}><a href={`/publicprofile/${this.props.auth.user_id}`} ><span><img src={sec2Item1}/> </span> Public profile</a></li> */}
                    </ul>
                </div>
    
                <div className="clearfix"></div>
                </div>
    }

                <div className="clearfix"></div>
               
                    
                


            </div>
        );

        
    }

}
const mapStateToProps = state=>{
    return{
    loader : state.loaderReducer.loader,
      auth : state.authReducer.authData,
      profilePic : state.authReducer.profileImage,
  
    }
  }


export default connect(mapStateToProps)(withRouter(LeftAside));
