import React,{ useState } from 'react';
import Header from '../../Components/Header/header'
import DeveloperModeIcon from '@material-ui/icons/DeveloperMode';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import { connect } from 'react-redux';
import { manageAuth,updateAuth } from '../../redux/Actions/authAction';
import { publicWorklist,publicEdulist,publicpvtlist,publicexplist,personalSkill,
  feedback,publicDomain,jobSkill,jobSkillList,publicconsultation,JobPublic,domainPublic } from '../../providers/auth';
import Accordion from 'react-bootstrap/Accordion'
import { PublicExp,PublicWork,PublicEducation,PublicInfo,PublicSkill,
  PublicjobSkill,PublicDomain,PublicFeedback,PublicConsultation,Publicexpjob,
  PublicConsultjob,PublicConsultDomain} from '../../providers/globleauth';
import Card from 'react-bootstrap/Card';
import { Button } from 'react-bootstrap';
import cover from '../../assets/images/profile_cover.jpg';
import user from '../../assets/images/userns.png';
import ShowMoreText from 'react-show-more-text';
import Speech from 'react-speech';
import moment, { calendarFormat } from 'moment';
import { showLoader, hideLoader } from '../../redux/Actions/loaderAction';
import ReviewRequest from '../../Components/Modal/ReviewRequest';
import DocumentViewer from '../../Components/Modal/documentViewer';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import ModalVideo from 'react-modal-video';
import 'react-modal-video/css/modal-video.min.css';
import ReactStars from "react-rating-stars-component";
import Tooltip from '@material-ui/core/Tooltip';
import EmailIcon from '@material-ui/icons/Email';
import Story from '../../Components/Modal/Story';



const rand = () => Math.floor(Math.random() * 255)

const genData = () => ({
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
    {
      type: 'line',
      label: 'Dataset 1',
      borderColor: `rgb(${rand()}, ${rand()}, ${rand()})`,
      borderWidth: 2,
      fill: false,
      data: [rand(), rand(), rand(), rand(), rand(), rand()],
    },
    {
      type: 'bar',
      label: 'Dataset 2',
      backgroundColor: `rgb(${rand()}, ${rand()}, ${rand()})`,
      data: [rand(), rand(), rand(), rand(), rand(), rand(), rand()],
      borderColor: 'white',
      borderWidth: 2,
    },
    {
      type: 'bar',
      label: 'Dataset 3',
      backgroundColor: `rgb(${rand()}, ${rand()}, ${rand()})`,
      data: [rand(), rand(), rand(), rand(), rand(), rand(), rand()],
    },
  ],
})
const options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  }
  const data = genData();

  const piedata = {
    labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
    datasets: [
      {
        label: '# of Votes',
        data: [12, 19, 3, 5, 2, 3],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
        ],
        borderWidth: 1,
      },
    ],
  }


  const lineData = {
    labels: ['1', '2', '3', '4', '5', '6'],
    datasets: [
      {
        label: '# of Votes',
        data: [12, 19, 3, 5, 2, 3],
        fill: false,
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgba(255, 99, 132, 0.2)',
      },
    ],
  }
  
  const lineOptions = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  }

class globalprofile extends React.Component{
  dataimageObject = {}

  constructor(props) {
    super(props);
    this.profilestep=localStorage.getItem('step');
    
    this.state= {
      worklist:[],
      educationlist:[],
      personalinfo:[],
      publicpostlist:[],
      audioShow : false,
      reviewShow : false,
      photoIndex: 0,
      isOpen: false,
      situisOpen: false,
      taskisOpen: false,
      actisOpen: false,
      resisOpen: false,
      storyView : false,
      skilllist:[],
      ratinglist :[],
      joblist:[],
      expvalue:'Other Experiences',
      consultvalue:'Endorsed Experiences',
      puclicconslist:[],
      storyUrl:''
    };

    this.alllist = this.alllist.bind(this);
    this.props.startLoader();

  }

  componentDidMount() {
    const id = this.props.match.params.id;
    
    PublicWork(id).then(res=>{
     
        console.log(res.data);
        this.setState({
          worklist:res.data.data
          });
          this.props.hideLoader();
          console.log(this.state.worklist);
  
      });
     
      PublicEducation(id).then(res=>{
        console.log(res.data);
        this.setState({
          educationlist:res.data.data
          });
          console.log(this.state.educationlist);
  
      });
      this.props.startLoader();
      PublicInfo(id).then(res=>{
        console.log(res.data);
        this.setState({
          personalinfo:res['data']
          });
          this.props.hideLoader();
          this.setState({storyUrl:this.state.personalinfo[0].user_story})
          this.setState({storyView:true})
          
  
      });

      PublicExp(id).then(res=>{
        console.log(res.data);
        this.setState({
          publicpostlist:res['data']
          });
          console.log(this.state.publicpostlist);
  
      });

      PublicConsultation(id).then(res=>{
        console.log(res.data);
        this.setState({
          puclicconslist:res['data']
          });
          console.log(this.state.puclicconslist);
  
      });

      PublicSkill(id).then(res=>{
        console.log(res.data);
        this.setState({
          skilllist:res.data.data
          });
          console.log(this.state.skilllist);
  
      });

      PublicFeedback(id).then(res=>{
        console.log(res.data);
        this.setState({
          ratinglist:res.data
          });
          console.log(this.state.ratinglist);
  
      });

      PublicjobSkill(id).then(res=>{
                  console.log(res.data);
                  this.setState({
                    joblist:res.data
                    });
                    console.log(this.state.joblist);
            
                });
     
}
  modalVideo= e => event =>{
    this.setState({index:e});
    this.setState({isOpen:true});
  }
  situmodalVideo= e => event =>{
    this.setState({index:e});
    this.setState({situisOpen:true});
  }
  taskmodalVideo= e => event =>{
    this.setState({index:e});
    this.setState({taskisOpen:true});
  }
  actmodalVideo= e => event =>{
    this.setState({index:e});
    this.setState({actisOpen:true});
  }
  resmodalVideo= e => event =>{
    console.log(e);
    this.setState({index:e});
    this.setState({resisOpen:true});
  }

  imgSlider = e => event => {
    this.setState({ reviewShow: true })
    this.dataimageObject = e;
    console.log(this.dataimageObject);
  }


  audioPlayer = e => event => {
    this.setState({ audioShow: true });
    this.dataaudioObject=e;
  }

  docViewer= (doc)=>event =>{
    console.log(doc);
      doc.forEach(el=>{
        this.state.docFile=el;
      })
      console.log(this.state.docFile);
     this.datadocObject = {"doc":this.state.docFile};
   //this.datadocObject = {"doc":this.state.docFile};
      this.setState({docView: !this.state.docView});
      console.log(this.datadocObject);
 }

 jobskill = (id,skill)=>{
  console.log(id);
  console.log(skill);
  this.props.startLoader();
  Publicexpjob(id,this.props.auth.user_id).then(res=>{
    console.log(res.data);
    this.setState({
      publicpostlist:res['data'],expvalue:skill
      });
      console.log(this.state.publicpostlist);

  });

  PublicConsultjob(id,this.props.auth.user_id).then(res=>{
    console.log(res.data);
    this.setState({
      puclicconslist:res['data'],consultvalue:skill
      });
      console.log(this.state.puclicconslist);
      this.props.hideLoader();

  });
 }

  skilldisplay = (id,skill)=>{
    console.log(id);
    console.log(skill);
    this.props.startLoader();
    PublicDomain(id,this.props.auth.user_id).then(res=>{
      console.log(res.data);
      this.setState({
        publicpostlist:res['data'],expvalue:'Experiences:'+' '+skill
        });
        console.log(this.state.publicpostlist);
  
    });

    PublicConsultDomain(id,this.props.auth.user_id).then(res=>{
      console.log(res.data);
      this.setState({
        puclicconslist:res['data'],consultvalue:'Endorsed Experiences:'+' '+skill
        });
        console.log(this.state.puclicconslist);
        this.props.hideLoader();
  
    });
  }

  alllist(){
    this.setState({
      expvalue:'Other Experiences'
      });
      PublicExp(this.props.auth.user_id).then(res=>{
      console.log(res.data);
      this.setState({
        publicpostlist:res['data']
        });
        console.log(this.state.publicpostlist);

    });

  }

  alllistconsultation=()=>{
    this.setState({
      consultvalue:'Endorsed Experiences'
      });
      PublicConsultation(this.props.auth.user_id).then(res=>{
        console.log(res.data);
        this.setState({
          puclicconslist:res['data']
          });
          console.log(this.state.puclicconslist);
  
      });

  }


    render(){
      console.log(this.state.dataimageObject);
      let requestModalClose = () => { this.xte({ audioShow: false }) }
      let  docViewClose=() =>{this.setState({docView:false})}
      let  storyViewClose=() =>{this.setState({storyView:false})}
        return(
          <div>
          <Header/>
            <section id="profile-sec2">
            <div className="container">
            <div className="row">

            <div className="w-100">
              {
             this.state.personalinfo.map((e, key)=>{ 
            return<div key={key} className="rapropic">
            <img src={e.user_cover} onError={(e)=>{e.target.onerror = null; e.target.src=cover}} alt="" className="img-fluid" />
            </div>
            })} 
            <div className="raflro">
            <div className="card info-card">
            <div className="row card-body">
            <div className="col-md-6 d-flex px-5 py-4 rabord">
            {
             this.state.personalinfo.map((e, key)=>{ 
            return<div key={key} className="raprf">
            <img  src={e.user_image} onError={(e)=>{e.target.onerror = null; e.target.src=user}} alt=""/>
            </div>
            })} 
            {
             this.state.personalinfo.map((e, key)=>{ 
            
           return <div key={key} className="ml-4 raproleft">
            <h2>{e.user_fname +" "+e.user_lname}</h2>
            <h4>{e.role}</h4>
            <p><EmailIcon className="raicon"/>{e.user_email}</p>
            <button class="btn btn-success rabtn rabtnbgs" type="submit" onClick={()=>this.setState({storyView:true,storyData:e.user_story})}>< PlayCircleFilledIcon/>{e.user_fname+"'s"} Story</button>
            {/* <button type="button" class="btn btn-outline-primary ml-2 rabtn">Export Profile</button>
            <a href="" className="rashare"><i className="fa fa-share"></i></a> */}
            </div>
             })} 
            {/* <div className="raprice">$44/hr</div> */}
        
            </div>
          {
            this.state.personalinfo.map((e, key)=>{ 
          
           return <div key={key} className="col-md-6 px-5">
            <ul className="raprofdtls">
                        <li><span>Availability</span> Full-time</li>
                       {e.age!=''&& e.age!=null&& <li><span>Age</span> {e.age} </li> } 
                       {e.user_address!=''&& e.user_address!=null&& <li><span>Location</span> {e.user_address}</li> } 
                        <li><span>Year Experience</span> 18years</li>
                        <li><span>Degree</span> M.A</li>
                        {e.user_phno!='' && e.user_phno!=null && <li><span>Phone</span> {e.user_phno}</li>}
                        <li><span>Follow Me</span>  
                        <a href="#"><i className="fa fa-facebook"></i></a>
                         <a href="#"><i className="fa fa-twitter"></i></a>
                         <a href="#"><i className="fa fa-linkedin"></i></a>
                         </li>
                    </ul>
            </div>
             
            })} 
            </div>
          
            </div>
        <div className="row">
        <div className="col-md-6">
        <div className="radashb">
       
    <h2 className="text-blue">Job Skill </h2>
             <div className="rajobskill">
               {
                 this.state.joblist.length==0 &&
                 <>
                 <div className="card-counter" >
        <span className="count-numbers" >0</span>
        <span className="count-name">Add Job Skill</span>
      </div>
      <div className="card-counter" >
        <span className="count-numbers" >0</span>
        <span className="count-name">Add Job Skill</span>
      </div>
      <div className="card-counter" >
        <span className="count-numbers" >0</span>
        <span className="count-name">Add Job Skill</span>
      </div>
      <div className="card-counter" >
        <span className="count-numbers" >0</span>
        <span className="count-name">Add Job Skill</span>
      </div>
      </>
               }
             {
             this.state.joblist.map((e, key)=>{ 
     return <div key={key} className="card-counter" onClick={() =>{this.jobskill(e.skill_id,e.skill)}}>
        <span className="count-numbers" >{e.count}</span>
        <span className="count-name">{e.skill}</span>
      </div>

      })}
    </div>  
    
  </div>
 
 <div className="radashb">
    <h2 className="text-blue">Personal Skill </h2>
             <div className="rajobskill">
               {
                 this.state.skilllist == 0 &&
                 <>
                 <div className="card-counter" >
        <span className="count-numbers" >0</span>
        <span className="count-name">Add Personal Skill</span>
      </div>
      <div className="card-counter" >
        <span className="count-numbers" >0</span>
        <span className="count-name">Add Personal Skill</span>
      </div>
      <div className="card-counter" >
        <span className="count-numbers" >0</span>
        <span className="count-name">Add Personal Skill</span>
      </div>
      <div className="card-counter" >
        <span className="count-numbers" >0</span>
        <span className="count-name">Add Personal Skill</span>
      </div>
      </>
               }
             {
             this.state.skilllist.map((e, key)=>{ 
              return  <div  key={key} className="card-counter info" onClick={() =>{this.skilldisplay(e.skill_id,e.skill)}}>
      
        <span className="count-numbers" >{e.count}</span>
        <span className="count-name" >{e.skill}</span>
      </div>
             })}

    </div>  
  </div>
  
        </div>
            
        <div className="col-md-6">
        <div className="card info-card skill">
                <div className="card-body">
                    <h2 className="text-blue">Skills</h2>
                    <div className="row">
                    {
                       this.state.ratinglist.map((e, key)=>{
                       return <div key={key} className="col-lg-6 raskilsr">
                       
                            <h4>{e.skill}</h4>
                           
                            <div  className="rastars">

                            <ReactStars
                                 count={5}
                                 edit={false}
                                 value={e.rating}
                                  isHalf={true}
                                  size={18}
                                 activeColor="#f07d3a"/>

                            </div> 
                           
                        </div>
                         })} 

                    </div>
                    
                </div>
            </div>
        </div>
        </div>
           <div className="row">
             
        <div className="col-lg-8">

        <div className="card info-card rasamheht">
        <div className="card-body">
        <h2 className="text-blue">{this.state.consultvalue}
        {this.state.consultvalue!='Endorsed Experiences' &&
        <a  className="ravall" onClick={this.alllistconsultation}>Show All</a>
        }
        </h2>
        {
          this.state.puclicconslist.length== 0 &&
          <h2>No Records found</h2>
        }
       

        { 
       this.state.puclicconslist.map((e, key)=>{
      
          return  <Accordion className="mainacr"><Card className="racardpfl">
          	<div key={key}  className="">
              
            
            <Card.Header>
                      <div className="d-flex justify-content-between">
                          <div className="extitle">
                          <Speech 
                    textAsButton={true}    
                      displayText={<i className="fa fa-volume-up"></i>}
                       text={e.experience.user_exp_title} voice="Google UK English Female"></Speech>
                          
                          <Accordion.Toggle as={Button} variant="link" eventKey="0" className="pl-0 pb-0">
                          
					               	<h2>{e.experience.user_exp_title}</h2>
                        </Accordion.Toggle>
                         
                         
                          <a href="#">{moment(e.exp_date_added).format('LL')}</a>
                          <p className="mb-0 mt-2">{e.experience.user_exp_summary}</p>
						</div>
           
                          {/* <div className="exoption d-flex">
                         
                          <a href="#"><i className="fa fa-ellipsis-v"></i></a>
  
                          </div> */}
                      </div>

                      </Card.Header>
                      <Accordion.Collapse eventKey="0">
                      <Card.Body>
                      <div className="taglist mb-3">
      <div className="tagdomain">
           {e.domain_tagged!=''&&  <span className="domain">Skill :</span>}
            {e.domain_tagged && e.domain_tagged.map((jc,jcb)=>{
					return	<span key={jcb}> <i  className="fa fa-tags"></i>    {jc.name}  |</span> 
       })} 
       </div>

       <div className="tagdomain">
           {e.user_association!=''&&  <span className="domain">Organization :</span>}
            {e.user_association && e.user_association.map((jc,jcb)=>{
					return	<span key={jcb}> <i ></i>    {jc.organization_name}  |</span> 
       })} 
       </div>

       <div className="tagcolg">
{e.user_tagged!=''&&  <span className="colgi">Colleagues : </span>}
{e.user_tagged && e.user_tagged.map((jc,jcb)=>{
					return	<span key={jcb}><i  className="fa fa-user"></i>    {jc.f_name + " "+jc.l_name}  |</span> 
       })} 
       </div>
       <div className="ragenfile">
      {e.user_image_files>0 &&
            <a onClick={this.imgSlider(e.user_image_files)} >
          <span className="badge">{e.user_image_files.length}</span>
            <i className="fa fa-file-image-o"></i>

            </a>
     }
      {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.isOpen} url={e.videos[0]} onClose={() => this.setState({isOpen: false})} />
     }
      {e.videos.length>0 &&
            <a onClick={this.modalVideo(key)}>
            <span className="badge">{e.videos.length}</span>
            <i className="fa fa-file-video-o"></i>
           
            </a>
     }
      {e.audio.length>0 &&
      <a  onClick={this.audioPlayer(e.audio)}  >
       
            <span className="badge ">{e.audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
          
            </a>
     }
      {e.doc.length>0 &&
            <a onClick={this.docViewer(e.doc)}>
             
            <span className="badge ">{e.doc.length}</span>
            <i className="fa fa-files-o"></i>
          
            </a>
     }
            </div>

       </div>
       
          <div className="multiimg pb-2">
          <div className="row">
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
        <h3>My Situation</h3>
         
          <div className="d-flex justify-content-left rafile">
          {e.experience.user_situation_files.length>0 &&
            <a >
        <span className="badge rabadge">{e.experience.user_situation_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
       }
        {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.isOpen} url={e.situation_video[0]} onClose={() => this.setState({isOpen: false})} />
     }
        {e.situation_video.length>0 &&
            <a onClick={this.modalVideo(key)}>
            <span className="badge rabadge">{e.situation_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
       {e.situation_audio.length>0 &&
            <a onClick={this.audioPlayer(e.situation_audio)} >
            <span className="badge rabadge">{e.situation_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
       {e.situation_doc_file.length>0 &&
            <a onClick={this.docViewer(e.situation_doc_file)}>
            <span className="badge rabadge">{e.situation_doc_file.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
            </Accordion.Toggle>
      
      </Card.Header>
      <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
      <p>{e.experience.user_situation}</p>
           </ShowMoreText>
     
      </Card.Body>
      </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
          <h3>My Task</h3>
           <div className="d-flex justify-content-left rafile">
           {e.experience.user_task_files.length>0 &&
            <a onClick={this.imgSlider(e.experience.user_task_files)}>
        <span className="badge rabadge">{e.experience.user_task_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
       }
       {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.taskisOpen} url={e.experience.user_task_files[0]} onClose={() => this.setState({taskisOpen: false})} />
     }
        {e.experience.user_task_files.length>0 &&
            <a onClick={this.taskmodalVideo(key)}>
            <span className="badge rabadge">{e.experience.user_task_files.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
       }
        {e.experience.user_task_files.length>0 &&
            <a onClick={this.audioPlayer(e.experience.user_task_files)}>
            <span className="badge rabadge">{e.experience.user_task_files.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
       }
        {e.experience.user_task_files.length>0 &&
            <a  onClick={this.docViewer(e.experience.user_task_files)}>
            <span className="badge rabadge">{e.experience.user_task_files.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
       }
           </div>
           </Accordion.Toggle>
      
      </Card.Header>
      <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
                <p>{e.experience.user_task}</p>
           </ShowMoreText>
      
      </Card.Body>
      </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
     
          </div>

          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
          <h3>My Action</h3>
          <div className="d-flex justify-content-left rafile">
          {e.experience.user_action_files.length>0 &&
            <a onClick={this.reviewActiondata(e)} >
        <span className="badge rabadge">{e.experience.user_action_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
       }
            </div>
            </Accordion.Toggle>
      
      </Card.Header>
      <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
    
                <p>{e.experience.user_action}</p>
           </ShowMoreText>
      
      </Card.Body>
      </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
          <h3>My Result</h3>
      
           <div className="d-flex justify-content-left rafile">
           {e.experience.user_result_files.length>0 &&
            <a  onClick={this.reviewResultdata(e)}>
        <span className="badge rabadge">{e.experience.user_result_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
       }
       {this.state.index==key &&
            <ModalVideo channel='custom' isOpen={this.state.actisOpen} url={e.experience.user_result_files[0]} onClose={() => this.setState({actisOpen: false})} />
            }
        {e.experience.user_result_files.length>0 &&
            <a onClick={this.actmodalVideo(key)}>
            <span className="badge rabadge">{e.experience.user_result_files.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
       }
        {e.experience.user_result_files.length>0 &&
            <a onClick={this.audioPlayer(e.experience.user_result_files)}>
            <span className="badge rabadge">{e.experience.user_result_files.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
       }
        {e.experience.user_result_files.length>0 &&
            <a onClick={this.docViewer(e.experience.user_result_files)}>
            <span className="badge rabadge">{e.experience.user_result_files.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
       }
            </div>
            </Accordion.Toggle>
      
      </Card.Header>
      <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
                <p>{e.experience.user_result}</p>
           </ShowMoreText>
      
        </Card.Body>
        </Accordion.Collapse>
  </Card>
</Accordion>

          </div>
          </div>
          </div>
      
       </div>   
      
      <div className="rafrom">
        <h2>Request to</h2>
        { 
         e.expert_tagged.map((jc,jcb)=>{
        
        return<ul key={jcb}>
          <li>Expert : <span>{jc.f_name +" " + jc.l_name}</span></li>
          <li>Date : <span>{moment(e.exp_date_added).format('LL')}</span></li>
          <li>Parameters : 
          {e.parameters.map((jc,jcb)=>{
            
            return  <span key={jcb}> {jc.name} |</span>
             
           })} 
            </li>
        </ul>
         })}
         </div>

         <div className="exprtfeed">
         <h2>Expert Feedback</h2>
         <div className="rariview">
          
      
                 <ul>
                { e.feedback_rate &&
              e.feedback_rate.map((ac,cc)=>{
                return <li  key ={cc}>{ac.feedback_param} <span>{ac.feedback_point}/10</span>
                <Tooltip  title={ac.feedback_text}>
                <i className="fa fa-envelope"></i>
                </Tooltip></li>
              })}
           
           </ul>
           
            <span className="ratotal">Total : <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
           {/* <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
             ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo 
             viverra maecenas accumsan lacus vel facilisis. </p> */}
         </div>
    <div class="clearfix"></div>
         </div>
         
</Card.Body>
    </Accordion.Collapse>
      
                      <div className="clearfix"></div>
          
                  </div>

                  </Card>
                </Accordion>    
       
      })
  }

          </div>
          </div>

         <div className="clearfix">
           </div>   

        <div className="card info-card rasamheht">
        <div className="card-body">

        <h2 className="text-blue">{this.state.expvalue}  
        {this.state.expvalue!='Other Experiences' &&
        <a  className="ravall" onClick={this.alllist}>Show All</a>
        }
        </h2>
        { this.state.publicpostlist.length == 0 &&
        <h2>No Records Found</h2>

        }

       
{ 
   this.state.publicpostlist.map((e, key)=>{
    
        return	<Accordion key={key} className="mainacr"><Card className="racardpfl">
          <div   className="">


<Card.Header>
     
					<div className="d-flex justify-content-between">
          
						<div className="extitle"> 
            <Speech 
  textAsButton={true}    
   displayText={<i className="fa fa-volume-up"></i>}
  text={e.user_exp_title} voice="Google UK English Female"></Speech>

            <Accordion.Toggle as={Button} variant="link" eventKey="0" className="pl-0 pb-0">
            
						<h2>{e.user_exp_title}</h2>
            </Accordion.Toggle>
						<a href="#">{moment(e.exp_date_added).format('LL')}</a>
            
     <p className="mb-0 mt-2">{e.summary}</p>
     
						</div>
           
						<div className="exoption d-flex">
             
					
						</div>
					</div>
          
    </Card.Header>
    <Accordion.Collapse eventKey="0">
    <Card.Body>
      <div className="taglist">
      <div className="tagdomain">
           {e.domain_tagged!=''&&  <span className="domain">Skill :</span>}
            {e.domain_tagged && e.domain_tagged.map((jc,jcb)=>{
					return	<span key={jcb}> <i  className="fa fa-tags"></i>    {jc.name}  |</span> 
       })} 
       </div>

       <div className="tagdomain">
           {e.user_association!=''&&  <span className="domain">Organization :</span>}
            {e.user_association && e.user_association.map((jc,jcb)=>{
					return	<span key={jcb}> <i ></i>    {jc.organization_name}  |</span> 
       })} 
       </div>

<div className="tagcolg">
{e.user_tagged!=''&&  <span className="colgi">Colleagues : </span>}
{e.user_tagged && e.user_tagged.map((jc,jcb)=>{
					return	<span key={jcb}><i  className="fa fa-user"></i>    {jc.f_name + " "+jc.l_name}  |</span> 
       })} 
       </div>
       <div className="ragenfile">
      {e.user_image_files.length>0 &&
            <a onClick={this.imgSlider(e.user_image_files)} >
          <span className="badge">{e.user_image_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
     }
     {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.isOpen} url={e.videos[0]} onClose={() => this.setState({isOpen: false})} />
     }
      {e.videos.length>0 &&
            <a onClick={this.modalVideo(key)}>
            <span className="badge">{e.videos.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
      {e.audio.length>0 &&
      <a onClick={this.audioPlayer(e.audio)} >
            <span className="badge ">{e.audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
      {e.doc.length>0 &&
            <a onClick={this.docViewer(e.doc)}>
            <span className="badge ">{e.doc.length}</span>
            <i className="fa fa-files-o"></i>
            
            {/* <p>Files</p> */}
            </a>
     }
            </div>


      </div>
					<p>{e.user_exp_answer} </p>
          <div className="multiimg">
          <ul>
          {e.exp_image && e.exp_image.map((val,key)=>
            <li>
					<img src={val.user_exp_img} alt="" className="img-fluid resize" />
            </li>
              )}
              
          </ul>
          <div className="row">
          {e.situation_text !='' &&
          <div className="col-sm-12">
          <div className="yract">
          <Accordion defaultActiveKey="0">
  <Card>
 
    <Card.Header>
   
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
      Situation
      <div className="d-flex justify-content-left rafile">
      {e.user_situation_files.length>0 &&
            <a onClick={this.imgSlider(e.user_situation_files)}>
          <span className="badge rabadge">{e.user_situation_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
          }
          {this.state.index==key &&
           <ModalVideo channel='custom' isOpen={this.state.situisOpen} url={e.situation_video[0]} onClose={() => this.setState({situisOpen: false})} />
          }
          {e.situation_video.length>0 &&
            <a onClick={this.situmodalVideo(key)}>
            <span className="badge rabadge">{e.situation_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
       {e.situation_audio.length>0 &&
            <a onClick={this.audioPlayer(e.situation_audio)} >
            <span className="badge rabadge">{e.situation_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
       {e.situation_doc_file.length>0 &&
            <a onClick={this.docViewer(e.situation_doc_file)}>
            <span className="badge rabadge">{e.situation_doc_file.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
      </Accordion.Toggle>
      
    </Card.Header>
    
    <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
              
      <p>{e.situation_text}</p>
      </ShowMoreText>
          
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
     }
     {e.task_text !='' &&
          <div className="col-sm-12">
          <div className="yract">
          <Accordion  defaultActiveKey="0">
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
      Task
      <div className="d-flex justify-content-left rafile">
      {e.user_task_files.length>0 &&
            <a onClick={this.imgSlider(e.user_task_files)}>
          <span className="badge rabadge">{e.user_task_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
     }
     {this.state.index==key &&
      <ModalVideo channel='custom' isOpen={this.state.taskisOpen} url={e.task_video[0]} onClose={() => this.setState({taskisOpen: false})} />
     }
      {e.task_video.length>0 &&
            <a onClick={this.taskmodalVideo(key)}>
            <span className="badge rabadge">{e.task_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
      {e.task_audio.length>0 &&
            <a onClick={this.audioPlayer(e.task_audio)}>
            <span className="badge rabadge">{e.task_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
      {e.task_doc_file.length>0 &&
            <a onClick={this.docViewer(e.task_doc_file)}>
            <span className="badge rabadge">{e.task_doc_file.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
      <p>{e.task_text}</p>
           </ShowMoreText>
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
     }
     {e.action_text !='' &&
          <div className="col-sm-12">
          <div className="yract">
          <Accordion  defaultActiveKey="0">
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
      Action
      <div className="d-flex justify-content-left rafile">
      {e.user_action_files.length>0 &&
            <a onClick={this.imgSlider(e.user_action_files)}>
            <span className="badge rabadge">{e.user_action_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
            }
            {this.state.index==key &&
            <ModalVideo channel='custom' isOpen={this.state.actisOpen} url={e.action_video[0]} onClose={() => this.setState({actisOpen: false})} />
            }
             {e.action_video.length>0 &&
            <a onClick={this.actmodalVideo(key)}>
            <span className="badge rabadge">{e.action_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
       {e.action_audio.length>0 &&
            <a onClick={this.audioPlayer(e.action_audio)}>
            <span className="badge rabadge">{e.action_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
      {e.action_doc.length>0 &&
            <a onClick={this.docViewer(e.action_doc)}>
            <span className="badge rabadge">{e.action_doc.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="0">
      <Card.Body>
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
     
      <p>{e.action_text}</p>
      </ShowMoreText>
          
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
     }
     {e.result_text !='' &&
          <div className="col-sm-12">
          <div className="yract">
          <Accordion  defaultActiveKey="0">
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
      Result
      <div className="d-flex justify-content-left rafile">
      {e.user_result_files.length>0 &&
            <a onClick={this.imgSlider(e.user_result_files)}>
          <span className="badge rabadge">{e.user_result_files.length}</span>
            <i className="fa fa-file-image-o"></i>
            {/* <p>Images</p> */}
            </a>
     }
     {this.state.index==key &&
       <ModalVideo channel='custom' isOpen={this.state.resisOpen} url={e.result_video[0]} onClose={() => this.setState({resisOpen: false})} />
     }
     {e.result_video.length>0 && 
            <a onClick={this.resmodalVideo(key)}>
            <span className="badge rabadge">{e.result_video.length}</span>
            <i className="fa fa-file-video-o"></i>
            {/* <p>Video</p> */}
            </a>
     }
       {e.result_audio.length>0 && 
            <a onClick={this.audioPlayer(e.result_audio)}>
            <span className="badge rabadge">{e.result_audio.length}</span>
            <i className="fa fa-file-audio-o"></i>
            {/* <p>Audio</p> */}
            </a>
     }
       {e.result_doc.length>0 && 
            <a onClick={this.docViewer(e.result_doc)}>
            <span className="badge rabadge">{e.result_doc.length}</span>
            <i className="fa fa-files-o"></i>
            {/* <p>Files</p> */}
            </a>
     }
            </div>
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="0">
      <Card.Body>  
      <ShowMoreText
                /* Default options */
                lines={3}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                onClick={this.executeOnClick}
                expanded={false}
                width={480}
            >
              <p>{e.result_text}</p>
               
        
           </ShowMoreText>
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>
          </div>
          </div>
     }
          </div>
        
          <div className="clearfix"></div>
          </div>
          </Card.Body>
    </Accordion.Collapse>
          <div className="clearfix"></div>
         
   
				</div>
        </Card>
</Accordion>
        
     
	})
}
        <div className="clearfix"></div>
        </div>

        </div>
    
       
        </div>

    
        <div className="col-lg-4">
        <div className="card info-card rawork">
                <div className="card-body">
                    <h2 className="text-blue">Work History</h2>
                    <div className="">
                      {this.state.worklist == 0 &&
                      <h2> Upload your Work History</h2>

                      }


                      {
                      this.state.worklist.map((e, key)=>{

                        return <div key={key} className="d-flex">
                        <div className="racmlg"><i className="fa fa-briefcase"></i></div>
                        <div>
                        <h4><strong>{e.user_prf_value} </strong></h4>
                            <span>{moment(e.start_date).format('ll')} To {moment(e.end_date).format('ll')}</span>
                            <p>{e.user_prf_role}</p>
                        </div>
                        </div>
                      
                     })} 
                    </div>
                    
                </div>
            </div>
        {/* <div className="card info-card">
        <div className="card-body">
        <h2 className="text-blue">About</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
            Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.
        </p>
        </div>
        </div> */}
          {
            this.state.personalinfo.map((e, key)=>{
          
        return <div key={key} className="card">
                <div className="card-body info-card social-about">
                    <h2 className="text-blue">Professional Info</h2>
                    <ul className="raprofdtls">
            <li><span>Name</span> {e.user_fname +" "+e.user_lname}</li>
                       {e.age!=''&& e.age!=null && <li><span>Age</span> {e.age} </li>} 
                       {e.user_address!=''&& e.user_address!=null && <li><span>Location</span> {e.user_address}</li> } 
                        <li><span>Experience</span> 18years</li>
                        <li><span>Degeree</span> M.A</li>
                       {e.role!='' && e.role!= null && <li><span>Career Lavel</span> {e.role}</li> } 
                       {e.user_phno!='' && e.user_phno!=null && <li><span>Phone</span> {e.user_phno}</li> } 
                        {/* <li><span>Fax</span> 033 2244 6655</li> */}
                       {e.user_email!=''&& e.user_email!=null && <li><span>E-mail</span> {e.user_email}</li>} 
                        { e.website!=''&& e.website!=null && <li><span>Website Url</span> {e.website}</li>}
                      
                    </ul>

                </div>
            </div>
              })}  
            

                <div className="card info-card">
                <div className="card-body">
                    <h2 className="text-blue">Education</h2>
                    <div className="row">
                      { this.state.educationlist ==0 &&
                        <h2>Upload Your Education</h2>
                      }
                      {
                        this.state.educationlist.map((e, key)=>{
                      
                        return <div key={key} className="col-lg-12">
                            <h4><strong>{e.user_eprf_value} </strong></h4>
                            <p>{e.start_date} to {e.end_date} </p>
                        </div>
                       
                         })} 
                    </div>
                </div>
            </div>
       
       
        </div>
    </div>
        

 </div>
 {this.state.reviewShow && (
          <Lightbox
            mainSrc={this.dataimageObject[this.state.photoIndex]}
            nextSrc={this.dataimageObject[(this.state.photoIndex + 1) % this.dataimageObject.length]}
            prevSrc={this.dataimageObject[(this.state.photoIndex + this.dataimageObject.length - 1) % this.dataimageObject.length]}
            onCloseRequest={() => this.setState({ reviewShow: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (this.state.photoIndex + this.dataimageObject.length - 1) % this.dataimageObject.length,
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (this.state.photoIndex + 1) % this.dataimageObject.length,
              })
            }
          />
        )}

<Story
        show={this.state.storyView}
        onHide={storyViewClose}
        dataobject={this.state.storyUrl}
        authStatus={false}
      />
 <DocumentViewer
        show={this.state.docView}
        onHide={docViewClose}
        dataobject={this.datadocObject}
      />
                {/* <Imageslider/> */}
                 <ReviewRequest
        show={this.state.audioShow}
        onHide={requestModalClose}
       dataobject={this.dataaudioObject}
      />  
 </div>
 </div>
 </div>
 </section>
 </div>
        );
    }
}
const mapStateToProps = state=>{
  return{
    auth : state.authReducer.authData,
    loader : state.loaderReducer.loader,

  }
}

const mapDisptchToProps = dispatch=>{
  return{
    fetchAuth : () =>dispatch(manageAuth()),
    startLoader : () =>dispatch(showLoader()),
      hideLoader : () =>dispatch(hideLoader()),
  }
  

}

export default connect(mapStateToProps,mapDisptchToProps)(globalprofile);
