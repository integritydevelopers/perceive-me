import React from 'react';
import blog1 from '../../assets/images/blog1.jpg'
import blog2 from '../../assets/images/blog2.jpg'
import blog3 from '../../assets/images/blog3.jpg'

class ArticleSection extends React.Component{
    render(){
        return(
            <section id="sec4">
<div className="custom-container">
<h1><span>All Articles</span> Blogs and related articles</h1>
<div className="row">
<div className="col-sm-4">
<div className="sec4-inner">
<div className="bloig-img-area">
<img src={blog1}/>
<span>WordPress</span>
</div>
<h6>By : Admin &nbsp; &nbsp; 06 Aug 2017</h6>
<h4>Lorem Ipsum is simply dummy text</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
</div>
</div>
<div className="col-sm-4">
<div className="sec4-inner">
<div className="bloig-img-area">
<img src={blog2}/>
<span className="backpink">ThemeForest</span>
</div>
<h6>By : Admin &nbsp; &nbsp; 06 Aug 2017</h6>
<h4>Lorem Ipsum is simply dummy text</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
</div>
</div>
<div className="col-sm-4">
<div className="sec4-inner">
<div className="bloig-img-area">
<img src={blog3}/>
<span className="backorang">Envato</span>
</div>
<h6>By : Admin &nbsp; &nbsp; 06 Aug 2017</h6>
<h4>Lorem Ipsum is simply dummy text</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
</div>
</div>
</div>
</div>
</section>
        )
    }
}

export default ArticleSection;