import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
//import FileViewer from 'react-file-viewer';
import PDFViewer from 'pdf-viewer-reactjs'
class DocumentViewer extends React.Component{

    constructor(props){
        super(props);
        this.state = { 
          docFile:''
       };

    }
    componentWillReceiveProps(nextProps)
    {
      console.log(nextProps);
      if(nextProps.show)
      {
        this.setState({docFile:nextProps.dataobject.doc});
      }
    }
   render(){
    const type='pdf';
  
    console.log(this.state.docFile);
        return(
            <Modal
            {...this.props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Body>

                 <div className="ragenfile">
                 <PDFViewer
            document={{
                url: this.state.docFile
            }}
        />
                 </div>

              
            </Modal.Body>

          </Modal>
        );
    }

}
    export default DocumentViewer;