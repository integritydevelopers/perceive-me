import React from "react";
import Header from '../../Components/Header/header';
import Banner from '../../Components/Banner/banner';
import HelpSection from '../../Components/Sections/Help-section';
import ExpertSection from '../../Components/Sections/Expert-section';
import Testimonials from '../../Components/Sections/Testimonials';
import ArticleSection from '../../Components/Sections/Article-section';
import Newslatter from '../../Components/Sections/Newslatter-section';
import Footer from '../../Components/Footer/footer';


class Home extends React.Component{
    render(){
        return(
            <div>
               
                <Header/>
                <div className="clearfix"></div>
                <Banner/>
                <div className="clearfix"></div>
                <HelpSection/>
                <div className="clearfix"></div>
                <ExpertSection/>
                <div className="clearfix"></div>
                <Testimonials/>
                <div className="clearfix"></div>
                <ArticleSection/>
                <div className="clearfix"></div>
                <Newslatter/>
                <div className="clearfix"></div>
                <Footer/>
                
                
            </div>
            
        )
    }
}

export default Home;

