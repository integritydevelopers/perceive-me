import { combineReducers } from 'redux';
import { loaderReducer } from './loaderReducer';
import { authReducer } from './authReducer';
import { mediaReducer } from './mediaReducer';

const rootReducer = combineReducers({
    loaderReducer,authReducer,mediaReducer
});

export default rootReducer;