import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import { goalEdit } from '../../providers/auth';
import ReactTags from 'react-tag-autocomplete';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import audioImage from '../../assets/images/audiofile.png';
import docImage from '../../assets/images/documentfile.png';
import SimpleImageSlider from "react-simple-image-slider";
import WorkIcon from '@material-ui/icons/Work';
import DateRangeIcon from '@material-ui/icons/DateRange';
import MicIcon from '@material-ui/icons/Mic';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import {  DomainTag } from '../../providers/auth';
import  Speechtotext  from '../Media/speechTotext';
const isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
const isFirefox = typeof InstallTrigger !== 'undefined';
var isOpera = (!!window.opera && !!window.opera.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && window['safari'].pushNotification));
class EditGoals extends React.Component{

    constructor(props){
        super(props);
        this.state = {
          
            goaltime:'',
            position:[],
            orgname:[],
            
            tagsDomain: [],
            suggestionsDomain: [],
            tagsDomain: [],
            suggestionsDomain: [],
            gettagdomain:[],

            // new code
            addTitleInput : false,
            situationInput : false,
            audioShow : false,
            taskInput : false,
            actionInput : false,
            resultInput : false,
            openMic : false,
            openCam:false,
            textboxId : '',
            imagevalue:[],
            summery:'',
           
            title:'',
            tagdomain:false,
            goaldate:false,
            cmpname:false,
            goalId:'',
        };

        this.reactTags = React.createRef()
        //this.handlesubmit = this.handlesubmit.bind(this);

     
    }

    componentWillReceiveProps(nextProps)
    {
        if(nextProps.show)
        {
          this.setState({ ModalShow:true});
      
          console.log(nextProps);
          if(!this.state.openMic)
          {
           this.setState({
         
          title:nextProps.dataobject.title,
          summery:nextProps.dataobject.goal_desc,
          goaldate:nextProps.dataobject.end_date,
          goalId:nextProps.dataobject.goal_id,
          tagdomain:nextProps.dataobject.skill,
          orgname:nextProps.dataobject.cmpname
         
         }) 
        }
        }
        else
        {
          this.setState({ ModalShow:false});
        }
        console.log(this.state.title);

      if(nextProps.text.textboxId == 'goaleditsummery')
      {
        this.setState({summery :nextProps.text.text});
      }
      else if(nextProps.text.textboxId == 'goaleditsituation')
            {
              this.setState({situation :nextProps.text.text});
            } 
            else if(nextProps.text.textboxId == 'goaledittask')
                  {
                    this.setState({task :nextProps.text.text});
                  }
                  else if(nextProps.text.textboxId == 'goaleditaction')
                  {
                    this.setState({action :nextProps.text.text});
                  }
                  else if(nextProps.text.textboxId == 'goaleditresult')
                  {
                    this.setState({result :nextProps.text.text});
                  }
                  else if(nextProps.text.textboxId == 'goaledittitle')
                        {
                          this.setState({title :nextProps.text.text});
                        }
                        else if(nextProps.text.textboxId == 'goaleditlocation')
                        {
                          this.setState({location :nextProps.text.text});
                        }

    }

    isOpenTag = (e)=>{
        const currentstate=this.state.tagdomain;
        this.setState({tagdomain:!currentstate});
      }
      changeInput = (e)=>{
        switch(e.target.id){
          case 'title' :
          this.setState({addTitleInput:!this.state.addTitleInput});
          break;
          case 'situation' :
          this.setState({situationInput:!this.state.situationInput});
          this.setState({taskInput:false});
          this.setState({actionInput:false});
          this.setState({resultInput:false});
          break;
          case 'task' :
          this.setState({taskInput:!this.state.taskInput});
          this.setState({situationInput:false});
          this.setState({actionInput:false});
          this.setState({resultInput:false});
          break;
          case 'action' :
          this.setState({actionInput:!this.state.actionInput});
          this.setState({situationInput:false});
          this.setState({taskInput:false});
          this.setState({resultInput:false});
          break;
          case 'result' :
          this.setState({resultInput:!this.state.resultInput});
          this.setState({situationInput:false});
          this.setState({actionInput:false});
          this.setState({taskInput:false});
          break;
          case 'description':
            this.setState({situationInput:false});
            this.setState({actionInput:false});
            this.setState({taskInput:false});
            this.setState({resultInput:false});
          break;
        }
        
      }
      handlesummary = (e) =>{
        this.setState({summery:e.target.value});
      }
      handletitle = (e) =>{
        this.setState({title:e.target.value});
      }
      handlelocation = (e) =>{
        this.setState({location:e.target.value});
      }
      
      onDelete (i) {
        const tags = this.state.tags.slice(0)
        tags.splice(i, 1)
        this.setState({ tags })
       // console.log(this.state.tags);
      }
      
      onAddition (tag) {
        const tags = [].concat(this.state.tags, tag)
        this.setState({ tags })
        setTimeout(() => {
        }, 2000);
       
      }
    
      openMic = (id) =>{
        this.setState({openMic : !this.state.openMic})
        this.setState({textboxId : id});
      }
      openCam = () =>{
        this.setState({openCam : !this.state.openCam})
      }
      imageChange = (e)=>{
        let imageTemoArray=[];
        e.preventDefault();
        this.setState({reviewShow: true});
      
        for (let i = 0; i < e.target.files.length; i++) {
          let file = e.target.files[i];
          //console.log(file);
        let reader = new FileReader();
        reader.readAsDataURL(file);
       
        const scope = this
        reader.onload = function(event) {
         
          imageTemoArray.push(event.target.result);
            scope.setState({ imagevalue:imageTemoArray},console.log(scope.state.imagevalue))
            console.log(scope.state.imagevalue);
            scope.dataimageObject=scope.state.imagevalue;
          
        }
        
      }
      
     
      } 
    locationChange=(e)=>
    {
          this.setState({addLocation:!this.state.addLocation});
    }
      audioChange = (e) =>{
        let file = e.target.files[0];
        let audioTemoArray=[];
        let reader = new FileReader();
        reader.readAsDataURL(file);
        const scope = this
        reader.onload = function(event) {
          //console.log(event.target.result);
          //imageTemoArray.push(reader.result);
          audioTemoArray.push(event.target.result);
          scope.setState({ audioFile:audioTemoArray})
          console.log(scope.state.audioFile.length);
          if(scope.state.audioFile.length>0)
          {
            scope.setState({audioShow: true});
          }
          //scope.dataimageObject=scope.state.audioFile;
          //onsole.log(this.state.audioFile);
       
        }
      }
      pdfChange = (e) =>{
        let pdfTemoArray=[];
        let file = e.target.files[0];
        let reader = new FileReader();
        reader.readAsDataURL(file);
        const scope = this
        reader.onload = function(event) {
          console.log(event.target.result);
          pdfTemoArray.push(event.target.result);
          scope.setState({pdfFile: pdfTemoArray});
          console.log(scope.state.pdfFile);
          if(scope.state.pdfFile.length>0)
          {
            scope.setState({docShow: true});
          }
        }
      }

      handleInputChange = (e) =>{
        this.setState({[e.target.name]:e.target.value})
    }

    handlePositionChange = (e) =>{
      this.setState({[e.target.name]:[e.target.value]})
  }

  componentDidMount(){
    // skill api call

    DomainTag().then(res=>{
      this.setState({
        gettagdomain:res['data']['data']
      
      });      
  this.state.gettagdomain.forEach(el=>{
    this.state.suggestionsDomain.push({id:el.user_exp_dom_id,name:el.user_exp_domain});
  })
          });
   console.log(this.state.gettagdomain);

}

onDeletedomain (i) {
    const  tagsDomain = this.state. tagsDomain.slice(0)
    tagsDomain.splice(i, 1)
    this.setState({  tagsDomain })
  
  }
  
  onAdditiondomain (tagd) {
    const  tagsDomain = [].concat(this.state. tagsDomain, tagd)
    this.setState({  tagsDomain })
    setTimeout(() => {
  
    }, 2000);
  }
  
    targetDate = (e)=>{
        const currentstate=this.state.goaldate;
        this.setState({goaldate:!currentstate});
      }
      cmpName = (e)=>{
        const currentstate=this.state.cmpname;
        this.setState({cmpname:!currentstate});
      }


      handlesubmit = ()=>{
         
          this.props.startLoader();
            
                  if(this.state.title !='' && this.state.summery != '' ){
                   goalEdit(this.state.goalId,this.state.title,this.state.goaldate,this.state.summery,this.state.tagdomain,this.state.orgname).then(res=>{
                     
                     toast.success(res.data.msg,{});
                   
                       window.location.reload();
                      this.props.hideLoader();
                     console.log(res);
                     
                   });
                  }else{
                   this.props.hideLoader();
                    toast.error('Please enter Mindetry field')
                   
                  }
              }



    render(){
      console.log(this.state.orgname);
        let requestModalClose =() =>{this.setState({openMic:false})}
        let requestModalCamClose =() =>{this.setState({openCam:false})}
        let getvideoData =() =>{this.setState({openCam:false})}
  
        const images = [];
       
        this.state.imagevalue.forEach(el=>{
          images.push({url:el});
        })

        console.log(this.state.title);
        console.log(this.state.tagdomain);
        console.log(this.state.goalId);


      

  
        return(
            <div>

<Modal
            {...this.props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            // show={this.state.ModalShow}
            onHide={this.props.onHide}
            className="raexpmodal"
          >   
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
               Edit Goals
              </Modal.Title>
            </Modal.Header>
            <Modal.Body >

            <div>
              <div className="d-flex mb-4">
              <div className="addpic">
              <img src={this.props.profilepic} className="img-fluid" />	
              </div>
              <div className="poption">
            
        <p>{this.props.auth.user_fname} {this.props.auth.user_lname}</p>
        <span className="ratime">2 November, 2020</span>
              </div>

              </div>
           
            <div>
  
            <form>
              
{!this.state.openMic &&
<div className="form-group">
  <textarea  type="text" className="form-control" name="summery" onChange={this.handlesummary} id="exampleFormControlTextarea1" rows="5" defaultValue={this.props.text.textboxId == 'exsummery'  ? this.state.summery : this.state.summery} placeholder={"what is your goals " + this.props.auth.user_fname + "?"}></textarea>
  {isChrome && !isFirefox && !isOpera && !isSafari &&
  <a onClick={()=>this.openMic('goaleditsummery')}><MicIcon className="audiospk"/></a>
  }
  <a onClick={this.openCam}><CameraAltIcon className="webcam"/></a>
  </div>
}

  {this.state.addTitleInput && !this.state.openMic &&
  <div className="form-group">
  <input type="text" className="form-control" id="exampleFormControlInput1"  name="title" onChange={this.handletitle} defaultValue={this.state.title} placeholder="Add your goals title here"/>
  {isChrome && !isFirefox && !isOpera && !isSafari &&
  <a onClick={()=>this.openMic('goaledittitle')}><MicIcon className="audiospk"/></a>
  }
  <a onClick={this.openCam}><CameraAltIcon className="webcam"/></a>
  </div>
}

<div className="radocfile">
                   {this.state.audioShow &&
      <a href="#"><img src={audioImage} alt="" /></a> 
                   }
                   {this.state.docShow &&
      <a href="#"><img src={docImage} alt="" /></a>} 
       </div>
            
  </form>
  <div className="clearfix"></div>
  <div className="adtitle">
  <button id="title" onClick={this.changeInput}>Add Title</button>Help the people see your goals
  </div>
  {this.state.goaldate &&
  <div className="form-group">
    <p>Target Completion Date</p>
    <input type="date" className="form-control" name="goaldate" onChange={this.handleInputChange} defaultValue={this.state.goaldate} placeholder={"Target Completion Date"}/>
    </div>
  }
  {this.state.cmpname &&
  <div className="form-group">
    <input type="text" className="form-control"  name="orgname" onChange={this.handlePositionChange} defaultValue={this.state.orgname} placeholder={"Add your company name"}/>
    </div>
  }
  {this.state.tagdomain &&
            <div className="form-group ">
              
            <ReactTags
       ref={this.reactTags}
       tags={this.state.tagdomain}
       suggestions={this.state.suggestionsDomain}
       onDelete={this.onDeletedomain.bind(this)}
       onAddition={this.onAdditiondomain.bind(this)}

        placeholderText ={'Tag Domains'} />
        </div>
              }
           

  <div className="clearfix"></div>
  {(this.state.reviewShow || images.length>0)?
 
            <SimpleImageSlider
           width={520}
           height={234}
           images={images}
       />:''}
  <div className="d-flex justify-content-between exprimedia">
 
  <div></div>
  <div className="exmedia">
            <a onClick={this.targetDate}><DateRangeIcon className="date-icon"/></a>
            <a onClick={this.cmpName}><WorkIcon className="company-icon"/></a>
            {/* <a href=""><WcIcon className="job-iocn"/></a> */}
            {/* <a href=""><LocationOnIcon className="location_icon"/></a> */}
            <a onClick={this.isOpenTag}><LocalOfferIcon className="tag_icon"/></a>
            </div>

  </div>
 
  <div className="clearfix"></div>
  </div>
  </div>

            </Modal.Body>
            <Modal.Footer>
              {/* <Button onClick={this.props.onHide} >Close</Button>
              <Button onClick={this.submitgoal} >Save</Button> */}
              <Button onClick={this.handlesubmit} className="rapost">Save Your Goal</Button>
            </Modal.Footer>
            <Speechtotext show={this.state.openMic} onHide={requestModalClose} textboxId={this.state.textboxId}/>
          </Modal>


            </div>
            
        );
    }
    
}

const mapStateToProps = state=>{
    return{
   // loader : state.loaderReducer.loader
   auth : state.authReducer.authData,
   profilepic : state.authReducer.profileImage,
   text : state.mediaReducer.text
  
    }
}
const mapDisptchToProps = dispatch=>{
    return{
      startLoader : () =>dispatch(showLoader()),
      hideLoader : () =>dispatch(hideLoader())
    }
    
  }
  export default connect(mapStateToProps,mapDisptchToProps)(EditGoals);