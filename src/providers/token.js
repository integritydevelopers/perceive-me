import React from 'react';
import {Redirect} from 'react-router-dom';

export const getToken = () =>{
    return {'Authorization':'JWT ' + localStorage.getItem('authToken')}
}
export const getData = () =>{
    return {'authData': JSON.parse(localStorage.getItem('authData'))}
}
export const getAdminToken = () =>{
    return {'Authorization':'JWT ' + localStorage.getItem('auth')}
}
export const checkToken = () =>{
    console.log("expired")
    localStorage.clear();
    return <Redirect to='/login'/>
  
}