import React from 'react';
// import { Button } from 'react-bootstrap';
import { showLoader, hideLoader } from '../../redux/Actions/loaderAction';
import { connect } from 'react-redux';
import { manageAuth,updateAuth } from '../../redux/Actions/authAction';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';
import ReactStars from "react-rating-stars-component";
import { advisorProfile } from '../../providers/auth';

class Consulatationhub extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            currentTab:'get',
            openAdvisor:false,
        }
        advisorProfile().then(res=>{
            console.log(res);
        })
    }
    
    gotoInner = () =>{
        console.log(this.props.history)
        this.props.history.push({
            pathname: '/hubinner',
            data:"offlineconsult"
          })
    }
   
    gotoInnerprofile = () =>{
        console.log(this.props.history)
        this.props.history.push({
            pathname: '/hubinner',
            data:"profile"
          })
    }

    advisorModal = ()=>{
        this.props.history.push({
            pathname: '/createadvisor'
          })
    }
    setKey = (key)=>{
        this.setState({currentTab:key})
    }
    getConsultation = ()=>{
        this.props.history.push({
            pathname: '/consultation'
          })
    }

    render(){
       
        return(
            <>
            <div className="col-sm-6">
                <Tabs defaultActiveKey="get" transition={false} id="noanim-tab-example" className="raconstab" onSelect={(k) => this.setKey(k)}>
                    <Tab eventKey="get" title="Get Consultaion">
                    </Tab>
                    <Tab eventKey="offer" title="Offer Consultaion">
                    </Tab>
                    
                    </Tabs>
                    {this.state.currentTab == 'get' &&
                    <>
                     <div  className="exbg mt-3 raconshub" >
                        <div className="row" onClick={this.getConsultation}>
                            <div className="col-sm-2 col-3"><span className="conshub"><i className="fa fa-th-list"></i></span></div>
                            <div className="col-sm-10 col-9">
                            <h4>My Consultaion View</h4>
                            <p>Offline Consultaion <span className="badge badge-pill badge-warning float-right">34</span></p>
                            <p>Public Profile Resume <span className="badge badge-pill badge-danger float-right">10</span></p>
                            <p>Live Career Advice<span className="badge badge-pill badge-success float-right">0</span></p>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                        </div>
                        <div  className="exbg mt-3 raconshub" >
                        <div className="row" onClick={this.gotoInner}>
                            <div className="col-sm-2 col-3"><span className="conshub1"><i className="fa fa-users"></i></span></div>
                            <div className="col-sm-10 col-9">
                            <h4>Get Offline Consultaion</h4>
                            <p>
                                Get offline Consultaion from an expert about one of your exproences
                            </p>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                        </div>
                        <div  className="exbg raconshub" >
                        <div className="row" onClick={this.gotoInnerprofile}>
                            <div className="col-sm-2 col-3"><span className="conshub2"><i className="fa fa-address-book"></i></span></div>
                            <div className="col-sm-10 col-9">
                            <h4>Public Profile Resume</h4>
                            <p>
                                Have an expert review your Public Profile Resume
                            </p>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                        </div>
                        <div  className="exbg raconshub" >
                        <div className="row" onClick={this.gotoInner}>
                            <div className="col-sm-2 col-3"><span className="conshub3"><i className="fa fa-comments"></i></span></div>
                            <div className="col-sm-10 col-9">
                            <h4>Live Career Advice</h4>
                            <p>
                                Get live career advice from an industry expert
                            </p>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                        </div>
           </>
    }
                    {this.state.currentTab == 'offer' &&
                    <>
                        <div  className="exbg mt-3 raconshub" >
                        <div className="row" onClick={this.advisorModal}>
                            <div className="col-sm-2"><span className="conshub1"><i className="fa fa-users"></i></span></div>
                            <div className="col-sm-10">
                            <h4>Become an Advisor</h4>
                            <p>
                            To become an Adviser, you have to complete your profile, log your best Experiences and strengthen the public profile.
                            </p>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                        </div>

                        {/* <div className="d-flex justify-content-between ">
                        <div>
                            <label className="switch">
						<input type="checkbox" onClick={this.togglelistview} />
						<span className="slider round" ></span>
						</label>
						Incomimg Request
                        </div>
                        <div><a href="#"><NotificationsActiveIcon/></a></div>
                        </div> */}

                        <div  className="exbg mt-3 raconshub" >
                        <div className="row" onClick={this.advisorModal}>
                            <div className="col-sm-2 col-3"><span className="conshub2"><i className="fa fa-users"></i></span></div>
                            <div className="col-sm-10 col-9">
                            <h4>Incomimg Request</h4>
                            <p>
                            To become an Adviser, you have to complete your profile, log your best Experiences and strengthen the public profile.
                            </p>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                        </div>

                        
                        <div className="exbg mt-2">
                        <div className="row">
                        <div className="col-sm-8">
                        <div className="advsorprf">
                         <h1>Silvia Sanyal</h1> 
                         <span>Google</span>  <span>Intel</span>  <span>Microsoft</span>
                         <h2>Leadership, Resume Review, Career Consulting</h2> 
                         <p>Status :<span>Apporved</span></p>
                        </div>
                        </div>
                        <div className="col-sm-4 text-center adsorleft">
                        <lable className="racoin">100 </lable>
                       
                            <ReactStars
                            count={5}
                            size={24}
                            isHalf={true}
                            emptyIcon={<i className="far fa-star"></i>}
                            halfIcon={<i className="fa fa-star-half-alt"></i>}
                            fullIcon={<i className="fa fa-star"></i>}
                            activeColor="#ffd700"
                            className="react-stars"
                            />
                        <span>(100 Reviews)</span>
                        </div>
                        </div>

                        <div className="advisordlt">
                            <h2>Advisor Summery :</h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                        </div>
                        <div className="advisordlt">
                            <h2>Language Spoken :</h2>
                            <p>English, Hindi, Spanish, and Russian</p>
                        </div>
                        <div className="advisordlt">
                            <h2>Services Offerd :</h2>
                            <p>Leadership, Resume Review, Career Consulting</p>
                        </div>
                        <div className="advisordlt">
                            <h2>Availability & Charges <a className="float-right"><i className="fa fa-pencil"></i></a></h2>

                                <form className="rainnerhub mt-3">
                                <h2>For Offline Reviews</h2>
                                <ul className="raconis">
                                <li>Total weekly Availability Slot</li>
                                <li>4 Slot</li>
                                <li>Resume Reviews </li>
                                <li>20 Coins</li>
                                <li>Exprience Reviews </li>
                                <li>30 Coins</li>
                                <div className="clearfix"></div>
                                </ul>

                                <h2>For Online Reviews</h2>
                                <ul className="raconis">
                                <li>Cost of each 30 mint Slot</li>
                                <li>30 Coins</li>
                                <div className="clearfix"></div>
                                </ul>
                                </form>

                        </div>


                        <div>
                        </div> 
                        </div>


           </>
    }
        </div>

        <div className="col-sm-3">
        <div className="exbg rightpanl">
        <h4>Help Text</h4>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
            when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
        </div>
        </div>
</>
        
        )
    }
}
const mapStateToProps = state=>{
    return{
      auth : state.authReducer.authData,
      loader : state.loaderReducer.loader,
  
    }
  }
  
  const mapDisptchToProps = dispatch=>{
    return{
      fetchAuth : () =>dispatch(manageAuth()),
      startLoader : () =>dispatch(showLoader()),
        hideLoader : () =>dispatch(hideLoader()),
    }
    
  
  }
export default connect(mapStateToProps,mapDisptchToProps)(Consulatationhub);
