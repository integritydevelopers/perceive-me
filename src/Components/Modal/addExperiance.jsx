import React from 'react';
import SimpleImageSlider from "react-simple-image-slider";
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import ReactTags from 'react-tag-autocomplete';
import VideocamIcon from '@material-ui/icons/Videocam';
import ImageIcon from '@material-ui/icons/Image';
import MicIcon from '@material-ui/icons/Mic';
import DescriptionIcon from '@material-ui/icons/Description';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import GraphicEqIcon from '@material-ui/icons/GraphicEq';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import VideoCallIcon from '@material-ui/icons/VideoCall';
import CloseIcon from '@material-ui/icons/Close';
import { connect } from 'react-redux';
import  Speechtotext  from '../Media/speechTotext';
import TempWebCam from '../Media/webcamera';
import { AddUserExp,UserlistTag,DomainTag,jobtagslist } from '../../providers/auth';
import { toast } from 'react-toastify';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import 'react-image-lightbox/style.css';
import audioImage from '../../assets/images/audiofile.png';
import docImage from '../../assets/images/documentfile.png';
import ReactPlayer from 'react-player';
import {emptyVideoData} from '../../redux/Actions/mediaAction';
import fileUpload from '../../Components/Aws/aws';
import { MDBCloseIcon } from 'mdbreact';
import TextFieldsIcon from '@material-ui/icons/TextFields';
import { DataExchange } from 'aws-sdk';
import {Recorder} from 'react-voice-recorder';
import 'react-voice-recorder/dist/index.css';

const isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
const isFirefox = typeof InstallTrigger !== 'undefined';
var isOpera = (!!window.opera && !!window.opera.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && window['safari'].pushNotification));
class Addexperiance extends React.Component{
  dataimageObject = {}
  constructor(props){
    super(props)
    this.steps=localStorage.getItem('step');

    if(this.steps==0 || this.steps==1 || this.steps==2 || this.steps==3 || this.steps==4 )
    {
        this.state = {
            step : this.steps,
            userImage:'',       
            };
   }
   else
   {
    this.state = {
        step : 1,
        userImage:'',       
        };

   }

    this.reactTags = React.createRef();
    this.state = {
      situimgShow : false,
      taskimgShow : false,
      actimgShow : false,
      resimgShow : false,
      genimgShow : false,
      situaudioShow : false,
      taskaudioShow : false,
      actaudioShow : false,
      resaudioShow : false,
      genaudioShow : false,
      situdocShow : false,
      taskdocShow : false,
      actdocShow : false,
      resdocShow : false,
      gendocShow : false,
      addTitleInput : false,
      addLocation:false,
      situationInput : true,
      taskInput : false,
      actionInput : false,
      resultInput : false,
      openMic : false,
      openCam:false,
      tagdomain:false,
      genvideochng:false,
      situvideochng:false,
      taskvideochng:false,
      actvideochng:false,
      resvideochng:false,
      ModalShow : false,
      photoIndex: 0,
      location:'',
      textboxId : '',
      imagevalue:[],
      situimagevalue:[],
      taskimagevalue:[],
      actimagevalue:[],
      resimagevalue:[],
      imgiconvalue:[],
      situaudioFile : [],
      taskaudioFile : [],
      actaudioFile : [],
      resaudioFile : [],
      genaudioFile : [],
      situpdfFile:[],
      taskpdfFile:[],
      actpdfFile:[],
      respdfFile:[],
      genpdfFile:[],
      gettaguser:[],
      suggestions: [],
      tags: [],
      genvideo:[],
      situvideo:[],
      taskvideo:[],
      actvideo:[],
      resvideo:[],
      summery:'',
      situation:'',
      task:'',
      action:'',
      result:'',
      title:'My Experience',
      videoShow:false,
      situvideoShow:false,
      taskvideoShow:false,
      actvideoShow:false,
      resvideoShow:false,
      videovalue:[],
      situvideovalue:[],
      taskvideovalue:[],
      actvideovalue:[],
      resvideovalue:[],
      imagevalueLink:[],
      situimagevalueLink:[],
      taskimagevalueLink:[],
      actimagevalueLink:[],
      resimagevalueLink:[],
      situpdfFileLink:[],
      taskpdfFileLink:[],
      actpdfFileLink:[],
      respdfFileLink:[],
      genpdfFileLink:[],
      genvideoLink:[],
      situvideoLink:[],
      taskvideoLink:[],
      actvideoLink:[],
      resvideoLink:[],
      situaudioFileLink:[],
      taskaudioFileLink:[],
      actaudioFileLink:[],
      resaudioFileLink:[],
      genaudioFileLink:[],
      webcamBase64:[],
      webcamAwsLink:'',
      audioName:'',
      docName:'',
      domainsuggestions: [],
      domaintags: [],
      getdomaintaguser:[],
      jobtags:[],
      getjobtag:[],
      jobtagsuggestions:[],
      toggleindex:1,
      showstarframework:false,
      indexarray : ['','situation', 'task','action','result'],
      inputtoggletext:[false,false,false, false,false],
      audioDetails: {
        url: null,
        blob: null,
        chunks: null,
        duration: {
          h: 0,
          m: 0,
          s: 0
        },
      },
      showaudio:false,
     
    }
    UserlistTag().then(res=>{
      this.setState({
        gettaguser:res['data']['data']
      
      });
      this.state.gettaguser.forEach(el=>{
        this.state.suggestions.push({id:el.user_id,name:el.user_fname+' '+el.user_lname});
      })


        });


        DomainTag().then(res=>{
          this.setState({
            getdomaintaguser:res['data']['data']
          
          });
          console.log(this.state.getdomaintaguser);
          this.state.getdomaintaguser.forEach(el=>{
            this.state.domainsuggestions.push({id:el.user_exp_dom_id,name:el.user_exp_domain});
          })
              console.log(this.state.domainsuggestions);
    
            });


            jobtagslist().then(res=>{
              this.setState({
                getjobtag:res['data']['data']
              
              });
              console.log(this.state.getjobtag);
              this.state.getjobtag.forEach(el=>{
                this.state.jobtagsuggestions.push({id:el.job_skills_id,name:el.job_skills_name});
              })
                  console.log(this.state.jobtagsuggestions);
        
                });



                this.togglestarframework = this.togglestarframework.bind(this);
                this.toggletext= this.toggletext.bind(this);
                this.toggleaudio = this.toggleaudio.bind(this);
               
                
  }

  
  componentDidMount()
  {
    
  }
    componentWillReceiveProps(nextProps)
    {
      console.log(nextProps.videodata);
      console.log(nextProps.blobvideodata);
      this.setState({webcamBase64:nextProps.videodata});
      fileUpload(nextProps.blobvideodata,'expWebcam').then((awsLink)=>{
        this.setState({ webcamAwsLink:awsLink},console.log(this.state.webcamAwsLink));
     })
      if(nextProps.show)
      {
        this.setState({ ModalShow:true});
      }
      else
      {
        this.setState({ ModalShow:false});
      }
if(this.state.openMic)
{
      if(nextProps.text.textboxId == 'exsummery')
      {
        this.setState({summery :nextProps.text.text});
      }
      else if(nextProps.text.textboxId == 'exsituation')
            {
              this.setState({situation :nextProps.text.text});
            } 
            else if(nextProps.text.textboxId == 'extask')
                  {
                    this.setState({task :nextProps.text.text});
                  }
                  else if(nextProps.text.textboxId == 'exaction')
                  {
                    this.setState({action :nextProps.text.text});
                  }
                  else if(nextProps.text.textboxId == 'exresult')
                  {
                    this.setState({result :nextProps.text.text});
                  }
                  else if(nextProps.text.textboxId == 'extitle')
                        {
                          this.setState({title :nextProps.text.text});
                        }
                        else
                        {
                          this.setState({location :nextProps.text.text});
                        }
}

    }
    isOpenTag = (e)=>{
      const currentstate=this.state.tagdomain;
      this.setState({tagdomain:!currentstate});
    }

    togglestarframework() {
      const currentState=this.state.showstarframework;
      this.setState({ showstarframework: !currentState});
      
    }

     changeInput = (index,id)=>{
    const currentState = index;
    this.setState({ step: currentState,toggleindex:index });
    localStorage.setItem('step',currentState);
    
    // console.log(e);
    switch(id){
      case 'title' :
      this.props.clearVideo();
      this.setState({addTitleInput:!this.state.addTitleInput});
      break;
      case 'situation' :
      this.props.clearVideo();
      this.setState({situationInput:!this.state.situationInput});
      this.setState({taskInput:false});
      this.setState({actionInput:false});
      this.setState({resultInput:false});
      break;
      case 'task' :
      this.props.clearVideo();
      this.setState({taskInput:!this.state.taskInput});
      this.setState({situationInput:false});
      this.setState({actionInput:false});
      this.setState({resultInput:false});
      break;
      case 'action' :
      this.props.clearVideo();
      this.setState({actionInput:!this.state.actionInput});
      this.setState({situationInput:false});
      this.setState({taskInput:false});
      this.setState({resultInput:false});
      break;
      case 'result' :
      this.props.clearVideo();
      this.setState({resultInput:!this.state.resultInput});
      this.setState({situationInput:false});
      this.setState({actionInput:false});
      this.setState({taskInput:false});
      break;
      case 'description':
        this.props.clearVideo();
        this.setState({situationInput:false});
        this.setState({actionInput:false});
        this.setState({taskInput:false});
        this.setState({resultInput:false});
      break;
    } 
  }


  changeaddInput = ()=>{
    this.props.clearVideo();
    this.setState({addTitleInput:!this.state.addTitleInput});
  }

  handlesummary = (e) =>{
    this.setState({summery:e.target.value});
  }
  handletitle = (e) =>{
    this.setState({title:e.target.value});
  }
  handlelocation = (e) =>{
    this.setState({location:e.target.value});
  }
  handlesituation = (e) =>{
    this.setState({situation:e.target.value});
  }
  handletask = (e) =>{
    this.setState({task:e.target.value});
  }
  handleaction = (e) =>{
    this.setState({action:e.target.value});
  }
  handleresult = (e) =>{
    this.setState({result:e.target.value});
  }
  onDelete (i) {
    const tags = this.state.tags.slice(0)
    tags.splice(i, 1)
    this.setState({ tags })
   // console.log(this.state.tags);
  }
  
  onAddition (tag) {
    const tags = [].concat(this.state.tags, tag)
    this.setState({ tags })
    setTimeout(() => {
    }, 2000);
   
  }

  onDeletedomain (i) {
    const domaintags = this.state.domaintags.slice(0)
    domaintags.splice(i, 1)
    this.setState({ domaintags })
   // console.log(this.state.tags);
  }
  

  onAdditiondomain (domaintag) {
    const domaintags = [].concat(this.state.domaintags, domaintag)
    this.setState({ domaintags })
    setTimeout(() => {
    }, 2000);
   
  }

  onDeletejob (i) {
    const jobtags = this.state.jobtags.slice(0)
    jobtags.splice(i, 1)
    this.setState({ jobtags })
   // console.log(this.state.tags);
  }
  

  onAdditionjob (jobtag) {
    const jobtags = [].concat(this.state.jobtags, jobtag)
    this.setState({ jobtags })
    setTimeout(() => {
    }, 2000);
   
  }

  handlesubmit = (e)=>{
    console.log(this.state);
    console.log(this.state.domaintags);
    console.log(this.state.jobtags);
    console.log(this.state.genvideoLink);
    console.log(this.state.genpdfFileLink)
    this.props.startLoader();
    
  if(this.state.title !=''){
   AddUserExp(this.state.title,this.state.situation,this.state.situimagevalueLink,
    this.state.taskimagevalueLink,this.state.actimagevalueLink,this.state.resimagevalueLink,
    this.state.imagevalueLink,this.state.situaudioFileLink,this.state.taskaudioFileLink,
    this.state.actaudioFileLink,this.state.resaudioFileLink,this.state.genaudioFileLink,
    this.state.situpdfFileLink,this.state.actpdfFileLink,this.state.taskpdfFileLink,
    this.state.respdfFileLink,this.state.genpdfFileLink,this.state.task,this.state.action,
    this.state.result,this.state.summery,this.state.imagevalue,this.state.tags,this.state.domaintags,this.state.jobtags,
    this.state.audioFile,this.state.pdfFile,this.state.location,this.state.genvideoLink,
    this.state.situvideoLink,this.state.taskvideoLink,this.state.actvideoLink,
    this.state.resvideoLink).then(res=>{
     toast.success(res.data.msg);
     window.location.reload();
     this.props.hideLoader();
     
   });
  }else{
   this.props.hideLoader();
    toast.error('Please enter experience title')
    toast.error('Please enter Summary')
  }
   
 
  }
 
  openMic = (id) =>{
    this.setState({openMic : !this.state.openMic})
    this.setState({textboxId : id});
  }
  openCam = () =>{
    this.props.clearVideo();
    this.setState({openCam : !this.state.openCam});
    if(!this.state.situationInput &&!this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
    {
      this.setState({genvideochng:false});
      this.setState({videoShow: true});
      this.state.videovalue.pop();
      this.state.genvideo.pop();
    }
    else if(this.state.situationInput &&!this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
    {
      this.setState({situvideochng:false});
      this.setState({situvideoShow: true});
      this.state.situvideovalue.pop();
      this.state.situvideo.pop();
    }
    else if(!this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
    {
      this.setState({taskvideochng:false});
      this.setState({taskvideoShow: true});
      this.state.taskvideovalue.pop();
      this.state.taskvideo.pop();
    }
    else if(!this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput)
    {
      this.setState({actvideochng:false});
      this.setState({actvideoShow: true});
      this.state.actvideovalue.pop();
      this.state.actvideo.pop();
    }
    else if(!this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput)
    {
      this.setState({resvideochng:false});
      this.setState({resvideoShow: true});
      this.state.resvideovalue.pop();
      this.state.resvideo.pop();
    }
} 
  imageChange = (e)=>{
    let imageTemoArray=[];
    let link=[];
    e.preventDefault();
   
  
    for (let i = 0; i < e.target.files.length; i++) {
      let file = e.target.files[i];
      fileUpload(file,'experience').then((awsLink)=>{
        link.push(awsLink);
     })
      let reader = new FileReader();
      reader.readAsDataURL(file);
     
      const scope = this
      reader.onload = function(event) {
      imageTemoArray.push(event.target.result);
    if(scope.state.situationInput &&!scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
     {
      scope.setState({ situimagevalue:imageTemoArray},console.log(scope.state.situimagevalue))
      scope.setState({ situimagevalueLink:link},console.log(scope.state.situimagevalueLink))
      if(scope.state.situimagevalue.length>0)
      {
       scope.setState({situimgShow: true});
      }
      }else if(scope.state.taskInput && !scope.state.situationInput && !scope.state.actionInput && !scope.state.resultInput)
      {
       scope.setState({ taskimagevalue:imageTemoArray},console.log(scope.state.taskimagevalue))
       scope.setState({ taskimagevalueLink:link},console.log(scope.state.taskimagevalueLink))
       if(scope.state.taskimagevalue.length>0)
       {
        scope.setState({taskimgShow: true});
       }
       }else if(scope.state.actionInput &&!scope.state.taskInput && !scope.state.situationInput && !scope.state.resultInput)
       {
        scope.setState({ actimagevalue:imageTemoArray},console.log(scope.state.actimagevalue))
        scope.setState({ actimagevalueLink:link},console.log(scope.state.actimagevalueLink))
        if(scope.state.actimagevalue.length>0)
        {
         scope.setState({actimgShow: true});
        }
        }else if(scope.state.resultInput &&!scope.state.situationInput && !scope.state.actionInput && !scope.state.taskInput)
        {
         scope.setState({ resimagevalue:imageTemoArray},console.log(scope.state.resimagevalue))
         scope.setState({ resimagevalueLink:link},console.log(scope.state.resimagevalueLink))
         if(scope.state.resimagevalue.length>0)
         {
          scope.setState({resimgShow: true});
         }
         }else {
          scope.setState({ imagevalue:imageTemoArray},console.log(scope.state.imagevalue))
          scope.setState({ imagevalueLink:link},console.log(scope.state.imagevalueLink))
          if(scope.state.imagevalue.length>0)
         {
          scope.setState({genimgShow: true});
         }
         }
      }
    
    
  }
  
 
  } 
  videoChange = (e)=>{
   let videoTemoArray=[];
   let link=[];
    e.preventDefault();
    for (let i = 0; i < e.target.files.length; i++) {
      let file = e.target.files[i];
      fileUpload(file,'experience').then((awsLink)=>{
        link.push(awsLink);
     })
    let reader = new FileReader();
    reader.readAsDataURL(file);
   
    const scope = this
    reader.onload = function(event) {
      scope.props.startLoader();
      videoTemoArray.pop();
      videoTemoArray.push(event.target.result);
      if(!scope.state.situationInput &&!scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
       {
        scope.props.clearVideo();
        scope.state.genvideo.pop();
        scope.state.videovalue.pop();
        scope.setState({videoShow: false});
        scope.setState({genvideochng:true});
        scope.setState({ videovalue:videoTemoArray},console.log(scope.state.videovalue))
        scope.state.genvideoLink.pop();
        scope.setState({ genvideoLink:link},console.log(scope.state.genvideoLink))
        console.log(scope.state.genvideoLink)
        scope.props.hideLoader();
        console.log(scope.state.videovalue);
        //scope.dataimageObject=scope.state.videovalue;
       }
       else  if(scope.state.situationInput && !scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
       {
        scope.props.clearVideo();
        scope.state.situvideo.pop();
        scope.state.situvideovalue.pop();
        scope.setState({situvideoShow: false});
        scope.setState({situvideochng:true});
        scope.setState({ situvideovalue:videoTemoArray},console.log(scope.state.situvideovalue))
        scope.state.situvideoLink.pop();
        scope.setState({ situvideoLink:link},console.log(scope.state.situvideoLink))
        scope.props.hideLoader();
        console.log(scope.state.situvideovalue);
        //scope.dataimageObject=scope.state.videovalue;
       }
       else  if(!scope.state.situationInput && scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
       {
        scope.props.clearVideo();
        scope.state.taskvideo.pop();
        scope.state.taskvideovalue.pop();
        scope.setState({taskvideoShow: false});
        scope.setState({taskvideochng:true});
        scope.setState({ taskvideovalue:videoTemoArray},console.log(scope.state.taskvideovalue))
        scope.state.taskvideoLink.pop();
        scope.setState({ taskvideoLink:link},console.log(scope.state.taskvideoLink))
        scope.props.hideLoader();
        console.log(scope.state.taskvideovalue);
        //scope.dataimageObject=scope.state.videovalue;
       }
       else  if(!scope.state.situationInput && !scope.state.taskInput && scope.state.actionInput && !scope.state.resultInput)
       {
        scope.props.clearVideo();
        scope.state.actvideo.pop();
        scope.state.actvideovalue.pop();
        scope.setState({actvideoShow: false});
        scope.setState({actvideochng:true});
        scope.setState({ actvideovalue:videoTemoArray},console.log(scope.state.actvideovalue))
        scope.state.actvideoLink.pop();
        scope.setState({ actvideoLink:link},console.log(scope.state.actvideoLink))
        scope.props.hideLoader();
        console.log(scope.state.actvideovalue);
        //scope.dataimageObject=scope.state.videovalue;
       }
       else  if(!scope.state.situationInput && !scope.state.taskInput && !scope.state.actionInput && scope.state.resultInput)
       {
        scope.props.clearVideo();
        scope.state.resvideo.pop();
        scope.state.resvideovalue.pop();
        scope.setState({resvideoShow: false});
        scope.setState({resvideochng:true});
        scope.setState({ resvideovalue:videoTemoArray},console.log(scope.state.resvideovalue))
        scope.state.resvideoLink.pop();
        scope.setState({ resvideoLink:link},console.log(scope.state.resvideoLink))
        scope.props.hideLoader();
        console.log(scope.state.resvideovalue);
        //scope.dataimageObject=scope.state.videovalue;
       }
    }
    
  }
  
 
  }
locationChange=(e)=>
{
      this.setState({addLocation:!this.state.addLocation});
}


toggletext(index)
{
  let temp = this.state.inputtoggletext;
  temp[index] = !this.state.inputtoggletext[index];
  
  this.setState({inputtoggletext:temp});
  console.log(this.state.inputtoggletext);
}  

  audioChange = (e) =>{
    console.group(e.target.files[0]);
    if(e.target.files[0])
    {
      let file = e.target.files[0];
      console.log(e);
        this.setState({audioName:file.name});
      let link=[];
      fileUpload(file,'experience').then((awsLink)=>{
        console.log(awsLink);
        link.push(awsLink);
        console.log(link)
    })
      let audioTemoArray=[];
      let reader = new FileReader();
      reader.readAsDataURL(file);
      const scope = this
      reader.onload = function(event) {
        //console.log(event.target.result);
        //imageTemoArray.push(reader.result);
        audioTemoArray.push(event.target.result);
        if(scope.state.situationInput &&!scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
        {
          scope.setState({ situaudioFile:audioTemoArray})
          scope.setState({ situaudioFileLink:link})
          console.log(scope.state.situaudioFile.length);
          if(scope.state.situaudioFile.length>0)
          {
          scope.setState({situaudioShow: true});
          }
        //scope.dataimageObject=scope.state.audioFile;
        //onsole.log(this.state.audioFile);
          }
          else  if(!scope.state.situationInput && scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
          {
            scope.setState({ taskaudioFile:audioTemoArray})
            scope.setState({ taskaudioFileLink:link})
            console.log(scope.state.taskaudioFile.length);
            if(scope.state.taskaudioFile.length>0)
            {
            scope.setState({taskaudioShow: true});
            }
          //scope.dataimageObject=scope.state.audioFile;
          //onsole.log(this.state.audioFile);
          }
          else  if(!scope.state.situationInput && !scope.state.taskInput && scope.state.actionInput && !scope.state.resultInput)
          {
            scope.setState({ actaudioFile:audioTemoArray})
            scope.setState({ actaudioFileLink:link})
            //console.log(scope.state.taskaudioFile.length);
            if(scope.state.actaudioFile.length>0)
            {
            scope.setState({actaudioShow: true});
            }
          //scope.dataimageObject=scope.state.audioFile;
          //onsole.log(this.state.audioFile);
          }
          else  if(!scope.state.situationInput && !scope.state.taskInput && !scope.state.actionInput && scope.state.resultInput)
          {
            scope.setState({ resaudioFile:audioTemoArray})
            scope.setState({ resaudioFileLink:link})
            //console.log(scope.state.taskaudioFile.length);
            if(scope.state.resaudioFile.length>0)
            {
            scope.setState({resaudioShow: true});
            }
          //scope.dataimageObject=scope.state.audioFile;
          //onsole.log(this.state.audioFile);
          }
          else
          {
            scope.setState({ genaudioFile:audioTemoArray})
            scope.setState({ genaudioFileLink:link})
            //console.log(scope.state.taskaudioFile.length);
            if(scope.state.genaudioFile.length>0)
            {
            scope.setState({genaudioShow: true});
            }
          //scope.dataimageObject=scope.state.audioFile;
          //onsole.log(this.state.audioFile);
          }

      }
  }
  }
  pdfChange = (e) =>{
    if(e.target.files[0])
    {
    let pdfTemoArray=[];
    let file = e.target.files[0];
    this.setState({docName:file.name});
    let link=[];
    fileUpload(file,'experience').then((awsLink)=>{
      link.push(awsLink);
   })
    let reader = new FileReader();
    reader.readAsDataURL(file);
    const scope = this
    reader.onload = function(event) {
      console.log(event.target.result);
      pdfTemoArray.push(event.target.result);
      if(scope.state.situationInput &&!scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
      {
      scope.setState({situpdfFile: pdfTemoArray});
      scope.setState({situpdfFileLink: link});
      //console.log(scope.state.pdfFile);
      if(scope.state.situpdfFile.length>0)
      {
        scope.setState({situdocShow: true});
      }
    }
    else if(!scope.state.situationInput && scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
    {
    scope.setState({taskpdfFile: pdfTemoArray});
    scope.setState({taskpdfFileLink: link});
    //console.log(scope.state.pdfFile);
    if(scope.state.taskpdfFile.length>0)
    {
      scope.setState({taskdocShow: true});
    }
  }
  else if(!scope.state.situationInput && !scope.state.taskInput && scope.state.actionInput && !scope.state.resultInput)
  {
  scope.setState({actpdfFile: pdfTemoArray});
  scope.setState({actpdfFileLink: link});
  //console.log(scope.state.pdfFile);
  if(scope.state.actpdfFile.length>0)
  {
    scope.setState({actdocShow: true});
  }
}
else if(!scope.state.situationInput && !scope.state.taskInput && !scope.state.actionInput && scope.state.resultInput)
{
scope.setState({respdfFile: pdfTemoArray});
scope.setState({respdfFileLink: link});
//console.log(scope.state.pdfFile);
if(scope.state.respdfFile.length>0)
{
  scope.setState({resdocShow: true});
}
}
else if(!scope.state.situationInput && !scope.state.taskInput && !scope.state.actionInput && !scope.state.resultInput)
{
scope.setState({genpdfFile: pdfTemoArray});
scope.setState({genpdfFileLink: link});
//console.log(scope.state.pdfFile);
if(scope.state.genpdfFile.length>0)
{
  scope.setState({gendocShow: true});
}
}

    }
  }
  }
 
            handleAudioStop(data){
              console.log(data)
              this.setState({ audioDetails: data });
          }

          handleAudioUpload(file) {
            console.log(file);
          //       var reader = new FileReader();
          //      reader.readAsDataURL(file); 
          //      reader.onloadend = function() {
          //      var base64data = reader.result;                
          //      console.log(base64data);
          //  }
          }

          handleRest() {
            const reset = {
              url: null,
              blob: null,
              chunks: null,
              duration: {
                h: 0,
                m: 0,
                s: 0
              }
            };
            this.setState({ audioDetails: reset });
          }

          toggleaudio() {
            const currentState=this.state.showaudio;
            this.setState({ showaudio: !currentState});
           
          }


    render(){
      
      if(this.state.videoShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
         if(this.state.webcamBase64!='')
         {
          this.state.genvideo[0]=this.state.webcamBase64;
          this.state.genvideoLink.pop();
          this.state.genvideoLink[0]=this.state.webcamAwsLink;
         }
      }
      else if(this.state.genvideochng && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
        if(this.state.videovalue.length>0)
        {
        this.state.videovalue.forEach(el=>{
          this.state.genvideo[0]=el;
        })
        }
        
      }
      if(this.state.situvideoShow && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
        if(this.state.webcamBase64!='')
        {
         this.state.situvideo[0]=this.state.webcamBase64;
         this.state.situvideoLink.pop();
         this.state.situvideoLink[0]=this.state.webcamAwsLink;
        }
      }
      else if(this.state.situvideochng && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
        console.log(this.state.situvideovalue);
        if(this.state.situvideovalue.length>0)
        {
        this.state.situvideovalue.forEach(el=>{
          this.state.situvideo[0]=el;
        })
        }
      }
      if(this.state.taskvideoShow && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
        if(this.state.webcamBase64!='')
        {
         this.state.taskvideo[0]=this.state.webcamBase64;
         this.state.taskvideoLink.pop();
         this.state.taskvideoLink[0]=this.state.webcamAwsLink;
        }
      }
      else if(this.state.taskvideochng && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput)
      {
        console.log(this.state.taskvideovalue);
        if(this.state.taskvideovalue.length>0)
        {
        this.state.taskvideovalue.forEach(el=>{
          this.state.taskvideo[0]=el;
        })
        }
      }
      if(this.state.actvideoShow && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput)
      {
        if(this.state.webcamBase64!='')
        {
         this.state.actvideo[0]=this.state.webcamBase64;
         this.state.actvideoLink.pop();
         this.state.actvideoLink[0]=this.state.webcamAwsLink;
        }
      }
      else if(this.state.actvideochng && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput)
      {
        console.log(this.state.actvideovalue);
        if(this.state.actvideovalue.length>0)
        {
        this.state.actvideovalue.forEach(el=>{
          this.state.actvideo[0]=el;
        })
        }
      }
      if( this.state.resvideoShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput)
      {
        if(this.state.webcamBase64!='')
        {
         this.state.resvideo[0]=this.state.webcamBase64;
         this.state.resvideoLink.pop();
         this.state.resvideoLink[0]=this.state.webcamAwsLink;
        }
      }
      else if(this.state.resvideochng && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput)
      {
        console.log(this.state.resvideovalue);
        if(this.state.resvideovalue.length>0)
        {
        this.state.resvideovalue.forEach(el=>{
          this.state.resvideo[0]=el;
        })
        }
      }
      let requestModalClose =() =>{this.setState({openMic:false})}
      let requestModalCamClose =() =>{this.setState({openCam:false})}
    
      const situimages = [];
      const taskimages = [];
      const actionimages = [];
      const resultimages = [];
      const images = [];
      const videos = [];
     if(this.state.situationInput){
      this.state.situimagevalue.forEach(el=>{
        situimages.push({url:el});
      })
    }
    else if(this.state.taskInput)
    {
      this.state.taskimagevalue.forEach(el=>{
        taskimages.push({url:el});
      })
    }
    else if(this.state.actionInput)
    {
      this.state.actimagevalue.forEach(el=>{
        actionimages.push({url:el});
      })
    }
    else if(this.state.resultInput)
    {
      this.state.resimagevalue.forEach(el=>{
        resultimages.push({url:el});
      })
    }
    else
    {
      this.state.imagevalue.forEach(el=>{
        images.push({url:el});
      })
    }
      this.state.videovalue.forEach(el=>{
        videos.push(el);
      })

         
        return(
            <Modal
            {...this.props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            show={this.state.ModalShow}
            onHide={this.props.onHide}
            className="raexpmodal"
          >
              
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Add Your Experience
              </Modal.Title>
              {this.state.showstarframework && 
              <a href="#" className="quckadd" onClick={this.togglestarframework} > Advance Add</a> 
              }
              {  this.state.showstarframework == false &&
                <a href="#" className="quckadd" onClick={this.togglestarframework}> Quick Add </a>
              }
               
            </Modal.Header>
            <Modal.Body className="palash">
           
            <div>
              {this.state.showstarframework==false &&
              <>
               <div className="raiconnext">
               {/* <a  id= "general" className={(this.state.toggleindex=='0'? 'ractive' : '')}  onClick={()=>this.changeInput(0,'description')}><i className="fa fa-child"></i><span>General</span></a> */}
               <a id= "situation" className={(this.state.toggleindex=='1'? 'ractive' : '')}  onClick={()=>this.changeInput(1,'situation')}><i className="fa fa-hourglass-start"></i><span>Situation</span></a>
               <a  id="task" className={(this.state.toggleindex=='2'? 'ractive' : '')}><i onClick={()=>this.changeInput(2,'task')}className="fa fa-tasks"></i><span>Task</span></a>
               <a id="action" className= {(this.state.toggleindex=='3'? 'ractive' : '')}><i onClick={()=>this.changeInput(3,'action')} className="fa fa-handshake-o"></i><span>Action</span></a>
               <a  id="result" className={(this.state.toggleindex=='4'? 'ractive' : '')} ><i onClick={()=>this.changeInput(4,'result')} className="fa fa-trophy"></i><span>Result</span></a>
               </div>
    

              <div className="raadexp">
              <a onClick={this.openCam} ><VideoCallIcon className="video_icon" /> Video </a>
              <a onClick={this.toggleaudio} ><MicIcon className="audio_icon" /> Audio</a>
              <a onClick={()=>this.toggletext(this.state.toggleindex)} ><TextFieldsIcon className="document_icon"/> Text</a>
              </div>

              {this.state.showaudio &&
                          <Recorder
                        record={true}
                        title={"Recording"}
                        audioURL={this.state.audioDetails.url}
                        showUIAudio
                        handleAudioStop={data => this.handleAudioStop(data)}
                        handleAudioUpload={data => this.handleAudioUpload(data)}
                        handleRest={() => this.handleRest()} 
                        />
                      }
            </>
            
    }

       

           

              {/* <div className="d-flex mb-2">
              <div className="addpic">
              <img src={this.props.profilepic} className="img-fluid" />	
              </div>
              <div className="poption">
        <p>{this.props.auth.user_fname} {this.props.auth.user_lname}</p>
                <div className="form-group">
                <select className="form-control" id="sel1" name="sellist1">
                <option>Private</option>
                <option>Public</option>
                </select>
                </div>
              </div>
              </div> */}


              <div className="ramodalh">
            <form>
         
              {this.state.showstarframework &&
              <>

               <div className="raadexp">
               <a onClick={this.openCam} ><VideoCallIcon className="video_icon" /> Video </a>
              <a onClick={()=>this.openMic('exsummery')} ><MicIcon className="audio_icon" /> Audio</a>
              <a onClick={()=>this.toggletext(this.state.toggleindex)} ><TextFieldsIcon className="document_icon"/> Text</a>

              </div>
              {this.state.inputtoggletext[0] === true &&
              <div className="form-group raquickadd">
              <textarea type="text" name="summery" onChange={this.handlesummary} className="form-control" id="exampleFormControlTextarea1" rows="6" defaultValue={this.state.summery} placeholder={"what about your Experience " }></textarea>
              {/* {isChrome && !isFirefox && !isOpera && !isSafari &&
              <a onClick={()=>this.openMic('exsummery')}><MicIcon className="audiospk"/></a>
              }    */}
              {/* <a onClick={this.openCam}><VideoCallIcon className="webcam"/></a> */} 
              <a  onClick={()=>this.toggletext(0)}><CloseIcon className="raclose"/></a>
              </div>
              }
              </>
              }
            
              {/* <div>{this.state.showstarframework==false && this.state.inputtoggletext[1] === true && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput}</div> */}
            
            {this.state.showstarframework==false && this.state.inputtoggletext[1] === true && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
            <div className="form-group raadvaddd">
            <textarea type="text" name="situation" onChange={this.handlesituation} className="form-control" id="exampleFormControlTextarea1" rows="6" defaultValue={this.state.situation} placeholder={"Describe your Experience Situation "  }></textarea>
            {/* {isChrome && !isFirefox && !isOpera && !isSafari &&
            <a onClick={()=>this.openMic('exsituation')}><MicIcon className="audiospk"/></a>
            } */}
            {/* <a onClick={this.openCam}><VideoCallIcon className="webcam"/></a> */}
            <a onClick={()=>this.toggletext(this.state.toggleindex)}><CloseIcon className="raclose"/></a>
            </div>
          }

            { this.state.showstarframework==false && this.state.inputtoggletext[2] === true && this.state.taskInput && !this.state.situationInput && !this.state.actionInput && !this.state.resultInput &&
            <div className="form-group raadvaddd">
            <textarea type="text" name="task" onChange={this.handletask} className="form-control" id="exampleFormControlTextarea1" rows="6" defaultValue={this.state.task} placeholder={"Describe your Experience Task "  }></textarea>
            {/* {isChrome && !isFirefox && !isOpera && !isSafari &&
            <a onClick={()=>this.openMic('extask')}><MicIcon className="audiospk"/></a>
            } */}
            {/* <a onClick={this.openCam}><VideoCallIcon className="webcam"/></a> */}
            <a onClick={()=>this.toggletext(this.state.toggleindex)}><CloseIcon className="raclose"/></a>
            </div>
    }
            { this.state.showstarframework==false && this.state.inputtoggletext[3] ===true && this.state.actionInput && !this.state.taskInput && !this.state.situationInput && !this.state.resultInput &&
            <div className="form-group raadvaddd">
            <textarea type="text" name="action" onChange={this.handleaction} className="form-control" id="exampleFormControlTextarea1" rows="6" defaultValue={this.state.action} placeholder={"Describe your Experience Action " }></textarea>
            {/* {isChrome && !isFirefox && !isOpera && !isSafari &&
            <a onClick={()=>this.openMic('exaction')}><MicIcon className="audiospk"/></a>
            } */}
            {/* <a onClick={this.openCam}><VideoCallIcon className="webcam"/></a> */}
            <a onClick={()=>this.toggletext(this.state.toggleindex)}><CloseIcon className="raclose"/></a>
            </div>
    }
            { this.state.showstarframework==false && this.state.inputtoggletext[4] === true && this.state.resultInput && !this.state.actionInput && !this.state.taskInput && !this.state.situationInput &&
            <div className="form-group raadvaddd">
            <textarea type="text" name="result" onChange={this.handleresult} className="form-control" id="exampleFormControlTextarea1" rows="6" defaultValue={this.state.result} placeholder={"Describe your Experience Result "}></textarea>
            {/* {isChrome && !isFirefox && !isOpera && !isSafari &&
            <a onClick={()=>this.openMic('exresult')}><MicIcon className="audiospk"/></a>
            } */}
            {/* <a onClick={this.openCam}><VideoCallIcon className="webcam"/></a> */}
            <a onClick={()=>this.toggletext(this.state.toggleindex)}><CloseIcon className="raclose"/></a>
            </div>
    }
            
    
   
            </form>
            <div>
            <div className="clearfix"></div>
             {/* {this.state.reviewShow && 
            <SimpleImageSlider
           width={520}
           height={234}
           images={images}
       />} */}
            </div>

                   <div className="radocfile">
                   {this.state.situaudioShow && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
                    <div className="pa-text">
                    <a href="#"><img src={audioImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ situaudioShow: false }) }} /></a> 
                    <div><p>{this.state.audioName}</p></div>
                    </div>
                   }
                    {this.state.taskaudioShow && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
                      <div className="pa-text">
                      <a href="#"><img src={audioImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ taskaudioShow: false }) }} /></a> 
                      <div><p>{this.state.audioName}</p></div>
                      </div>
                   }
                    {this.state.actaudioShow && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput &&
                     <div className="pa-text"> 
                      <a href="#"><img src={audioImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ actaudioShow: false }) }} /></a>
                      <div><p>{this.state.audioName}</p></div>
                     </div>
                   }
                    {this.state.resaudioShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput &&
                    <div className="pa-text">
                      <a href="#"><img src={audioImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ resaudioShow: false }) }} /></a>
                      <div><p>{this.state.audioName}</p></div>
                    </div>
                   }
                    {this.state.genaudioShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
                    <div className="pa-text">
                    <a href="#"><img src={audioImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ genaudioShow: false }) }} /></a>
                    <div><p>{this.state.audioName}</p></div>
                    </div>
                   }
                   {this.state.situdocShow && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
                    <div className="pa-text">
                    <a href="#"><img src={docImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ situdocShow: false }) }} /></a>
                    <div><p>{this.state.docName}</p></div>
                    </div>
      } 
      {this.state.taskdocShow && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
      <div className="pa-text">
      <a href="#"><img src={docImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ taskdocShow: false }) }} /></a>
      <div><p>{this.state.docName}</p></div>
      </div>
      } 
      {this.state.actdocShow && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput &&
      <div className="pa-text">
      <a href="#"><img src={docImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ actdocShow: false }) }} /></a>
      <div><p>{this.state.docName}</p></div>
      </div>
      } 
      {this.state.resdocShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput &&
      <div className="pa-text">
      <a href="#"><img src={docImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ resdocShow: false }) }} /></a>
      <div><p>{this.state.docName}</p></div>
      </div>
      } 
      {this.state.gendocShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
      <div className="pa-text">
      <a href="#"><img src={docImage} alt="" /><MDBCloseIcon onClick={() => { this.setState({ gendocShow: false }) }} /></a>
      <div><p>{this.state.docName}</p></div>
      </div>
      } 
                   {this.state.audioShow &&
      <a href="#" className="radoc"><i className="fa fa-remove"></i><span className="raindoc">Task</span><img src={audioImage} alt="" /></a> 
                   }
                   {this.state.docShow &&
      <a href="#" className="radoc"><i className="fa fa-remove"></i><span className="raindoc">Task</span><img src={docImage} alt="" /></a>} 
       </div>
            </div>
            <div className="clearfix"></div>
            <div className="adtitle d-flex justify-content-between">
              {/* {this.state.addTitleInput &&
                <button id="title" onClick={this.changeaddInput}>Title</button>
              } */}
              {/* {!this.state.addTitleInput &&
                <button id="title" onClick={this.changeaddInput}>Add Title</button>
               } */}
    
            {/* {this.state.addTitleInput &&
            <div className="form-group w-sm-75 mb-0">
            <textarea type="text" name="title" onChange={this.handletitle} className="form-control mb-0" defaultValue={this.state.title} placeholder="Add your experience title here" rows="1"/>
            {isChrome && !isFirefox && !isOpera && !isSafari &&
            <a onClick={()=>this.openMic('extitle')}><MicIcon className="audiospk"/></a>
            }
            <a onClick={this.openCam}><VideoCallIcon className="webcam"/></a>
            </div>
    } */}
            {/* {(this.state.resultInput || this.state.situationInput || this.state.actionInput || this.state.taskInput )?<button id="description" onClick={this.changeInput}>Add Description</button>:''
            } */}
            </div>
            {/* {this.state.addLocation &&
            <div className="input-group ralocation">
<i class="fa fa-map-marker" aria-hidden="true"></i>
            <input type="text" name="location" onChange={this.handlelocation} className="form-control mb-2" id="exampleFormControlInput1" defaultValue={this.state.location} placeholder="Add your experience location here"/>
            </div>
    } */}
            {this.state.tagdomain &&
            <div className="form-group ">
              
            <ReactTags
        ref={this.reactTags}
        tags={this.state.tags}
        suggestions={this.state.suggestions}
        onDelete={this.onDelete.bind(this)}
        onAddition={this.onAddition.bind(this)}

        placeholderText ={'Tag Colleagues'} />
        </div>
              }


{this.state.tagdomain &&
            <div className="form-group ">
              
            <ReactTags
        ref={this.reactTags}
        tags={this.state.domaintags}
        suggestions={this.state.domainsuggestions}
        onDelete={this.onDeletedomain.bind(this)}
        onAddition={this.onAdditiondomain.bind(this)}

        placeholderText ={'Tag Domain'} />
        </div>
              }

{this.state.tagdomain &&
            <div className="form-group ">
              
            <ReactTags
        ref={this.reactTags}
        tags={this.state.jobtags}
        suggestions={this.state.jobtagsuggestions}
        onDelete={this.onDeletejob.bind(this)}
        onAddition={this.onAdditionjob.bind(this)}

        placeholderText ={'Job Skill'} />
        </div>
              }

            <div className="clearfix"></div>
            
            {this.state.situimgShow && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
            <>
              <MDBCloseIcon onClick={() => { this.setState({ situimgShow: false })}} />
              <SimpleImageSlider
                  width={520}
                  height={234}
                  images={situimages}
              />
           </>
       }
        {this.state.taskimgShow && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
          <>
            <MDBCloseIcon onClick={() => { this.setState({ taskimgShow: false }) }} />  
            <SimpleImageSlider
                width={520}
                height={234}
                images={taskimages}
            />
          </>
       }
        {this.state.actimgShow && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput &&
            <>
             <MDBCloseIcon onClick={() => { this.setState({ actimgShow: false }) }} />  
            <SimpleImageSlider
               width={520}
               height={234}
               images={actionimages}
           />
          </>
       }
        {this.state.resimgShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput &&
          <>
            <MDBCloseIcon onClick={() => { this.setState({ resimgShow: false }) }} /> 
           <SimpleImageSlider
               width={520}
               height={234}
               images={resultimages}
       />
       </>
       }
        {this.state.genimgShow && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
            <>
            <MDBCloseIcon onClick={() => { this.setState({ genimgShow: false }) }} /> 
             <SimpleImageSlider
              width={520}
              height={234}
              images={images}
       />
       </>
       }
        {this.state.genvideochng && this.state.genvideo.length>0 && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
        <>
          <MDBCloseIcon onClick={() => { this.setState({ genvideochng: false }) }} />
          <ReactPlayer width={520} height={234} className="ravidp" controls playing url={this.state.genvideo} />
        </>
       }
        {this.state.videoShow && this.state.genvideo.length>0 && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
        <>
            <MDBCloseIcon onClick={() => { this.setState({ videoShow: false }) }} />
            <ReactPlayer width={520} height={234} className="ravidp" controls playing url={this.state.genvideo} />
       </>
       
       }
         {this.state.situvideochng && this.state.situvideo.length>0 && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
          <> 
           <MDBCloseIcon onClick={() => { this.setState({ situvideochng: false }) }} />
           <ReactPlayer width={520} height={234}  className="ravidp" controls playing url={this.state.situvideo} />
          </>
        }
         {this.state.situvideoShow && this.state.situvideo.length>0 && this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
          <>
            <MDBCloseIcon onClick={() => { this.setState({ situvideoShow: false }) }} />
            <ReactPlayer width={520} height={234} className="ravidp" controls playing url={this.state.situvideo} />
          </>
        }
         {this.state.taskvideochng && this.state.taskvideo.length>0 && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
            <>
             <MDBCloseIcon onClick={() => { this.setState({ taskvideochng: false }) }} />
             <ReactPlayer width={520} height={234} className="ravidp" controls playing url={this.state.taskvideo} />
           </>
        }
         {this.state.taskvideoShow && this.state.taskvideo.length>0 && !this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
          <>
            <MDBCloseIcon onClick={() => { this.setState({ taskvideoShow: false }) }} />
            <ReactPlayer width={520} height={234} className="ravidp" controls playing url={this.state.taskvideo} />
          </>
        }
         {this.state.actvideochng && this.state.actvideo.length>0 && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput &&
          <>
           <MDBCloseIcon onClick={() => { this.setState({ actvideochng: false }) }} /> 
           <ReactPlayer width={520} height={234}  className="ravidp" controls playing url={this.state.actvideo} />
          </>
        }
          {this.state.actvideoShow && this.state.actvideo.length>0 && !this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput &&
           <>
           <MDBCloseIcon onClick={() => { this.setState({ actvideoShow: false }) }} /> 
           <ReactPlayer width={520} height={234} className="ravidp" controls playing url={this.state.actvideo} />
           </>
        }
         {this.state.resvideochng  && this.state.resvideo.length>0 && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput &&
          <>
          <MDBCloseIcon onClick={() => { this.setState({ resvideochng: false }) }} /> 
          <ReactPlayer width={520} height={234} className="ravidp" controls playing url={this.state.resvideo} />
          </>
        }
         {this.state.resvideoShow && this.state.resvideo.length>0 && !this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput &&
           <>
           <MDBCloseIcon onClick={() => { this.setState({ resvideoShow: false }) }} /> 
           <ReactPlayer width={520} height={234} className="ravidp" controls playing url={this.state.resvideo} />
           </>
        }
       <div className="clearfix"></div>
       {/* {this.state.audioShow &&
       <img src={audioImage} alt="" />
    }
     {this.state.docShow &&
       <img src={docImage} alt="" />
     } */}
       <div className="clearfix"></div>
            <div className="d-flex justify-content-between exprimedia">
            {/* {this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
            <p>Add Your Situation Files</p>
            }
            {!this.state.situationInput && this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
            <p>Add Your Task Files</p>
            }
            {!this.state.situationInput && !this.state.taskInput && this.state.actionInput && !this.state.resultInput &&
            <p>Add Your Action Files</p>
            }
            {!this.state.situationInput && !this.state.taskInput && !this.state.actionInput && this.state.resultInput &&
            <p>Add Your Result Files</p>
            }
            {!this.state.situationInput && !this.state.taskInput && !this.state.actionInput && !this.state.resultInput &&
            <p>Add Your Experience</p>
            } */}
            <div></div>
            <div className="exmedia">
            {/* <a href="#">
            <input type="file" onChange={this.imageChange} accept="image/x-png,image/jpeg" multiple className="dropzone" style={{width:"4%",overflow:'hidden'}}/>
            <ImageIcon className="photo_icon"/>
            </a> */}
            {/* <a href="#">
            <input type="file" onChange={this.videoChange} accept="video/mp4" className="dropzone" style={{width:"4%",overflow:'hidden'}}/>
            <VideocamIcon className="video_icon"/>
            </a> */}
            {/* <a href="#">
            <input type="file" onChange={this.audioChange} accept="audio/mp3" className="dropzone" style={{width:"4%",overflow:'hidden'}}/>
              <GraphicEqIcon className="audio_icon"/></a>
            <a href="#">
            <input type="file" onChange={this.pdfChange} accept="application/pdf" className="dropzone" style={{width:"4%",overflow:'hidden'}}/>
              <DescriptionIcon className="document_icon"/></a>
            <a onClick={this.locationChange}>
              <LocationOnIcon className="location_icon"/>
              </a> */}
            {/* <a onClick={this.isOpenTag}><LocalOfferIcon className="tag_icon"/></a> */}
            </div>
         
            </div>
            <div className="clearfix"></div>
            <ReactTags
        ref={this.reactTags}
        tags={this.state.domaintags}
        suggestions={this.state.domainsuggestions}
        onDelete={this.onDeletedomain.bind(this)}
        onAddition={this.onAdditiondomain.bind(this)}

        placeholderText ={'Related Skill'} />

            {/* <span class="has-float-label mt-3">
            <input class="form-control" id="first" type="text" placeholder="Enter Your Skill"/>
            <label for="first">Job Skill</label>
            </span> */}
            {/* <div className="starf">
            <ul>
              <li><a id="situation" onClick={this.changeInput}>Situation</a></li>
              <li><a id="task" onClick={this.changeInput}>Task</a></li>
              <li><a id="action" onClick={this.changeInput}>Action</a></li>
              <li><a id="result" onClick={this.changeInput}>Result</a></li>
            </ul>
            <div className="clearfix"></div>
            </div> */}
            </div>
          
         
            </Modal.Body>
            <Modal.Footer className="addraft">
              {/* <Button onClick={this.props.onHide} >Close</Button> */}
              {this.state.toggleindex!='1'&& this.state.showstarframework==false &&
                <Button onClick={()=>this.changeInput(this.state.toggleindex-1,this.state.indexarray[this.state.toggleindex-1])} className="ranewpre" >Previous</Button>
              }
             {this.state.toggleindex!='4'&& this.state.showstarframework==false && 
             
               <Button  onClick={()=>this.changeInput(this.state.toggleindex+1,this.state.indexarray[this.state.toggleindex+1])} className="ranewnext">Next</Button>
             
             }
             { this.state.toggleindex=='4'&& this.state.showstarframework==false &&
              <Button onClick={this.handlesubmit} className="ranewnext">Submit Your Experience</Button>
             }

             {this.state.showstarframework && 
              <Button onClick={this.handlesubmit} className="ranewnext">Submit Your Experience</Button>
             }
              
             
              
            </Modal.Footer>
             <Speechtotext show={this.state.openMic} onHide={requestModalClose} textboxId={this.state.textboxId}/>
            <TempWebCam show={this.state.openCam} onHide={requestModalCamClose} />
          </Modal>
        );
                 
    }
   
}

const mapStateToProps = state=>{
  console.log(state);
  return{
      //loader : state.loaderReducer.loader,
      auth : state.authReducer.authData,
      profilepic : state.authReducer.profileImage,
      text : state.mediaReducer.text,
      videodata : state.mediaReducer.videoData,
      blobvideodata :state.mediaReducer.blobVideoData
    }
}

const mapDispatchFromProps = dispatch=>{
  return{
       startLoader : () =>dispatch(showLoader()),
      hideLoader : () =>dispatch(hideLoader()),
      clearVideo :()=>dispatch(emptyVideoData())
  }
}

export default connect(mapStateToProps,mapDispatchFromProps)(Addexperiance);