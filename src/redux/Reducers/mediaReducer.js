
import mediaTypes from '../Actions/mediaTypes';

const initialState = {text:'',videoData:'',blobVideoData:''};

export function mediaReducer(state = initialState,action){
    switch(action.type){
        case mediaTypes.saveText : return{
            text : '',
            ...state,
            text : action.text
        }
        case mediaTypes.fetchText : return{
            ...state,
            text : action.text
        }
        case mediaTypes.saveVideoData : return{
            videoData : '',
            blobVideoData:'',
            ...state,
            videoData : action.videoData,
            blobVideoData:action.blobVideoData
        }
        case mediaTypes.fetchVideoData : return{
            ...state,
            videoData : action.videoData,
            blobVideoData:action.blobVideoData
        }
        case mediaTypes.emptyVideoData : return{
           videoData : '',
           blobVideoData :''
        }
        default : return{
            ...state,
        }


    }
}