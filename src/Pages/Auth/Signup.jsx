import React from 'react';
import logo from '../../assets/images/logo.png';
import login from '../../assets/images/login.png';
import social1 from '../../assets/images/social-icon1.png';
import social2 from '../../assets/images/social-icon2.png';
import social3 from '../../assets/images/social-icon3.png';
import social4 from '../../assets/images/social-icon4.png';
import {Link} from 'react-router-dom';
import { signUp } from '../../providers/auth';
import { completeProfile } from '../../providers/auth';
import { verifyOtp } from '../../providers/auth';
import { toast } from 'react-toastify';
import DatePicker from "react-datepicker";  
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import { connect } from 'react-redux';
import AwesomeSlider from 'react-awesome-slider';
import '../../assets/css/awseme.css';


class Signup extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            fname: '',
            lname: '',
            password: '',
            cPassword : '',
            otp : '',
            dob: '',
            gender : '',
            phone : '',
            seletedSlides : 'slide1',
            position : '',
            company : '',
            hidden: true,
            confirmhidden: true,
            errors : {"email":'',"password" : '',"phone":'',"cPassword" : '',"otp" : '',"fname":'',"lname":'',"position":'',"company":''}
        }
        // this.toggleShow = this.toggleShow.bind(this);
        // this.confirmToggleShow = this.confirmToggleShow.bind(this);
    }
    toggleShow = ()=> {
        this.setState({ hidden: !this.state.hidden });
      }
   confirmToggleShow = ()=>{
    this.setState({ confirmhidden: !this.state.confirmhidden });
   }

    handleEmailChange = (e) =>{
        if(e.target.value !== "undefined" || e.target.value !==''){
       let pattern = new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(e.target.value);
        if (!pattern) {
            this.state.errors.email = "Please enter valid email address.";
          }else{
            this.state.errors.email = "";
          }
            
        }
        else{
            this.state.errors.email = "Please enter email address."
        }
        this.setState({email: e.target.value});   
     }
     handlePasswordChange = (e) =>{
        if(e.target.value !== "undefined" || e.target.value !==''){
            let pattern = new RegExp('/(?=.*[0-9])/').test(e.target.value);
             if (e.target.value.length<6) {
                 this.state.errors.password = "Invalid Password";
               }else{
                 this.state.errors.password = "";
               }
            }
            else{
                this.state.errors.password = "Please enter password."
            }
            this.setState({password: e.target.value});
     }
     
     handleFnameChange = (e) => {
         if(e.target.value.length<1 || e.target.value ==''){
            this.state.errors.fname = "Please enter your first name";
         }else{
            this.state.errors.fname = "";
         }
        this.setState({fname: e.target.value});
     }
     handleLnameChange = (e) => {
         if(e.target.value.length<1 || e.target.value ==''){
            this.state.errors.lname = "Please enter your last name";
         }else{
            this.state.errors.lname = "";
         }
        this.setState({lname: e.target.value});
     }
     handlepCasswordChange = (e) => {
         if(e.target.value !== this.state.password || e.target.value ==''){
            this.state.errors.cPassword = "Password and confirm password doesn't match.";
         }else{
            this.state.errors.cPassword = "";
         }
        this.setState({cPassword: e.target.value});
     }

     handleSignup = ()=> {
         let userType = 1;
       if(this.state.email != '' && this.state.password != '' && this.state.cPassword && this.state.fname && this.state.lname){
        this.props.startLoader();
       signUp(this.state.email,this.state.fname,this.state.lname,this.state.password,userType).then((res)=>{
        this.props.hideLoader();
           
           if(res.data.status){
               localStorage.setItem('authData',JSON.stringify(res.data.data)); 
               localStorage.setItem('authToken',res.data.token);
               toast.success(res.data.msg,{});
               this.setState({'seletedSlides':'slide2'})

           }else
           toast.error(res.data.msg,{}); 
       });
        }
        else{
            toast.error('Please fill mandetory fields.',{});
         }
   }

   handleOtpChange = (e) =>{
        if(e.target.value.length < 1 || e.target.value ==''){
            this.state.errors.otp = "Please enter your Otp." 
        }
        else{
            this.state.errors.password = "";
        }
        this.setState({'otp':e.target.value});
   }
   handleSkip = ()=>{
    this.setState({'seletedSlides':'slide4'})
   }

   handleOtp = () =>{
    if(this.state.otp != ''){
        verifyOtp(this.state.otp).then((res)=>{
            
            if(res.data.status){
                this.setState({'seletedSlides':'slide3'})
            }else{
                toast.error(res.data.msg);
            }
        });       
    }
    else{    
        toast.error('Please fill mandetory fields.',{});
    }
}
   handleNext = () =>{
    this.setState({'seletedSlides':'slide4'})
   }
   handlePrev = () =>{
    this.setState({'seletedSlides':'slide3'})
   }

   handleDateChange = (e) =>{
    this.setState({"dob" : e.target.value});
   }
   changeGender = (e)=>{
       this.setState({"gender":e.target.value});
    
   }
   handlePhoneChange = (e)=>{
       this.setState({"phone":e.target.value});
   }
   handlePositionChange = (e)=>{
       this.setState({"position":e.target.value});
   }
   handleCompanyChange = (e)=>{
       this.setState({"company":e.target.value});
   }
   handleFinish = ()=>{
       
    this.props.startLoader();
    completeProfile(this.state.dob,this.state.gender,this.state.phone,this.state.company,this.state.position).then(res=>{
        
        localStorage.setItem('authData',JSON.stringify(res.data.data));
         this.props.hideLoader();
         toast.success('Your Account is created Successfully',{});
         this.props.history.push('/profile');
        
        
    })
   }
   

    render(){
        return(
            <section id="login">
                <div className="container">
                <div className="logintotal d-sm-flex">
                <div className="col-sm-6 d-sm-flex pl-sm-0">
                <div className="login-left">
                <Link to="/home">
                <img src={logo} />
                </Link>

                <div className="login-slider">
                <AwesomeSlider> 
                <div className="carousel-inner">
                    <div className="carousel-item active">
                    <img src={login}/>
                    <h4>Track Progress</h4>
                    <p>Track progress, see how you fair against others with similar profiles and goals</p>
                    </div>
                    <div className="carousel-item">
                    <img src={login}/>
                    <h4>Track Progress</h4>
                    <p>Track progress, see how you fair against others with similar profiles and goals</p>
                    </div>
                    <div className="carousel-item">
                    <img src={login}/>
                    <h4>Track Progress</h4>
                    <p>Track progress, see how you fair against others with similar profiles and goals</p>
                    </div>
                </div>
                </AwesomeSlider> 
               

                </div>
                </div>
                </div>
                <div className="col-sm-6" style={{display : (this.state.seletedSlides == 'slide1' ? 'block':'none')}}>
                <div className="login-right">
                <h4>Welcome</h4>
                <p>Don't miss your next opportunity. Sign in to stay updated on your professional world.</p>
                <ul>
                <li><a href="#"><img src={social1}/></a></li>
                <li><a href="#"><img src={social3}/></a></li>
                <li><a href="#"><img src={social2}/></a></li>
                <li><a href="#"><img src={social4}/></a></li>
                </ul>
                <div className="loginwith">
                <h6>Or login with Email</h6>
                </div>
                <div className="form-group">
                <label>First Name</label>
                <input type="text" value={this.state.fname} onChange={this.handleFnameChange} className="form-control" placeholder="First Name"/>
                </div>
                <p style={{color:"red",fontSize:"14px"}}>{this.state.errors.fname}</p>
                <div className="form-group">
                <label>Last Name</label>
                <input type="text" value={this.state.lname} onChange={this.handleLnameChange} className="form-control" placeholder="Last Name"/>
                </div>
                <p style={{color:"red",fontSize:"14px"}}>{this.state.errors.lname}</p>
                <div className="form-group">
                <label>Email Address</label>
                <input value={this.state.email} onChange={this.handleEmailChange} type="email" className="form-control" placeholder="Email Address"/>
                </div>
                <p style={{color:"red",fontSize:"14px"}}>{this.state.errors.email}</p>
                <div className="form-group showeye">
                <label>Password</label>
                <input type={this.state.hidden ? "password" : "text"} className="form-control" placeholder="Password" value={this.state.password} onChange={this.handlePasswordChange}/>
                {this.state.hidden &&
                <i onClick={this.toggleShow} className="fa fa-eye-slash" aria-hidden="true"></i>
                    }
                    {!this.state.hidden &&
                        <i onClick={this.toggleShow} className="fa fa-eye" aria-hidden="true"></i> 
                    }
                </div>
                <p style={{color:"red",fontSize:"14px"}}>{this.state.errors.password}</p>
                <div className="form-group showeye">
                <label>Confirm Password</label>
                <input type={this.state.confirmhidden ? "password" : "text"} className="form-control" placeholder="Confirm Password" value={this.state.cPassword} onChange={this.handlepCasswordChange}/>
                {this.state.confirmhidden &&
                <i onClick={this.confirmToggleShow} className="fa fa-eye-slash" aria-hidden="true"></i>
                    }
                    {!this.state.confirmhidden &&
                        <i onClick={this.confirmToggleShow} className="fa fa-eye" aria-hidden="true"></i> 
                    }
                </div>
                <p style={{color:"red",fontSize:"14px"}}>{this.state.errors.cPassword}</p>
                <div className="form-group">
                <p><input type="checkbox"/> Keep me Logged in</p>
                </div>
                <div className="form-group">
                <button className="login-to-perceiveMe" onClick={this.handleSignup}>Signup to PerceiveMe</button>
                </div>

                <div className="join-now">
                <p>Already have an account? <Link to="/login">Login now</Link></p>
                </div>
                </div>

                </div>

                <div className="col-sm-6" style={{display : (this.state.seletedSlides == 'slide2' ? 'block':'none')}}>
                <div className="login-right">
                <h4>Welcome</h4>
                <p>Don't miss your next opportunity. Sign in to stay updated on your professional world.</p>
               
                <div className="form-group">
                <label>OTP</label>
                <input type="text"  onChange={this.handleOtpChange} className="form-control" placeholder="Enter your otp here"/>
                </div>
                <p style={{color:"red",fontSize:"14px"}}>{this.state.errors.otp}</p>
            
                <div className="form-group">
                <button className="login-to-perceiveMe" onClick={this.handleOtp}>Validate Otp</button>
                </div>

                </div>

                </div>

                <div className="col-sm-6" style={{display : (this.state.seletedSlides == 'slide3' ? 'block':'none')}}>
                <div className="login-right">
                <h4>Welcome</h4>
                <p>Don't miss your next opportunity. Sign in to stay updated on your professional world.</p>
                
                <div className="form-group">
                <label>Date of Birth</label>
                <div>
                <input type="date"  onChange={this.handleDateChange} className="form-control" />
                </div>
                
                </div>
                <p style={{color:"red",fontSize:"14px"}}>{this.state.errors.fname}</p>
                <div className="form-group">
                <label>Gender</label>
                <div>
                        <select id="company" className="form-control" onChange={this.changeGender}>
                        <option value=''>Select Your Gender</option>   
                        <option value='male'>Male</option>
                        <option value='female'>Female</option>
                        <option value='others'>Others</option>
                        </select> 
                    </div>
                </div>
                <div className="form-group">
                <label>Phone no</label>
                <input type="text" value={this.state.phone} onChange={this.handlePhoneChange} className="form-control" placeholder="Enter phone no"/>
                </div>

                <div className="row">
                <div className="form-group col-md-6">
                <button className="login-to-perceiveMe" onClick={this.handleSkip}>Skip</button>
                </div>
                <div className="form-group col-md-6">
                <button className="login-to-perceiveMe" onClick={this.handleNext}>Next</button>
                </div>
                </div>
               


                </div>

                </div>


                <div className="col-sm-6" style={{display : (this.state.seletedSlides == 'slide4' ? 'block':'none')}}>
                <div className="login-right">
                <h4>Welcome</h4>
                <p>Don't miss your next opportunity. Sign in to stay updated on your professional world.</p>
                
               
                <div className="form-group">
                <label>Current Position</label>
                <input type="text" value={this.state.position} onChange={this.handlePositionChange} className="form-control" placeholder="Current position"/>
                </div>
                <p style={{color:"red",fontSize:"14px"}}>{this.state.errors.position}</p>
                <div className="form-group">
                <label>Current Company</label>
                <input type="text" value={this.state.company} onChange={this.handleCompanyChange} className="form-control" placeholder="Current company"/>
                </div>
                <p style={{color:"red",fontSize:"14px"}}>{this.state.errors.company}</p>
                <div className="row">
                <div className="form-group col-md-6">
                <button className="login-to-perceiveMe" onClick={this.handlePrev}>Previous</button>
                </div>
                <div className="form-group col-md-6">
                <button className="login-to-perceiveMe" onClick={this.handleFinish}>Finish</button>
                </div>
                </div>
                


                </div>

                </div>
                </div>
                
                <div className="login-footer-menu">
                <ul>
                <li><a href="#">PerceiveMe.ai © 2020</a></li>
                <li><a href="#"> User</a></li>
                <li><a href="#">Agreement</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Community Guidelines</a></li>
                <li><a href="#">Cookie Policy</a></li>
                <li><a href="#">Copyright Policy</a></li>
                <li><a href="#">Send Feedback</a></li>
                </ul>

                </div>
                </div>
        </section>
        )
    }
}

const mapStateToProps = state=>{
    return{
        loader : state.loaderReducer.loader
      }
}

const mapDispatchFromProps = dispatch=>{
    return{
        startLoader : () =>dispatch(showLoader()),
        hideLoader : () =>dispatch(hideLoader())
    }
}

export default connect(mapStateToProps,mapDispatchFromProps)(Signup);