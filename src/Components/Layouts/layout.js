import React from 'react';
import Header from '../Header/header';
import Nav from '../Header/nav';
import LeftAside from '../Aside/leftAside';
import RightAside from '../Aside/rightAside';
import {Route,Switch} from 'react-router-dom';
import Experience from '../../Pages/Experience/experience';
import Mygoals from '../../Pages/Mygoals/mygoals';
import Mytest from '../../Pages/Mytest/mytest';
import Consulatation from '../../Pages/Consultation/consultation';
import Profile from '../../Pages/Profile/profile'; 
import Skills from '../../Pages/Skills/skills';
import Publicprofile from '../../Pages/Profile/Publicprofile'; 
import Consulatationhub from '../../Pages/Consultation/Consultationhub'; 
import Hubinner from '../../Pages/Consultation/Hubinner'; 
import Home from '../../Pages/Home/home';
import Subscription from '../../Pages/Subscription/Subscription';
import { connect } from 'react-redux';
import { manageAuth,updateAuth } from '../../redux/Actions/authAction';
import Createaadvisor from '../../Pages/Advisor/Createadvisor';


class Layout extends React.Component{

    constructor(props){
        super(props);
        this.layoutstep=localStorage.getItem('step');
    }
    componentDidMount(){
        if(this.layoutstep!=1)
        {
            const currentState = 1;
            localStorage.setItem('step',currentState);
            //window.location.reload();
        }
    }
    render(){
        return(
            <div>
                <Header/>
                {/* <Nav/> */}
                <section id="profile-sec2">
    <div className="container">
        <div className="row">
            <LeftAside/>

                                <Switch>
                                    <Route exact path='/' component={Home}/>
                                    <Route exact path='/experience' component={Experience}/>
                                    <Route exact path='/mygoals' component={Mygoals}/>
                                    <Route exact path='/mytest' component={Mytest}/>
                                    <Route exact path='/consultation' component={Consulatation}/>
                                    <Route exact path='/consulatationhub' component={Consulatationhub}/>
                                    <Route exact path='/hubinner' component={Hubinner}/>
                                    <Route exact path='/profile' component={Profile}/>
                                    <Route exact path='/skills' component={Skills}/>
                                    <Route exact path='/subscription' component={Subscription}/>
                                    <Route exact path='/publicprofile/:id?' component={Publicprofile}/>
                                    <Route exact path='/createadvisor' component={Createaadvisor}/>
                                    
                                  </Switch>

           
            <RightAside/>
            
            
            </div>
            </div>
        </section>
        </div>
        )
    }
}
const mapStateToProps = state=>{
    return{
      auth : state.authReducer.authData
  
    }
  }
const mapDisptchToProps = dispatch=>{
    return{
      fetchAuth : () =>dispatch(manageAuth()),
    }
    
  
  }
  
  export default connect(mapStateToProps,mapDisptchToProps)(Layout);





