
import { apiUrl } from '../const';
import axios from 'axios';
import { getToken,checkToken } from './token';
import React from 'react';
import {Redirect} from 'react-router-dom';

const signupUrl = 'user/create';

    export const signUp = (email,fname,lname,password,userType)=>{
    return new Promise( (resolve, reject) => {
     axios.post(apiUrl + signupUrl,{email : email,first_name : fname,last_name : lname,password : password, user_type : userType}).then(res=>{
         resolve(res);
     },err=>{reject(err)})
    })
}

const signinUrl = 'user/login';
export const signIn = (userName,passWord)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+signinUrl,{email:userName,password:passWord}).then(res=>{
            resolve(res);

        },err=>{reject(err)})
    })
}

const completeProfileUrl = 'completeprofile';
export const completeProfile = (userDob,userGender,userPhopne,userCompany,userDesignation)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+completeProfileUrl,{user_dob:userDob,user_gender:userGender,user_phno:userPhopne,user_current_company:userCompany,user_profile_designation:userDesignation},{
    headers: getToken()
  }).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const verifyOtpUrl = 'user/authenticate';
export const verifyOtp = (otp)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+verifyOtpUrl,{otp:otp},{
    headers: getToken()
  }).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const updateprofile = 'edit/profile';
export const updateProfile = (fname,lname,ph,address,gender,dob,fb,twiter,linkedin)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+updateprofile,{first_name:fname,last_name:lname,user_phno:ph,
        address:address,user_dob:dob,user_gender:gender,facebook:fb,twitter:twiter,linkedin:linkedin},{
    headers: getToken()
  }).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const getProfilepicUrl = 'profile/pic/show';
export const getProfilePic = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+getProfilepicUrl,{},{
            headers:

                getToken()
            
        }).then(res=>{
            resolve(res);
        }).catch((error) => {
            console.log(error);
            return <Redirect to='/login'/>
        })
    })
}
const getCoverpicUrl = 'cover/pic/show';
export const getCoverPic = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+getCoverpicUrl,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const educationListUrl = 'education/listing';
export const getEducationList = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+educationListUrl,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const workListUrl = 'work/listing';
export const workList = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+workListUrl,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const coverPicSaveUrl = 'cover/pic/save';
export const coverPicSave = (base64) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+coverPicSaveUrl,{image:base64},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const profilePicSaveUrl = 'profile/pic/save';
export const profilePicSave = (base64) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+profilePicSaveUrl,{image:base64},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const saveWorkUrl = 'user/work';
export const saveWork = (companyName,companyLocation,jobRole,companyWebsite,empType,startDate,endDate,description) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+saveWorkUrl,{previous_company:companyName,start_date:startDate,end_date:endDate,role:jobRole,location:companyLocation,website:companyWebsite,type:empType,description:description},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const EditWorkUrl = 'edit/work';
export const editWork = (jobId,companyName,companyLocation,jobRole,companyWebsite,empType,startDate,endDate,description) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+EditWorkUrl,{job_profile_id :jobId, previous_company:companyName,start_date:startDate,end_date:endDate,role:jobRole,location:companyLocation,website:companyWebsite,type:empType,description:description},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const adduserexp = 'user/experience';
export const AddUserExp = (title,situation,situimage,taskimage,actionimage,resultimage,genimage,situaudio,taskaudio,actaudio,resaudio,genaudio,situdoc,actdoc,taskdoc,resdoc,gendoc,task,action,solution,summery,solpic,tags,domain,jobskill,audio,pdf,location,genvideo,situvideo,taskvideo,actvideo,resvideo)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+adduserexp,{user_exp_title:title,situation_text:situation,task_text:task,action_text:action,
            result_text:solution, summary:summery,images:genimage,user:tags,domain:domain,job_skills:jobskill,user_audio:genaudio,gen_doc:gendoc,location:location,
            situation_files:situimage,task_files:taskimage,action_files:actionimage,result_files:resultimage,situation_audio:situaudio,
            task_audio:taskaudio,action_audio:actaudio,result_audio:resaudio,situation_doc:situdoc,task_doc:taskdoc,action_doc:actdoc,result_doc:resdoc,
            video:genvideo,situation_video:situvideo,task_video:taskvideo,action_video:actvideo,result_video:resvideo
        },{
        headers: getToken()
  }).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const edituserexp = 'experience/edit';
export const EditUserExp = (masid,subject,problem,task,action,solution,summary,usertag,situimagelink,taskimagelink,actimagelink,resimagelink,imagelink,videolink,situvideolink,taskvideolink,actvideolink,resvideolink,genaudiolink,situaudiolink,taskaudiolink,actaudiolink,resaudiolink,pdflink,situpdflink,taskpdflink,actpdflink,respdflink,location)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+edituserexp,{user_exp_mas_id:masid,user_exp_title:subject,situation_text:problem,task_text:task,action_text:action,
            result_text:solution, summary:summary,user:usertag,situation_files:situimagelink,task_files:taskimagelink,
            action_files:actimagelink,result_files:resimagelink,image_list:imagelink,video_list:videolink,situation_video:situvideolink,task_video:taskvideolink,action_video:actvideolink,result_video:resvideolink,
            audio_list:genaudiolink,situation_audio:situaudiolink,task_audio:taskaudiolink,action_audio:actaudiolink,result_audio:resaudiolink,
            doc_list:pdflink,situation_doc:situpdflink,task_doc:taskpdflink,action_doc:actpdflink,result_doc:respdflink,location:location
           
        },{
        headers: getToken()
  }).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const adduserconsultation = 'user/consultation';
export const AddUserConsultation = (QuestionTitie,QuestionDetails,tagexpart,conimage)=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+adduserconsultation,{user_con_title:QuestionTitie,user_con_content:QuestionDetails,tag_expert:tagexpart,
            image:conimage
        },{
        headers: getToken()
  }).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const saveEducationUrl = 'user/education';
export const saveEducation = (schoolName,degree,feildStudy,grade,eduStartDate,eduEndDate,activities,eduDescription,eduType) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+saveEducationUrl,{institution_name:schoolName,degree:degree,fieldofstudy:feildStudy,grade:grade,activities:activities,start_date:eduStartDate,end_date:eduEndDate,description:eduDescription,profile_field:eduType},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const addexpquestion = 'user/experience/questions';
export const addExpQuestion = ()=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+addexpquestion,{},{
        headers: getToken()
  }).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const editEducationUrl = 'edit/education';
export const editEducation = (education_profile_id,schoolName,degree,feildStudy,edutype,grade,eduStartDate,eduEndDate,activities,eduDescription) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+editEducationUrl,{education_profile_id:education_profile_id,profile_field:edutype,institution_name:schoolName,degree:degree,fieldofstudy:feildStudy,grade:grade,activities:activities,start_date:eduStartDate,end_date:eduEndDate,description:eduDescription},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const userpost = 'experience';
export const UserPost = ()=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+userpost,{},{
        headers: getToken()
  }).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const consultationUrl = 'consultation/listing';
export const consultationList = ()=>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+consultationUrl,{},{
        headers: getToken()
  }).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const educationTypeUrl = 'user/education/list';
export const educationType = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+educationTypeUrl,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const usertagUrl = 'users/list';
export const UserlistTag = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+usertagUrl,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(checkToken())})
    })
}

const domaintag = 'domain/list';
export const DomainTag = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+domaintag,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const jobtagapi = 'job/skills/list';
export const jobtagslist = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+jobtagapi,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const adviserSkill = 'advisor/skill/list';
export const adviserSkilllist = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+adviserSkill,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const languageListUrl = 'language/list';
export const languageList = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+languageListUrl,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const deleteWorkUrl = 'delete/details';
export const deleteWork = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+deleteWorkUrl,{user_prf_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const domain = 'sub/domain';
export const Domain = (userid) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+domain,{user_exp:userid},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const deleteEduUrl = 'delete/education';
export const deleteEdu = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+deleteEduUrl,{user_eprf_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const expert = 'experts/list';
export const expertlist = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+expert,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const addreview = 'review/request';
export const reviewrequest = (userid,domainid=[],avlexpert) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+addreview,{user_exp:userid,sub_domain:{id:[]=domainid},expert:avlexpert},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const profilecompletion = 'profile/complete/status';
export const profilecmplt = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+profilecompletion,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const associationlist = 'association/list';
export const associationcmplt = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+associationlist,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const addGoal = 'set/goals';
export const addMyGoal = (title,endDate,desc,goalSkill,position,image) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+addGoal,{goals_title:title,goal_start_date:endDate,goal_description:desc,
            goal_skills:goalSkill,goal_companies:position,goal_image:image},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const goallist = 'goals/listing';
export const GoalList = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+goallist,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const filter = 'exp/filter';
export const getFilter = (domainlist,strtdate,enddate,datecount) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+filter,{domain_id:domainlist,start_date:strtdate,end_date:enddate,days:datecount},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const sort = 'exp/sort';
export const getsorting = (cond) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+sort,{condition:cond},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const consultationrequest = 'review/request';
export const consultationrRequest= (id,domain,expert) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+consultationrequest,{user_exp:id,sub_domain:domain,expert:expert},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const displaytestrequest = 'display/test';
export const displayTestRequest= () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+displaytestrequest,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const displayquestionrequest = 'display/question';
export const displayQuestionrequest= (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+displayquestionrequest,{test:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const finishtest = 'mock/test';
export const finishTest= (testid,question) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+finishtest,{test:testid,question:question},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const goaledit = 'goals/edit';
export const goalEdit= (goalid,title,date,desc,domain,cmpname) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+goaledit,{goal_id:goalid,goals_title:title,goal_end_date:date,goal_description:desc,goal_skills:domain,goal_companies:cmpname},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const deletegoalUrl = 'goals/delete';
export const deleteGoal = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+deletegoalUrl,{goal_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const testList = 'detail/test';
export const testlisting= () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+testList,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const expsearch = 'exp/search';
export const ExpSearch= (text) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+expsearch,{search:text},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const publicpvt = 'exp/type';
export const Publicpvt = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicpvt,{user_exp_mas_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const deletetesturl = 'delete/test';
export const deleteTest = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+deletetesturl,{test_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}


const publicworkurl = 'work/listing';
export const publicWorklist = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicworkurl,{user_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const publiceduurl = 'public/education/list';
export const publicEdulist = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publiceduurl,{user_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const publicinfourl = 'public/info';
export const publicpvtlist = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicinfourl,{user_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const publicexpurl = 'public/exp';
export const publicexplist = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicexpurl,{user_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const feedbacklist = 'feedback/params';
export const feedbackvaluelist = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+feedbacklist,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const expertview = 'expert/view';
export const expertView = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+expertview,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const addfeed = 'add/feedback';
export const addfeedback = (id,rate) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+addfeed,{user_exp_mas_id:id,points:rate},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const Expdel = 'experience/delete';
export const deleteExpList = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+Expdel,{user_exp_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const skill_list = 'skills/list';
export const skill_List = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+skill_list,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const chkskill = 'check/skills';
export const checkskill = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+chkskill,{goal_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const forgotpass = 'forgot/password';
export const forgotPass = (mail) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+forgotpass,{email:mail}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const cnfirmotp = 'receive/fcode';
export const cnfrmotp = (otp) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+cnfirmotp,{otp:otp},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const newpass = 'reset/newpass';
export const NewPass = (pass) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+newpass,{password:pass},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const personalskillapi = 'public/skills';
export const personalSkill = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+personalskillapi,{user_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const feedbackapi = 'public/feedback';
export const feedback = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+feedbackapi,{user_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const PublicExpDomainapi = 'public/domain';
export const publicDomain = (skillid,id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+PublicExpDomainapi,{skill_id:skillid,user_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const jobskilapi = 'public/jobskills';
export const jobSkill = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+jobskilapi,{user_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const jobskillistapi = 'public/exp/jobs';
export const jobSkillList = (skillid,id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+jobskillistapi,{skill_id:skillid,user_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const publicconstapi = 'public/consultation';
export const publicconsultation = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+publicconstapi,{user_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const constpublicapi = 'consultation/set/public';
export const constpubpvt = (id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+constpublicapi,{exp_mas:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const sonsultdomainapi = 'public/consult/domain';
export const domainPublic = (skillid,id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+sonsultdomainapi,{skill_id:skillid,user_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}

const jobpublicapi = 'public/consult/jobs';
export const JobPublic = (skillid,id) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+jobpublicapi,{skill_id:skillid,user_id:id},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const storypublicapi = 'public/story';
export const storyPublic = (storyVideo) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+storypublicapi,{add_story_video:storyVideo},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const advisorUrl = 'advisor/apply';
export const applyAdvisor = (summary,skills,languages,charge_experience,charge_resume,charge_interview,avail_time) =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+advisorUrl,{advisor_sum:summary,skills:skills,languages:languages,charge_experience:charge_experience,charge_resume:charge_resume,charge_interview:charge_interview,avail_time:avail_time},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}
const advisorprofileUrl = 'advisor/profile';
export const advisorProfile = () =>{
    return new Promise((resolve,reject)=>{
        axios.post(apiUrl+advisorprofileUrl,{},{headers: getToken()}).then(res=>{
            resolve(res);
        },err=>{reject(err)})
    })
}