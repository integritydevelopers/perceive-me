import React from 'react';
import loaderGif from '../../assets/images/Pulse-1s-200px.gif'

class PageLoader extends React.Component{

    render(){
        return(
            <div className="loader-container">
                <div className="loader">
                    <img src={loaderGif}/>
                </div>
            </div>
        );
    }

}

export default PageLoader;