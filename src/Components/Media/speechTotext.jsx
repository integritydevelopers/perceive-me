import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import listen from '../../assets/images/listen.gif';
import mic1 from '../../assets/images/mic.png';
import { connect } from 'react-redux';
import { saveText } from '../../redux/Actions/mediaAction';
const isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
const isFirefox = typeof InstallTrigger !== 'undefined';
var isOpera = (!!window.opera && !!window.opera.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && window['safari'].pushNotification));
class Speechtotext extends React.Component{
    constructor(props) {
        super(props);
        if(isChrome && !isFirefox && !isOpera && !isSafari)
        {
          const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
          this.mic = new SpeechRecognition()
          this.mic.continuous = true
          this.mic.interimResults = true
          this.mic.lang = 'en-US'
        }
        this.state = {
            listening: true,
            speechTitle : ''
           };
            this.toggleListen = this.toggleListen.bind(this);
            this.handleListen = this.handleListen.bind(this);
        //    this.toggleListen();
    }

    toggleListen() 
    {
       this.setState({listening: !this.state.listening},this.handleListen());
       console.log(this.state.listening);
    }
   
    handleListen(){
        console.log(this.state.listening)
        console.log(this.props.textboxId);
        if(this.state.listening) 
        { 
            this.mic.start();
        }
        else {
           this.mic.stop();
           this.mic.onend = () => {
            this.props.onHide();
            this.setState({speechTitle:''});
           }
         }
          this.mic.onresult = event => {
           const transcript = Array.from(event.results)
             .map(result => result[0])
             .map(result => result.transcript)
             .join('')
           console.log(transcript);
           this.setState({speechTitle : transcript});
           this.props.savetext({text:transcript,textboxId:this.props.textboxId});
           this.mic.onerror = event => {
             console.log(event.error);
           }
         }
       }
    render(){
        return(
            <Modal
             {...this.props}
            size="xl"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            // show={this.state.ModalShow}
             onHide={this.props.onHide}
            className="raspechmodal"
          >
              
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Speech to text
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="raspeak">
                <h2>Speak Now</h2>
                <img src={listen} alt=""/>
                <div className="clearfix"></div>
                </div>
                <h2>{this.state.speechTitle}</h2>
            </Modal.Body>
            <Modal.Footer>
                {this.state.listening && 
              <Button onClick={this.toggleListen} className="rapost">Speak</Button>
            }
             <p>click on speak button for recording</p>
            {!this.state.listening && 
              <Button onClick={this.toggleListen} className="rapost">Stop</Button>
            }
            </Modal.Footer>
            </Modal>
        );
    }
}
const mapDispatchFromProps = dispatch=>{
    return{
        savetext : (data) =>dispatch(saveText(data))
    }
  }
  
export default connect(null,mapDispatchFromProps)(Speechtotext);