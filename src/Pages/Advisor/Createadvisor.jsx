import React,{ useState } from 'react';
import Button from 'react-bootstrap/Button';
import { connect } from 'react-redux';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import fileUpload from '../../Components/Aws/aws';
import {Link} from 'react-router-dom';
import ReactMultiSelectCheckboxes from 'react-multiselect-checkboxes';
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import { TimeGridScheduler, classes } from '@remotelock/react-week-scheduler';
import '@remotelock/react-week-scheduler/index.css';
import ReactTags from 'react-tag-autocomplete';
import {adviserSkilllist,languageList,applyAdvisor}  from '../../providers/auth';
 
class Createaadvisor extends React.Component{

    constructor(props){
        super(props);
        this.reactTags = React.createRef();
        this.state = {          
          rangeStrings:[
            ["2019-03-03T22:45:00.000Z", "2019-03-04T01:15:00.000Z"],
            ["2019-03-04T22:15:00.000Z", "2019-03-05T01:00:00.000Z"],
            ["2019-03-05T22:00:00.000Z", "2019-03-06T01:00:00.000Z"],
            ["2019-03-06T22:00:00.000Z", "2019-03-07T01:00:00.000Z"],
            ["2019-03-07T05:30:00.000Z", "2019-03-07T10:00:00.000Z"],
          
          ],
          defaultSchedule:'',
          jobtagsuggestions:[],
          getjobtag:[],
          tags:[],
          languageList:[],
          languageSuggestions:[],
          languages:[],
          summary:'',
          publicProfile:'http://localhost:3000/globalprofile/41cf84d7c99ab486c80acc97b8c04904',
          availabilitySlot:'',
          resumeCost:'',
          experienceCost:'',
          onlineCost:'',
          availability:[],
          defaultKey:'0',
        }

        // this.state.defaultSchedule = this.state.rangeStrings.map(range =>
        //   range.map(dateString => {
        //     new Date(dateString)}),
        // );

        adviserSkilllist().then(res=>{
          this.setState({
            getjobtag:res['data']['data']
          
          });
          this.state.getjobtag.forEach(el=>{
            this.state.jobtagsuggestions.push({id:el.advisor_skill_id,name:el.advisor_skill_name});
          })
            });
            languageList().then(res=>{
          this.setState({
            languageList:res['data']['data']
          
          });
          this.state.languageList.forEach(el=>{
            this.state.languageSuggestions.push({id:el.advisor_skill_id,name:el.advisor_skill_name});
          })
    
            });
        
    }
   componentWillReceiveProps(nextProps){
     console.log(nextProps);
    
   }

   gotoExpertise = (e)=>{
     if(e.target.id==0){
      this.setState({defaultKey:'1'})
     }
      else if(e.target.id==1){
      this.setState({defaultKey:'2'})
     }
     else if(e.target.id==2){
      this.setState({defaultKey:'3'})
     }
     
   }
  
   changeSummary = (event)=>{
    console.log(event.target.id);
    this.setState({[event.target.name]:event.target.value})
   }


   handleUpload = () =>{
     this.props.startLoader();
    applyAdvisor(this.state.summary,this.state.tags,this.state.languages,this.state.experienceCost,this.state.resumeCost,this.state.onlineCost,this.state.rangeStrings).then(res=>{
      console.log(res);
      this.props.hideLoader();
      this.props.history.push({
        pathname: '/consulatationhub'
      })
    })
} 

setSchedule = ()=>{
  console.log(this.state.rangeStrings);
}
onDelete (i) {
  const tags = this.state.tags.slice(0)
  tags.splice(i, 1)
  this.setState({ tags })
 // console.log(this.state.tags);
}

onAddition (tag) {
  const tags = [].concat(this.state.tags, tag)
  this.setState({ tags },console.log(tags))
  setTimeout(() => {
  }, 2000);
 
}
onDeleteLanguage (i) {
  const languages = this.state.languages.slice(0)
  languages.splice(i, 1)
  this.setState({ languages })
 // console.log(this.state.tags);
}

onAdditionLanguage (tag) {
  console.log(tag);
  const languages = [].concat(this.state.languages, tag);
  this.setState({ languages},console.log(this.state.languages))
  setTimeout(() => {
  }, 2000);
 
}




    render(){
        const options = [
          { label: 'English', value: 1},
          { label: 'Hindi', value: 2},
          { label: 'Bengali', value: 3},
        ];
        const availabilityOptions = [
          { label: 'Online', value: 1},
          { label: 'offline', value: 2},
        ];
        
         
        
        return(
           <div className="col-sm-6">
            <h2 className="innerhed"> Become a Advisor <a href="#" className="closebc float-right">Close</a> </h2>
               <Accordion defaultActiveKey={this.state.defaultKey} activeKey={this.state.defaultKey} className="becomead">
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="0" className="w-100 raadvprf">
        <div className="row">
        <div className="col-sm-2"><span className="conshub">1</span></div>
        <div className="col-sm-10">
        <h4>Create Advisor Profile</h4>
        <p>To become an Adviser you have enter detilas </p>
        </div>
        </div>
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="0">
      <Card.Body>
      <form className="rainnerhub">
      <div className="form-group">
             <span className="has-float-label">
             <textarea className="form-control" id="summary" rows="3" placeholder="Enter Your Summary" name="summary" defaultValue={this.state.summary}  onChange={this.changeSummary}></textarea>
            <label htmlFor="first">Profile Summary</label>
            </span>
             </div>
             <div className="form-group">
             <span className="has-float-label">
            <input className="form-control" id="profilelink" readOnly value="http://localhost:3000/globalprofile/41cf84d7c99ab486c80acc97b8c04904" type="text" placeholder="Share your Public Profile link"/>
            <label htmlFor="first"> Your Public Profile link</label>
            </span>
             </div>
             <div className="float-right m-4">
            
             <Button className="ranewnext" id="0" onClick={this.gotoExpertise}>Next</Button>
             </div>
      </form>
      </Card.Body>
    </Accordion.Collapse>
  </Card>
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="1" className="w-100 raadvprf">
      <div className="row">
        <div className="col-sm-2"><span className="conshub1">2</span></div>
        <div className="col-sm-10">
        <h4>Choos Your area expertise</h4>
        <p>Enter your expertise area</p>
        </div>
        </div>
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="1">
      <Card.Body>
        <form className="rainnerhub">
        <div className="form-group">
            <ReactTags
        ref={this.reactTags}
        tags={this.state.tags}
        suggestions={this.state.jobtagsuggestions}
        onDelete={this.onDelete.bind(this)}
        onAddition={this.onAddition.bind(this)}

        placeholderText ={'Choose Your areas of expertise'} />
             </div>
             <div className="float-right m-4">
            
             <Button className="ranewnext" id="1" onClick={this.gotoExpertise}>Next</Button>
             </div>
        </form>
      </Card.Body>
    </Accordion.Collapse>
  </Card>
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="2" className="w-100 raadvprf">
      <div className="row">
        <div className="col-sm-2"><span className="conshub2">3</span></div>
        <div className="col-sm-10">
        <h4>Languages Spoken</h4>
        <p>Enter your language to comfort with communication</p>
        </div>
        </div>
        
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="2">
      <Card.Body>
      <form className="rainnerhub">
        <div className="form-group">
        {/* <input type="text" className="form-control" id="exampleInputEmail1" placeholder="Languages"/> */}
        {/* <ReactMultiSelectCheckboxes options={options} /> */}
        <ReactTags
        ref={this.reactTags}
        tags={this.state.languages}
        suggestions={this.state.languageSuggestions}
        onDelete={this.onDeleteLanguage.bind(this)}
        onAddition={this.onAdditionLanguage.bind(this)}

        placeholderText ={'Select language'} />
        </div>
        <div className="float-right m-4">
            
            <Button className="ranewnext" id="2" onClick={this.gotoExpertise}>Next</Button>
            </div>
        </form>
      </Card.Body>
    </Accordion.Collapse>
  </Card>
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="3" className="w-100 raadvprf">
      <div className="row">
        <div className="col-sm-2"><span className="conshub3">4</span></div>
        <div className="col-sm-10">
        <h4>Availability & Charges</h4>
        <p>Online offline Availability or Charges Choose here</p>
        </div>
        </div>
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="3">
      <Card.Body>
      <form className="rainnerhub">
<h2>For Offline Reviews</h2>
<ul className="raconis">
  <li>Total weekly Availability Slot</li>
  <li><input type="text" className="form-control" id="exampleInputEmail1" placeholder="4 Days" /></li>
  <li>Resume Reviews Cost </li>
  <li><input type="text" className="form-control" id="exampleInputEmail1" placeholder="10 Coins" name="resumeCost" defaultValue={this.state.resumeCost}  onChange={this.changeSummary}/></li>
  <li>Exprience Reviews Cost </li>
  <li><input type="text" className="form-control" id="exampleInputEmail1" placeholder="10 Coins" name="experienceCost" defaultValue={this.state.experienceCost}  onChange={this.changeSummary}/></li>
  <div className="clearfix"></div>
</ul>


<h2>For Online Reviews</h2>
<ul className="raconis">
  <li>Cost of each 30 mint Slot</li>
  <li><input type="text" className="form-control" id="exampleInputEmail1" placeholder="30 coins" name="onlineCost" defaultValue={this.state.onlineCost}  onChange={this.changeSummary}/></li>
  <div className="clearfix"></div>
</ul>
{/* <ul className="weekly">
<h3>Weekly Availability</h3>
  <li>Sunday</li>
  <li><input type="text" className="form-control" id="exampleInputEmail1" placeholder="Start Time"/> <input type="text" className="form-control" id="exampleInputEmail1" placeholder="End Time"/></li>
  <li>Monday</li>
  <li><input type="text" className="form-control" id="exampleInputEmail1" placeholder="Start Time"/><input type="text" className="form-control" id="exampleInputEmail1" placeholder="End Time"/></li>
  <li>Tuesday</li>
  <li><input type="text" className="form-control" id="exampleInputEmail1" placeholder="Start Time"/><input type="text" className="form-control" id="exampleInputEmail1" placeholder="End Time"/></li>
  <li>Wednesday</li>
  <li><input type="text" className="form-control" id="exampleInputEmail1" placeholder="Start Time"/><input type="text" className="form-control" id="exampleInputEmail1" placeholder="End Time"/></li>
  <li>Thursday</li>
  <li><input type="text" className="form-control" id="exampleInputEmail1" placeholder="Start Time"/><input type="text" className="form-control" id="exampleInputEmail1" placeholder="End Time"/></li>
  <li>Friday</li>
  <li><input type="text" className="form-control" id="exampleInputEmail1" placeholder="Start Time"/><input type="text" className="form-control" id="exampleInputEmail1" placeholder="End Time"/></li>
  <li>Saturday</li>
  <li><input type="text" className="form-control" id="exampleInputEmail1" placeholder="Start Time"/><input type="text" className="form-control" id="exampleInputEmail1" placeholder="End Time"/></li>
  <div className="clearfix"></div>
</ul> */}
<TimeGridScheduler
      classes={classes}
      style={{ width: "100%", height: "400px" }}
      originDate={new Date('2019-03-04')}
      schedule={this.state.rangeStrings}
      visualGridVerticalPrecision={15}
        verticalPrecision={15}
        cellClickPrecision={60}
        // onChange={this.setSchedule()}
    />
     <div className="float-right mt-4">
             <Link to='/consulatationhub'><Button className="ranewpre" >Back</Button></Link>
             <Button className="ranewnext" onClick={this.handleUpload}>Submit</Button>
             </div>
        </form>
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>
            
           </div>
        
        )
    }
}

const mapStateToProps = state=>{
    console.log(state);
    return{
        //loader : state.loaderReducer.loader,
        auth : state.authReducer.authData,
        profilepic : state.authReducer.profileImage,
        text : state.mediaReducer.text,
        videodata : state.mediaReducer.videoData,
        blobvideodata :state.mediaReducer.blobVideoData
      }
  }
  
  const mapDispatchFromProps = dispatch=>{
    return{
         startLoader : () =>dispatch(showLoader()),
        hideLoader : () =>dispatch(hideLoader())
    }
  }
export default connect(mapStateToProps,mapDispatchFromProps)(Createaadvisor);