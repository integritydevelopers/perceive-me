export default{
    showLoader : 'SHOW_LOADER',
    hideLoader : 'HIDE_LOADER',
    manageAuth : 'MANAGE_AUTH',
    removeAuth : 'REMOVE_AUTH',
    updateAuth : 'UPDATE_AUTH'
}