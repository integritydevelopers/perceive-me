import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { connect } from 'react-redux';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import fileUpload from '../../Components/Aws/aws';
import {Link} from 'react-router-dom';
import ReactMultiSelectCheckboxes from 'react-multiselect-checkboxes';

class Registeradvisor extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            

        }
    }
   componentWillReceiveProps(nextProps){
     console.log(nextProps);
    
   }
  


   handleUpload = () =>{
    
} 

    render(){
        let requestModalCamClose =() =>{this.setState({openCam:false})}
        const options = [
          { label: 'English', value: 1},
          { label: 'Hindi', value: 2},
          { label: 'Bengali', value: 3},
        ];
        const availabilityOptions = [
          { label: 'Online', value: 1},
          { label: 'offline', value: 2},
        ];
        return(
            <Modal
            {...this.props}
            size="xl"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            // show={this.state.ModalShow}
            onHide={this.props.onHide}
            className="advisormodal"
          >   
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
              Create Advisor Profile
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <form className="rainnerhub">
             <div className="form-group">
             <span class="has-float-label">
            <input class="form-control" id="first" type="text" placeholder="Profile Summary"/>
            <label for="first">Profile Summary</label>
            </span>
             </div>
             <div className="form-group">
             <span class="has-float-label">
            <input class="form-control" id="first" type="text" placeholder="Share your Public Profile link"/>
            <label for="first">Share your Public Profile link</label>
            </span>
             </div>
             
             <div className="form-group">
             <span class="has-float-label">
            <input class="form-control" id="first" type="text" placeholder="Choose Your areas of expertise"/>
            <label for="first">Choose Your areas of expertise</label>
            </span>
             </div>
             <div className="form-group">
             <span class="has-float-label">
            <label for="first">Languages spoken</label>
            <ReactMultiSelectCheckboxes options={options} />
            </span>
             </div>
             <div className="form-group">
             <span class="has-float-label">
            <label for="first">Availability Type</label>
            <ReactMultiSelectCheckboxes options={availabilityOptions} />
            </span>
             </div>
             {/* <div className="form-group">
             <span class="has-float-label">
            <input class="form-control" id="first" type="text" placeholder="Type of Availability"/>
            <label for="first">Availability & Charges</label>
            </span>
             </div> */}
             <div className="float-right mt-2">
             <Link to='/consulatationhub'><Button className="ranewpre" >Back</Button></Link>
             <Button className="ranewnext">Submit</Button>
             </div>
             <div className="clearfix"></div>
            </form>
            </Modal.Body>
          </Modal>
        
        )
    }
}

const mapStateToProps = state=>{
    console.log(state);
    return{
        //loader : state.loaderReducer.loader,
        auth : state.authReducer.authData,
        profilepic : state.authReducer.profileImage,
        text : state.mediaReducer.text,
        videodata : state.mediaReducer.videoData,
        blobvideodata :state.mediaReducer.blobVideoData
      }
  }
  
  const mapDispatchFromProps = dispatch=>{
    return{
         startLoader : () =>dispatch(showLoader()),
        hideLoader : () =>dispatch(hideLoader())
    }
  }
export default connect(mapStateToProps,mapDispatchFromProps)(Registeradvisor);