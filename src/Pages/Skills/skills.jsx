import React from 'react';
import { connect } from 'react-redux';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import { skill_List,checkskill } from '../../providers/auth';

class Skills extends React.Component{
    constructor(props) {
        super(props);
       console.log(this.props);

        this.state={
            skill_listData:[],
            goalId:''
            
        }

        if( this.props && this.props.location.data != null){
            this.state.goalId = this.props.location.data.user_goal_id

            checkskill(this.state.goalId).then(res=>{
                console.log(res.data);
                this.setState({
                    skill_listData:res.data
                  
                  });
                 console.log(this.state.skill_listData);
                });
        }
        else{

            skill_List().then(res=>{
                console.log(res.data);
                this.setState({
                    skill_listData:res.data
                  
                  });
                 console.log(this.state.skill_listData);
                });
        }

           
        


      


    }

    render(){
        return(
                   <div className="col-md-9">
              <div className="row raskills">
                  {
                      this.state.skill_listData.map((e, key)=>{
                  
           return <div key={key} className="col-lg-12">
       <div className="card card-margin">
           <div className="card-header no-border">
               <h5 className="card-title">{e.goal_title}</h5>
            </div>
            {e.skills && e.skills.map((jc,jcb)=>{
           return<div key={jcb} className="card-body pt-0">
               <div className="widget-49">
                   <div className="widget-49-title-wrapper">
                       <div className="widget-49-date-primary">
                           <span className="widget-49-date-day">09</span>
                           <span className="widget-49-date-month">apr</span>
                        </div>
                       <div className="widget-49-meeting-info">
                           <span className="widget-49-pro-title">{jc.name}</span>
                           <span className="widget-49-meeting-time">{jc.approx_time}</span>
                       </div>
                   </div>
                   <ol className="widget-49-meeting-points">
                       <li className="widget-49-meeting-item"><span>{jc.explanation}</span></li>
                      
                   </ol>
               </div>
           </div>
            })}
       </div>
   </div>
                      })}
  
</div>
 </div>
         
     
 
        );
    }
}
const mapStateToProps = state=>{
    return{
    loader : state.loaderReducer.loader,
    
  
    }
  }
  const mapDisptchToProps = dispatch=>{
    return{
      startLoader : () =>dispatch(showLoader()),
      hideLoader : () =>dispatch(hideLoader())
    }
    
  }
  export default connect(mapStateToProps,mapDisptchToProps)(Skills);
  
