import React from 'react';
import ReactPlayer from 'react-player';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import VideocamIcon from '@material-ui/icons/Videocam';
import { connect } from 'react-redux';
import TempWebCam from '../Media/webcamera';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import fileUpload from '../../Components/Aws/aws';
import {storyPublic} from '../../providers/auth';

class Story extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            storyData:'',
            openCam:false,
            videoData:'',
            videoBlobData:'',
            uploadStatus:false,
            uploadType:'',
            authStatus:'',

        }
        
    }
   componentWillReceiveProps(nextProps){
     console.log(nextProps);
    if(nextProps.videodata){
      this.setState({storyData:nextProps.videodata});
    this.setState({videoBlobData:nextProps.blobvideodata});
    this.setState({authStatus:nextProps.authStatus});
    this.setState({uploadStatus:true});
    this.setState({uploadType:'storyWebcam'});
    }
    else{
      this.setState({storyData:nextProps.dataobject});
      this.setState({authStatus:nextProps.authStatus});
    }
   }
   openCam = () =>{
    this.setState({openCam : !this.state.openCam});   
} 

videoChange = (e)=>{
   e.preventDefault();
   this.setState({videoBlobData:e.target.files[0]});
   let reader = new FileReader();
   reader.readAsDataURL(e.target.files[0]);
   const scope = this
   reader.onload = function(event) {
     console.log(event.target);
       scope.setState({storyData:event.target.result})
       scope.setState({uploadStatus:true});
       scope.setState({uploadType:'story'});
    }
  }

   handleUpload = () =>{
    this.props.startLoader();
     console.log(this.state.videoBlobData);
      fileUpload(this.state.videoBlobData,this.state.uploadType).then((awsLink)=>{
        console.log(awsLink);
        storyPublic(awsLink).then(res=>{
          this.props.hideLoader();
          window.location.reload();
        })
     })
    
    
} 

    render(){
        let requestModalCamClose =() =>{this.setState({openCam:false})}
        return(
            <Modal
            {...this.props}
            size="xl"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            // show={this.state.ModalShow}
            onHide={this.props.onHide}
            className="rastorymodal"
          >   
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                My Story
              </Modal.Title>
            </Modal.Header>
            <Modal.Body >
            {!this.state.storyData && this.state.authStatus &&
              <div className="row py-5">
                <div className="col-md-6">
                <a className="text-center raupld">
                <i className="fa fa-upload"><input type="file" onChange={this.videoChange} accept="video/mp4" className="dropzone" style={{width:'100%',overflow:'hidden',left:'0'}}/></i> 
                <span>Upload New Story</span>
                </a>
                </div>
               
                <div className="col-md-6" onClick={this.openCam}>
                <a className="text-center raupld">
                <i className="fa fa-video-camera"></i>
                <span>Record New Story</span>
                </a>
                </div>
              </div>
            }
            {this.state.storyData &&
            <ReactPlayer url={this.state.storyData} width="100%" controls/>
            }
            </Modal.Body>
            <Modal.Footer>
            {this.state.storyData && this.state.authStatus &&
            <>
            <Button className="ulstry"> <i className="fa fa-upload"></i> <input type="file" onChange={this.videoChange} accept="video/mp4" className="dropzone" style={{width:"18%",overflow:'hidden'}}/>Upload new story</Button>
            <Button className="rlstry"  onClick={this.openCam}><i className="fa fa-video-camera"></i> Record new story</Button>
            </>
            }
            {this.state.uploadStatus &&
            <Button className="rlstry" onClick={()=>this.handleUpload()}><i className="fa fa-video-camera"></i> Upload</Button>
            }
            
            </Modal.Footer>
            <TempWebCam show={this.state.openCam} onHide={requestModalCamClose} />
          </Modal>
        
        )
    }
}

const mapStateToProps = state=>{
    console.log(state);
    return{
        //loader : state.loaderReducer.loader,
        auth : state.authReducer.authData,
        profilepic : state.authReducer.profileImage,
        text : state.mediaReducer.text,
        videodata : state.mediaReducer.videoData,
        blobvideodata :state.mediaReducer.blobVideoData
      }
  }
  
  const mapDispatchFromProps = dispatch=>{
    return{
         startLoader : () =>dispatch(showLoader()),
        hideLoader : () =>dispatch(hideLoader())
    }
  }
export default connect(mapStateToProps,mapDispatchFromProps)(Story);