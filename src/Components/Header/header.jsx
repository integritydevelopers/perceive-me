import React from 'react';
import logo from '../../assets/images/logo.png';
import menu1 from '../../assets/images/menu1.png';
import menu2 from '../../assets/images/menu2.png';
import menu3 from '../../assets/images/menu3.png';
import menu4 from '../../assets/images/menu4.png';
import menu5 from '../../assets/images/menu5.png';
import menu6 from '../../assets/images/menu6.png';
import wallet from '../../assets/images/wallet.svg';
import help from '../../assets/images/help.svg';
import home from '../../assets/images/home.svg';
import notification from '../../assets/images/notification.svg';
import {Link} from 'react-router-dom';
import user from '../../assets/images/user.png';
import { connect } from 'react-redux';
import { manageAuth } from '../../redux/Actions/authAction';
// import { getData } from '../../providers/token';
import { Dropdown } from 'react-bootstrap';

class Header extends React.Component{

    state = {toogle:false}

    constructor(props){
        super(props)
        this.props.fetchAuth();
    }
    logout= ()=>{
        console.log("logout")
        console.log(this.props);
		window.location.href = '/login';
		localStorage.clear();
    }
    toggle = () =>{
        if(this.state.toogle)
        this.setState({toogle:false})
        else
        this.setState({toogle:true})
    }
  
    render(){
        if(!this.props.auth)
        return(
            <section id="main-nav">
        <div className="custom-container">
        <nav className="navbar navbar-expand-lg navbar-light">
        <a className="navbar-brand" href="#"><img src={logo}/></a>
        <button onClick={this.toggle} className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent" style={{display : this.state.toogle ? 'block' : 'none'}}>
            <ul className="navbar-nav mr-auto">
            <li className="nav-item">
                <a className="nav-link" href="/home">Home</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="#">My profile</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="#">Add experience</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="#">Public Profile</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="#">My wallet</a>
            </li>
            
            </ul>
            <ul className="nav-right">
            <li className="nav-item"><Link className="nav-link" to="/login">Sign In</Link></li>
            <li className="get-started-btn"><Link to="/signup">Get Started</Link></li>
            </ul>
        </div>
        </nav>
        </div>
</section>

        )
        else
            return(
                <section id="profile-nav">
    <div className="container">
    <div className="bs-example">
    <nav className="navbar navbar-expand-md navbar-light">
        <a href="#" className="navbar-brand">
            <img src={logo} alt="Logo"/>
        </a>
        <button type="button" onClick={this.toggle} className="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarCollapse" style={{display : this.state.toogle ? 'block' : 'none'}}>
            <div className="navbar-nav ml-auto">
                <a href="/home" className="nav-item nav-link"> <span><img src={home} height="26"/></span> Home</a>
               {/* <a href="#" className="nav-item nav-link"> <span><img src={menu2}/></span> Online Exam</a> */}
                 {/* <a href="#" className="nav-item nav-link"> <span><img src={menu3}/></span> Skill Test</a> */}
                 <a href="#" className="nav-item nav-link"> <span><img src={wallet} height="26"/></span> My wallet</a>
                 <a href="#" className="nav-item nav-link"> <span><img src={notification} height="26"/></span> Notification</a>
                 <a href="#" className="nav-item nav-link"> <span><img src={help} height="26"/></span> Help</a>
            </div>
            <div className="navbar-nav ml-auto">

            <form id="custom-search-form" className="form-search form-horizontal pull-right">
                <div className="input-append span12">
                    <input type="text" className="search-query" placeholder="Search"/>
                    <button type="submit" className="btn"><i className="fa fa-search"></i></button>
                </div>
            </form>
          
            <Dropdown>
  <Dropdown.Toggle variant="Warning"  id="dropdown-basic">
  {this.props.auth.user_fname} <img src={user}/>
  </Dropdown.Toggle>

  <Dropdown.Menu>
    <Dropdown.Item href="/profile">My Profile</Dropdown.Item>
    <Dropdown.Item onClick={this.logout}>Logout</Dropdown.Item>
  </Dropdown.Menu>
</Dropdown>
             
            </div>
        </div>
    </nav>
</div>
    </div>
    </section>
            );
        
    }
} 


const mapStateToProps = state=>{
    return{
      auth : state.authReducer.authData
  
    }
  }
  
  const mapDisptchToProps = dispatch=>{
    return{
      fetchAuth : () =>dispatch(manageAuth()),
    }
    
  
  }

export default connect(mapStateToProps,mapDisptchToProps)(Header);