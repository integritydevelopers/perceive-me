import React from 'react';
import sec1Item1 from '../../assets/images/sec1-item1.png';
import sec1Item2 from '../../assets/images/sec1-item2.png';
import sec1Item3 from '../../assets/images/sec1-item3.png';
import cros2 from '../../assets/images/cros2.png';

class Nav extends React.Component{
    render(){
        return(
             <section id="profile-sec1">
    <div className="container">
        <div className="row">
        <div className="col-sm-4">
            <div className="profile-sec1-item">
            <img src={sec1Item1}/>
                <h3>Complte Your Profile</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            </div>
            </div>
            
            <div className="col-sm-4">
            <div className="profile-sec1-item">
            <img src={sec1Item2}/>
                <h3>Online Interview</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            </div>
            </div>
            
            <div className="col-sm-4">
            <div className="profile-sec1-item">
            <img src={sec1Item3}/>
                <h3>Skill Test By Expert</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            </div>
            </div>
            
            <span><img src={cros2}/></span>
        </div>
        </div>
    </section>
        )
    }
}

export default Nav;