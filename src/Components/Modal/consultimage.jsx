import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal';
import { consultationList } from '../../providers/auth';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import Button from 'react-bootstrap/Button';

class ConsultImage extends React.Component{
    
    constructor(props){
        super(props);
       
       this.state = { 
           getpostValue:[],
           expId:'',
           action:'',
           situation:'',
           task:'',
           result:''
       }; 
       consultationList().then(res=>{
     
        this.setState({
          getpostValue:res.data
          });
          
        });
    

    }

    componentWillReceiveProps(nextProps){
      
       this.setState({
        expId : nextProps.dataobject.exp_Id,
        action: nextProps.dataobject.user_action,
        situation:nextProps.dataobject.user_situation,
        task:nextProps.dataobject.user_task,
        result:nextProps.dataobject.user_result
});


         


   
      }

      
    
    handlesubmit=()=>{

       
     
    //    // this.props.history.push('/consulatation');
    //    // window.location.reload();


   
        
    }


    render(){
      const responsive = {

        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 1
        },
    };
   
        return(
            <Modal
            {...this.props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
               Images
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>

                 <div className="form-group">
  

                 </div>
                 <div className="form-group">
                 <div className="owl-theme">
            <Carousel responsive={responsive}>
            {this.state.getpostValue.map((e, key) => {
             
               	return <div key={key} className="item">

                <div className="sec3-item-bottom">
                { this.state.expId==e.user_exp_id && this.state.action==e.experience.user_action && e.experience.user_action_files.map((val, jc) =>
                <img src={val} />
          
                )}
                { this.state.expId==e.user_exp_id && this.state.result==e.experience.user_result &&e.experience.user_result_files.map((val, jc) =>
                <img src={val} />
                )} 
                 
                {this.state.expId==e.user_exp_id && this.state.situation==e.experience.user_situation && e.experience.user_situation_files.map((val, jc) =>
                <img src={val} />
                )}
                 {this.state.expId==e.user_exp_id && this.state.task==e.experience.user_task && e.experience.user_task_files.map((val, jc) =>
                <img src={val} />
                )} 
                </div>
            
                </div>
                	})}
      
               
                          </Carousel>
            </div>
            </div>
              {/* <div className="form-group">
              <select className="form-control"  name="exprt" onChange={this.handletitlechange} >
				<option>Avialble expert</option>
                {this.state.avlexpert.map((e, key) => {
       			return <option key={key} value={e.user_id}>{e.user_fname+" "+e.user_lname}</option>
 											})}
				</select>
                 </div>  */}
              
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.props.onHide}>Close</Button>
              <Button onClick={this.handlesubmit}>send request</Button>
            </Modal.Footer>
          </Modal>
        );
    }

}
    export default ConsultImage;