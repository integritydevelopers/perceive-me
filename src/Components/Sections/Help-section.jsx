import React,{ useRef} from "react";
import sec1 from '../../assets/images/sec1-img.png'

class HelpSection extends React.Component{
 
  constructor(props) {
    super(props);
    this.myRef = React.createRef()
    this.state = {
      step : 0,
      scrollTop:0
    }
 
  }
  
   handleScroll= () =>{
    const scrollY = window.pageYOffset 
    const scrollTop = this.myRef.current.scrollTop
    console.log(`onScroll, window.scrollY: ${scrollY} myRef.scrollTop: ${scrollTop}`)
    this.setState({
      scrollTop: scrollTop
    })
    if(this.state.scrollTop>0 && this.state.scrollTop<480)
    {
      const currentState = 0;
      this.setState({ step: currentState });
    }
    else if(this.state.scrollTop>480 && this.state.scrollTop<960)
    {
      const currentState = 1;
      this.setState({ step: currentState });
    }
    else if(this.state.scrollTop>960 && this.state.scrollTop<1450)
    {
      const currentState = 2;
      this.setState({ step: currentState });
    }
    else if(this.state.scrollTop>1450 && this.state.scrollTop<1800)
    {
      const currentState = 3;
      this.setState({ step: currentState });
    }
    else if(this.state.scrollTop>1800 && this.state.scrollTop<3000)
    {
      const currentState = 4;
      this.setState({ step: currentState });
    }
   
   }
    render(){
      const {scrollTop} = this.state
        return(
            <section id="sec1">
<div className="custom-container">
<h1><span>How we help</span> Check These Unique Sites for your Dream</h1>


<div className="sec-inner-text">

<div className="bd-example">
  <div className="row">
    <div className="col-sm-5 col-sm-offset-right-1">
      <div id="list-example" className="list-group">
        <a className={(this.state.step=== 0 ? 'list-group-item list-group-item-action active': 'list-group-item list-group-item-action')}   href="#list-item-1">
        <span>1</span>
        <div>
        <h5>Logging their experience</h5>
        <p>Logging their experience on a regular basis and as and when they come up</p>
        </div>
        </a>
        <a className={(this.state.step=== 1 ? 'list-group-item list-group-item-action active': 'list-group-item list-group-item-action')}   href="#list-item-2">
         <span>2</span>
        <div>
        <h5>Practicing questions </h5>
        <p>Practicing pre-defined or use generated questions with computers, live experts or offline</p>
        </div>
        </a>
        <a className={(this.state.step=== 2? 'list-group-item list-group-item-action active': 'list-group-item list-group-item-action')}   href="#list-item-3">
         <span>3</span>
        <div>
        <h5>Get specific Recommendations</h5>
        <p>Get specific recommendations based on areas of improvement, goals and progress</p>
        </div>
        </a>
        <a className={(this.state.step=== 3? 'list-group-item list-group-item-action active': 'list-group-item list-group-item-action')}   href="#list-item-4">
         <span>4</span>
        <div>
        <h5>Track Progress</h5>
        <p>Track progress, see how you fair against others with similar profiles and goals</p>
        </div>
        </a>
         <a className={(this.state.step=== 4  ?'list-group-item list-group-item-action active': 'list-group-item list-group-item-action')}  href="#list-item-5">
         <span>5</span>
        <div>
        <h5>Access Everything</h5>
        <p>At home, or on the go, access everything from desktop or mobile devices</p>
        </div>
        </a>
      </div>
    </div>
    <div className="col-sm-6">
      <div data-spy="scroll" ref={this.myRef} onScroll={this.handleScroll} data-target="#list-example"  data-offset="0" className="scrollspy-example">
        <h4 id="list-item-1" ></h4>
        <img src={sec1}/>
        <h4 id="list-item-2"></h4>
          <img src={sec1}/>
        <h4 id="list-item-3"></h4>
       <img src={sec1}/>
       
        <h4  id="list-item-4"></h4>
        <img src={sec1}/>
        
         <h4 id="list-item-5"></h4>
      <img src={sec1}/>
      </div>
    </div>
  </div>
</div>
</div>

</div>
</section>
        )
    }
}

export default HelpSection;