import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { Domain } from '../../providers/auth';
import { expertlist } from '../../providers/auth';
import { reviewrequest } from '../../providers/auth';
import Select from 'react-select';
import { toast } from 'react-toastify';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import testimonial from '../../assets/images/testimonial.jpg';
import AudioPlayer from 'react-h5-audio-player';
import 'react-h5-audio-player/lib/styles.css';
import { showLoader, hideLoader } from '../../redux/Actions/loaderAction';
import { connect } from 'react-redux';
class ReviewRequest extends React.Component{

    constructor(props){
        super(props);
        this.state = { 
         audioPlayer:[],
         audio:''
       };
      
      }
    componentWillReceiveProps(nextProps)
    {
      console.log(nextProps);
      if(nextProps.show)
      {
      this.setState({audioPlayer:nextProps.dataobject});
      nextProps.dataobject.forEach(el => {
        this.setState({audio:el});
      })
      }
    }
   render(){
     return(
            <Modal
            {...this.props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            {/* <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
               Review Request
              </Modal.Title>
            </Modal.Header> */}
            <Modal.Body>

                 <div className="form-group">
   
{/* <Select 
 options={this.state.domainlist.map((e) => ({ label: e.user_exp_sub_domain,value: e.user_exp_sub_dom_id}))}
 isMulti
 onChange={newValue => this.setState({ selectedOptions: newValue })}

/> */}
 <AudioPlayer
            autoPlay
            src={this.state.audio}
            onPlay={e => console.log("onPlay")}
            // other props here
          />

                 </div>
                 {/* <div className="form-group">
                 <div className="owl-theme">
            <Carousel responsive={responsive}>
            {this.state.avlexpert.map((e, key) => {
               	return <div key={key} className="item">
                <div className="sec3-item-bottom">
                <img src={testimonial}/>
                <h5>{e.user_fname+" "+e.user_lname}</h5>
                <h7>{e.user_email}</h7>
                <br/>
                <h8>{e.company}</h8>
                <span>{e.designation}</span>
                </div>
                </div>
                	})}
      
               
                          </Carousel>
            </div>
            </div> */}
              {/* <div className="form-group">
              <select className="form-control"  name="exprt" onChange={this.handletitlechange} >
				<option>Avialble expert</option>
                {this.state.avlexpert.map((e, key) => {
       			return <option key={key} value={e.user_id}>{e.user_fname+" "+e.user_lname}</option>
 											})}
				</select>
                 </div>  */}
              
            </Modal.Body>
            {/* <Modal.Footer>
              <Button onClick={this.props.onHide}>Close</Button>
              <Button onClick={this.handlesubmit}>send request</Button>
            </Modal.Footer> */}
          </Modal>
        );
    }

}
const mapStateToProps = state=>{
  return{
  loader : state.loaderReducer.loader

  }
}
const mapDisptchToProps = dispatch=>{
  return{
    startLoader : () =>dispatch(showLoader()),
    hideLoader : () =>dispatch(hideLoader())
  }
  
}

export default connect(mapStateToProps,mapDisptchToProps)(ReviewRequest);