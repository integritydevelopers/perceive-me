import React from 'react';
import logo from '../../assets/images/logo.png';
import login from '../../assets/images/login.png';
import { toast } from 'react-toastify';
import { forgotPass,cnfrmotp,NewPass } from '../../providers/auth';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import { connect } from 'react-redux';
import AwesomeSlider from 'react-awesome-slider';
import '../../assets/css/awseme.css';
import {Link} from 'react-router-dom';

class Forgotpassword extends React.Component{

    constructor(props){
        super(props); 
        this.state = {
            email: '',
            otp:'',
            password: '',
            cPassword : '',
            showemail:true,
            showotp: false,
            hidden: true,
            hiddennew: true,
            showpass:false,
            errors : {"password":'',"cPassword" : ''}
         }
         this.toggleShow = this.toggleShow.bind(this);
         this.toggleShowpass = this.toggleShowpass.bind(this);
         this.toggleShowpassnew = this.toggleShowpassnew.bind(this);
         this.togglepassfield = this.togglepassfield.bind(this);
         }
    
         toggleShow() {
           if(this.state.email != ''){
            console.log (this.state.email);
            this.props.startLoader();
            forgotPass(this.state.email).then((res)=>{
                this.props.hideLoader();
                console.log(res);
                if(res.data.status){
                    toast.success(res.data.msg);
                    localStorage.setItem('authToken',res.data.token);
                    this.setState({ showotp: !this.state.showotp });
                }else{
                    toast.error(res.data.msg);
                }
            });
           }else{
            toast.error('Please Enter Email.',{});
           }
           
          }


          toggleShowpass() {
            this.setState({ hidden: !this.state.hidden });

          }
          toggleShowpassnew() {
            this.setState({ hiddennew: !this.state.hiddennew });

          }

          togglepassfield() {
            if(this.state.otp!=''){
                 console.log(this.state.otp);

                  this.props.startLoader();
                 cnfrmotp(this.state.otp).then((res)=>{
                     this.props.hideLoader();
                     if(res.data.status){
                         toast.success(res.data.msg)
                         this.setState({ showpass: !this.state.showpass });
                     }else{
                         toast.error(res.data.msg);

                     }
                     
                 });
            }else{
                toast.error('Please Enter otp.',{});
            }
          }

          handleEmailChange = (e) =>{
           
            this.setState({email: e.target.value});   
         }

         handleotpChange = (e) =>{
           
            this.setState({otp: e.target.value});   
         }

         handlePasswordChange = (e) =>{
            if(e.target.value !== "undefined" || e.target.value !==''){
                let pattern = new RegExp('/(?=.*[0-9])/').test(e.target.value);
                 if (e.target.value.length<6) {
                     this.state.errors.password = "Invalid Password";
                   }else{
                     this.state.errors.password = "";
                   }
                }
                else{
                    this.state.errors.password = "Please enter password."
                }
                this.setState({password: e.target.value});
         }

         handlepCasswordChange = (e) => {
            if(e.target.value !== this.state.password || e.target.value ==''){
               this.state.errors.cPassword = "Password and confirm password doesn't match.";
            }else{
               this.state.errors.cPassword = "";
            }
           this.setState({cPassword: e.target.value});
        }

        handlesubmit=()=>{
            if(this.state.password!=''){

                this.props.startLoader();
                NewPass(this.state.password).then((res)=>{
                    this.props.hideLoader();
                    if(res.data.status){
                        toast.success(res.data.msg)
                        this.props.history.push('/login');
                    }else{
                        toast.error(res.data.msg);

                    }
                    
                });

            }else{
                toast.error('Please Enter password.',{});
            }
            // console.log(this.state.cPassword);
            // console.log(this.state.password);
            
        }
       
    
    render(){
        return(
            <section id="login">
                <div className="container">
                <div className="logintotal d-sm-flex">
                <div className="col-sm-6 d-sm-flex pl-0">
                <div className="login-left">
                <Link to="/home">
                <img src={logo} />
                </Link>
                <div className="login-slider">
                <AwesomeSlider> 
                <div className="carousel-inner">
                    <div className="carousel-item active">
                    <img src={login}/>
                    <h4>Track Progress</h4>
                    <p>Track progress, see how you fair against others with similar profiles and goals</p>
                    </div>
                    <div className="carousel-item">
                    <img src={login}/>
                    <h4>Track Progress</h4>
                    <p>Track progress, see how you fair against others with similar profiles and goals</p>
                    </div>
                    <div className="carousel-item">
                    <img src={login}/>
                    <h4>Track Progress</h4>
                    <p>Track progress, see how you fair against others with similar profiles and goals</p>
                    </div>
                </div>
                </AwesomeSlider> 
               
            
                </div>
               
                </div>
                
                </div>
            
                <div className="col-sm-6 d-sm-flex">
                <div className="login-right">
                <h4>Welcome</h4>
                <p>Don't miss your next opportunity. Sign in to stay updated on your professional world.</p>
               
                <div className="loginwith">
                
                </div>
                {
                     this .state.showemail && this.state.showotp ==false &&
                     <div className="form-group">
                     <label>  Registered Email Address</label>
                     <input type="email" className="form-control" onChange={this.handleEmailChange} placeholder="Email Address"/>
                     </div>
                }
               
                {
                    this.state.showotp && this.state.showpass==false &&

                    <div className="form-group">
                    <label> Enter OTP</label>
                    <input type="text" className="form-control" placeholder="OTP" onChange={this.handleotpChange}></input>
                    </div>
                }

                {
                   this.state.showpass && 
                   <>

                   <div className="form-group showeye">
                <label>New Password</label>
                <input type={this.state.hiddennew ? "password" : "text"}
                className="form-control" placeholder="Password" value={this.state.password} onChange={this.handlePasswordChange}/>
                {this.state.hiddennew &&
                <i onClick={this.toggleShowpassnew} className="fa fa-eye-slash" aria-hidden="true"></i>
                    }
                    {!this.state.hiddennew &&
                        <i onClick={this.toggleShowpassnew} className="fa fa-eye" aria-hidden="true"></i> 
                    }
                     <p style={{color:"red",fontSize:"14px"}}>{this.state.errors.password}</p>
                </div>

                <div className="form-group showeye">
                <label>Confirm Password</label>
                <input type={this.state.hidden ? "password" : "text"}
                className="form-control" placeholder="Password" value={this.state.cPassword} onChange={this.handlepCasswordChange}/>
                {this.state.hidden &&
                <i onClick={this.toggleShowpass} className="fa fa-eye-slash" aria-hidden="true"></i>
                    }
                    {!this.state.hidden &&
                        <i onClick={this.toggleShowpass} className="fa fa-eye" aria-hidden="true"></i> 
                    }
                    <p style={{color:"red",fontSize:"14px"}}>{this.state.errors.cPassword}</p>
                </div>
                </>
                }
                {
                    this .state.showemail && this.state.showotp ==false &&
                    <div className="form-group">
                <button className="login-to-perceiveMe" onClick={this.toggleShow} >Submit</button>
                </div>
                }

                {
                     this.state.showotp && this.state.showpass==false &&
                     <div className="form-group">
                     <button className="login-to-perceiveMe" onClick={this.togglepassfield} >Submit</button>
                     </div>
                }
                {
                     this.state.showpass && 
                     <div className="form-group">
                     <button className="login-to-perceiveMe"  onClick={this.handlesubmit}>Submit</button>
                     </div>
                }



                <div className="join-now">
               
                </div>
                </div>

                </div>
                </div>
                <div className="login-footer-menu">
                <ul>
                <li><a href="#">PerceiveMe.ai © 2020</a></li>
                <li><a href="#"> User</a></li>
                <li><a href="#">Agreement</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Community Guidelines</a></li>
                <li><a href="#">Cookie Policy</a></li>
                <li><a href="#">Copyright Policy</a></li>
                <li><a href="#">Send Feedback</a></li>
                </ul>

                </div>
                </div>
        </section>
        )
    }
}

const mapStateToProps = state=>{
    return{
        loader : state.loaderReducer.loader
      }
}

const mapDispatchFromProps = dispatch=>{
    return{
        startLoader : () =>dispatch(showLoader()),
        hideLoader : () =>dispatch(hideLoader())
    }
}

export default connect(mapStateToProps,mapDispatchFromProps)(Forgotpassword);