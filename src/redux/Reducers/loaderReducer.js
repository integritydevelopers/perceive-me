import actionTypes from '../Actions/actionTypes';

const intialState = {loader:false};

export function loaderReducer(state = intialState,action) {
    switch(action.type){
        case actionTypes.showLoader : return{
            ...state,
            loader : true
        }
        case actionTypes.hideLoader : return{
            ...state,
            loader : false
        }
        default : return{
            ...state
        }
    }
    
}