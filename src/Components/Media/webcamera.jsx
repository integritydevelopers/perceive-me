import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import AlbumIcon from '@material-ui/icons/Album';
import PauseCircleFilledIcon from '@material-ui/icons/PauseCircleFilled';
import VideocamIcon from '@material-ui/icons/Videocam';
import VideoCallIcon from '@material-ui/icons/VideoCall';
import PublishIcon from '@material-ui/icons/Publish';
import Webcam from "react-webcam";
import { connect } from 'react-redux';
import { saveVideoData,emptyVideoData } from '../../redux/Actions/mediaAction';

const TempWebCam = (show) => {
  const webcamRef = React.useRef(null);
  const mediaRecorderRef = React.useRef(null);
  const [capturing, setCapturing] = React.useState(false);
  const [recordedChunks, setRecordedChunks] = React.useState([]);
  const handleStartCaptureClick = React.useCallback(() => {
    
    setCapturing(true);
    mediaRecorderRef.current = new MediaRecorder(webcamRef.current.stream, {
      mimeType: "video/webm"
    });
    mediaRecorderRef.current.addEventListener(
      "dataavailable",
      handleDataAvailable
    );
    mediaRecorderRef.current.start();
  }, [webcamRef, setCapturing, mediaRecorderRef]);

  const handleDataAvailable = React.useCallback(
    ({ data }) => {
      if (data.size > 0) {
       setRecordedChunks((prev) => prev.concat(data));
      }
    },
    [setRecordedChunks]
  );

  const handleStopCaptureClick = React.useCallback(() => {
    mediaRecorderRef.current.stop();
    setCapturing(false);
  }, [mediaRecorderRef, webcamRef, setCapturing]);

  const handleDownload = React.useCallback(() => {
    if (recordedChunks.length) {
      var blob = new Blob(recordedChunks, {
        type: "video/webm"
      });
      
       var reader = new FileReader();
 reader.readAsDataURL(blob); 
 reader.onloadend = function() {
     var base64data = reader.result;  
     show.savevideodata(base64data,blob);
     show.onHide();
     recordedChunks.pop();
 }
 
      // const url = URL.createObjectURL(blob);
      // const a = document.createElement("a");
      // document.body.appendChild(a);
      // a.style = "display: none";
      // a.href = url;
      // a.download = "react-webcam-stream-capture.webm";
      // a.click();
      // window.URL.revokeObjectURL(url);
      // setRecordedChunks([]);
    }
  }, [recordedChunks]);

  return (
    <>
     <Modal
            
             size="xl"
             aria-labelledby="contained-modal-title-vcenter"
             centered
              show={show.show}
              onHide={show.onHide}
             className="ravideomodal"
           >
      <Webcam audio={true} ref={webcamRef} />
   
      {capturing ? (
      <button onClick={handleStopCaptureClick}  title="Pause Video" className="rapvideo"><PauseCircleFilledIcon  /></button>
      ) : (
       <button onClick={handleStartCaptureClick} title="Record Video" className="rapvideo"><AlbumIcon /></button>
       )}
     
      {recordedChunks.length > 0 && (
        <button onClick={handleDownload} title="Upload Video" className="radvideo"><PublishIcon /></button>
        // <button onClick={handleDownload}>Download</button>
      )}
      </Modal>
    </>
  );
};
const mapDispatchFromProps = dispatch=>{
  return{
    savevideodata : (data,blob) =>dispatch(saveVideoData(data,blob)),
    clearVideo :()=>dispatch(emptyVideoData())
  }
}

  export default connect(null,mapDispatchFromProps)(TempWebCam);

  
 