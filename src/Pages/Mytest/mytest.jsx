import React from 'react';

import cross from '../../assets/images/cros.png';
import userin from '../../assets/images/userin.png';
import loader from '../../assets/images/loding.png';
import experience from '../../assets/images/exprience.jpg';
import video from '../../assets/images/video.jpg';
import  Speechtotext  from '../../Components/Media/speechTotext';
import TempWebCam from '../../Components/Media/webcamera';
import { displayTestRequest,displayQuestionrequest,testlisting,deleteTest} from '../../providers/auth';
import MicIcon from '@material-ui/icons/Mic';
import VideoCallIcon from '@material-ui/icons/VideoCall';
import { connect } from 'react-redux';
import {finishTest} from '../../providers/auth';
import { toast } from 'react-toastify';
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card';
import { Button } from 'react-bootstrap';
import AwesomeSlider from 'react-awesome-slider';
import moment, { calendarFormat } from 'moment';
import ReactPlayer from 'react-player';
import {emptyVideoData} from '../../redux/Actions/mediaAction';
import { confirmAlert } from 'react-confirm-alert';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
const isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
const isFirefox = typeof InstallTrigger !== 'undefined';
var isOpera = (!!window.opera && !!window.opera.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && window['safari'].pushNotification));

class Mytest extends React.Component{
    constructor(props) {
        super(props);
        if(isChrome && !isFirefox && !isOpera && !isSafari)
        {
          const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
          this.mic = new SpeechRecognition();
          this.mic.continuous = true
          this.mic.interimResults = true
          this.mic.lang = 'en-US'
        }
        this.state = {
            listening: false,
            speechTitle : '',
            displayTest:[],
            listdisplay:[],
            questionlist:[],
            displayQuestion:[],
            questionView: false,
            index:0,
            listindex:0,
            openMic : false,
            openCam:false,
            textboxId : '',
            record:[],
            question:[],
            answer:[],
            testId:'',
            count:0,
            videoValueArray:[],
            videoValueData:''
           };
           this.toggleListen = this.toggleListen.bind(this);
           this.handleListen = this.handleListen.bind(this);
           this.handlerecord = this.handlerecord.bind(this);
        }
    componentDidMount()
    {
      this.props.startLoader();
      displayTestRequest().then(res => {
        console.log(res);
      this.setState({displayTest:res.data.data});
      console.log(this.state.displayTest);
     
     });

     testlisting().then(res => {
      console.log(res);
      this.setState({listdisplay:res.data.data});
      console.log(this.state.listdisplay);
      
      this.state.listdisplay.forEach(el => {
        this.state.questionlist.push(el.question);
      })
      this.props.hideLoader();
      console.log(this.state.questionlist);
      
   });

    }
    
    toggleListen() 
    {
       this.setState({listening: !this.state.listening},this.handleListen());
       console.log(this.state.listening);
    }
    handleListen(){
        if(this.state.listening) 
        { 
            this.mic.start();
        }
        else {
           this.mic.stop();
           this.mic.onend = () => {
             console.log('Stopped Mic on Click')
           }
         }
          this.mic.onresult = event => {
           const transcript = Array.from(event.results)
             .map(result => result[0])
             .map(result => result.transcript)
             .join('')
           console.log(transcript);
           this.setState({speechTitle : transcript})
           this.mic.onerror = event => {
             console.log(event.error);
           }
         }
        
       }
       componentWillReceiveProps(nextProps)
       {
         console.log(nextProps);
         if(this.state.openMic)
         {
         this.state.record[nextProps.text.textboxId]=nextProps.text.text;
         this.state.videoValueArray[nextProps.text.textboxId]='';
         }
       }
       questionViewer= (e)=>{
        this.setState({index:0,testId:e.target.value});
        this.setState({questionView:true});
        displayQuestionrequest(e.target.value).then(res => {
          this.setState({displayQuestion:res.data.data});
          console.log(this.state.displayQuestion);
        });
       }
       Next= (key,id)=>{
        this.props.clearVideo();
         if((this.state.record[key]!='' && this.state.record[key]!=undefined)||(this.state.videoValueArray[key]!='' && this.state.videoValueArray[key]!=undefined))
        {
          this.state.question[key]={id:id,answer:this.state.record[key],video:this.state.videoValueArray[key]};
          console.log(this.state.question);
          this.setState({index:key+1});
        }
        else
        {
          toast.error('Please fill mandetory fields.',{});
        }
        }


        listNext= (key,id)=>{
         
           this.state.question[key]={id:id};
           this.setState({listindex:key+1});
         
                  }


       Prev= (key)=>{
        this.setState({index:key-1});
       }

       listPrev= (key)=>{
        this.setState({listindex:key-1});
       }

       openMic = () =>{
        this.setState({openMic : !this.state.openMic});
      
      }
      openCam=(key)=>{
        this.setState({openCam : !this.state.openCam});
        this.setState({videoValueData:key});
      }
      handlerecord(event,key){
         this.state.record[key]=event.target.value;
         this.state.videoValueArray[key]='';
       }
       Finish= (key,qid)=>{
        if((this.state.record[key]!='' && this.state.record[key]!=undefined)||(this.state.videoValueArray[key]!='' && this.state.videoValueArray[key]!=undefined))
        {
        this.state.question[key]={id:qid,answer:this.state.record[key],video:this.state.videoValueArray[key]};
        console.log(this.state.question);
        finishTest(this.state.testId,this.state.question).then(res => {
         console.log(res);
         toast.success(res.data.msg, {});
         window.location.reload();
        });
        }
        else
        {
          toast.error('Please fill mandetory fields.',{});
        }
       }

       retest = (id)=>{
         console.log(id);
        this.setState({index:0,testId:id});
        this.setState({questionView:true});
        displayQuestionrequest(id).then(res => {
          this.setState({displayQuestion:res.data.data});
          console.log(this.state.displayQuestion);
        });

       }

       deletetestlist = (id)=>{
        confirmAlert({
            title: 'Confirm to Delete',
            message: 'Are you sure to do this.',
            buttons: [
              {
                label: 'Yes',
                onClick: () => {
                    this.props.startLoader();
                    deleteTest(id).then(res=>{
                        this.props.hideLoader();
                        if(res.data.status){
                            toast.success(res.data.msg,{autoClose:5000});
                            window.location.reload();
                        }else{
                            toast.error(res.data.msg,{autoClose:5000});
                            window.location.reload();
                        }
                        testlisting().then(res => {
                          console.log(res);
                          this.setState({listdisplay:res.data.data});
                          console.log(this.state.listdisplay);
                          
                          this.state.listdisplay.forEach(el => {
                            this.state.questionlist.push(el.question);
                          })
                          console.log(this.state.questionlist);
                          
                       });
                    })
                }
              },
              {
                label: 'No',
                onClick: () => console.log("not deleted")
              }
            ]
          });
    }


    render() {
      console.log(this.props.videodata);
      let requestModalClose =() =>{this.setState({openMic:false})}
      let requestModalCamClose =() =>{this.setState({openCam:false})}
      if(this.props.videodata!='')
      {
       this.state.videoValueArray[this.state.videoValueData]=this.props.videodata;
       console.log(this.state.videoValueArray);
       this.state.record[this.state.videoValueData]='';
      }
       return (

<div className="col-sm-6">
<div className="exbg mytest">
 <div className="form-group mb-0">
 <label htmlFor="sel1">Select your Test</label>
 <select className="form-control" id="sel1" name="sellist1" onChange={this.questionViewer}>
 <option>Please Select Your Test</option>
 { 
 this.state.displayTest.map((e, key)=>{
  return <option value={e.mock_test_id} key={key}>{e.mock_title_name}</option>
 })
}
 </select>
</div>
<div className="clearfix"></div>
</div>
{this.state.questionView && this.state.displayQuestion.map((e, key)=>{
  if(this.state.index==key && !this.state.openMic)
  {
return <div className="exbg mytestqu">

 <h3 key={key}>{e.mock_question}</h3>
<form>
    <div  className="form-group">
    {!this.state.videoValueArray[key] &&
    <textarea type="text" name="record" onChange={(event)=>this.handlerecord(event,key)} defaultValue={this.state.record[key]} className="form-control ramtest" placeholder="Record/Type your response"></textarea>
    }
    {!this.state.videoValueArray[key] && isChrome && !isFirefox && !isOpera && !isSafari &&
    <a  onClick={()=>this.openMic()} ><MicIcon className="audiospk"/></a>
    }
    {!this.state.videoValueArray[key] &&
    <a onClick={()=>this.openCam(key)}><VideoCallIcon className="webcam"/></a>
    }
    {this.state.videoValueArray[key] &&
    <ReactPlayer width={520} height={234} controls playing url={this.state.videoValueArray[key]} />
    }
    </div>
    </form>
    
       <h3>{this.state.speechTitle}</h3>
       {this.state.index!=0 &&
       <button type="button" onClick={()=>this.Prev(key)}  className="btn btn-primary float-left next mt-3">Previous</button>
       }
       {this.state.index!=(this.state.displayQuestion.length-1) &&
         <button type="button" onClick={()=>this.Next(key,e.mock_question_id)} className="btn btn-primary float-right next mt-3">Next</button>
       }
       {this.state.index==(this.state.displayQuestion.length-1) &&
         <button type="button" onClick={()=>this.Finish(key,e.mock_question_id)} className="btn btn-primary float-right next mt-3">Finish</button>
       }
    <div className="clearfix"></div>

   
    
</div>
  }
  
  else if(this.state.index==key && this.state.openMic)
  {
    return <Speechtotext show={this.state.openMic} textboxId={key} onHide={requestModalClose}/>
  }
 }) }


{ this.state.listdisplay.map((e, key)=>{
 return <div>
 <Accordion className="mainacr rastest">
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="0"  className="pl-0 pb-0" >
        
          <p className="mb-0 testh">{e.user_test_name}</p>
     
      </Accordion.Toggle>
      <p className="mb-0">{moment(e.created_date).format('LL')}</p>
      <a href="#" onClick={() =>{this.retest(e.user_test)}} className="raretest">Retest</a>
      <a onClick={() =>{this.deletetestlist(e.user_mock_test_id)}} className="ratestdl" ><i className="fa fa-trash"></i></a>
    </Card.Header>
    <Accordion.Collapse eventKey="0">
      <Card.Body>
      <AwesomeSlider> 

      { e.question.map((item, key2) => {
        console.log(item);
       
      return <div className="mytestqu">
 
      <h3 key={key2}>{item.user_question}</h3>
      <form>
        <div  className="form-group">
          <p>{item.user_test_text}</p> 
          <ReactPlayer width={520} height={234} controls playing url={item.user_test_video} />
        
        </div>
      </form>
      
         
            {/* {this.state.listindex!=0 &&
              <button type="button" onClick={()=>this.listPrev(key2)}  className="btn btn-primary float-left next mt-3">Previous</button>
            }
            {this.state.listindex!=(this.state.questionlist.length-1) &&
              <button type="button" onClick={()=>this.listNext(key2,e.mock_question_id)} className="btn btn-primary float-right next mt-3">Next</button>
            } */}
            
         <div className="clearfix"></div>
        
     </div>
        
        })
      }
      </AwesomeSlider>
      </Card.Body>
     
    </Accordion.Collapse>
  </Card>
  
</Accordion>
<TempWebCam show={this.state.openCam} onHide={requestModalCamClose}/>
 </div>
  })
}
</div>

        );
    }
 }
 const mapStateToProps = state=>{
  console.log(state);
  return{
      text : state.mediaReducer.text,
      videodata : state.mediaReducer.videoData 
    }
}
const mapDisptchToProps = dispatch=>{
  return{
    startLoader : () =>dispatch(showLoader()),
    hideLoader : () =>dispatch(hideLoader()),
    clearVideo :()=>dispatch(emptyVideoData())
  }
}
 export default connect(mapStateToProps,mapDisptchToProps) (Mytest);
