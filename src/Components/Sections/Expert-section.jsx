import React from 'react';
import sec2 from '../../assets/images/sec2-img.png'

class ExpertSection extends React.Component{
    render(){
        return(
            <section id="sec2">
                <div className="custom-container">
                <div className="row">
                <div className="col-sm-7">
                <div className="sec2-inner">
                <h1>Talk to the experts 
                    <span>from your industry</span></h1>
                    <p>This section talks about how we bring experts from the industry to help with the live and offline skills prep, rated specifically focusing on interpersonal skills. We would like to showcase experts in this area with their consent and will be a rotating section. We need to look at our user base and see which companies should be highlighted here for maximum impact.</p>
                </div>
                </div>

                <div className="col-sm-5">
                <div className="sec2-inner-img">
                <img src={sec2}/>
                </div>
                </div>
                </div>
                </div>
            </section>
        )
    }
}

export default ExpertSection;