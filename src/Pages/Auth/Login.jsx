import React from 'react';
import logo from '../../assets/images/logo.png';
import login from '../../assets/images/login.png';
import social1 from '../../assets/images/social-icon1.png';
import social2 from '../../assets/images/social-icon2.png';
import social3 from '../../assets/images/social-icon3.png';
import social4 from '../../assets/images/social-icon4.png';
import { toast } from 'react-toastify';
import { signIn } from '../../providers/auth';
import { showLoader,hideLoader } from '../../redux/Actions/loaderAction';
import { connect } from 'react-redux';
import AwesomeSlider from 'react-awesome-slider';
import '../../assets/css/awseme.css';
import {Link} from 'react-router-dom';

class Login extends React.Component{

    constructor(props){
        super(props); 
        this.state = {
            email: '',
            password: '',
            emailerror:'',
            pswderror:'',
            hidden: true,
        
     
        
         }
         this.toggleShow = this.toggleShow.bind(this);
         }
    

    handleEmailChange = (e) =>{
        if(e.target.value !== "undefined" || e.target.value !==''){
        let pattern = new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(e.target.value);
        if (!pattern) {
            this.state.emailerror = "Please enter valid email address.";
          }
          else{
            this.state.emailerror = "";
          }
        }else{
            this.state.emailerror = "Please enter email address."
        }
        this.setState({email: e.target.value});       
        
        
     }
     handlePasswordChange = (e) =>{
        if(e.target.value !== "undefined" || e.target.value !==''){
            let pattern = new RegExp('/(?=.*[0-9])/').test(e.target.value);
             if (e.target.value.length<6) {
                 this.state.pswderror = "Invalid Password";
               }else{
                 this.state.pswderror = "";
               }
        
            }
            else{
                this.state.pswderror = "Please enter password."
            }
            this.setState({password: e.target.value});
    
        
     }
        handleLogin = ()=> {
            if(this.state.email != '' && this.state.password != ''){
                this.props.startLoader();
            signIn(this.state.email,this.state.password).then((res)=>{
                this.props.hideLoader();
                
                if(res.data.status){
                    localStorage.setItem('authData',JSON.stringify(res.data.data));
                    localStorage.setItem('coverPic',res.data.data.user_cover);
                    localStorage.setItem('profilePic',res.data.data.user_image);
                    localStorage.setItem('authToken',res.data.token);
                    this.props.history.push('/profile');
                    window.location.reload();
                }else{
                    toast.error(res.data.msg,{});
                }
               
            });
        }else{
            toast.error('Please fill mandetory fields.',{});
        }
        }

        toggleShow() {
            this.setState({ hidden: !this.state.hidden });

          }
       
        componentDidMount() {
            if (this.props.password) {
              this.setState({ password: this.props.password });
            }
          }
    
    render(){
        return(
            <section id="login">
                <div className="container">
                <div className="logintotal d-sm-flex">
                <div className="col-sm-6 d-sm-flex pl-0">
                <div className="login-left">
                <Link to="/home">
                <img src={logo} />
                </Link>
                <div className="login-slider">
                <AwesomeSlider> 
                <div className="carousel-inner">
                    <div className="carousel-item active">
                    <img src={login}/>
                    <h4>Track Progress</h4>
                    <p>Track progress, see how you fair against others with similar profiles and goals</p>
                    </div>
                    <div className="carousel-item">
                    <img src={login}/>
                    <h4>Track Progress</h4>
                    <p>Track progress, see how you fair against others with similar profiles and goals</p>
                    </div>
                    <div className="carousel-item">
                    <img src={login}/>
                    <h4>Track Progress</h4>
                    <p>Track progress, see how you fair against others with similar profiles and goals</p>
                    </div>
                </div>
                </AwesomeSlider> 
               
            
                </div>
               
                </div>
                
                </div>
            
                <div className="col-sm-6 d-sm-flex">
                <div className="login-right">
                <h4>Welcome</h4>
                <p>Don't miss your next opportunity. Sign in to stay updated on your professional world.</p>
                <ul>
                <li><a href="#"><img src={social1}/></a></li>
                <li><a href="#"><img src={social3}/></a></li>
                <li><a href="#"><img src={social2}/></a></li>
                <li><a href="#"><img src={social4}/></a></li>
                </ul>
                <div className="loginwith">
                <h6>Or login with Email</h6>
                </div>
                <div className="form-group">
                <label>Email Address</label>
                <input type="text" className="form-control" placeholder="Email Address" value={this.state.email} onChange={this.handleEmailChange}/>
                </div>
                <div className="form-group showeye">
                <label>Password</label>
                <input type={this.state.hidden ? "password" : "text"}
                className="form-control" placeholder="Password" value={this.state.password} onChange={this.handlePasswordChange}/>
                {this.state.hidden &&
                <i onClick={this.toggleShow} className="fa fa-eye-slash" aria-hidden="true"></i>
                    }
                    {!this.state.hidden &&
                        <i onClick={this.toggleShow} className="fa fa-eye" aria-hidden="true"></i> 
                    }
                </div>
                <div className="form-group">
                <p><input type="checkbox"/> Keep me Logged in</p>
                <a href="/forgotpassword">Forgot password?</a>
                </div>
                <div className="form-group">
                <button className="login-to-perceiveMe" onClick={this.handleLogin}>Login to PerceiveMe</button>
                </div>

                <div className="join-now">
                <p>New to PerceiveMe.ai? <a href="/signup">Join now</a></p>
                </div>
                </div>

                </div>
                </div>
                <div className="login-footer-menu">
                <ul>
                <li><a href="#">PerceiveMe.ai © 2020</a></li>
                <li><a href="#"> User</a></li>
                <li><a href="#">Agreement</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Community Guidelines</a></li>
                <li><a href="#">Cookie Policy</a></li>
                <li><a href="#">Copyright Policy</a></li>
                <li><a href="#">Send Feedback</a></li>
                </ul>

                </div>
                </div>
        </section>
        )
    }
}

const mapStateToProps = state=>{
    return{
        loader : state.loaderReducer.loader
      }
}

const mapDispatchFromProps = dispatch=>{
    return{
        startLoader : () =>dispatch(showLoader()),
        hideLoader : () =>dispatch(hideLoader())
    }
}

export default connect(mapStateToProps,mapDispatchFromProps)(Login);